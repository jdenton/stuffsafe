<?php
/*
 * ENGLISH TRANSLATION
 */

/*
 * ERROR/SUCCESS MESSAGES
 */
$lang['error_page_title']     = 'Error';
$lang['no_records_found']     = 'No records were found.';
$lang['error_save']           = 'There was an error saving.'; // **
$lang['error_missing_data']   = 'You are missing some required information.';
$lang['error_file_too_large'] = 'The file you tried to upload is too large.'; // **
$lang['error_incorrect_file_type'] = 'You are trying to upload an incorrect file type. Supported file types are %s';
$lang['error_unknown_error']  = 'There was an error but we don\'t know why!';
$lang['error_cant_view_this'] = 'You do not have permission to view this item. Sorry about that!';
$lang['error_task_deleted']   = 'This task had been deleted!'; // **
$lang['error_event_deleted']  = 'This event had been deleted!'; // **
$lang['error_password_missing_field'] = 'You must enter your current password and new password.'; // **
$lang['error_password_incorrect']     = 'Your current password is not right.'; // **
$lang['error_account_exists']         = 'An account with this password already exists. Please select a new password.'; //**
$lang['error_title_404']              = 'Uh oh! The page you want is not here!';
$lang['error_title_PHP']              = 'Uh oh! There was a programming error somewhere!';
$lang['error_title_general']          = 'Uh oh! Some kind of error happened! (We\'ll look into it.)';
$lang['error_title_database']         = 'Uh oh! The database is acting funny!';
$lang['error_title_offline']          = 'Uh oh! StuffSafe is down for maintenance!';
$lang['error_site_text']              = "We'll do our best to fix the problem soon. In the meantime you can <strong><a href=\"contact\">contact us</a></strong> with any questions or comments.<br /><br />Thanks for your patience!";
$lang['error_login_empty_field']      = 'Please enter a User Name and Password.';

$lang['success_password_saved']       = 'Your new password has been saved.';
$lang['success_data_import']          = 'Your data was imported successfully!';

/*
 * MAIN, SUB MENU ITEMS
 */
$lang['menu_places']                = 'My properties';
$lang['menu_view_places']           = 'View properties';
$lang['menu_new_place']             = 'Add new property';

/*
 * REPORTS
 */
$lang['reports_inventory_by_property']          = 'Inventory value by property'; // **
$lang['reports_inventory_by_category']          = 'Inventory value by category'; // **
$lang['reports_inventory_purchase_replacement_category'] = 'Inventory purchase & replacement cost (by category)'; // **
$lang['reports_inventory_purchase_replacement_property'] = 'Inventory purchase & replacement cost (by property)'; // **
$lang['reports_finance_excel_export']     = 'Export report in Excel format'; // **

/*
 * COMMON WORDS, PHRASES AND TERMS
 */
$lang['common_account']        = 'Account';
$lang['common_email']          = 'Email address';
$lang['common_form_more_link'] = 'Show more fields';
$lang['common_field_select']   = 'Select...';
$lang['common_first_name']     = 'First Name';
$lang['common_last_name']      = 'Last Name';
$lang['common_messages']       = 'Messages';
$lang['common_messages_invoice'] = 'Invoice messages';
$lang['common_messages_expense'] = 'Expense messages';
$lang['common_language']       = 'Change language';
$lang['common_alerts']         = 'Alerts';
$lang['common_search']         = 'Search';
$lang['common_company_name']   = 'Company Name';
$lang['common_profile_info']   = 'Profile information';
$lang['common_invoice_total']  = 'Invoice Total';
$lang['common_number_projects']= 'Number of Projects';
$lang['common_loading_clients']= 'Loading clients...';
$lang['common_loading_map']    = 'Loading map...';
$lang['common_loading_data']   = 'Loading data...';
$lang['common_hours']          = 'Hours';
$lang['common_status']         = 'Status';
$lang['common_status_update']  = 'Update status';
$lang['common_tags']           = 'Tags';
$lang['common_totals']         = 'Totals';
$lang['common_total']          = 'Total';
$lang['common_subtotal']       = 'Sub total';
$lang['common_title']          = 'Title';
$lang['common_number']         = 'Number';
$lang['common_help']           = 'Help';
$lang['common_working']        = 'Working...';
$lang['common_loading']        = 'Loading...';
$lang['common_title']          = 'Title';
$lang['common_company']        = 'Company';
$lang['common_phone']          = 'Phone';
$lang['common_phone_number']   = 'Phone number';
$lang['common_welcome']        = 'Welcome';
$lang['common_search_stuff']   = 'Search your stuff';
$lang['common_search_tags']    = 'Search tags';
$lang['common_send_message']   = 'Send a message';
$lang['common_send_message_to']= 'Send this message to';
$lang['common_send']           = 'Send';
$lang['common_date']           = 'Date';
$lang['common_date_start']     = 'Date start';
$lang['common_date_end']       = 'Date end';
$lang['common_date_due']       = 'Date due';
$lang['common_priority']       = 'Priority';
$lang['common_add_another']    = 'Add another';
$lang['common_download_file']  = 'Download file';
$lang['common_vcard']          = 'Download vCard';
$lang['common_other']          = 'Other';
$lang['common_filter_by']      = 'Filter by';

$lang['common_all']            = 'All';
$lang['common_open']           = 'Open';
$lang['common_closed']         = 'Closed';
$lang['common_past_due']       = 'Past due';
$lang['common_dates']          = 'Dates';

$lang['common_work']           = 'Work';
$lang['common_mobile']         = 'Mobile';
$lang['common_fax']            = 'Fax';
$lang['common_pager']          = 'Pager';
$lang['common_home']           = 'Home';
$lang['common_skype']          = 'Skype';
$lang['common_other']          = 'Other';
$lang['common_personal']       = 'Personal';
$lang['common_url']            = 'Website';

$lang['common_aim']            = 'AIM';
$lang['common_msn']            = 'MSN';
$lang['common_icq']            = 'ICQ';
$lang['common_jabber']         = 'Jabber';
$lang['common_yahoo']          = 'Yahoo';
$lang['common_googletalk']     = 'Google Talk';

$lang['common_immediately']    = 'Immediately';
$lang['common_specific_amount']= 'Specific amount';
$lang['common_specific_date']  = 'Specific date';
$lang['common_per_month']      = 'Per month';
$lang['common_billable']       = 'Billable';
$lang['common_create_report']  = 'Create report';
$lang['common_comments']       = 'Comments';
$lang['common_user']           = 'User';
$lang['common_yes']            = 'Yes';
$lang['common_no']             = 'No';
$lang['common_month']          = 'Month';

$lang['common_view']           = 'View'; // **
$lang['common_view_all']       = 'View all'; // **
$lang['common_view_personal_messages'] = 'View personal messages only'; // **
$lang['common_create']         = 'Create';  // **
$lang['common_update']         = 'Update'; // **
$lang['common_delete']         = 'Delete'; // **
$lang['common_select_all']     = 'Select all'; // **
$lang['common_photo_url']      = 'Photo URL'; // **
$lang['common_when']           = 'When'; // **
$lang['common_location']       = 'Location'; // **
$lang['common_no_tags']        = 'You have no tags here yet.'; // **
$lang['common_more']           = 'More'; // **
$lang['common_less']           = 'Less'; // **
$lang['common_add_guests']     = 'Add guests'; // **
$lang['common_contacts']       = 'Contacts'; // **
$lang['common_view_contacts']  = 'View contacts'; // **
$lang['common_find_contacts']  = 'Find contacts'; // **
$lang['common_contact']        = 'Contact'; // **
$lang['common_percent']        = 'Percent'; // **
$lang['common_created_by']     = 'Created by'; // **
$lang['common_page']           = 'Page'; // **
$lang['common_view_personal']  = 'View personal entries only'; // **
$lang['common_view_personal_trash']  = 'View personal trash items only'; // **
$lang['common_upload']         = 'Upload'; // **
$lang['common_per_hour']       = 'Per hour'; // **
$lang['common_per_day']        = 'Per day'; // **
$lang['common_language']       = 'Language'; // **
$lang['common_change_photo']   = 'Change photo'; // **
$lang['common_account_creator']= 'Account creator'; // **
$lang['common_vendor']         = 'Vendor'; // **
$lang['common_vendors']        = 'Vendors'; // **
$lang['common_trashcan']       = 'Trash can'; // **
$lang['common_no_trash']       = 'There are no items in the trash can.'; // **
$lang['common_message']        = 'Message'; // **
$lang['common_list_view']      = 'List view'; // **
$lang['common_list_view_return'] = 'Return to list view'; // **
$lang['common_warning']          = 'Warning'; // **
$lang['common_restore']          = 'Restore'; // **
$lang['common_new_password']     = 'New password'; // **
$lang['common_current_password'] = 'Your current password'; // **
$lang['common_no_projects']      = 'There are no projects right now.'; // **
$lang['common_no_invoices']      = 'There are no invoices right now.'; // **
$lang['common_no_messages']      = 'There are no messages right now.'; // **
$lang['common_no_expenses']      = 'There are no expenses right now.'; // **
$lang['common_no_clients']       = 'There are no clients right now.'; // **
$lang['common_no_tasks']         = 'There are no tasks right now.'; // **
$lang['common_assigned_to']      = 'Assigned to'; // **
$lang['common_category']         = 'Category'; // **
$lang['common_select_category']  = 'Select category'; // **
$lang['common_select_room']      = 'Select room';
$lang['common_theme']            = 'Theme'; // **
$lang['common_address']          = 'Address'; // **
$lang['common_address_1']        = 'Address 1'; // **
$lang['common_address_2']        = 'Address 2'; // **
$lang['common_city']             = 'City';
$lang['common_state']            = 'State';
$lang['common_zip']              = 'Zip/Postal code';
$lang['common_country']          = 'Country';
$lang['common_currency']         = 'Currency'; // **
$lang['common_data']             = 'Data'; // **
$lang['common_data_import']      = 'Import data'; // **
$lang['common_data_export']      = 'Export data'; // **
$lang['common_import_summary']   = 'Import summary'; // **
$lang['common_archive']          = 'Archive'; // **
$lang['common_upgrade_account']  = 'Upgrade your account.';
$lang['common_mark_message_read']= 'Mark message as read.';
$lang['common_export_excel']     = 'Export in Excel format';
$lang['common_export_options']   = 'Export options';
$lang['common_add_files']        = 'Add files';
$lang['common_start_upload']     = 'Start upload';
$lang['common_add_selected_items'] = 'Add selected items';
$lang['common_information']        = 'Information';
$lang['common_avatar']             = 'Avatar';
$lang['common_amount_due']         = 'Due';
$lang['common_new_entry']          = 'New entry';
$lang['common_grid_view']          = 'Grid view';
$lang['common_entry_view']         = 'Entry view';
$lang['common_previous']           = 'Previous';
$lang['common_next']               = 'Next';
$lang['common_no_entries_this_week'] = 'No entries for this week.';
$lang['common_back']                 = 'Back';
$lang['common_select_project_files'] = 'Select a project to view files.';
$lang['common_actions']              = 'Actions';
$lang['common_no_templates']         = 'You have no project templates right now.';
$lang['common_edit_template']        = 'Edit template';
$lang['common_square_feet']          = 'Square feet';
$lang['common_square_meter']         = 'Square meters';
$lang['common_items']                = 'Items';
$lang['common_item']                 = 'Item';
$lang['common_description']          = 'Description';
$lang['common_amount']               = 'Amount';
$lang['common_make']                 = 'Make';
$lang['common_model']                = 'Model';
$lang['common_serial_number']        = 'Serial number';
$lang['common_purchase_date']        = 'Purchase date';
$lang['common_condition']            = 'Condition';
$lang['common_purchase_price']       = 'Purchase price';
$lang['common_purchase_cost']        = 'Purchase cost';
$lang['common_replacement_price']    = 'Replacement price';
$lang['common_replacement_cost']     = 'Replacement cost';
$lang['common_drop_files']           = 'Drop files here';
$lang['common_insurance_policies']   = 'Insurance policies';
$lang['common_industry']             = 'Your industry';
$lang['common_your_name_form']       = 'Your name (first, last)';
$lang['common_place_map']            = 'Property map';
$lang['common_view_map']             = 'View map';
$lang['common_im']                   = 'Instant messaging';
$lang['common_timezone']             = 'Timezone';
$lang['common_export_inventory']     = 'Export your inventory';
$lang['common_inventory_value']      = 'Total inventory value';
$lang['common_pdf']                  = 'PDF';
$lang['common_excel']                = 'Excel';
$lang['common_csv']                  = 'CSV';
$lang['common_word']                 = 'MS Word';
$lang['common_item_name']            = 'Item name';
$lang['common_printed_by']           = 'Printed by';
$lang['common_rooms']                = 'Rooms';
$lang['common_info_saved']           = 'Your information has been saved.';

$lang['insert_table']            = 'Insert table';
$lang['insert_image']            = 'Insert image';

/*
 * PROPERTIES
 */
$lang['place_my_places']        = 'My properties';
$lang['place_name']             = 'Property name';
$lang['place_type']             = 'Property type';
$lang['place_add_place']        = 'Add new property';
$lang['place_edit_place']       = 'Edit property';
$lang['place_form_name']        = 'Property name';
$lang['place_form_area']        = 'Area of property';
$lang['place_form_description'] = 'Description of this property';
$lang['place_no_places']        = 'You don\'t have any properties yet. <a href=\'placeupdate\'>Add a property now.</a>';
$lang['place_property_info']    = 'Property information';
$lang['place_properties']       = 'Properties';

/*
 * INVENTORY
 */
$lang['inventory']              = 'Inventory';
$lang['inventory_add_item']     = 'Add item to inventory';
$lang['inventory_items']        = 'Inventory items';
$lang['inventory_no_items']     = 'There are no inventory items for this property.';
$lang['inventory_reports']      = 'Inventory reports';
$lang['inventory_condition_excellent'] = 'Excellent';
$lang['inventory_condition_good']      = 'Good';
$lang['inventory_condition_fair']      = 'Fair';
$lang['inventory_condition_poor']      = 'Poor';
$lang['inventory_default_room']        = 'Room 1';

/*
 * ROOMS
 */
$lang['room_delete'] = 'Delete room';

/*
 * FORM VALIDATIONS
 */
$lang['form_required_place_name'] = 'A property name or title is required.';

/*
 * MESSAGE TEXT
 */
$lang['message_sent_concerning']    = '%s sent you a message concerning %s on %s.'; // Janice sent you a message concerning invoice on Jan 1, 2009.
$lang['message_sent']               = '%s sent you a message on %s.'; // Janice sent you a message on Jan 1, 2009.
$lang['message_concerning_file']    = 'a file'; 

/*
 * BUTTON TEXT
 */
$lang['button_save']         = 'Save';
$lang['button_cancel']       = 'Cancel';
$lang['button_add']          = 'New';
$lang['button_edit']         = 'Edit';
$lang['button_delete']       = 'Delete';
$lang['button_options']      = 'Options';
$lang['button_print']        = 'Print';
$lang['button_pdf']          = 'Create PDF';
$lang['button_new_contact']  = 'New contact';
$lang['button_close']        = 'Close';
$lang['button_clear']        = 'Clear';
$lang['button_remove']       = 'Remove';
$lang['button_export_excel'] = 'Excel export';
$lang['button_im_done']      = 'I\'m done';

/*
 * LOGIN AREA MESSAGES
 */
$lang['login_page_title']               = 'Office Login';
$lang['login_user_name']                = 'User Name';
$lang['login_password']                 = 'Password';
$lang['login_button']                   = 'Login';
$lang['login_logout']                   = 'Logout';
$lang['login_forget_password']          = 'I forgot my password.'; 
$lang['login_message_logout']           = 'You are logged out.';
$lang['login_message_inactiveAccount']  = 'Your account is not active.';
$lang['login_message_roleMissing']      = 'You have not been assigned a role yet. A role is a user designation like Contractor, Super User, Accountant, etc. Please contact the person who set up your account and ask them to assign you to a user role.';
$lang['login_message_missingField']     = 'You are missing some fields.';
$lang['login_message_noAccount']        = 'We cannot find your account. Perhaps your email or password are incorrect or your account has been deactivated. Please contact <a href="mailto:'.MAIL_SUPPORT.'">'.MAIL_SUPPORT.'</a> with any questions.';
$lang['login_client_page_title']        = 'Client Login';
$lang['login_find_password']            = 'Find my password';
$lang['login_default']                  = 'Welcome! Please login to your StuffSafe account below.';
$lang['login_message_expired']          = 'For security reasons, your session has expired. Please login again.';
$lang['login_message_noAccount2']       = 'We cannot find your account. Please try again.';

/*
 * CONTACTS AREA
 */
$lang['contact_no_contacts']    = "You have no contacts yet.";
$lang['contact_create']         = "Create a new contact.";
$lang['contact_new']            = "Add new contact";
$lang['contact_error_no_email'] = "You must enter at least 1 email address for a team member.";
$lang['contact_error_no_role']  = "You must select a role for a team member.";
$lang['contact_search']         = "Start typing to search your contacts";
$lang['contact_search_projects']= "Start typing to search your team members or contacts";

/*
 * FILES AREA MESSAGES
 */
$lang['files_files']               = 'Files';
$lang['files_file_manager']        = 'File manager';
$lang['files_upload_files']        = 'Upload files';
$lang['files_folder']              = 'Folder';
$lang['files_new_folder']          = 'New folder';
$lang['files_new_folder_name']     = 'New folder name';
$lang['files_upload']              = 'Upload files';
$lang['files_upload_logo_small']   = 'Upload small logo'; // **
$lang['files_upload_logo_hires']   = 'Upload hi-res logo'; // **
$lang['files_upload_photo']        = 'Upload photo';
$lang['files_select']              = 'Select files';
$lang['files_attach']              = 'Attach files';
$lang['files_view_this_file']      = 'View this file'; // **
$lang['files_avatar_size']         = '100 x 100 pixels max.'; // **
$lang['file_import_folder']        = 'Imported Files'; // **
$lang['files_all_files']           = 'All files';
$lang['files_delete_folder']       = 'Delete folder';
$lang['files_delete_file']         = 'Delete file';
$lang['files_file_share']          = 'Click on a file below to share it with your client.';

/*
 * TEAM AREA MESSAGES
 */
$lang['team_my_team']              = 'My Team';
$lang['team_select_team_member']   = 'My Team';
$lang['team_select_team_member']   = 'Select team member';
$lang['team_member']               = 'Team member';
$lang['team_members']              = 'Team members'; // **
$lang['team_set_roles']            = 'Roles and permissions';    // **
$lang['team_role_create']          = 'Create new role'; // **
$lang['team_role_edit']            = 'Update existing role'; // **
$lang['team_role_edit_this']       = 'Update this role'; // **
$lang['team_role_delete_this']     = 'Delete role'; // **
$lang['team_role_saved']           = 'Role and permissions were saved.'; // **
$lang['team_new_team_member']      = 'New team member'; // **
$lang['team_select_role']          = 'Select role';    // **
$lang['team_make_team_member']     = 'This person is a team member.'; // **
$lang['team_role_people_form']     = 'If this person is a team member'; // **
$lang['team_select_team_members']  = 'Select team members'; // **

/*
 * Roles
 */
$lang['role_admin']          = 'Super administrator';
$lang['role_user']           = 'General user';
$lang['role_user_view_only'] = 'View only user';

/*
 * ADMINISTRATION AREA MESSAGES
 */
$lang['admin_page_title'] = 'Administration';

/*
 * SETTINGS AREA TERMS
 */
$lang['settings_account_info']       = 'Account information';
$lang['settings_personal_info']      = 'Personal information';
$lang['settings_update_personal_info'] = 'Update your personal information';
$lang['settings_update_account_contact_info']  = 'Update account contact information';
$lang['settings_page_title']         = 'Settings';
$lang['settings_logos']              = 'Company logos';
$lang['settings_email_project']      = 'I am assigned to a project.';
$lang['settings_email_project_updated'] = 'My projects are updated.';
$lang['settings_email_message']      = 'Someone sends me a message.';
$lang['settings_email_event']        = 'An event is shared with me.';
$lang['settings_email_task']         = 'I am assigned to a task.';
$lang['settings_email_task_update']  = 'My tasks are updated.';
$lang['settings_email_task_overdue'] = 'My tasks are overdue or coming due.';
$lang['settings_email_calendar']     = 'A calendar is shared with me.';
$lang['settings_email_timer']        = 'I leave a job timer clock running.';
$lang['settings_email_notification'] = 'Receive email notifications when...';
$lang['settings_email_send_notification'] = 'Send email notifications when...';
$lang['settings_email_client_create']     = 'A new client is created (send client area access details)';
$lang['settings_change_password']         = 'Change your password';
$lang['settings_web_address']             = 'Your account web address';

$lang['settings_account_security']            = 'Account Security';
$lang['settings_upload_logo']                 = 'Upload your company logo';
$lang['settings_update_company_information']  = 'Update your personal &amp; company information';
$lang['settings_update_invoice']              = 'Update your invoice settings';
$lang['settings_update_calendar']             = 'Update your calendar settings';
$lang['settings_account_login_on_item_view']  = 'Automatically log me into my account when I view an item from my account RSS feed (like an event or task RSS item).';
$lang['settings_account_login_from_email']    = 'Automatically log me into my account when I click a link in an email message sent from my account (like task reminders).';
$lang['settings_update_all']                  = 'Update all office settings';
$lang['settings_update_own']                  = 'Update personal settings (email settings, password, avatar, etc.)';
$lang['settings_success']                     = 'Your settings have been saved.';
$lang['settings_cancel_account']              = 'Cancel my account';

$lang['settings_dbase_host']                  = 'Database host name';
$lang['settings_dbase_name']                  = 'Database name';
$lang['settings_dbase_username']              = 'Database user name';
$lang['settings_dbase_password']              = 'Database password';
$lang['settings_file_path']                   = 'Version 1.0 user_files file path';
$lang['settings_no_file_import']              = 'Do not import files';

/*
 * NOTES
 */
$lang['notes_new_note']    = 'New note';
$lang['notes_make_public'] = 'Make this note public.';
$lang['notes_show_client'] = 'Make visible to client.';
$lang['notes_visible_to_client'] = 'This note can be seen by the client.';
$lang['notes_project_notes']     = 'Project notes';

/*
 * BUG REPORT AREA TERMS
 */
$lang['bugreport_page_title']   = 'Report Bugs';
$lang['bugreport_instructions'] = 'Please describe the bug in the space below.';
$lang['bugreport_submit']       = 'Submit bug';
$lang['bugreport_success']      = 'Bug has been submitted.';

/*
 * Plan information
 */
$lang['plan_cost']              = 'Cost per month';
$lang['plan_locations']         = 'Locations';
$lang['plan_team']              = 'Team members';
$lang['plan_storage']           = 'Storage space';
$lang['plan_update_plan']       = 'Change your plan';
$lang['plan_update_cc']         = 'Update credit card';
$lang['plan_update_plan_or_cc'] = 'Change your plan or update credit card';

/*
 * Categories
 */
$lang['cat_single_family_house'] = 'Single family, detached house';
$lang['cat_condominium']         = 'Condominium';
$lang['cat_townhouse']           = 'Townhouse';
$lang['cat_multi_family']        = 'Multi-family dwelling (Duplex)';
$lang['cat_apartment_unit']      = 'Apartment';
$lang['cat_business']            = 'Office or business';
$lang['cat_boat']                = 'Boat';
$lang['cat_plane']               = 'Plane';

/*
 * Insrance policy
 */
$lang['policy_add_policy_info']  = 'Add policy information';
$lang['policy_company_name']     = 'Insurance company name';
$lang['policy_company_url']      = 'Insurance company website';
$lang['policy_number']           = 'Policy number';
$lang['policy_agent_name']       = 'Agent name (first, last)';
$lang['policy_agent_email']      = 'Agent email address';
$lang['policy_agent_phone']      = 'Agent phone number';
$lang['policy_agent_fax']        = 'Agent fax number';
$lang['policy_purchase_date']    = 'Purchase date';
$lang['policy_no_policies']      = 'You have not entered any insurance policies.';

/*
 * Data import/export
 */
$lang['import_page_title']       = 'Import your data';
$lang['import_import_clients']   = 'Import clients';
$lang['import_import_contacts']  = 'Import contacts';
$lang['import_upload_data_file'] = 'Upload data file';
$lang['import_first_row_headers']= 'First row of data is column headers.';
$lang['import_send_client_email']= 'Send an email message to each client with their account access information.';
$lang['import_accept_import']    = 'Accept import';
$lang['import_cancel_import']    = 'Do not accept import!';
$lang['import_import_data']      = 'Import data';
$lang['import_success_message']  = 'Your data was imported successfully.';
$lang['import_rollBack_message'] = 'Your imported data has been deleted.';
$lang['import_map_fields']       = 'Map your data fields';
$lang['import_confirm_data']     = 'Confirm your imported data';
$lang['import_client_data_column'] = 'Client data column';
$lang['import_contact_data_column'] = 'Contact data column';
$lang['import_your_data_column']    = 'Your data column';
$lang['import_contacts_are']        = 'These contacts are:';
$lang['import_from_ss']             = 'Import data from a StuffSafe version 1 account';
$lang['import_from_other']          = 'Import data from other software';
$lang['import_begin_import']        = 'Begin importing data';
$lang['export_ss_data']             = 'Export your data from StuffSafe';

$lang['feed_rss_text'] = 'Copy and paste the following link into your favorite RSS feed reader. Click the link to download the feed.';
$lang['feed_ical_text'] = 'Copy and paste the following link into Google Calendar or other ical applications. Click the link to download the feed.';
?>