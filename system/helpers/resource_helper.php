<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function renderResourceSmall($resourceArray){
    $rID         = $resourceArray['RID'];
    $rCatID      = $resourceArray['CategoryID'];
    $rTitle      = $resourceArray['Title'];
    $rDesc       = $resourceArray['Description'];
    $rLink       = $resourceArray['Link'];
    $rAuthors    = $resourceArray['Authors'];
    $rTags       = $resourceArray['Tags'];
    $rDateCreate = date('M j, Y',strtotime($resourceArray['DateCreated']));

    echo '<div class="resourceContainer">';
    echo '<h2><a href="'.$rLink.'">'.$rTitle.'</a></h2>';
    echo '<p class="subText">On '.$rDateCreate;
    if (!empty($rAuthors)) {
        echo ' by '.$rAuthors;
    }
    echo '</p>';
    if (!empty($rDesc)) {
        echo '<p>'.$rDesc.'</p>';
    }
    
    echo '</div>';
}

?>
