<?php

/**
 * getStatus creates a simple JSON string for
 * our tag autocompleter
 *
 * @access	public
 * @param	array $array array of tags from database query
 * @return	string (JSON)
 */
if ( ! function_exists('getInvoiceStatus')) {
	function getInvoiceStatus($status=null,$paid=null,$sent=null,$dateInv=null,$datePaid=null,$dateSent=null,$dateDue=NULL,$daysDue=NULL) {
		if ($status == 0) {
			if ($sent == 1) {
				$statusArray = array(lang('status_open_sent_on').' '.date('M j, Y', strtotime($dateSent)),'status_open_sent_on');
			} else {
				$statusArray = array(lang('status_open_created_on').' '.date('M j, Y', strtotime($dateInv)),'status_open_created_on');
			}

            /*
             * Check for overdue invoice
             */
            if ($dateDue == '0000-00-00') {
                $dateDue = '';
            }
            if (!empty($dateDue) || $daysDue>0) {
                $now = time();
                $dateInvUnix = strtotime($dateInv);
                if (!empty($dateDue)) {
                    $dateDueUnix = strtotime($dateDue);
                    $diffUnix = $now-$dateDueUnix;
                } elseif ($daysDue>0) {
                   $dateDueUnix = $dateInvUnix+(86400*$daysDue);
                }
                $diffUnix = $now-$dateDueUnix;

                if ($diffUnix>0) {
                    $days = days_between_dates($dateDueUnix,$now);
                    if ($days>365) {
                        /*
                         * more than 1 year past due
                         */
                        $statusDayText = strtolower(lang('status_past_due_text_year'));
                    } elseif ( floor($days/30)<1 ) {
                        /*
                         * days past due
                         */
                        $dayText = strtolower(lang('date_days'));
                        if ($days == 1) {
                            $dayText = strtolower(lang('date_day'));
                        }
                        $statusDayText = sprintf(lang('status_past_due_text'),$days,$dayText);
                    } elseif ( floor($days/30)>=1) {
                        /*
                         * months past due
                         */
                        $months = floor($days/30);
                        $monthText = strtolower(lang('date_months'));
                        if ($months == 1) {
                            $monthText = strtolower(lang('date_month'));
                        }
                        $statusDayText = sprintf(lang('status_past_due_text'),$months,$monthText);
                    }

                    $statusMessage = lang('status_overdue').': '.$statusDayText;
                    $statusArray = array($statusMessage,'status_open_overdue');
                }
            }
		} elseif ($status == 1) {
			if ($paid == 1) {
				$statusArray = array(lang('status_closed_paid_on').' '.date('M j, Y', strtotime($datePaid)),'status_closed_paid_on');
			} else {
				$statusArray = array(lang('status_closed_not_paid'),'status_closed_not_paid');
			}
		}
		return $statusArray;
	}
}

if ( ! function_exists('getInvoiceStatusForUpdate')) {
    function getInvoiceStatusForUpdate($newStatus) {
        switch ($newStatus) {
            case 'open':
                $statusArray['invoiceStatus'] = 0;
                $statusArray['payStatus'] = 0;
                break;
            case 'closed_paid':
                $statusArray['invoiceStatus'] = 1;
                $statusArray['payStatus'] = 1;
                break;
            case 'closed_not_paid':
                $statusArray['invoiceStatus'] = 1;
                $statusArray['payStatus'] = 0;
                break;
            default:
                $statusArray['invoiceStatus'] = 0;
                $statusArray['payStatus'] = 0;
        }

        return $statusArray;
    }
}

/**
	 * getStatus creates a simple JSON string for
	 * our tag autocompleter
	 *
	 * @access	public
	 * @param	array $array array of tags from database query
	 * @return	string (JSON)
	 */
if ( ! function_exists('getProjectStatus')) {
	function getProjectStatus($status,$dateStart=NULL,$dateEnd=NULL,$datesNeedConversion=FALSE) {
        /*
         * ### IMPORTANT: Everything here is predecated on CLIENT DATE not
         *                SERVER DATE.
         */
        if ($datesNeedConversion == TRUE) {
            if ($dateStart != NULL) {
                $dateStart = convertMySQLToGMT($dateStart,'push','Y-m-d H:i:s');
            }
            if ($dateEnd != NULL) {
                $dateEnd = convertMySQLToGMT($dateEnd,'push','Y-m-d H:i:s');
            }
        }

        $currentTimeStamp = strtotime(convertMySQLToGMT(date('Y-m-d H:i:s'),'push','Y-m-d H:i:s'));
        $todayServer      = ltrim(date('d',$currentTimeStamp),0);
        $dayDue           = ltrim(substr($dateEnd,8,2),0);

		$statusDayText = '';
		$days = '';
		if (!empty($dateEnd) && $dateEnd != '0000-00-00 00:00:00') {
			$endTimeStamp = strtotime($dateEnd);
            if ( ($status < 2 && $currentTimeStamp>$endTimeStamp) || $status == 4 ) {
				/*
				 * We're overdue on this project or task. Calculate how many
				 * days,months,years we are past due. If > 1 year past,
				 * don't keep track anymore - nobody cares anyway.
				 */
				$status = 4;
				$days = days_between_dates($endTimeStamp,$currentTimeStamp);
                if ($days == 0) {
                    /*
                     * If today number != $dateEnd day number, assume yesterday
                     */
                    if ($todayServer != $dayDue) {
                        $statusDayText = lang('status_due_yesterday');
                    } else {
                        $statusDayText = lang('status_due_today');
                    }
                } elseif ($days>365) {
					/*
					 * more than 1 year past due
					 */
					$statusDayText = strtolower(lang('status_past_due_text_year'));
				} elseif ( floor($days/30)<1 ) {
					/*
					 * days or hours past due
					 */                    
					$dayText = strtolower(lang('date_days'));
					if ($days == 1) {
						$dayText = strtolower(lang('date_day'));
					}

                    if ($days < 1) {
                        $statusDayText = lang('status_due_awhile_ago');
                    } else {
                        $statusDayText = sprintf(lang('status_past_due_text'),$days,$dayText);
                    }
				} elseif ( floor($days/30)>=1) {
					/*
					 * months past due
					 */
					$months = floor($days/30);
					$monthText = strtolower(lang('date_months'));
					if ($months == 1) {
						$monthText = strtolower(lang('date_month'));
					}
					$statusDayText = sprintf(lang('status_past_due_text'),$months,$monthText);
				} 
			} else {
				$days = days_between_dates($currentTimeStamp,$endTimeStamp);
				/*
				 * Calculate how many days,months,years until project is due.
				 * If not due for > 1 year, just print the due date
				 */               
                
                if ($days == 0) {
                    if ($todayServer<$dayDue) {
                        $statusDayText = lang('status_due_tomorrow');
                    } else {
                        $statusDayText = lang('status_due_today');
                    }
                } elseif ($days == 1) {
                    $statusDayText = lang('status_due_tomorrow');
                } elseif ($days>365) {
					/*
					 * More than 1 year until due.
					 */
					$statusDayText = sprintf(strtolower(lang('status_due_on')),date('M j, Y', strtotime($dateEnd)));
				} elseif ( floor($days/30)<1 ) {
					/*
					 * days till due
					 */
					$dayText = strtolower(lang('date_days'));
					if ($days == 1) {
						$dayText = strtolower(lang('date_day'));
					}
					$statusDayText = strtolower(sprintf(lang('status_due_in'),$days,$dayText));
				} elseif ( floor($days/30)>=1) {
					/*
					 * months till due
					 */
					$months = floor($days/30);
					$monthText = strtolower(lang('date_months'));
					if ($months == 1) {
						$monthText = strtolower(lang('date_month'));
					}
					$statusDayText = strtolower(sprintf(lang('status_due_in'),$months,$monthText));
				} elseif ($days<1) {
                    /*
                     * Completed
                     */
                    $statusDayText = '';
                }
			}
		}
		switch ($status) {
			case 0: // Not Started
	   			$statusArray = array(lang('status_not_started'),'status_not_started',$statusDayText);
	  		break;
			case 1: // In Progress
	   			$statusArray = array(lang('status_in_progress'),'status_in_progress',$statusDayText);
	   		break;
			case 2: // Completed
                $statusDayText = '';
				$statusArray = array(lang('status_completed'),'status_completed',$statusDayText);
	   		break;
	   		case 3: // On Hold
	   			$statusArray = array(lang('status_on_hold'),'status_on_hold',$statusDayText);
	   		break;
	   		case 4: // Overdue
	   			$statusArray = array(lang('status_past_due'),'status_past_due',$statusDayText);
	   		break;
		}

		return $statusArray;
	}
}

if ( ! function_exists('getTaskStatus')) {
	function getTaskStatus($status,$dateStart=null,$dateEnd=null) {
		//return $statusArray;
	}
}

if ( ! function_exists('getTaskPriorityIcon')) {
	function getTaskPriorityIcon($priority) {
		switch ($priority) {
			case 1: // Low
	   			$priorityArray = array(lang('task_priority_low'),'low');
	  		break;
			case 2: // Medium
	   			$priorityArray = array(lang('task_priority_medium'),'medium');
	   		break;
			case 3: // High
				$priorityArray = array(lang('task_priority_high'),'high');
	   		break;
	   		case 4: // Extreme
	   			$priorityArray = array(lang('task_priority_extreme'),'extreme');
	   		break;
            default:
                $priorityArray = array(lang('task_priority_low'),'low');
            break;
		}

		return $priorityArray;
	}
}
?>