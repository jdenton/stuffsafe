<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Rewrite all outgoing text into UTF8 compatible streams
* Author: Matt Carter <m@ttcarter.com>
* Info: http://hash-bang.net/2009/02/utf8-with-codeigniter
*/
/*
function ob_utf8($string) {
	return utf8_decode($string);
}
ob_start('ob_utf8');
 
foreach ($_POST as $key => $val) // Re-write all incoming text into UTF8 streams
	$_POST[$key] = utf8_encode($val);
*/
function decode_utf8($string) {
    /* Only do the slow convert if there are 8-bit characters */
    /* avoid using 0xA0 (\240) in ereg ranges. RH73 does not like that */
    if (! ereg("[\200-\237]", $string) and ! ereg("[\241-\377]", $string))
        return $string;

    // decode three byte unicode characters
    $string = preg_replace("/([\340-\357])([\200-\277])([\200-\277])/e",       
    "'&#'.((ord('\\1')-224)*4096 + (ord('\\2')-128)*64 + (ord('\\3')-128)).';'",   
    $string);

    // decode two byte unicode characters
    $string = preg_replace("/([\300-\337])([\200-\277])/e",
    "'&#'.((ord('\\1')-192)*64+(ord('\\2')-128)).';'",
    $string);

    return $string;
} 
?>