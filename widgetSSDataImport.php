<? 
include("includes/config.inc.php");

function array_transform($array){
	$output = '';
	foreach($array as $key => $value){
		if(!is_array($value)){
			$output .= "<$key>".htmlspecialchars($value)."</$key>\r\n";
		} else {
			$output .="<$key>";
			array_transform($value);
			$output .="</$key>\r\n";
		}
	}
	return $output;

}

/*
 * Try login
 */
$userid   = $_GET['userid'];
$password = $_GET['pw'];
$xmlString  = '<?xml version="1.0" encoding="UTF-8"?>'."\r\n";

// Check for valid login information
$sql = "SELECT * from ss_login WHERE UserName = '$userid' AND Password = '$password' AND Confirmed = '1'";
$result = mysql_db_query($dbname,$sql);
if (mysql_num_rows($result)<1) {
    $xmlString .= '<document>'."\r\n";
    $xmlString .= '<status>'."\r\n";
    $xmlString .= '<statusType>error</statusType>'."\r\n";
    $xmlString .= '<statusMessage>badLogin</statusMessage>'."\r\n";
    $xmlString .= '</status>'."\r\n";
    $xmlString .= '</document>';
	echo $xmlString;
} else {
	$userRow = mysql_fetch_assoc($result);
	$userid = $userRow['UID'];
	
	/*
	 * Start grabbing stuff for this user from the database
	 */
	$xmlString .= '<document>'."\r\n";
	$xmlString .= '<status>'."\r\n";
    $xmlString .= '<statusType>success</statusType>'."\r\n";
    $xmlString .= '<statusMessage>success</statusMessage>'."\r\n";
    $xmlString .= '</status>'."\r\n";

    /*
     * Get user file directory
     */
    $sql = "SELECT * from cpo_user_profile WHERE Userid = '$userid'";
    $result = mysql_db_query($dbname,$sql);
    $row = mysql_fetch_assoc($result);
    $userDir = $row['UserDir'];
    $xmlString .= '<userDir>'.$userDir.'</userDir>';

	/*
	 * Clients
	 */
	$sql = "SELECT * from cpo_clients WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<clients>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<client>';
		$xmlString .= array_transform($value);
		$xmlString .= '</client>';
	}
	$xmlString .= '</clients>'."\r\n";
	
	/*
	 * Projects
	 */
	$sql = "SELECT * from cpo_project WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
    $projectIDArray = array();
	
	$xmlString .= '<projects>';
    $pCount=0;
	while ($value = mysql_fetch_assoc($result)) {
        $projectIDArray[$pCount] = $value['ID'];
		$xmlString .= '<project>';
		$xmlString .= array_transform($value);		
		$xmlString .= '</project>';
        $pCount++;
	}
	$xmlString .= '</projects>'."\r\n";
	
	/*
	 * Project Contacts
	 */
	$sql = "SELECT PC.ID,PC.Proj_ID,PC.Name,PC.Email,PC.Phone,PC.Temp from cpo_project_contacts PC
			LEFT JOIN cpo_project P ON P.ID = PC.Proj_ID 
			WHERE P.Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<project_contacts>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<project_contact>';
		$xmlString .= array_transform($value);		
		$xmlString .= '</project_contact>';
	}
	$xmlString .= '</project_contacts>'."\r\n";
	
	/*
	 * Project Tasks
	 */
	$sql = "SELECT * from cpo_project_tasks WHERE Userid = '$userid' ORDER BY Milestone DESC";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<tasks>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<task>';
		$xmlString .= array_transform($value);
		$xmlString .= '</task>';
	}
	$xmlString .= '</tasks>'."\r\n";
	
	/*
	 * Project Team Members
	 */
	$sql = "SELECT * from cpo_project_team_member WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<project_team_members>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<project_team_member>';
		$xmlString .= array_transform($value);
		$xmlString .= '</project_team_member>';
	}
	$xmlString .= '</project_team_members>'."\r\n";
	
	/*
	 * Invoices
	 */
	$sql = "SELECT * from cpo_invoice WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<invoices>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<invoice>';
		$xmlString .= array_transform($value);
		$xmlString .= '</invoice>';
	}
	$xmlString .= '</invoices>'."\r\n";
	
	/*
	 * Invoice Items
	 */
	$sql = "SELECT * from cpo_invoice_items WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<invoice_items>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<invoice_item>';
		$xmlString .= array_transform($value);
		$xmlString .= '</invoice_item>';
	}
	$xmlString .= '</invoice_items>'."\r\n";
	
	/*
	 * User Categories
	 */
	$sql = "SELECT * from cpo_main_cat WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<categories>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<category>';
		$xmlString .= array_transform($value);
		$xmlString .= '</category>';
	}
	$xmlString .= '</categories>'."\r\n";
	
	/*
	 * Notes
	 */
	$sql = "SELECT * from cpo_notes WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<notes>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<note>';
		$xmlString .= array_transform($value);
		$xmlString .= '</note>';
	}
	$xmlString .= '</notes>'."\r\n";
	
	/*
	 * Project Time
     * We need to pull timesheet entries by Proj_ID instead of Userid
     * in order to get timesheet entries from team members too.
	 */
    $xmlString .= '<times>';
    foreach($projectIDArray as $pID) {
        $sql = "SELECT * from cpo_project_time WHERE Proj_ID = '$pID'";
        $result = mysql_db_query($dbname,$sql);
        while ($value = mysql_fetch_assoc($result)) {
            $xmlString .= '<time>';
            $xmlString .= array_transform($value);
            $xmlString .= '</time>';
        }
    }
	$xmlString .= '</times>'."\r\n";
	
	/*
	 * Settings Invoice
	 */
	$sql = "SELECT * from cpo_settingsInvoice WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<settings>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<setting>';
		$xmlString .= array_transform($value);
		$xmlString .= '</setting>';
	}
	$xmlString .= '</settings>'."\r\n";
	
	/*
	 * Vendors
	 */
	$sql = "SELECT * from cpo_vendors WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<vendors>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<vendor>';
		$xmlString .= array_transform($value);
		$xmlString .= '</vendor>';
	}
	$xmlString .= '</vendors>'."\r\n";
	
	/*
	 * Tags
	 */
	$sql = "SELECT * from cpo_tags WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<tags>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<tag>';
		$xmlString .= array_transform($value);
		$xmlString .= '</tag>';
	}
	$xmlString .= '</tags>'."\r\n";
	
	/*
	 * RSS Feed
	 */
	$sql = "SELECT * from cpo_RSSRead WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<feeds>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<feed>';
		$xmlString .= array_transform($value);
		$xmlString .= '</feed>';
	}
	$xmlString .= '</feeds>'."\r\n";
	
	/*
	 * API's
	 */
	$sql = "SELECT * from cpo_APILogin WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<apis>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<api>';
		$xmlString .= array_transform($value);
		$xmlString .= '</api>';
	}
	$xmlString .= '</apis>'."\r\n";
	
	/*
	 * Calendar
	 */
	$sql = "SELECT * from cpo_calendar WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<events>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<event>';
		$xmlString .= array_transform($value);
		$xmlString .= '</event>';
	}
	$xmlString .= '</events>'."\r\n";
	
	/*
	 * Expenses
	 */
	$sql = "SELECT * from cpo_expense WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<expenses>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<expense>';
		$xmlString .= array_transform($value);
		$xmlString .= '</expense>';
	}
	$xmlString .= '</expenses>'."\r\n";

    /*
	 * Logins (just for clients and team members, not for account owners)
	 */
	$sql = "SELECT * from cpo_login WHERE Parent_Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<logins>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<login>';
		$xmlString .= array_transform($value);
		$xmlString .= '</login>';
	}
	$xmlString .= '</logins>'."\r\n";

    /*
	 * Files
	 */
	$sql = "SELECT * from cpo_files WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);

	$xmlString .= '<files>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<file>';
		$xmlString .= array_transform($value);
		$xmlString .= '</file>';
	}
	$xmlString .= '</files>'."\r\n";

    /*
     * Team members
     */
    $sql = "SELECT
				First_Name,Last_Name,PhoneOffice,
				Email,Signup_Date,Comments,TimeZone,GMTOffset,
				L.Userid,L.User_Type,L.Role,L.UID
				FROM cpo_user_profile P
				LEFT JOIN cpo_login L ON P.Userid = L.UID
				WHERE L.User_Type = '".USER_TYPE_TEAM."' AND
				L.Parent_Userid = '$userid'";
    $result = mysql_db_query($dbname,$sql);

	$xmlString .= '<team_members>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<team_member>';
		$xmlString .= array_transform($value);
		$xmlString .= '</team_member>';
	}
	$xmlString .= '</team_members>'."\r\n";
	
	/*
	 * Frequent Tasks
	 */
	$sql = "SELECT * from cpo_freq_tasks WHERE Userid = '$userid'";
	$result = mysql_db_query($dbname,$sql);
	
	$xmlString .= '<freq_tasks>';
	while ($value = mysql_fetch_assoc($result)) {
		$xmlString .= '<freq_task>';
		$xmlString .= array_transform($value);
		$xmlString .= '</freq_task>';
	}
	$xmlString .= '</freq_tasks>'."\r\n";
	
	$xmlString .= '</document>';			
	echo $xmlString;
}
?>