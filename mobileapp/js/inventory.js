var inventoryObj = {
    init : function() {
        var roomListControl = new Ext.List({
            itemTpl: $('#roomList').html(),
            indexBar: false,
            store: roomStore,
            listeners: {
                'itemtap' : function(dataView,index,item,e) {
                    Ext.getCmp('inventoryContainerPanel').layout.setActiveItem(1,'slide');
                    var roomInventoryObj = dataView.store.getAt(index);
                    roomID = roomInventoryObj.data.RoomID;
                    roomName = roomInventoryObj.data.RoomName;
                    renderRoomName = roomName;
                    var roomInventory = roomInventoryObj.data.Inventory;
                    roomInventoryStore.loadData(roomInventory);

                    if (roomName.length>15) {
                        renderRoomName = roomName.substr(0,16)+'...';
                    }
                    Ext.getCmp('inventoryListPanel').dockedItems.items[0].setTitle(renderRoomName);
                    inventoryListControl.scroller.scrollTo({x: 0,y: 0});
                }
            }
        });

        var inventoryListControl = new Ext.List({
            itemTpl: $('#inventoryList').html(),
            store: roomInventoryStore,
            singleSelect: true,
            listeners: {
                'itemtap': function(dataView,index,item,e) {
                    var listItem = dataView.store.getAt(index);
                    var itemName = listItem.data.ItemName;

                    if (itemName.length>15) {
                        itemName = itemName.substr(0,16)+'...';
                    }
                    inventoryItemName = itemName;
                    Ext.getCmp('inventoryDetailPanel').dockedItems.items[0].setTitle(inventoryItemName);
                                        
                    listItem.data.PlaceName = placeName;
                    listItem.data.RoomName = roomName;

                    /*
                     * Populate attached items store
                     */
                    var files = listItem.data.Files;
                    attachedItemStore.loadData(files);

                    Ext.getCmp('inventoryDetailPanel').update(listItem.data);
                    Ext.getCmp('inventoryContainerPanel').layout.setActiveItem(2,'slide');
                }
            }
        });

        var roomPanel = new Ext.Panel({
            layout: 'fit',
            width: 320,
            height: 450,
            id: 'roomListPanel',
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    defaults: {
                        iconMask: true
                    },
                    items: [{
                        iconCls: 'home',
                        ui: 'back',
                        handler: function() {
                            Ext.getCmp('propertiesContainerPanel').layout.setActiveItem(0,{ type: 'slide', reverse: true });
                        }
                    }]
            }],
            items: [
                    roomListControl
                ]
        });

        var inventoryPanel = new Ext.Panel({
            layout: 'fit',
            width: 320,
            height: 450,
            id: 'inventoryListPanel',
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                defaults: {
                    iconMask: true
                },
                items: [{
                    text: 'Rooms',
                    ui: 'back',
                    handler: function() {
                        Ext.getCmp('inventoryContainerPanel').layout.setActiveItem(0,{ type: 'slide', reverse: true });
                    }
                }]
            }],
            items: [inventoryListControl],
            listeners: {
                render: function() {

                }
            }
        });

        var inventoryDetailPanel = new Ext.Panel({
            layout: 'fit',
            id: 'inventoryDetailPanel',
            scroll: 'vertical',
            tpl: Ext.XTemplate.from('inventoryDetails'),
            listeners: {
                afterrender: function(c) {
                    c.getEl().on('click', function(e) {
                        var buttonId = e.target.id;
                        if (buttonId == 'fileAttachedBar' || buttonId == 'fileAttached') {
                            Ext.getCmp('inventoryContainerPanel').layout.setActiveItem(3,'slide');
                        }
                    });
                }
            },
            dockedItems: [{
                dock:  'top',
                xtype: 'toolbar',
                items: [{
                    text: 'Back',
                    ui: 'back',
                    handler: function() {
                        Ext.getCmp('inventoryContainerPanel').layout.setActiveItem(1, { type: 'slide', reverse: true });
                    }
                }]
            }]
        });

        var attachedItemsContainer = attachedItems.init();        

        ss.inventory = new Ext.Panel({
            id: 'inventoryContainerPanel',
            layout: 'card',
            activeItem: 0,
            items: [roomPanel,inventoryPanel,inventoryDetailPanel,attachedItemsContainer]
        });
        return ss.inventory;
    }
}

var inventorySupportObj = {
    getInventoryItemThumbnail : function(itemObj) {
        var files = itemObj.Files;
        var gotThumbnail = false;
        var thumbnailPath = null;
        if (files.length>0) {
            for(a=0;a<=files.length-1;a++) {
                if (gotThumbnail == false) {
                    if (files[a].FileType == 'image/png' || files[a].FileType == 'image/jpg' || files[a].FileType == 'image/jpeg' || files[a].FileType == 'image/gif') {
                        gotThumbnail = true;
                        thumbnailPath = files[a].thumbnailLink;
                    }
                }
            }
            return thumbnailPath;
        } else {
            return false;
        }
    }
}