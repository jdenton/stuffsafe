var inventoryAddObj = {
    init : function() {
        var propertySelect = new Ext.form.Select({
            placeHolder: 'Property',
            id: 'selectProperty',
            name: 'selectProperty',
            displayField: 'PlaceName',
            valueField: 'PlaceID',
            store:      propertyStore
        });

        var roomSelect = new Ext.form.Select({
            placeHolder: 'Room',
            id: 'selectRoom',
            name: 'selectRoom',
            displayField: 'RoomName',
            valueField: 'RoomID',
            disabled: true,
            store: roomSelectStore
        });

        var categorySelect = new Ext.form.Select({
            placeHolder: 'Category',
            id: 'selectCategory',
            name: 'selectCategory',
            displayField: 'ItemName',
            valueField: 'ItemID',
            disabled: false,
            store: categoryStore
        });

        roomSelectStore.on('datachanged', function(ds,records,o) {
            if (roomSelectStore.data.length == 0) {
                roomSelect.disable();
            } else {
                roomSelect.enable();
                roomSelect.setValue(0);
            }
        });

        propertySelect.on('change',function(select,value) {
            roomSelectStore.loadData(inventoryUpdateObj.getRoomsFromPropertyStore(value));
            roomSelect.setValue(0);
        });

        var inventoryFormPanel = new Ext.form.FormPanel({
            id: 'inventoryAddForm',
            items: [{
                xtype: 'fieldset',
                defaults: {
                    labelWidth: '50%'
                },
                items: [
                    propertySelect,
                    roomSelect,
                    {
                        xtype: 'textfield',
                        name: 'itemName',
                        id: 'itemName',
                        placeHolder: 'Item name',
                        autoCapitalize : true,
                        useClearIcon: true
                    },
                    categorySelect,
                    {
                        xtype: 'textareafield',
                        placeHolder: 'Description',
                        name: 'itemDesc',
                        id: 'itemDesc',
                        height: 75,
                        autoCapitalize : true,
                        grow: true
                    },{
                        xtype: 'selectfield',
                        name: 'itemCondition',
                        id: 'itemCondition',
                        placeHolder: 'Condition',
                        options: [{
                            text: 'Excellent',
                            value: 'excellent'
                        },{
                            text: 'Good',
                            value: 'good'
                        },{
                            text: 'Fair',
                            value: 'fair'
                        },{
                            text: 'Poor',
                            value: 'Poor'
                        }]
                    },{
                        xtype: 'textfield',
                        placeHolder: 'Make',
                        name: 'itemMake',
                        id: 'itemMake',
                        useClearIcon: true,
                        autoCapitalize : true
                    },{
                        xtype: 'textfield',
                        placeHolder: 'Model',
                        name: 'itemModel',
                        id: 'itemModel',
                        useClearIcon: true,
                        autoCapitalize : true
                    },{
                        xtype: 'textfield',
                        placeHolder: 'Serial number',
                        name: 'itemSN',
                        id: 'itemSN',
                        useClearIcon: true
                    },{
                        xtype: 'datepickerfield',
                        picker: {
                            slotOrder: ['month', 'day', 'year'],
                            yearFrom: 1900
                        },
                        value: new Date(),
                        label: 'Purchase date',
                        name: 'itemDate',
                        id: 'itemDate',
                        useClearIcon: true
                    },{
                        xtype: 'numberfield',
                        name: 'pricePurchase',
                        id: 'pricePurchase',
                        placeHolder: 'Purchase price',
                        autoCapitalize : false,
                        useClearIcon: true
                    },{
                        xtype: 'numberfield',
                        name: 'priceReplace',
                        id: 'priceReplace',
                        placeHolder: 'Replacement price',
                        autoCapitalize : false,
                        useClearIcon: true
                    }]
            },{
                layout: 'hbox',
                style: 'margin-top: -20px',
                items: [{
                    xtype: 'button',
                    id: 'buttonTakePhoto',
                    cls: 'buttonTakePhoto',
                    handler: function() {
                        cameraObj.capturePhoto();
                    }
                },{
                    xtype: 'button',
                    id: 'buttonSelectPhoto',
                    cls: 'buttonSelectPhoto',
                    handler: function() {
                        cameraObj.getPhoto();
                    }
                },{
                    xtype: 'button',
                    id: 'imageCountButton',
                    cls: 'buttonAttached',
                    text: '0',
                    handler: function() {
                        Ext.getCmp('inventoryAddContainerPanel').layout.setActiveItem(1,'slide');
                    }
                },{
                    xtype: 'component',
                    hidden: true,
                    id: 'messageAccountExceeded',
                    html: '<div class="alertSmall iconAsterisk">You have exceeded your account file limit. Please select a plan with more storage space or delete some files.</div>'
                }]
            }]
        });

        var inventoryAddContainer = new Ext.Panel({
            scroll: 'vertical',
            dockedItems: [{
                xtype: 'toolbar',
                title: 'Add item',
                dock: 'top',
                defaults: {
                    iconMask: true
                },
                items: [
                {
                    text: 'Cancel',
                    ui: 'back',
                    handler: function() {
                        Ext.Msg.confirm('Abandon changes?','', function(btn) {
                            if (btn == 'yes') {
                                Ext.getCmp('inventoryAddForm').reset();
                                attachedItemStore.removeAll();
                                attachedItemStore.getProxy().clear();
                                inventoryUpdateObj.updateAttachCount();
                                Ext.getCmp('mainTabPanel').layout.setActiveItem(0,{ type: 'slide', reverse: true });
                            } else {
                                return false;
                            }
                        });
                    }
                },{
                    xtype: 'spacer'
                },{
                    text: 'Save',
                    ui: 'confirm',
                    handler: function() {
                        inventoryUpdateObj.saveInventoryItem();
                    }
                }]
            }],
            items: [inventoryFormPanel]
        });

        var inventoryDetailPanelAdd = new Ext.Panel({
            id: 'inventoryDetailPanelAdd',
            layout: 'fit',
            scroll: 'vertical',
            tpl: Ext.XTemplate.from('inventoryDetails'),
            listeners: {
                afterrender: function(c) {
                    c.getEl().on('click', function(e) {
                        var buttonId = e.target.id;
                        if (buttonId == 'fileAttachedBar' || buttonId == 'fileAttached') {
                            //Ext.getCmp('inventoryContainerPanel').layout.setActiveItem(3,'slide');
                        }
                    });
                }
            },
            dockedItems: [{
                dock:  'top',
                xtype: 'toolbar',
                items: [{
                    text: 'Add another',
                    ui: 'back',
                    handler: function() {
                        attachedItemStore.removeAll();
                        attachedItemStore.getProxy().clear();
                        inventoryUpdateObj.updateAttachCount();
                        Ext.getCmp('inventoryAddContainerPanel').layout.setActiveItem(0, { type: 'slide', reverse: true });
                    }
                },{
                    xtype: 'spacer'
                },{
                    ui: 'action',
                    text: 'Done',
                    handler: function() {
                        Ext.getCmp('mainTabPanel').layout.setActiveItem(0, { type: 'slide', reverse: true });
                    }
                }]
            }]
        });

        var attachedItemsContainer = attachedItems.init();
        
        ss.inventoryAdd = new Ext.Panel({
            title: 'Add',
            iconCls: 'add',
            id: 'inventoryAddContainerPanel',
            layout: 'card',
            activeItem: 0,
            listeners: {
                show: function() {
                    if (placeID>0) {
                        propertySelect.setValue(placeID);
                        roomSelectStore.loadData(inventoryUpdateObj.getRoomsFromPropertyStore(placeID));
                    } else {
                        /*
                         * Get rooms for first property in the list
                         */
                        roomSelectStore.loadData(inventoryUpdateObj.getRoomsFromPropertyStore(propertySelect.getValue()));
                    }
                    propertySelect.setValue(0);
                    if (roomID>0) {
                        roomSelect.setValue(roomID);
                    }

                    if (!Ext.isDefined(categoryStore.totalLength) && !categoryStore.lastOptions) {
                        categoryStore.proxy.actionMethods.read = 'POST';
                        categoryStore.proxy.extraParams = {authKey: authKey}
                        categoryStore.load();
                    }
                }
            },
            items: [inventoryAddContainer, attachedItemsContainer, inventoryDetailPanelAdd]
        });

        return ss.inventoryAdd;
    }
}

var inventoryUpdateObj = {
    saveInventoryItem : function() {
        saveMask.show();
        /*
         * Save entry to database and reset form.
         */
        var placeID          = Ext.getCmp('selectProperty').getValue();
        var placeNameIndex   = propertyStore.find('PlaceID', placeID);
        var placeNameObj     = propertyStore.getAt(placeNameIndex);
        placeName = placeNameObj.data.PlaceName;

        var roomID           = Ext.getCmp('selectRoom').getValue();
        var roomNameIndex    = roomSelectStore.find('RoomID', roomID);
        var roomNameObj      = roomSelectStore.getAt(roomNameIndex);
        var invRoomName      = roomNameObj.data.RoomName;
        roomName = invRoomName;

        var invName          = Ext.getCmp('itemName').getValue();
        var invDescription   = Ext.getCmp('itemDesc').getValue();
        var invPricePurchase = Ext.getCmp('pricePurchase').getValue();
        var invPriceReplace  = Ext.getCmp('priceReplace').getValue();

        var invCategory      = Ext.getCmp('selectCategory').getValue();;
        var invMake          = Ext.getCmp('itemMake').getValue();
        var invModel         = Ext.getCmp('itemModel').getValue();
        var invSN            = Ext.getCmp('itemSN').getValue();
        var invDate          = Ext.getCmp('itemDate').getValue();

        var invCondition     = Ext.getCmp('itemCondition').getValue();
        var invTags = '';

        if (invName == '') {
            Ext.Msg.alert('Oops!', 'You must enter an item name.', Ext.emptyFn);
        } else {
            Ext.Ajax.request({
                url: siteURL+'inventory/saveInventoryItem',
                method: 'post',
                placeID: placeID,
                params: {
                    action:           'add',
                    save:             1,
                    invID:            '',
                    placeID:          placeID,
                    invRoomID:        roomID,
                    invRoomNew:       invRoomName,
                    invName:          invName,
                    invDescription:   invDescription,
                    invPricePurchase: invPricePurchase,
                    invPriceReplace:  invPriceReplace,
                    invCategory:      invCategory,
                    invMake:          invMake,
                    invModel:         invModel,
                    invSN:            invSN,
                    invTags:          invTags,
                    invDate:          invDate,
                    invCondition:     invCondition,
                    authKey:          authKey
                },
                failure : function(response) {
                     data = Ext.decode(response.responseText);
                     Ext.Msg.alert('Oops!', data.ErrorMessage, Ext.emptyFn);
                },
                success: function(response, opts) {
                    attachedItemStore.removeAll();
                    attachedItemStore.getProxy().clear();
                    var responseObj = eval('('+response.responseText+')');
                    var itemName = responseObj.ItemName;

                    if (itemName.length>15) {
                        itemName = itemName.substr(0,16)+'...';
                    }
                    Ext.getCmp('inventoryDetailPanelAdd').dockedItems.items[0].setTitle(itemName);

                    /*
                     * Populate attached items store
                     */
                    var files = responseObj.Files;
                    attachedItemStore.loadData(files);

                    responseObj.PlaceName = placeName;
                    responseObj.RoomName = roomName;

                    Ext.getCmp('inventoryDetailPanelAdd').update(responseObj);
                    Ext.getCmp('inventoryAddContainerPanel').layout.setActiveItem(2,'slide');
                    saveMask.hide();

                    /*
                     * Clear form and reload store
                     */
                    Ext.getCmp('inventoryAddForm').reset();
                    inventoryUpdateObj.updateAttachCount();
                    inventoryStore.proxy.actionMethods.read = 'POST';
                    inventoryStore.proxy.extraParams = {authKey: authKey, placeID: response.request.options.placeID}
                    inventoryStore.load();
                }
            });
        }
    },
    updateAttachCount : function() {
        var attachCountButton = Ext.getCmp('imageCountButton');
        attachCountButton.update(attachedItemStore.data.items.length);
    },
    getRoomsForPlace : function(roomStore,placeID) {
        roomStore.proxy.actionMethods.read = 'POST';
        roomStore.proxy.extraParams = {authKey: authKey, placeID: placeID}
        roomStore.load();
    },
    getRoomsFromPropertyStore : function(placeID) {
        var propertyIndex = propertyStore.find('PlaceID',placeID);
        var propertyObj = propertyStore.getAt(propertyIndex);
        var roomObj = propertyObj.data.Rooms;
        return roomObj;
    },
    removeAttachedFile : function(fileObj) {
        var fileID = fileObj.data.FileID;
        Ext.Ajax.request({
            url: siteURL+'filemanager/deleteItem/'+fileID,
            method: 'post',
            params: {
                fileID:           fileID,
                authKey:          authKey
            },
            failure : function(response) {
                 data = Ext.decode(response.responseText);
                 Ext.Msg.alert('Oops!', data.ErrorMessage, Ext.emptyFn);
            },
            success: function(response, opts) {
                var returnObj = eval('('+response.responseText+')');
                mainApp.renderControlsBasedOnFilespace(returnObj.FileUseOver);
            }
        });
    }
}