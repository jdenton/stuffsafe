var searchObj = {
    init : function() {
        var searchListControl = new Ext.List({
            itemTpl: $('#inventoryList').html(),
            grouped: true,
            singleSelect: true,
            loadingText: null,
            store: inventorySearchStore,
            listeners: {
                'itemtap': function(dataView,index,item,e) {
                    var listItem = dataView.store.getAt(index);
                    var itemName = listItem.data.ItemName;

                    if (itemName.length>15) {
                        itemName = itemName.substr(0,16)+'...';
                    }
                    inventoryItemName = itemName;
                    Ext.getCmp('inventoryDetailPanelSearch').dockedItems.items[0].setTitle(inventoryItemName);

                    /*
                     * Populate attached items store
                     */
                    var files = listItem.data.Files;
                    attachedItemStore.loadData(files);

                    Ext.getCmp('inventoryDetailPanelSearch').update(listItem.data);
                    Ext.getCmp('searchContainerPanel').layout.setActiveItem(1,'slide');
                }
            }
        });

        var searchListPanel = new Ext.Panel({
            layout: 'fit',
            id: 'searchListPanel',
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                defaults: {
                    iconMask: true
                },
                items: [{
                    xtype : 'searchfield',
                    placeHolder: 'Search',
                    listeners: {
                        scope: this,
                        keyup: function(field) {
                            var value = field.getValue();
                            if (value.length>=3) {
                                /*
                                 * Load inventorySearchStore
                                 */
                                inventorySearchStore.proxy.actionMethods.read = 'POST';
                                inventorySearchStore.proxy.extraParams = {authKey: authKey, term: field.getValue()}
                                inventorySearchStore.load();

                                searchListControl.scroller.scrollTo({x: 0,y: 0});
                            }
                        }
                    }
                }]
            }],
            items: [
                    searchListControl
                ]
        });

        var inventoryDetailPanelSearch = new Ext.Panel({
            id: 'inventoryDetailPanelSearch',
            layout: 'fit',
            scroll: 'vertical',
            tpl: Ext.XTemplate.from('inventoryDetails'),
            listeners: {
                afterrender: function(c) {
                    c.getEl().on('click', function(e) {
                        var buttonId = e.target.id;
                        if (buttonId == 'fileAttachedBar' || buttonId == 'fileAttached') {
                            attachedItemsContainerSearch.dockedItems.items[0].setTitle('Files: '+inventoryItemName);
                            Ext.getCmp('searchContainerPanel').layout.setActiveItem(2,'slide');
                        }
                    });
                }
            },
            dockedItems: [{
                dock:  'top',
                xtype: 'toolbar',
                items: [{
                    text: 'Back',
                    ui: 'back',
                    handler: function() {
                        Ext.getCmp('searchContainerPanel').layout.setActiveItem(0, { type: 'slide', reverse: true });
                    }
                }]
            }]
        });

        var attachedItemsContainerSearch = attachedItems.init();

        ss.search = new Ext.Panel({
            title: 'Search',
            iconCls: 'search',
            listeners: {
                show: function() {
                    this.layout.setActiveItem(0);
                }
            },
            id: 'searchContainerPanel',
            layout: 'card',
            activeItem: 0,
            items: [searchListPanel,inventoryDetailPanelSearch,attachedItemsContainerSearch]
        });
        return ss.search;
    }
}