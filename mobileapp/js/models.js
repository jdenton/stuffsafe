var modelsObj = {
    initModels : function() {
        Ext.regModel('ModelPropertySelect', {
            fields: [
                'PlaceID', 'PlaceName'
            ]
        });

        Ext.regModel('ModelRoomSelect', {
            fields: ['RoomID', 'Title']
        });

        Ext.regModel('InventoryCategoryModel', {
            fields: ['ItemID', 'ItemName']
        });

        Ext.regModel('PropertyList', {
            fields: [
                {name: 'AvatarClass',  type: 'string', defaultValue: 'home'},
                {name: 'Rooms', type: 'array'},
                'PlaceID',
                'PlaceName',
                'Address',
                'City',
                'State',
                'Zip',
                {name: 'PlaceItemCount', type: 'string', defaultValue: '0'}
            ]
        });

        Ext.regModel('RoomSelect', {
            fields: ['RoomID', 'RoomName']
        });

        Ext.regModel('RoomList', {
            fields: [
                'RoomID',
                'RoomName',
                'InventoryItems',
                {name: 'RoomItemCount', type: 'string', defaultValue: '0'}
            ]
        });

        Ext.regModel('ModelAttachedItem', {
            fields: [
                'LinkID',
                'FileID',
                {name: 'thumbnailLink', type: 'string', defaultValue: siteURL+'images/avatars/stockAvatar46.png'},
                {name: 'fileLink', type: 'string'},
                {name: 'FileNameActual', type: 'string'},
                {name: 'FileClassName', type: 'string'}
            ]
        });

        Ext.regModel('InventoryRoomModel', {
            fields: [
                {name: 'RoomID', type: 'string'},
                {name: 'RoomName', type: 'string'},
                {name: 'Inventory', type: 'array'},
                {name: 'RoomTags',  type: 'string'},
                {name: 'RoomItemCount',  type: 'string'}
            ]
        });

        Ext.regModel('InventoryList', {
            fields: [
                {name: 'AvatarURL',  type: 'string'},
                {name: 'InventoryID', type: 'string'},
                {name: 'ItemName', type: 'string'},
                {name: 'ItemMake', type: 'string'},
                {name: 'ItemModel', type: 'string'},
                {name: 'ItemCondition', type: 'string'},
                {name: 'DatePurchaseHuman', type: 'string'},
                {name: 'Description',  type: 'string'},
                {name: 'PriceReplace',  type: 'string'},
                {name: 'PricePurchase',  type: 'string'},
                {name: 'MainCat',  type: 'string'},
                {name: 'RoomName', type: 'string'},
                {name: 'PlaceName', type: 'string'},
                {name: 'Files', type: 'array'}
            ]
        });

        Ext.regModel('InventorySearchModel', {
            fields: [
                {name: 'AvatarURL',  type: 'string'},
                {name: 'InventoryID', type: 'string'},
                {name: 'PlaceID', type: 'string'},
                {name: 'PlaceName', type: 'string'},
                {name: 'RoomID', type: 'string'},
                {name: 'RoomName', type: 'string'},
                {name: 'ItemName', type: 'string'},
                {name: 'ItemMake', type: 'string'},
                {name: 'ItemModel', type: 'string'},
                {name: 'ItemCondition', type: 'string'},
                {name: 'DatePurchaseHuman', type: 'string'},
                {name: 'Description',  type: 'string'},
                {name: 'PriceReplace',  type: 'string'},
                {name: 'PricePurchase',  type: 'string'},
                {name: 'MainCat',  type: 'string'},
                {name: 'Files', type: 'array'}
            ]
        });   
    }
}