var storageObj = {
    
}

var cameraObj = {
    capturePhoto : function() {
        // Take picture using device camera and retrieve image as base64-encoded string
        navigator.camera.getPicture(cameraObj.onPhotoURISuccess, cameraObj.onFail, { 
            quality: 30,
            destinationType: destinationType.FILE_URI
        });
    },
    getPhoto : function(source) {
        if (source == null) {
            source = pictureSource.PHOTOLIBRARY;
        }
        // Retrieve image file location from specified source
        navigator.camera.getPicture(cameraObj.onPhotoURISuccess, cameraObj.onFail, {
            quality: 30,
            destinationType: destinationType.FILE_URI,
            sourceType: source
        });
    },
    onPhotoURISuccess : function(imageURI) {
        var largeImage = $('#smallImage');
        largeImage.attr('src', imageURI);
        largeImage.show();

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.mimeType = "image/jpeg";

        var params = new Object();
        params.fromMobile = '1';
        params.authKey = authKey;
        options.params = params;

        var ft = new FileTransfer();
        ft.upload(imageURI, siteURL + 'filemanager/uploadFileHandler/0/inventory', cameraObj.onSuccess, cameraObj.onFail, options);
        uploadPhotoMask.show();
	},
    onFail : function(message) {
		uploadPhotoMask.hide();
	},
    onSuccess : function(r) {
        uploadPhotoMask.hide();
        var returnObj = eval('('+r.response+')');
        mainApp.renderControlsBasedOnFilespace(returnObj.Filespace.FileUseOver);
        attachedItemStore.add({LinkID: returnObj.LinkID, FileID: returnObj.FileID, thumbnailLink: returnObj.Thumbfilename, FileNameActual: returnObj.Filename});
        setTimeout(inventoryUpdateObj.updateAttachCount(),500);
    }
}