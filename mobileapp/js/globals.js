/*
 * Some global variables
 */
var authKey;
var propertyStore;
var roomSelectStore;
var roomStore;
var inventoryStore;
var roomInventoryStore;
var attachedItemStore;
var categoryStore;
var pictureSource;
var destinationType;
var placeID, placeName;
var roomID, roomName;
var inventoryItemName;
var loginMask, loadMask, uploadPhotoMask, searchMask;
var fileAttachedPanel;
var fileSpaceExceeded = false;
var siteURL = 'http://www.stuffsafe.com/';