document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    pictureSource = navigator.camera.PictureSourceType;
    destinationType = navigator.camera.DestinationType;
}

Ext.ns('ss');
Ext.setup({
    icon: siteURL+'images/mobile/appIcon.png',
    tabletStartupScreen: siteURL+'images/mobile/tablet_startup.png',
    phoneStartupScreen: siteURL+'images/mobile/phone_startup.png',
    glossOnIcon: true,
    onReady: function() {
        loginMask       = new Ext.LoadMask(Ext.getBody(), {msg: 'Logging in...'});
        placeMask       = new Ext.LoadMask(Ext.getBody(), {msg: 'Properties...'});
        inventoryMask   = new Ext.LoadMask(Ext.getBody(), {msg: 'Inventory...'});
        uploadPhotoMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Uploading...'});
        searchMask      = new Ext.LoadMask(Ext.getBody(), {msg: 'Searching...'});
        saveMask        = new Ext.LoadMask(Ext.getBody(), {msg: 'Saving...'});
        
        //authKey = window.localStorage.getItem('authKey');
        if (authKey != null && authKey != '') {
            mainApp.initApp();
        } else {
            loginObj.loginFormRender();
        }
    }    
});