var mainApp = {
    initApp : function() {
        modelsObj.initModels();
        mainApp.getCommonAppStores();
        mainApp.getFilespaceInfo();
        
        var propertyPanel     = propertyObj.init();
        var searchPanel       = searchObj.init();
        var inventoryAddPanel = inventoryAddObj.init();   

        var tabpanel = new Ext.TabPanel({
            id: 'mainTabPanel',
            tabBar: {
                dock: 'bottom',
                layout: {
                    pack: 'center'
                }
            },
            listeners: {
                cardswitch: function() {
                    /*
                     * Clear our file store.
                     */
                    attachedItemStore.removeAll();
                    attachedItemStore.getProxy().clear();
                    inventoryUpdateObj.updateAttachCount();
                }
            },
            fullscreen: true,
            ui: 'dark',
            activeItem: 0,
            cardSwitchAnimation: {
                type: 'slide',
                cover: true
            },
            items: [
                propertyPanel,
                inventoryAddPanel,
                searchPanel
            ]
        });
    },
    getCommonAppStores : function() {
        attachedItemStore = new Ext.data.Store({
            model: 'ModelAttachedItem',
            storeId: 'attachedItemStore',
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: ''
                }
            }
        });

        propertyStore = new Ext.data.Store({
            model: 'PropertyList',
            sorters: 'PlaceName',
            proxy: {
                type: 'ajax',
                url: siteURL+'places/getPlaces/0/json/0',
                params: {
                    authKey: authKey
                },
                reader: {
                    type: 'json',
                    root: 'Places'
                }
            }
        });

        roomSelectStore = new Ext.data.Store({
            model: 'RoomSelect',
            storeId: 'roomSelectStore',
            proxy: {
                type: 'ajax',
                url: siteURL+'inventory/getRoomsForPlace/0/json',
                reader: {
                    type: 'json',
                    root: ''
                }
            }
        });

        roomInventoryStore = new Ext.data.Store({
            model: 'InventoryList',
            storeId: 'roomInventoryStore',
            getGroupString : function(record) {
                return record.get('ItemName')[0].toUpperCase();
            },
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: ''
                }
            }
        });

        roomStore = new Ext.data.Store({
            model: 'RoomList',
            storeId: 'roomStore',
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: ''
                }
            }
        });

        inventoryStore = new Ext.data.Store({
            model: 'InventoryRoomModel',
            sorters: 'ItemName',
            proxy: {
                type: 'ajax',
                url: siteURL+'inventory/getInventoryForPlace/0/json',
                params: {
                    authKey: authKey
                },
                reader: {
                    type: 'json',
                    root: ''
                }
            },
            listeners: {
                load : function(store,records,options) {
                    roomStore.loadData(records);
                }
            }
        });

        inventorySearchStore = new Ext.data.Store({
            model: 'InventorySearchModel',
            storeId: 'inventorySearchStore',
            getGroupString : function(record) {
                return record.get('ItemName')[0].toUpperCase();
            },
            proxy: {
                type: 'ajax',
                url: siteURL+'sitesearch/inventorySearch',
                reader: {
                    type: 'json',
                    root: ''
                }
            }
        });

        categoryStore = new Ext.data.Store({
            model: 'InventoryCategoryModel',
            storeId: 'categoryStore',
            proxy: {
                type: 'ajax',
                url: siteURL+'inventory/getInventoryCategories',
                reader: {
                    type: 'json',
                    root: ''
                }
            }
        });

        searchMask.bindStore(inventorySearchStore);
        placeMask.bindStore(propertyStore);
        inventoryMask.bindStore(inventoryStore);
    },
    makeInfoPopup : function() {
        var infoPopup = new Ext.Panel({
            floating: true,
            modal: true,
            centered: true,
            width: 300,
            height: 300,
            styleHtmlContent: true,
            scroll: 'vertical',
            html: $('#aboutPanel').html(),
            dockedItems: [{
                dock: 'top',
                xtype: 'toolbar',
                title: 'About StuffSafe Mobile'
            }]
        });

        return infoPopup;
    },
    getFilespaceInfo : function() {
        Ext.Ajax.request({
            url: siteURL+'filemanager/getFileSpaceUsage/json',
            method: 'post',
            params: {
                authKey:          authKey
            },
            failure : function(response) {
                 data = Ext.decode(response.responseText);
            },
            success: function(response, opts) {
                var returnObj = eval('('+response.responseText+')');
                mainApp.renderControlsBasedOnFilespace(returnObj.FileUseOver);
            }
        });
    },
    renderControlsBasedOnFilespace : function(fileSpaceOver) {
        if (fileSpaceOver == '1') {
            fileSpaceExceeded = true;
            Ext.getCmp('buttonTakePhoto').hide();
            Ext.getCmp('buttonSelectPhoto').hide();
            Ext.getCmp('imageCountButton').hide();
            Ext.getCmp('messageAccountExceeded').show();
        } else {
            fileSpaceExceeded = false;
            Ext.getCmp('buttonTakePhoto').show();
            Ext.getCmp('buttonSelectPhoto').show();
            Ext.getCmp('imageCountButton').show();
            Ext.getCmp('messageAccountExceeded').hide();
        }
    }
}