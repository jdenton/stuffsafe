var propertyObj = {
    init : function() {
        var inventoryPanel = inventoryObj.init();

        var propertyListControl = new Ext.List({            
            itemTpl: $('#propertyList').html(),
            indexBar: true,
            loadingText: null,
            store: propertyStore,
            listeners: {
                'itemtap' : function(dataView,index,item,e) {
                    Ext.getCmp('propertiesContainerPanel').layout.setActiveItem(1,'slide');
                    var listItem = dataView.store.getAt(index);
                    placeID = listItem.data.PlaceID;
                    placeName = listItem.data.PlaceName;
                    var renderPlaceName = placeName;

                    if (placeName.length>15) {
                        renderPlaceName = placeName.substr(0,16)+'...';
                    }

                    Ext.getCmp('roomListPanel').dockedItems.items[0].setTitle(renderPlaceName);

                    inventoryStore.proxy.actionMethods.read = 'POST';
                    inventoryStore.proxy.extraParams = {authKey: authKey, placeID: placeID}
                    inventoryStore.load();
                }
            }
        });

        var propertyPanel = new Ext.Panel({
            layout: 'fit',
            width: 320,
            height: 450,
            id: 'propertyListPanel',
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                defaults: {
                    iconMask: true
                },
                items: [{
                    xtype: 'component',
                    html: '<span class="headerIcon"></span>'
                },{
                    xtype: 'spacer'
                },{
                    iconCls: 'info',
                    handler: function() {
                        if (!this.infoPopup) {
                            this.infoPopup = mainApp.makeInfoPopup();
                        }
                        this.infoPopup.show('pop');
                    }
                }]
            }],
            items: [
                    propertyListControl
                ],
            listeners: {
                render: function() {
                    /*
                     * If our store is not loaded, then load and bind.
                     */
                    if (!Ext.isDefined(propertyStore.totalLength) && !propertyStore.lastOptions) {
                        propertyStore.proxy.actionMethods.read = 'POST';
                        propertyStore.proxy.extraParams = {authKey: authKey}
                        propertyStore.load();
                        propertyListControl.bindStore(propertyStore);
                    }
                }
            }
        });

        ss.properties = new Ext.Panel({
            title: 'Properties',
            iconCls: 'home',
            id: 'propertiesContainerPanel',
            layout: 'card',
            activeItem: 0,
            items: [propertyPanel, inventoryPanel]
        });

        return ss.properties;
    }
}