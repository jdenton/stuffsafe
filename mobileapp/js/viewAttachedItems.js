var attachedItems = {
    init : function() {
        var attachedListPanel, fileViewPanel, activePanel;
                    
        var attachedListControl = new Ext.List({            
            itemTpl: $('#attachedItemList').html(),
            loadingText: null,
            store: attachedItemStore,
            listeners: {
                afterrender: function(c) {                    
                    this.scroller.scrollTo({x: 0,y: 0});
                },
                'itemtap' : function(dataView,index,element,e) {
                    var eventClassname = e.target.className;
                    var selectedRecord = dataView.store.getAt(index);
                    if (activePanel.id == 'inventoryAddContainerPanel') {
                        if (eventClassname.search('buttonListDelete')!= -1) {
                            Ext.Msg.confirm('Remove attached file','Are you sure?', function(btn) {
                                if (btn == 'yes') {
                                    dataView.store.remove(selectedRecord);
                                    inventoryUpdateObj.removeAttachedFile(selectedRecord);
                                    inventoryUpdateObj.updateAttachCount();
                                } else {
                                    return false;
                                }
                            });
                        }
                    } else {
                        if (selectedRecord.data.fileExt != 'png' && selectedRecord.data.fileExt != 'jpg' && selectedRecord.data.fileExt != 'gif') {
                            window.location = selectedRecord.data.fileLink;
                        } else {
                            attachedListPanel.layout.setActiveItem(1,{type: 'slide', cover: true});
                            attachedListPanel.dockedItems.items[0].items.items[2].show();
                            attachedListPanel.dockedItems.items[0].items.items[0].hide();
                            fileViewPanel.update('<img style="max-width: 100%; max-height: 100%;" src="'+selectedRecord.data.fileLink+'" />');
                        }
                    }
                }
            }
        });

        fileViewPanel = new Ext.Panel({
            html: ''
        });

        attachedListPanel = new Ext.Panel({
            layout: 'card',
            activeItem: 0,
            title: 'Files',
            items: [attachedListControl,fileViewPanel],
            listeners: {
                show: function() {
                    activePanel = Ext.getCmp('mainTabPanel').getActiveItem();
                    if (activePanel.id != 'inventoryAddContainerPanel') {
                        $('.buttonListDelete').hide();
                    }                    
                }
            },
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    defaults: {
                        iconMask: true
                    },
                    items: [{
                        xtype: 'button',
                        ui: 'back',
                        text: 'Back',
                        id: 'tbBackToInventoryItem',
                        handler: function() {
                            if (activePanel.id == 'propertiesContainerPanel') {
                                Ext.getCmp('inventoryContainerPanel').layout.setActiveItem(2,{ type: 'slide', reverse: true });
                            } else if (activePanel.id == 'searchContainerPanel') {
                                Ext.getCmp('searchContainerPanel').layout.setActiveItem(1,{ type: 'slide', reverse: true });
                            } else if (activePanel.id == 'inventoryAddContainerPanel') {
                                Ext.getCmp('inventoryAddContainerPanel').layout.setActiveItem(0,{ type: 'slide', reverse: true });
                            }
                        }
                    },{
                        xtype: 'spacer'
                    },{
                        xtype: 'button',
                        ui: 'confirm',
                        text: 'Close',
                        id: 'tbCloseFileView',
                        hidden: true,
                        handler: function() {
                            attachedListPanel.layout.setActiveItem(0,{type: 'slide', reverse: true, reveal: true});
                            this.hide();
                            attachedListPanel.dockedItems.items[0].items.items[0].show();
                        }
                    }]
                }
            ]
        });
        
        return attachedListPanel;
    }
}