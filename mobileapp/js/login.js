var loginForm;
var loginObj = {    
    tryLogin : function() {
        loginMask.show();

        var userid = loginForm.getValues().userid;
        var password = loginForm.getValues().password;
        Ext.Ajax.request({
            url: siteURL+'login/tryLogin/1',
            method: 'post',
            params: {
                userid: userid,
                password: password
            },
            failure : function(response){
                 data = Ext.decode(response.responseText);
            },
            success: function(response, opts) {
                data = Ext.decode(response.responseText);
                if (data.ErrorMessage != null)
                {
                    var message;
                    if (data.ErrorMessage == 'noAccount') {
                        message = 'We can\'t seem to find your account.';
                    } else if (data.ErrorMessage == 'missingField') {
                        message = 'You must enter an email address and password.';
                    } else {
                        message = 'An unknown error occured.';
                    }
                    Ext.Msg.alert('Oops!', message, Ext.emptyFn);
                    loginMask.hide();
                } else {
                    authKey = data.AuthKey;
                    loginMask.hide();
                    loginForm.hide();
                    mainApp.initApp();
                    window.localStorage.setItem('authKey', authKey);
                }
            }
        });
    },
    loginFormRender : function() {
        loginForm = new Ext.form.FormPanel({
            title: 'Welcome to StuffSafe!',
            xtype: 'form',
            id: 'loginForm',
            scroll: 'vertical',
            fullscreen: true,
            ui: 'light',
            dockedItems: [
                {
                    xtype: 'toolbar',
                    title: '<span class="headerIcon"></span>',
                    dock: 'top'
                }
            ],
            items: [{
                xtype: 'fieldset',
                    title: 'Welcome to StuffSafe!',
                    defaults: {
                        labelWidth: '40%'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'userid',
                            inputType: 'email',
                            id: 'userid',
                            placeHolder: 'Email address',
                            autoCapitalize : false,
                            required: true,
                            useClearIcon: true
                        },{
                            xtype: 'passwordfield',
                            name: 'password',
                            id: 'password',
                            placeHolder: 'Password',
                            autoCapitalize : false,
                            required: true,
                            useClearIcon: true
                        }
                    ]
                },{
                    xtype: 'button',
                    text: 'Login',
                    ui: 'confirm',
                    height: 50,
                    handler: function() {
                        Ext.get('keyboardClose').dom.focus();
                        loginObj.tryLogin();
                    }
                }]
        });
    }
}