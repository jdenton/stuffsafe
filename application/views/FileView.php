<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="individualFileView">
    <script type="text/javascript" language="javascript">
	/* <![CDATA[ */
		var projectData = '<?=$projectInformation; ?>';
	/* ]]> */
	</script>
    <input type="hidden" id="fileID" name="fileID" value="<?=$fileID; ?>" />
    <input type="hidden" id="fileLinkID" name="fileLinkID" value="<?=$fileLinkID; ?>" />
    <div id="fileViewInfomationContainer"></div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
