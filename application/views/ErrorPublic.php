<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <h1 class="iconHorror"><?=$errorTitle; ?></h1>

    <? if ($errorType == '404') { ?>
    <div style="margin: 6px 0 0 45px;">
        <p>Here are some other pages that might be of interest to you...</p>

        <p class="mTop12 mBottom12"><a href="/tour" title="StuffSafe tour" class="bigText">StuffSafe tour</a><br />
            See how StuffSafe can help you create a complete home inventory simply and quickly.</p>

        <p class="mTop12 mBottom12"><a href="/pricing" title="StuffSafe pricing" class="bigText">Pricing and signup</a><br />
            Whether you want to keep track of stuff in your dorm room or manage inventory for serveral properties, we
        have a competitively priced plan that will work for you.</p>

        <p class="mTop12 mBottom12"><a href="/contact" title="Contact StuffSafe" class="bigText">Contact us</a><br />
            Have a question, suggestion, or comment? Give us a shout...we'd love to hear from you.</p>

        
    </div>
    <? } else { ?>
    <p style="margin: 6px 0 0 45px;"><?=$errorMessage; ?></p>
    <? } ?>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>