<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="updatePlace">
    <form class="greyForm" id="formPlace" name="formPlace" action="<?=BASE_URL; ?>placeupdate/savePlace" method="POST">
	<input type="hidden" name="placeID" id="placeID" value="<?=$placeInformation['PlaceID']; ?><?=set_value('placeID'); ?>" />
	<input type="hidden" name="action" id="action" value="<?=$action; ?><?=set_value('action'); ?>" />
	<input type="hidden" name="save" id="save" value="1" />
        <p>
			<label class="required" for="placeName"><?=lang('place_form_name'); ?></label>
			<input type="text" value="<?=$placeInformation['PlaceName']; ?><?=set_value('placeName'); ?>" name="placeName" id="placeName" tabindex="1" style="width: 400px;" />
			<?=form_error('placeName','<span class="formError">','</span>'); ?>
		</p>

        <p>
            <label><?=lang('place_type'); ?></label>
            <select name="placeType" id="placeType" selected="<?=set_value('placeType'); ?>" tabindex="2" style="width: 408px;">
                <option value="0"><?=lang('common_field_select'); ?></option>
                <option value="0"></option>
                <option value="single family house" <? if ($placeInformation['Category'] == 'single family house') { echo 'selected="selected"'; } ?>><?=lang('cat_single_family_house'); ?></option>
                <option value="apartment unit" <? if ($placeInformation['Category'] == 'apartment unit') { echo 'selected="selected"'; } ?>><?=lang('cat_apartment_unit'); ?></option>
                <option value="multi family" <? if ($placeInformation['Category'] == 'multi family') { echo 'selected="selected"'; } ?>><?=lang('cat_multi_family'); ?></option>
                <option value="townhouse" <? if ($placeInformation['Category'] == 'townhouse') { echo 'selected="selected"'; } ?>><?=lang('cat_townhouse'); ?></option>
                <option value="condominium" <? if ($placeInformation['Category'] == 'condominium') { echo 'selected="selected"'; } ?>><?=lang('cat_condominium'); ?></option>
                <option value="business" <? if ($placeInformation['Category'] == 'business') { echo 'selected="selected"'; } ?>><?=lang('cat_business'); ?></option>
                <option value="boat" <? if ($placeInformation['Category'] == 'boat') { echo 'selected="selected"'; } ?>><?=lang('cat_boat'); ?></option>
                <option value="plane" <? if ($placeInformation['Category'] == 'plane') { echo 'selected="selected"'; } ?>><?=lang('cat_plane'); ?></option>
            </select>
        </p>

        <p>
			<label><?=lang('common_address'); ?></label>
			<textarea class="expanding" name="placeAddress" id="placeAddress" tabindex="3" style="width: 400px; height: 20px;"><?=$placeInformation['Address']; ?><?=set_value('placeAddress'); ?></textarea>
		</p>

        <p>
			<label for="placeCity"><?=lang('common_city'); ?></label>
			<input type="text" value="<?=$placeInformation['City']; ?><?=set_value('placeCity'); ?>" name="placeCity" id="placeCity" tabindex="4" style="width: 400px;" />
		</p>

        <p>
            <label><?=lang('common_state'); ?></label>
            <input type="text" name="placeState" id="placeState" value="<?=$placeInformation['State']; ?><?=set_value('placeState'); ?>" tabindex="5" style="width: 175px; margin-right: 10px;" />
            <span class="inlineLabel"><?=lang('common_zip'); ?></span>
            <input type="text" value="<?=$placeInformation['Zip']; ?><?=set_value('placeZip'); ?>" name="placeZip" id="placeZip" tabindex="6" style="width: 87px;" />
        </p>
        <p>
            <label><?=lang('common_country'); ?></label>
            <select name="placeCountry" id="placeCountry" selected="<?=set_value('placeCountry'); ?>" tabindex="7" style="width: 183px; margin-right: 10px;">
            <option><?=lang('common_select'); ?></option>
            <? $this->load->view('includes/country.html'); ?>
            </select>
            <input type="hidden" id="countryHolder" value="<?=$placeInformation['Country']; ?>" />
        </p>

        <p>
			<label><?=lang('place_form_description'); ?></label>
			<textarea class="expanding" name="placeDescription" id="placeDescription" tabindex="8" style="width: 400px; height: 20px;"><?=$placeInformation['Description']; ?><?=set_value('placeDescription'); ?></textarea>
		</p>

        <p>
            <label><?=lang('place_form_area'); ?></label>
            <input type="text" name="placeArea" id="placeArea" value="<?=$placeInformation['Area']; ?><?=set_value('placeArea'); ?>" tabindex="9" style="width: 175px; margin-right: 10px;" />
            <input type="radio" name="placeAreaUnit" value="feet" tabindex="10" <? if ($placeInformation['AreaUnit'] == 'feet') { echo 'checked'; } ?> /> <?=lang('common_square_feet'); ?>
            <input type="radio" name="placeAreaUnit" value="meters" tabindex="11" <? if ($placeInformation['AreaUnit'] == 'meters') { echo 'checked'; } ?> /> <?=lang('common_square_meter'); ?>
        </p>

        <div style="float: left;">
        <p class="noBorder">
			<label><?=lang('common_tags'); ?></label>
			<input type="text" name="placeTags" id="placeTags" tabindex="12" value="<?=$placeInformation['Tags']; ?><?=set_value('placeTags'); ?>" style="float: left; width: 359px;" />
			<div style="float: left; margin: -6px 0 0 7px;"><button class="smallButton popUpMenuClick" id="buttonTagEdit" tabindex="13" title="<?=lang('button_edit'); ?>"><span class="edit single"></span></button></div>
			<div style="clear: left;" class="formHelpText"><span class="subText"><?=lang('help_tag_format'); ?></span></div>
		</p>
		</div>
		<div style="float: left;">
			<?=help('help_formfield_tags','placeTags','iconHelpFormOutside'); ?>
		</div>
		<div style="clear: left;"></div>
		<p class="noBorder"></p>

		<div class="buttonContainer">
            <button class="buttonExpand blueGray primary" id="buttonSave" tabindex="14"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator save"><?=lang('button_save'); ?></span></span></button>
			<button class="buttonExpand yellow secondary" id="buttonCancel" tabindex="15"><span class="buttonOuterSpan yellow"><span class="buttonDecorator cancel"><?=lang('button_cancel'); ?></span></span></button>
            <div style="clear: left;"></div>
		</div>
    </form>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>