<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="settings">
	<div id="tabsSettings">
		<ul class="ui-tabs-nav">
            <li <? if ($permissions['settingsUpdate'] != 1) { echo 'class="hide"'; } ?>><a class="ui-tabs-selected" href="#windowSettingsAccount" id="tabSettingsAccount"><span><?=lang('settings_account_info'); ?></span></a></li>
            <li><a href="#windowSettingsPersonal" id="tabSettingsPersonal"><span><?=lang('settings_personal_info'); ?></span></a></li>
	        <li <? if ($permissions['settingsUpdate'] != 1) { echo 'class="hide"'; } ?>><a href="#windowSettingsInsurance" id="tabSettingsInsurance"><span><?=lang('common_insurance_policies'); ?></span></a></li>
	        <li <? if ($permissions['settingsUpdate'] != 1) { echo 'class="hide"'; } ?>><a href="#windowSettingsData" id="tabSettingsData"><span><?=lang('common_data'); ?></span></a></li>
        </ul>
	    <div style="margin-top: -1px; min-height: 200px;" class="windowFrame">
	    	<div id="windowSettingsAccount">
                <div style="padding: 6px;" id="">
                    <!-- Update account information -->
                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_vcard" style="margin: 6px;"><?=lang('settings_update_account_contact_info'); ?></h3>
                    </div>
                    <div class="hide" style="margin-bottom: 12px; padding-left: 24px;">
                        <form>
                        <input type="hidden" name="accountID" id="accountID" value="<?=$accountInformation['AccountID']; ?>" />
                        <p>
                            <label class="required"><?=lang('common_your_name_form'); ?></label>
                            <input tabindex="1" type="text" style="width: 195px;" name="accountNameFirst" id="accountNameFirst" value="<?=$accountInformation['NameFirst']; ?>" />
                            <input tabindex="2" type="text" style="width: 195px;" name="accountNameLast" id="accountNameLast" value="<?=$accountInformation['NameLast']; ?>" />
                        </p>
                        <p>
                            <label><?=lang('common_company_name'); ?></label>
                            <input tabindex="3" type="text" style="width: 400px;" name="accountCompanyName" id="accountCompanyName" value="<?=$accountInformation['Company']; ?>" />
                        </p>
                        <p>
                            <label><?=lang('common_address_1'); ?></label>
                            <input tabindex="4" type="text" style="width: 400px;" name="accountAddress1" id="accountAddress1" value="<?=$accountInformation['Address1']; ?>" />
                        </p>
                        <p>
                            <label><?=lang('common_address_2'); ?></label>
                            <input tabindex="5" type="text" style="width: 400px;" name="accountAddress2" id="accountAddress2" value="<?=$accountInformation['Address2']; ?>" />
                        </p>
                        <p>
                            <label><?=lang('common_city'); ?></label>
                            <input tabindex="6" type="text" style="width: 400px;" name="accountCity" id="accountCity" value="<?=$accountInformation['City']; ?>" />
                        </p>
                        <p>
                            <label><?=lang('common_state'); ?></label>
                            <input type="text" name="accountState" id="accountState" value="<?=$accountInformation['State']; ?>" tabindex="7" style="width: 175px; margin-right: 10px;" />
                            <span class="inlineLabel"><?=lang('common_zip'); ?></span>
                            <input type="text" value="<?=$accountInformation['Zip']; ?>" name="accountZip" id="accountZip" tabindex="8" style="width: 87px;" />
                        </p>
                        <p>
                            <label><?=lang('common_country'); ?></label>
                            <select name="accountCountry" id="accountCountry" tabindex="9" style="width: 183px; margin-right: 10px;">
                            <option value="0"><?=lang('common_field_select'); ?></option>
                            <? $this->load->view('includes/country.html'); ?>
                            </select>
                            <input type="hidden" id="countryHolder" value="<?=$accountInformation['Country']; ?>" />

                            <span class="inlineLabel"><?=lang('common_currency'); ?></span>
                            <select name="accountCurrency" id="accountCurrency" tabindex="10" class="accountCurrency" style="width: 137px;">
                                <option value="0"><?=lang('common_field_select'); ?></option>
                                <?
                                $currencies = currencies();
                                foreach($currencies as $key => $value) {
                                    echo '<option value="'.$key.'"';
                                    if ($key == $accountInformation['Currency']) {
                                        echo ' selected="selected" ';
                                    }
                                    if (empty($accountInformation['Currency']) && $key == 'USD') {
                                        echo ' selected="selected" ';
                                    }
                                    echo '>'.$value[0].'</option>';
                                }
                                ?>
                            </select>
                            <input type="hidden" id="currencyHolder" value="<?=$accountInformation['Currency']; ?>" />
                        </p>
                        <p>
                            <label><?=lang('common_phone'); ?></label>
                            <input tabindex="11" type="text" name="accountPhoneOffice" id="accountPhoneOffice" value="<?=$accountInformation['PhoneOffice']; ?>" style="width: 400px;" />
                        </p>
                        <p>
                            <label class="required"><?=lang('common_email'); ?></label>
                            <input tabindex="13" type="text" name="accountEmail" id="accountEmail" value="<?=$accountInformation['Email']; ?>" style="width: 400px;" />
                        </p>
                        <div class="buttonContainer">
                            <button class="buttonExpand blueGray primary" id="buttonAccountSave" tabindex="20"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator save"><?=lang('button_save'); ?></span></span></button>
                            <div style="float: left; margin-left: 12px;" id="messageAccountInfo"></div>
                            <div style="clear: left;"></div>
                        </div>
                        </form>
                    </div>

                    <!-- Update place or CC number -->
                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_briefcase" style="margin: 6px;"><?=lang('plan_update_plan_or_cc'); ?></h3>
                    </div>
                    <div class="hide" style="margin-bottom: 12px; padding-left: 24px;">
                        <h4 class="icon_briefcase" style="margin-bottom: 12px;">Your account plan: <?=$accountName; ?></h4>
                        <?=$accountDesc; ?>

                        <button class="buttonExpand blueGray" id="buttonUpdateAccount" style="margin: 12px 12px 0 0;"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator accountUpdate"><?=lang('plan_update_plan'); ?></span></span></button>
                        <div id="changeCCContainer">
                            <button class="buttonExpand green" id="buttonUpdateCCInfo" style="margin-top: 12px;"><span class="buttonOuterSpan green"><span class="buttonDecorator creditCardEdit"><?=lang('plan_update_cc'); ?></span></span></button>
                        </div>
                        <div style="clear: left;"></div>
                    </div>

                    <!-- Cancel account -->
                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_delete" style="margin: 6px;"><?=lang('settings_cancel_account'); ?></h3>
                    </div>
                    <div class="hide" style="margin-bottom: 12px;">
                        <form class="noBorder" id="accountFormCancel">
                            <div class="warningDiv" style="width: 643px; margin: 12px 0 12px 0;"><?=lang('help_account_cancel'); ?></div>

                            <div style="margin-left: 50px;"><?=lang('help_account_cancel_message'); ?></div>

                            <div style="margin: 6px 0 24px 47px;">
                                <button class="buttonExpand yellow" id="buttonAccountCancel"><span class="buttonOuterSpan yellow"><span class="buttonDecorator delete"><?=lang('settings_cancel_account'); ?></span></span></button>
                                <div style="clear: left;"></div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
			<div id="windowSettingsPersonal" class="ui-tabs-hide">
	    		<div style="padding: 6px;">
                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_vcard" style="margin: 6px;"><?=lang('settings_update_personal_info'); ?></h3>
                    </div>
                    <div class="hide" style="margin-bottom: 12px;">
                        <input type="hidden" id="pID" value="<?=$this->session->userdata('pid'); ?>" />
                        <input type="hidden" id="contactType" value="P" />
                        <? $this->load->view('includes/contactForm'); ?>
                    </div>

                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_password" style="margin: 6px;"><?=lang('settings_change_password'); ?></h3>
                    </div>
                    <div class="hide">
                        <form class="noBorder" id="accountFormPassword">
                            <p class="padBottom12">
                                <label><?=lang('common_current_password'); ?></label>
                                <input type="password" style="width: 150px;" id="currentPassword" name="currentPassword" />
                            </p>
                            <p>
                                <label for="password"><?=lang('common_new_password'); ?></label>
                                <input type="password" style="width: 150px;" id="newPassword" name="newPassword" />
                            </p>
                            <div style="margin: 6px 0 24px 262px;">
                                <button class="buttonExpand blueGray" id="buttonSaveAccountPassword"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator save"><?=lang('button_save'); ?></span></span></button>
                                <div style="float: left; margin-left: 12px;" id="messageSettingsAccountPassword"></div>
                                <div style="clear: left;"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
			<div id="windowSettingsInsurance" class="ui-tabs-hide">
	    		<div class="barYellow bottom">
					<div style="padding: 6px;">
                        <div style="float: right;">
                            <button class="buttonExpand blueGray taskCreate" id="buttonAddPolicyInformation"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator boxAdd"><?=lang('policy_add_policy_info'); ?></span></span></button>
                        </div>
                        <div style="clear: both;"></div>
					</div>
				</div>
                <div id="containerFormPolicy" style="margin-bottom: 12px; padding-left: 24px;" class="hide">
                    <form class="noBorder" id="accountFormPolicy">
                        <input type="hidden" name="policyID" id="policyID" />
                        <input type="hidden" name="action" id="action" value="add" />
                        <p>
                            <label><?=lang('policy_company_name'); ?></label>
                            <input tabindex="1" type="text" style="width: 400px;" name="policyCompanyName" id="policyCompanyName" />
                        </p>
                        <p>
                            <label><?=lang('policy_company_url'); ?></label>
                            <input tabindex="2" type="text" style="width: 400px;" name="policyCompanyURL" id="policyCompanyURL" />
                        </p>
                        <p>
                            <label><?=lang('policy_number'); ?></label>
                            <input tabindex="3" type="text" style="width: 178px;" name="policyNumber" id="policyNumber" />
                            
                            <span class="inlineLabel"><?=lang('policy_purchase_date'); ?></span>
                            <input type="text" class="dateField" tabindex="4" style="width: 100px;" name="policyDate" id="policyDate" />

                        </p>
                        <p>
                            <label><?=lang('policy_agent_name'); ?></label>
                            <input tabindex="5" type="text" style="width: 195px;" name="policyAgenctNameFirst" id="policyAgenctNameFirst" />
                            <input tabindex="6" type="text" style="width: 194px;" name="policyAgenctNameLast" id="policyAgenctNameLast" />
                        </p>
                        <p>
                            <label><?=lang('policy_agent_email'); ?></label>
                            <input tabindex="7" type="text" style="width: 400px;" name="policyAgentEmail" id="policyAgentEmail" />
                        </p>
                        <p>
                            <label><?=lang('policy_agent_phone'); ?></label>
                            <input tabindex="8" type="text" style="width: 400px;" name="policyAgentPhone" id="policyAgentPhone" />
                        </p>
                        <p>
                            <label><?=lang('policy_agent_fax'); ?></label>
                            <input tabindex="9" type="text" style="width: 400px;" name="policyAgentFax" id="policyAgentFax" />
                        </p>
                        <div class="buttonContainer">
                            <button class="buttonExpand blueGray primary" id="buttonSavePolicy" tabindex="10"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator save"><?=lang('button_save'); ?></span></span></button>
                            <button class="buttonExpand yellow" id="buttonCancelPolicy" tabindex="11"><span class="buttonOuterSpan yellow"><span class="buttonDecorator cancel"><?=lang('button_cancel'); ?></span></span></button>
                            <div style="clear: left;"></div>
                        </div>
                    </form>
                </div>
                <div id="containerPolicyInformation" class="activityMessageBig"> <?=lang('common_loading'); ?></div>
			</div>
            <div id="windowSettingsData" class="ui-tabs-hide">
                <div style="padding: 6px;" id="">
                    <!--
                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_export2" style="margin: 6px;"><?=lang('export_ss_data'); ?></h3>
                    </div>
                    <div class="hide padBottom12">
                        <p class="padBottom12"><?=lang('help_ss_export_overview'); ?></p>
                        <button class="buttonExpand blueGray" id="buttonDataExport"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator export"><?=lang('common_data_export'); ?></span></span></button>
                        <div style="clear: both;"></div>
                    </div>
                    -->

                    <div class="boxGradientGray padBottom12 rollPanelTrigger clickable">
                        <h3 class="icon_database_import" style="margin: 6px;"><?=lang('import_from_ss'); ?></h3>
                    </div>
                    <div class="hide">
                        <form class="noBorder" id="accountFormDataImport">
                            <p style="margin-left: 40px;"><?=lang('help_account_import_v10'); ?></p>
                            <p class="padBottom12">
                                <label><?=lang('login_user_name'); ?></label>
                                <input type="text" style="width: 150px;" id="version10UserName" name="version10UserName" />
                            </p>
                            <p>
                                <label><?=lang('login_password'); ?></label>
                                <input type="password" style="width: 150px;" id="version10Password" name="version10Password" />
                            </p>
                            <div class="buttonContainer">
                                <button class="buttonExpand blueGray" style="float: left;" id="buttonDataImport"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator import"><?=lang('common_data_import'); ?></span></span></button>
                                <div style="float: left; margin-left: 12px;" id="messageDataImport"></div>
                                <div style="clear: left;"></div>
                            </div>
                            <div style="margin: 6px 0 6px 266px;" id="importSummary"></div>

                        </form>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
