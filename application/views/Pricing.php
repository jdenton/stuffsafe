<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <div style="width: 615px; float: left;">
        <div class="whiteBoxTop_615"></div>
        <div class="whiteBoxContentContainter_615" style="padding-left: 5px;">
            <h1 style="margin: 0 0 12px 20px;">For only $29 per year you get...</h1>
            <div class="priceTag"></div>
            
            <div style="width: 45%; margin-right: 24px; float: left;">
                <ul class="check">
                    <li><strong>Unlimited</strong> number of properties</li>
                    <li><strong>Unlimited</strong> inventory items per property</li>
                    <li><strong>50 GB</strong> of file and document storage</li>
                    <li>Mobile applications FREE!</li>
                    <li>Invite other users to update or view your inventory</li>
                </ul>
            </div>
            <div style="width: 45%; float: left;">
                <ul class="check">
                    <li>Drag-and-drop multiple file upload</li>
                    <li>Inventory export in PDF or Excel format</li>
                    <li>Insurance policy manager</li>
                    <li>File manager for viewing all your photos and files</li>
                    <li>Inventory reports</li>
                </ul>
            </div>
            <div style="clear: left;"></div>            
        </div>
        <div class="whiteBoxBottom_615"></div>

        <div class="boxYellow" style="margin-top: 24px; padding: 6px 0 0 6px; width: 600px;">
            <div class="iconNoCCRequired bigText" style="margin-top: 7px; padding-top: 6px; float: left;">No credit card required to sign up!</div>
            <a href="/signup/basic" style="float: right;" class="buttonTryItNow" title="Try it now, FREE!"><span>Try it now, FREE!</span></a>
            <div style="clear: both;"></div>        
        </div>
    </div>
    <div style="clear: left;"></div>

    <h1 class="mBottom" style="margin-top: 20px;"><a href="/signup/basic">Get started now with a FREE 15 day trial!</a></h1>
    <p class="text">If you sign up and don't like StuffSafe for any reason, you can cancel your
    account within 15 days and you won't be billed anything.</p>

    <!--<h2 class="brown mBottom">Upgrade/downgrade your plan at any time.</h2>
    <p class="text">You're never locked into a plan that doesn't fit your business. Buy a small plan today and
    upgrade as your business expands and your team or project needs increase.</p>-->

    <h2 class="brown mBottom">Cancel your account at any time.</h2>
    <p class="text">You can cancel your account at any time with one click - no emails to support, no waiting.
        You will receive verification that your account has been cancelled and that your bank account is no longer being billed.
    </p>
    <h2 class="brown mBottom">Payment options.</h2>
    <p class="text">We currently accept Visa, MasterCard, American Express, and PayPal. We accept online payment only so
    we cannot accept a P.O. and we will not invoice you.</p>
    
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>