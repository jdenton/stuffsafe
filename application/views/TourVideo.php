<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <a href="<?=BASE_URL; ?>tour" class="h2link">Back to Tour Screenshots</a>
	<script type="text/javascript">
		// <![CDATA[
		var flashPath = '<?=ASSET_URL; ?>videos/controller.swf';
		var flashvars = {
			csConfigFile: '<?=ASSET_URL; ?>videos/<?=$videoName; ?>_config.xml'
		};
		var params = {
			_cx: 26,
			_cy: 26,
			WMode: 'window',
			Quality: 'high',
			Menu: -1,
			BGColor: '#ffffff',
			SeamlessTabbing: 1
		};
		var attributes = {};
		swfobject.embedSWF(flashPath, 'alternateVideoContent', "900", "500", "9.0.0",'', flashvars, params, attributes);
		// ]]>
	</script>
	<div id="alternateVideoContent">
		<a href="http://www.adobe.com/go/getflashplayer">
			<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
		</a>
	</div>
</div>
<div class="rightPane">
</div>
<? $this->load->view('includes/footerPublic'); ?>
