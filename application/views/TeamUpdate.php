<? $this->load->view('includes/header'); ?>
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
	var contactData = '<?=$teamMemberInfo; ?>';
/* ]]> */
</script>
<div id="pageLeftColumn" pageID="teamUpdate">
	<input type="hidden" id="contactType" value="T" />
    <div style="margin-top: -1px; min-height: 200px;" class="windowFrame">
        <div id="windowViewTeamMembers">
            <div class="barYellow bottom" id="teamMemberControl">
                <div style="margin: 6px; float: left; width: 650px;" class="infoMessageSmall lightText"><?=lang('help_team_add_team_member'); ?></div>
                <div style="padding: 6px; float: right;">
                    <button class="buttonExpand blueGray teamCreate hide" id="buttonNewContact"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator add"><?=lang('team_new_team_member'); ?></span></span></button>
                    <div style="clear: left;"></div>
                </div>
                <div style="clear: both;"></div>
            </div>
            <p id="noContactLinkContainer" style="display: none;"></p>
            <div id="contactsContainer" class="padBottom12" style="padding-left: 9px;"><div class="activityMessageBig" style="margin-top: 9px;"><?=lang('common_loading'); ?></div></div>
            <div id="formContactContainer" style="display: none; padding: 6px;">
                <? $this->load->view('includes/contactForm'); ?>
            </div>
        </div>
    </div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>