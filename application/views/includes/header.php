<?
if (!isset($pageTitle)) {
    $pageMetaTitle = SITE_TITLE;
} else {
    $pageMetaTitle = $pageTitle.' : '.SITE_TITLE;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="<?=$this->session->userdata('language'); ?>">
<head>
	<META http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<title><?=$pageMetaTitle; ?></title>
	<meta name="Description" content="StuffSafe is a simple, fast, and inexpensive online home inventory solution.
          Home owner insurance companies agree that a home asset inventory should be part of your disaster
          or theft recovery plan.">
	<meta name="Keywords" content="home owner insurance, home security, asset protection, disaster recover, home inventory, asset inventory, online home inventory, online inventory, home insurance, disaster recovery planning">
	<meta name="Author" content="UpStart Productions :: Portland OR :: info@UpStart-Productions.com">
	<META NAME="Rating" CONTENT="General">
	<META NAME="ROBOTS" CONTENT="INDEX, NOFOLLOW">
	<META NAME="Distribution" CONTENT="global">
	<!-- Favicon -->
	<link rel="shortcut icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />
	
	<!-- Style Sheets -->
    <?=cachedFile('browserReset.css','media="screen"'); ?>
    <?=cachedFile('alerts.css','media="screen"'); ?>
    <?=cachedFile('buttons.css','media="screen"'); ?>
    <?=cachedFile('colors.css','media="screen"'); ?>
    <?=cachedFile('forms.css','media="screen"'); ?>
    <?=cachedFile('icons.css','media="screen"'); ?>
    <?=cachedFile('icons_mime.css','media="screen"'); ?>
    <?=cachedFile('jquery.ui.css','media="screen"'); ?>
    <?=cachedFile('layout.css','media="screen"'); ?>
    <?=cachedFile('menus.css','media="screen"'); ?>
    <?=cachedFile('stylesMaster.css','media="screen"'); ?>
    <?=cachedFile('plupload.queue.css','media="screen"'); ?>
    <?=cachedFile('jquery.wysiwyg.css','media="screen"'); ?>
    <?=cachedFile('colorbox.css','media="screen"'); ?>

    <? if (isset($charts) && $charts == 1) { ?>
    <?=cachedFile('jquery.jqplot.css','media="screen"'); ?>
    <? } ?>
	
	<!-- Print Style Sheet -->
    <?=cachedFile('print.css','media="print"'); ?>
	
	<!--[if IE]>
        <?=cachedFile('excanvas/excanvas.compiled.js'); ?>
	<![endif]-->
    <!--[if IE 8]>
        <?=cachedFile('stylesIE8.css','media="screen"'); ?>
    <![endif]-->

    <!-- RSS Feeds -->
    <?
    if (isset($rssFeeds) && is_array($rssFeeds)) {
        foreach ($rssFeeds as $feed) {
            echo '<link rel="alternate" type="application/rss+xml" title="'.$feed['Title'].'" href="'.$feed['Link'].'" />'.Chr(10);
        }
    }
    ?>
    <!-- End RSS Feeds -->
</head>

<?
if ($this->session->userdata('logged') == TRUE) {
    $loggedClass = 'logged';
} else {
    $loggedClass = '';
}

if ($this->session->userdata('headerStyle')) {
    $headerStyle = $this->session->userdata('headerStyle');
} else {
    $headerStyle = 'default';
}
?>
<body id="<?=$pageLayoutClass; ?>" class="page<?=$page; ?><?=$loggedClass; ?>">
    <? if ($this->session->userdata('userType') == USER_TYPE_CLIENT) { ?>
    <input type="hidden" name="clientArea" id="clientArea" value="1" />
    <input type="hidden" name="clientID" id="clientID" value="<?=$this->session->userdata('clientID'); ?>" />
    <input type="hidden" name="clientUserid" id="clientUserid" value="<?=$this->session->userdata('userid'); ?>" />
    <? } ?>
	<div id="header" class="noPrint">
        <div class="blackBar">
            <div class="content">
                <? if ($this->session->userdata('logged') == TRUE) { ?>
                    <?
                    if ($this->session->userdata('userType') != USER_TYPE_CLIENT) {
                        $this->load->view('includes/menuMain');
                    }
                    ?>
                    <div id="headerControls">
                        <div style="float: right;">
                            <a href="<?=site_url('places'); ?>" title="<?=lang('place_my_places'); ?>" class="buttonDashboard headerButton"><span><?=lang('place_my_places'); ?></span></a>
                            <a href="<?=site_url('settings'); ?>" title="<?=lang('settings_page_title'); ?>" class="buttonSettings headerButton"><span><?=lang('settings_page_title'); ?></span></a>
                            <!--<a href="#" title="<?=lang('common_language'); ?>" class="buttonLanguage headerButton popUpMenuClick" content="contentLanguage" id="headerButtonLanguage"><span><?=lang('common_language'); ?></span></a>-->
                            <!--<a href="#" title="<?=lang('common_help'); ?>" class="buttonHelp headerButton popUpMenuClick" content="contentHelp" id="headerButtonHelp"><span><?=lang('common_help'); ?></span></a>-->
                            
                            <a href="#" title="<?=lang('bugreport_page_title'); ?>" class="buttonBugs headerButton popUpMenuClick" content="contentBugs" id="headerButtonBugs"><span><?=lang('bugreport_page_title'); ?></span></a>
                            <? if ($this->session->userdata('userType') == USER_TYPE_GOD) { ?>
                            <a href="<?=site_url('adminusers'); ?>" title="StuffSafe Administration" class="buttonAdmin headerButton"><span>StuffSafe Administration</span></a>
                            <? } ?>
                            <a href="<?=site_url('login/logout'); ?>" title="<?=lang('login_logout'); ?>" class="buttonLogout headerButton"><span><?=lang('login_logout'); ?></span></a>
                            <div style="clear: left;"></div>

                            <div id="contentBugs" class="tooltip">
                                <?=lang('bugreport_instructions'); ?>
                                <textarea class="bugReportContent" style="width: 270px; height: 75px;"></textarea>
                                <button class="smallButton buttonSubmitBugReport" style="float: left;"><span class="decorator bug"><?=lang('bugreport_submit'); ?></span></button>
                                <p style="float: left; margin-left: 6px;"></p>
                            </div>
                            <div id="contentHelp" class="tooltip">Help</div>
                            <div id="contentLanguage" class="tooltip">
                            <?
                            $languages = languages();
                            echo '<ul class="language">';

                            foreach($languages as $key => $value) {
                                $activeState = '';
                                if ($key == $this->session->userdata('language')) {
                                    $activeState = 'active';
                                }
                                echo '<li class="'.$activeState.'"><a href="#" id="lang_'.$key.'" class="languageSelect">'.$value.'</a></li>';
                            }
                            echo '</ul>';
                            ?>
                            </div>
                        <div id="headerControlsMessage"></div>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                <? } else { ?>
                    <p style="color: #fff; font-size: 16px; font-weight: bold; padding-top: 6px; text-align: center;">StuffSafe</p>
                <? } ?>
            </div>
        </div>
        <div class="field <?=$headerStyle; ?>">
            <div class="content <?=$headerStyle; ?>">
                <? if ($this->session->userdata('logged') == TRUE) { ?>
                <div class="accountIdentityContainer">
                    <?
                    $userCompany = $this->session->userdata('userCompany');
                    if (!empty($userCompany)) {
                        echo '<a href="'.site_url('places').'"><span>'.$this->session->userdata('userCompany').'</span></a>';
                    } else {
                        echo '<a href="'.BASE_URL.'" id="logoSSTop"></a>';
                    }
                    ?>
                </div>
                <div class="userAvatarContainer">
                    <p><? echo $this->session->userdata('fullName'); ?></p>
                    <?
                    $avatarMember = $this->session->userdata('avatarMember');
                    if (!empty($avatarMember)) { ?>
                        <img class="avatar22 default" src="<?=$this->session->userdata('avatarMember'); ?>" />
                    <? } else { ?>
                        <p class="avatar22 default"></p>
                    <? } ?>
                    <div style="float: left;"></div>
                </div>
                <? } ?>
            </div>
        </div>
        <div class="greenBar">
            <div class="content">
                <div id="pageTitleContainer">
                    <h1 class="pageTitle <?=$pageIconClass; ?>">
                    <?
                    if (isset($pageTitleLink)) {
                        echo '<a href="'.$pageTitleLink.'" title="'.$pageTitle.'">'.$pageTitle.'</a>';
                    } else {
                        echo $pageTitle;
                    }
                    if (!empty($pageSubTitle)) {
                        echo ' / <span class="pageSubTitle">'.$pageSubTitle.'</span>';
                    }
                    ?>
                    </h1>
                </div>
                <div id="pageControls">
                    <div id="pageControlsContainer" style="float: right; margin-right: 6px;">
                        <?
                        if (isset($pageButtons)) {
                            echo renderHeaderButtons($pageButtons);
                        }
                        ?>
                    </div>&nbsp;
                </div>
                <? if ($this->session->userdata('logged') == TRUE) { ?>
                <div id="siteSearchContainer">
                    <form action="<?=site_url('sitesearch'); ?>" method="POST" id="formSiteSearch">
                    <input type="text" class="appSearchBox clearOnFocus" id="appSearch" name="appSearch" value="<?=lang('common_search_stuff'); ?>" />
                    <button class="buttonAppSearch" id="buttonAppSearch"></button>
                    </form>
                </div>
                <? } ?>
            </div>
        </div>
	</div>	
	<div id="mainPageArea">
		<div id="contentArea" class="contentArea">
            <div id="fileViewContainer">
                <div class="barYellow bottom"><div style="padding: 6px;">
                    <button class="smallButton" id="closeFileView" style="float: left;" title="<?=lang('common_back'); ?>"><span class="back"><?=lang('common_back'); ?></span></button>
                    <button class="smallButton" id="downloadFileView" style="float: right;" title="<?=lang('common_download_file'); ?>"><span class="download"><?=lang('common_download_file'); ?></span></button>
                    <div style="clear: both;"></div>
                </div></div>
                <div id="fileViewInfomationContainer"></div>
                <div id="newMessageFile" itemID="" itemType="attachedFile" class="messageContainer"></div>
                <div id="messageListFile" style="margin-top: 6px;"></div>
            </div>