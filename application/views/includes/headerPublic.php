<?
$headerClass = 'home';
if ($child == TRUE) {
    $headerClass = 'child';
}
if (!isset($pageTitle)) {
    $pageTitle = SITE_TITLE;
} else {
    $pageTitle .= ' :: '.SITE_TITLE;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<META http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title><?=$pageTitle; ?></title>
	<meta name="Description" content="StuffSafe is a simple, fast, and inexpensive online home inventory solution. Home owner insurance companies agree that a home asset inventory should be part of your disaster or theft recovery plan.">
	<meta name="Keywords" content="home owner insurance, home security, asset protection, disaster recover, home inventory, asset inventory, online home inventory, online inventory, home insurance, disaster recovery planning">
	<meta name="Author" content="UpStart Productions :: Portland OR :: info@UpStart-Productions.com">
	<META NAME="Rating" CONTENT="General">
	<META NAME="Distribution" CONTENT="global">
	<!-- Favicon -->
    <link rel="icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />

    <!-- Style Sheets -->
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    
    <?=cachedFile('browserReset.css','media="screen"'); ?>
    <?=cachedFile('stylesPublic.css','media="screen"'); ?>
    <?=cachedFile('buttons.css','media="screen"'); ?>
    <?=cachedFile('alerts.css','media="screen"'); ?>
    <?=cachedFile('colorbox-public.css','media="screen"'); ?>
    <?=cachedFile('flexslider.css','media="screen"'); ?>

    <!--[if IE 8]>
        <?=cachedFile('stylesIE8.css','media="screen"'); ?>
    <![endif]-->

    <!-- RSS Feeds -->
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?=BASE_URL; ?>rss/Rss/productBlogFeed" />

    <!-- Print Style Sheet -->
    <?=cachedFile('print.css','media="print"'); ?>

    <style type="text/css">
        #header .headerTextContainer {
            background: url('<?=BASE_URL; ?>images/<?=$headerTextImage; ?>') 0px 27px no-repeat;
        }
    </style>
    <?=cachedFile('swfobject.js'); ?>
</head>
<body>
    <div id="header" class="<?=$headerClass; ?>">
        <div class="blackBar">
            <div class="content">
                <a href="<?=BASE_URL; ?>" class="logoMain" title="StuffSafe : HOME"></a>
                <div class="menuContainer">
                    <? if ($this->session->userdata('logged') == 1) { ?>
                        <div class="headerAvatar default">
                            <?
                            $avatarMember = $this->session->userdata('avatarMember');
                            if (empty($avatarMember)) {
                                $avatarMember = BASE_URL.'images/avatars/stockAvatar22.png';
                            }
                            ?>
                            <a href="/places"><img class="avatar22" style="float: right;" src="<?=$avatarMember; ?>" /></a>
                            <a href="/places">Go to your account</a> or <a href="/login/logout">Log out</a></div>
                            
                        <div style="clear: right;"></div>
                    <? } else { ?>
                        <div class="headerAvatar"><a href="/login">Login</a> or <a href="/pricing">Sign up</a></div>
                        <div style="clear: right;"></div>
                    <? } ?>                   
                    
                    <ul id="menuMain">
                        <li><a href="<?=BASE_URL; ?>" class="buttonMainMenu"><span>Home</span></a></li>
                        <li><a href="/pricing" class="buttonMainMenu"><span>Pricing</span></a></li>                        
                        <li><a href="/tour" class="buttonMainMenu"><span>Tour</span></a></li>
                        <li><a href="/faq" class="buttonMainMenu"><span>Faq</span></a></li>
                        <li><a href="http://blog.<?=SITE_DOMAIN; ?>" class="buttonMainMenu"><span>Blog</span></a></li>
                        <!-- <li><a href="http://forum.creativeprooffice.com" target="_blank" class="buttonMainMenu"><span>Forum</span></a></li> -->
                        <li><a href="/contact" class="buttonMainMenu"><span>Contact</span></a></li>
                    </ul>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
        <? if ($child == TRUE) { ?>
        <!-- Child page header field -->
        <div class="homeFieldBluePublic">
            <div class="content">
                <div class="headerTextContainer"></div>
            </div>
        </div>
        <? } else { ?>
        <!-- Home page header field -->
        <div class="homeFieldBlue">
            <div class="content">
                <div id="homeFieldButtonContainer">
                    <a href="/tour" class="buttonProductTour" title="Product Tour"><span>Product Tour</span></a>
                    <a href="/pricing" class="buttonSignup" title="Pricing and Signup"><span>Pricing and Signup</span></a>
                    <div class="homeClouds"></div>
                </div>

                <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="homeBG homeBG1"></div>
                    </li>
                    <li>
                        <div class="homeBG homeBG3"></div>
                    </li>
                    <li>
                        <div class="homeBG homeBG2"></div>
                    </li>
                </ul>
                </div>
            </div>
        </div>
        <? } ?>
    </div>
    <div id="pageBody">
