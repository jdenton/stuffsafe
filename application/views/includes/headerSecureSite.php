<?
$randomString = random_string();

$headerClass = 'home';
if ($child == TRUE) {
    $headerClass = 'child';
}

if (!isset($pageTitle)) {
    $pageTitle = 'StuffSafe';
} else {
    $pageTitle = $pageTitle.' : StuffSafe';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<META http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title><?=$pageTitle; ?></title>
	<!-- Favicon -->
    <link rel="icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />

    <!-- Style Sheets -->
	<link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/browserReset.css?<?=$randomString; ?>" type="text/css" />
    <link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/stylesPublic.css?<?=$randomString; ?>" type="text/css" />
    <link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/buttons.css?<?=$randomString; ?>" type="text/css" />
    <link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/alerts.css?<?=$randomString; ?>" type="text/css" />

    <!-- Print Style Sheet -->
	<link rel="STYLESHEET" media="print" href="<?=BASE_URL; ?>css/print.css?<?=$randomString; ?>" type="text/css" />

    <style type="text/css">
        #header .headerTextContainer {
            background: url('<?=BASE_URL; ?>images/<?=$headerTextImage; ?>') 0px 27px no-repeat;
        }
    </style>
</head>
<body>
    <div id="header" class="<?=$headerClass; ?>">
        <div class="blackBar">
            <div class="content">
                <a href="<?=BASE_URL; ?>" class="logoCPO" title="StuffSafe : HOME"></a>
                <div class="menuContainer">
                    <div class="headerAvatar default">
                        <a href="<?=INSECURE_URL; ?>places">Go to your account</a> or <a href="<?=INSECURE_URL; ?>login/logout">Log out</a></div>
                    <div style="clear: right;"></div>
                </div>
            </div>
        </div>
        <!-- Child page header field -->
        <div class="homeFieldBluePublic">
            <div class="content">
                <div class="headerTextContainer"></div>
            </div>            
        </div>        
    </div>
    <div id="pageBody">
        <div class="securePadlockHeader"></div>
