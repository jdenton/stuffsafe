<div style="clear: left;"></div> <!-- Clear page left, right panel floats -->    
</div> <!-- End #pageBody -->
<div id="footerBarBlue">
    <div class="content footerClouds">
        <h2 style="float: left; margin: 32px 36px 0 180px;">Free 15 day trial. Sign up now!</h2>
        <a href="/pricing" class="buttonSignup" title="Pricing and Signup" style="margin: 23px 0 0 0;"><span>Pricing and Signup</span></a>
    </div>
</div>
    <div id="footer">
        <div class="content">
            <div style="float: left; width: 25%;">
                <h3 class="blue">Navigation</h3>
                <ul class="arrow1">
                    <li><a href="<?=BASE_URL; ?>">Home</a></li>
                    <li><a href="<?=BASE_URL; ?>pricing">Pricing</a></li>
                    <li><a href="<?=BASE_URL; ?>tour">Tour</a></li>
                    <li><a href="<?=BASE_URL; ?>faq">Faq</a></li>
                    <li><a href="<?=BASE_URL; ?>login">Login</a></li>
                    <li><a href="<?=BASE_URL; ?>contact">Contact Us</a></li>
                </ul>
            </div>
            <div style="float: left; width: 25%;">
                <h3 class="blue">Questions?</h3>
                <ul class="arrow1">
                    <li><a href="<?=BASE_URL; ?>faq">Faq</a></li>
                    <li><a href="http://blog.<?=SITE_DOMAIN; ?>">Product Blog</a></li>
                    <li><a href="<?=BASE_URL; ?>contact">Contact Us</a></li>
                    <li><a href="<?=BASE_URL; ?>refund">Refund Policy</a></li>
                    <li><a href="<?=BASE_URL; ?>terms">Terms of Use</a></li>
                    <li><a href="<?=BASE_URL; ?>privacy">Privacy Policy</a></li>
                </ul>
            </div>
            <div style="float: left; width: 25%;">
                <h3 class="blue">Product Media</h3>
                <a href="https://www.facebook.com/pages/StuffSafe/237332326302446" class="iconFacebookSmall">Facebook</a>
                <a href="http://twitter.com/stuffsafe" class="iconTwitterSmall">Twitter</a>
                <a href="<?=BASE_URL; ?>rss/Rss/productBlogFeed" class="iconRSSSmall">RSS Feeds</a>
                <!--<a href="<?=BASE_URL; ?>podcast" class="iconMediaPlayerSmall">Podcast</a>-->
                <a href="<?=BASE_URL; ?>tour" class="iconTVSmall">Screencasts</a>
            </div>
            <div style="float: left; width: 25%;">
                <h3 class="blue iconHeartSmall" style="min-height: 0px; padding-left: 20px;">Special Thanks</h3>
                <ul class="arrow1">
                    <li><a href="http://www.smashingmagazine.com/2009/06/18/freebie-release-quartz-icon-set/">Quartz Icon Set</a></li>
                    <li><a href="http://www.visualpharm.com/">Visual Pharm Icons</a></li>
                    <li><a href="http://wefunction.com/2008/07/function-free-icon-set/">Function Icon Set</a></li>
                    <li><a href="http://www.pinvoke.com/">Fugue Icon Set</a></li>
                    <li><a href="http://www.woothemes.com/2009/09/woofunction-178-amazing-web-design-icons/">Woo Function Icon Set</a></li>
                </ul>
            </div>
            <div class="mBottom" style="clear: left;"></div>
            <div class="copyright">StuffSafe is an <a href="http://www.UpStart-Productions.com" title="UpStart Productions">UpStart Productions</a> service. &copy; 2003-<?=date('Y'); ?></div>
        </div>
    </div>

    <!-- JQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
    <?=cachedFile('jquery/plugins/jquery.animateToClass.js'); ?>
    <?=cachedFile('jquery/plugins/jquery.commonAppMethods.js'); ?>
    <?=cachedFile('jsPublicSite.js'); ?>
    <?=cachedFile('jquery/plugins/jquery.flexslider-min.js'); ?>
    <?=cachedFile('jquery/plugins/jquery.colorbox.min.js'); ?>
    <?
    if (is_array($jsFileArray)) {
        foreach ($jsFileArray as $jsFile) {
            echo cachedFile($jsFile);
        }
    }
    ?>
    <script type="text/javascript">
        /* <![CDATA[ */
        var siteURL = '<?=BASE_URL; ?>';
        var thisPage  = '<?=$_SERVER['PHP_SELF']; ?>';

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1427307-7']);
        _gaq.push(['_trackPageview']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        /* ]]> */
    </script>

    </body>
    </html>