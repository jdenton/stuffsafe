	<div style="height: 50px;"></div>
	</div> <!-- End contentArea -->
</div>     <!-- End id=mainContent  -->

<? if ($this->session->userdata('logged') == TRUE) {
    $permArray = $this->session->userdata('permissions');
?>
    <!-- Messaging Modal Windows  -->
    <div id="bigModal" class="noPrint">
        <div class="modalHeader bigModalClose" id="bigModalHeader">
            <h3 id="bigModalHeaderTitleContainer" style="float: left;"></h3>
            <div class="widgetControls" style="float: right; margin-top: -1px;">
                <a class="remove" title="<?=lang('common_close'); ?>" href="#" onClick="return false;" style="margin-left: 10px;"></a>
            </div>
            <div style="clear: both;"></div>
        </div>
        <div id="bigModalBody" class="modalBody">
            <div id="trashContent" class="widgetContentContainer scroller"><p class="icon_trash_small" style="font-weight: bold;"><?=lang('common_no_trash'); ?></p></div>
        </div>
    </div>

	<!-- ***** APP-WIDE FILE MANAGER MODAL POPUP ***** -->
	<div id="modalFileUpload" class="noPrint" style="background: #fff; min-width: 600px;" title="<?=lang('files_file_manager'); ?>">
		<input type="hidden" id="fileManagerSelectedFolder" value="0" />

		<div class="barYellow bottom">
			<div style="padding: 6px; min-height: 30px;">
                <div class="icon_folder_open_small" id="fileWindowNavContainer" style="float: left; margin: 6px 0 0 6px;" class="breadCrumb"></div>
                 
				<div style="float: right;">
					<div style="float: left; margin-right: 12px;">
                        <div id="fileAttachConfirm" style="float: left; margin-right: 12px;"></div>
                        <button class="buttonExpand yellow" style="float: right;" id="buttonSiteFileSelect"><span class="buttonOuterSpan yellow"><span class="buttonDecorator attach"><?=lang('files_attach'); ?></span></span></button>
                        <div style="clear: both;"></div>
                    </div>
					<div style="clear: left;"></div>
				</div>
				<div style="clear: both;"></div>
			</div>	
		</div>
		<div style="background: #fff;">
            <div id="fileListContainer" class="fileManagerWindow"></div>
			<div style="clear: both;"></div>
		</div>
	</div>

    <!-- ***** APP-WIDE GOOGLE MAP POPUP ***** -->
    <div id="modalMap" class="noPrint" style="background: #fff;" title="<?=lang('common_place_map'); ?>"></div>
    
    <!-- ***** APP-WIDE DELETE MODAL POPUP ***** -->
    <div id="modalDelete" class="noPrint" style="background: #fff; min-width: 400px;" title="<?=lang('common_delete'); ?>">
        <div style="padding: 6px;" id="deleteContentsContainer"></div>
        <button id="buttonDeleteModalDelete" class="smallButton" style="margin: 0 6px 0 6px; float: left;"><span class="delete"><?=lang('common_delete'); ?></span></button>
        <button id="buttonDeleteModalCancel" class="smallButton" style="margin-right: 6px; float: left;"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
        <div style="clear: left;"></div>
    </div>

    <!-- ***** PDF PRINT PLEASE WAIT ***** -->
    <div id="modalPleaseWait" class="noPrint" style="background: #fff; min-width: 400px;" title="Please wait...">
        <div style="padding: 6px;" id="waitContentsContainer">
            <div class="activityMessageBig">Your PDF will download in a moment...</div>
        </div>
    </div>

    <!-- ***** APP-WIDE ACCOUNT LIMIT MODAL POPUP ***** -->
    <div id="modalAccountLimit" class="noPrint" style="background: #fff; width: 300px;" title="<?=lang('common_upgrade_account'); ?>">
        <h2><?=lang('upgrade_account'); ?></h2>
        <div style="padding: 6px;">
            <div style="display: none;" id="maxPlacesContainer">
                <? printf(lang('help_max_places'),getAccountLevelSetting('locations')); ?>
            </div>
            <div style="display: none;" id="maxTeamMembersContainer">
                <? printf(lang('help_max_team_members'),getAccountLevelSetting('teamMembers')); ?>
            </div>
            <div style="display: none;" id="maxFilesContainer">
                <? printf(lang('help_max_files'),byte_format(getAccountLevelSetting('fileSpace'))); ?>
            </div>

            <div style="margin: 12px 0 12px 0;"><strong><a href="<?=SECURE_URL.'account/changeAccount/'.$this->session->userdata('accountUserid'); ?>"><?=lang('help_upgrade_account_link'); ?></a></strong></div>
            <button id="buttonAccountLimitModalClose" class="smallButton" style="margin-right: 6px; float: left;"><span class="close"><?=lang('button_close'); ?></span></button>
        </div>
    </div>

    <div id="modalWYSIWYGTableContainer">
        <div id="modalWYSIWYGTable" class="hide noPrint" style="padding: 6px; background: #fff; width: 300px;" title="<?=lang('insert_table'); ?>">
            <form class="wysiwyg">
                <table class="layout" style="margin-bottom: 12px;">
                    <tr>
                        <td>Number of columns:</td>
                        <td><input type="text" name="colCount" style="width: 50px;" value="3" /></td>
                    </tr>
                    <tr>
                        <td>Number of rows:</td>
                        <td><input type="text" name="rowCount" style="width: 50px;" value="3" /></td>
                    </tr>
                </table>
                <button class="smallButton submit" style="margin-right: 6px; float: left;"><span class="save"><?=lang('button_save'); ?></span></button>
                <button class="smallButton reset" style="margin-right: 6px; float: left;"><span class="close"><?=lang('button_close'); ?></span></button>
                <div style="clear: left;"></div>
            </form>
        </div>
    </div>

    <div id="modalWYSIWYGImageContainer">
        <div id="modalWYSIWYGImage" class="hide noPrint" style="padding: 6px; background: #fff; width: 300px;" title="<?=lang('insert_image'); ?>">
            <form class="wysiwyg">
                <table class="layout" style="margin-bottom: 12px;">
                    <tr>
                        <td>Image URL:</td>
                        <td><input type="text" name="url" style="width: 250px;" value="http://" /></td>
                    </tr>
                    <tr>
                        <td>Image Title:</td>
                        <td><input type="text" name="imagetitle" style="width: 250px;" value="" /></td>
                    </tr>
                    <tr>
                        <td>Image Description:</td>
                        <td><input type="text" name="description" style="width: 250px;" value="" /></td>
                    </tr>
                </table>
                <button class="smallButton submit" style="margin-right: 6px; float: left;"><span class="save"><?=lang('button_save'); ?></span></button>
                <button class="smallButton reset" style="margin-right: 6px; float: left;"><span class="close"><?=lang('button_close'); ?></span></button>
                <div style="clear: left;"></div>
            </form>
        </div>
    </div>

    <!-- ***** CANCEL ACCOUNT MODAL ***** -->
    <div id="dialogCancelAccount" class="noPrint hide" style="background: #fff;" title="<?=lang('settings_cancel_account'); ?>">
        <div style="padding: 6px;" id="cancelContentsContainer">
            <div class="warningDiv" style="width: 418px; margin: 12px 0 12px 0;"><?=lang('help_account_cancel'); ?></div>
        </div>
        <button id="buttonCancelAccount" class="smallButton" style="margin: 0 6px 0 6px; float: left;"><span class="delete"><?=lang('settings_cancel_account'); ?></span></button>
        <button id="buttonCancel" class="smallButton" style="margin-right: 6px; float: left;"><span class="cancel"><?=lang('button_cancel'); ?></span></button
        <div style="clear: left;"></div>
    </div>

    <!-- ***** FOOTER TOOLBAR ***** -->
	<div id="footerToolbar" class="noPrint">
		<a href="<?=INSECURE_URL; ?>" id="logoStuffSafe" title="StuffSafe Homepage"></a>
        <?
        if ($this->session->userdata('alerts') != FALSE && $_COOKIE['noSSAlerts'] != date('Y-m-d')) {
        ?>
        <div id="ssUpdateBar">
            <? foreach ($this->session->userdata('alerts') as $alert) { ?>
            <div class="scrollerHeadline"><a class="iconAlert" href="<? echo $alert['Link']; ?>" target="_blank"><? echo $alert['Title']; ?></a></div>
            <? } ?>
            <a class="buttonCloseSmall" id="closeFooterAlerts" title="<?=lang('common_close'); ?>" style="float: right; margin: 5px 4px;"></a>

        </div>
        <? } ?>
        
		<div id="pageAlerts">
			<div style="float: left;" id="buttonSiteTrash" class="iconTrashInactive"><span id="siteTrashCount"></span></div>
			<div style="float: left;" id="ajaxIndicatorOff"><div id="ajaxIndicatorOn"></div></div>
			<div style="clear: left;"></div>
		</div>
        <!--
		<div id="footerWidgets" style="margin-right: 36px;">
			<div style="float: left;">
				<div id="footerWidgetBar">
					<ul id="footerWidgetMenu">
                        <li></li>
                    </ul>
				</div>
				<div id="footerWidgetBarEnd"></div>
			</div>
        </div>
        -->
	</div>
<? } else { ?>
	<!-- End Main Page Content :: Begin Footer -->
	<div id="footer">
		<div class="footerContentWrapper">
			
		</div>
	</div>
	<!-- End Footer -->
<? } ?>

<!-- JQuery -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<?
    echo cachedFile('jsGlobals.js');
    echo cachedFile('swfobject.js');
?>

	<!-- Culture specific datejs file -->
	<?
    $langCode = 'en';
	if ($this->session->userdata('language')) {
		$langCode = $this->session->userdata('language');
	}
	?>
    <?=cachedFile('datejs/date-'.$langCode.'.js'); ?>
	<?=cachedFile('accounting.min.js'); ?>

	<!-- JQuery plugins should go here -->
<?
    echo cachedFile('jquery/plugins/jquery.ui-1.8.1.js');
    echo cachedFile('jquery/plugins/jquery.qtip-1.0.0-rc3.js');
    echo cachedFile('jquery/plugins/jquery.ui.notify.js');
    echo cachedFile('jquery/plugins/jquery.elastic.js');
    echo cachedFile('jquery/plugins/jquery.ajaxUpload.js');
    echo cachedFile('jquery/plugins/jquery.cookie.js');
    echo cachedFile('jquery/plugins/jquery.colorbox.js');

    if (isset($charts) && $charts == 1) {
        echo '<!-- jQuery Sparkline Charts -->';
        echo cachedFile('jquery/plugins/jquery.sparkline.js');

        if (isset($jqCharts) && $jqCharts == 1) {
            echo '<!-- jQuery jQPlot Charts and Plugins -->';
            echo cachedFile('jquery/plugins/jqplot/jquery.jqplot.min.js');
            echo cachedFile('jquery/plugins/jqplot/plugins/jqplot.barRenderer.min.js');
            echo cachedFile('jquery/plugins/jqplot/plugins/jqplot.categoryAxisRenderer.min.js');
            echo cachedFile('jquery/plugins/jqplot/plugins/jqplot.pointLabels.min.js');
            echo cachedFile('jquery/plugins/jqplot/plugins/jqplot.highlighter.min.js');
            echo cachedFile('jquery/plugins/jqplot/plugins/jqplot.cursor.min.js');
        }
    }
    
    echo '<!-- PLUpload files -->';
    echo cachedFile('plupload/plupload.full.js');
    echo cachedFile('plupload/jquery.plupload.queue.js');
    echo '<!-- End PLUpload files -->';

    echo cachedFile('jsFileManager.js');
    echo cachedFile('jsCommon.js');
    echo cachedFile('jsTrash.js');
    echo cachedFile('jquery/plugins/jquery.commonAppMethods.js');

    if (isset($wysiwyg) && $wysiwyg == 1) {
        echo cachedFile('jwysiwyg/jquery.wysiwyg.min.js');
    }

    if (isset($wysiwygTinyMCE) && $wysiwygTinyMCE == 1) {
        echo cachedFile('tinyMCE3.3.8/jquery.tinymce.js');
        echo cachedFile('tinyMCE3.3.8/tinyMCEConfig.js');
    }

    if (is_array($jsFileArray)) {
        foreach ($jsFileArray as $jsFile) {
            echo cachedFile($jsFile);
        }
    }
?>


<? if (isset($map) && $map == 1) { ?>
	<!-- Google Maps stuff -->
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?=GOOGLE_MAP_KEY; ?>" type="text/javascript"></script>
	<? echo cachedFile('jquery/plugins/jquery.gmap.js'); ?>
    <!-- End Google Maps stuff -->
<? } ?>

<? $this->load->view('includes/js'); ?>

<!-- Google Analytics -->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1427307-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<iframe name="printFrame" id="printFrame" style="width: 0; height: 0;" frameborder="0"></iframe>
<iframe name="exportFrame" id="exportFrame" style="width: 0; height: 0;" frameborder="0"></iframe>
</body>
</html>