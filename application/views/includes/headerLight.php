<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<META http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title><?=$pageTitle; ?></title>
	<!-- Favicon -->
    <link rel="icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />

    <!-- Style Sheets -->
    <?=cachedFile('browserReset.css','media="screen"'); ?>
    <?=cachedFile('stylesSubdomainLogin.css','media="screen"'); ?>
    <?=cachedFile('buttons.css','media="screen"'); ?>
    <?=cachedFile('icons.css','media="screen"'); ?>
    <?=cachedFile('alerts.css','media="screen"'); ?>    
</head>
<body>
