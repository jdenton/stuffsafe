<?
if (isset($rightColumnComponents)) {
    foreach($rightColumnComponents as $component) {
        $componentArray = explode('_',$component);
        if (sizeof($componentArray) == 2) {
            $component = $componentArray[0];
            $switch = $componentArray[1];
        } else {
            $switch = null;
        }
        if ($component == 'loginBox' && $this->session->userdata('logged') == 1) {
        } else {
            if ($switch != 'nobg') {
                echo '<div class="whiteBoxTop_300"></div>';
                echo '<div class="whiteBoxContentContainter_300">';
            }
            $this->load->view('includes/rightColumnComponentsPublic/'.$component);
            if ($switch != 'nobg') {
                echo '</div><div class="whiteBoxBottom_300"></div>';
            }
            echo '<div class="hSpacer_18"></div>';
        }
    }
}
?>

<a href="http://itunes.apple.com/us/app/stuffsafe/id477460603?mt=8" class="appStoreBig" style="margin-left: 5px;" title="StuffSafe is on the App Store!"></a>
