<?
if (!isset($pageTitle)) {
    $pageTitle = 'StuffSafe';
} else {
    $pageTitle = $pageTitle.' : StuffSafe';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<META http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title><?=$pageTitle; ?></title>
    <!-- Favicon -->
	<link rel="shortcut icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />

    <!-- Style Sheets -->
	<link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/browserReset.css" type="text/css" />
    <link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/stylesPublic.css" type="text/css" />

    <!-- Print Style Sheet -->
	<link rel="STYLESHEET" media="print" href="<?=BASE_URL; ?>css/print.css" type="text/css" />
    </head>
<body>
    <div id="pageBody">