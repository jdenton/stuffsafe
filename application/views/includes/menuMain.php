<?
/*
|---------------------------------------------------------------
| APPLICATION SUB-MENUS
|---------------------------------------------------------------
|
*/
$permArray = $this->session->userdata('permissions');
?>
<div id="menuMain">
    <ul id="navmenu-h">        
        <?
        if ($permArray['placeViewAll'] == 1 || $permArray['placeViewOwn'] == 1 || $permArray['placeCreate'] == 1) {
        ?>
        <li class="headlink">
            <a href="<?=site_url('places'); ?>" class="buttonMainMenu"><span><?=lang('menu_places'); ?></span></a>
        </li>
        <? } ?>
        <li><a href="<?=site_url('filemanager'); ?>" class="buttonMainMenu"><span><?=lang('files_files'); ?></span></a></li>
        <? 
        if ($permArray['teamView'] == 1 || $permArray['teamCreate'] == 1) {
        ?>
        <li><a href="<?=site_url('teamupdate'); ?>" class="buttonMainMenu"><span><?=lang('team_my_team'); ?></span></a></li>
        <? } ?>
</ul>
</div>