<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Families Lead :: Welcome!</title>
        <meta name="description" content="Families Lead">
        <meta name="author" content="Jeff Denton :: The Teaching Research Institute (dentonj@wou.edu)">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="shortcut icon" href="<?php echo BASE_URL; ?>/images/favicon.ico">
        <link rel="apple-touch-icon" href="<?php echo BASE_URL; ?>/images/apple-touch-icon.png">

        <link rel="STYLESHEET" type="text/css" href="<?php echo BASE_URL; ?>/css/browserReset.css">
        <link rel="STYLESHEET" type="text/css" media="screen" href="<?php echo BASE_URL; ?>/css/styles.css">

        <style>
            #photoContainer {
                background: url(<?php echo BASE_URL; ?>/images/pic1.png) no-repeat;
            }
        </style>
    </head>
    <body class="child">
        <div id="header">
            <div id="masthead">
                <div id="topBlackBar">
                    <ul class="horizMenu menuBlackBar">
                        <li><a href="<?php echo BASE_URL; ?>" class="linkWhiteSmall">Home</a></li>
                        <li><a href="<?php echo site_url('contact'); ?>" class="linkWhiteSmall">Contact us</a></li>
                        <li class="last"><a href="<?php echo site_url('about'); ?>" class="linkWhiteSmall">About this site</a></li>
                    </ul>
                </div>
                <div id="blueField">
                    <div id="logoContainer">
                        <div id="logo"></div>
                    </div>
                    <div class="clear"></div>
                    <?php $this->load->view('includes/menuMain'); ?>
                </div>
            </div>
        </div>
        <div id="pageBody">
            <div id="pageBodyContentContainer">
                <div id="pageBodyContent">
