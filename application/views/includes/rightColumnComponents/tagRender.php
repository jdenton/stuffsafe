<div id="tagsContainer">
    <h4 class="icon_tag"><?=lang('common_tags'); ?></h4>
    <div id="tagSelectContainer">
        <ul class="alphaList">
        <?
        $tagArray = $tagRenderArray['Tags'];
        $tagURL   = site_url('sitesearch/searchTags');
        if (count($tagArray) == 0) {
            echo lang('common_no_tags');
        } else {
            foreach($tagArray as $key => $value) {
                echo '<li><p class="letter" style="float: left; text-align: top;">'.$key.'</p>';
                echo '<p class="content" style="float: left;">';
                foreach($tagArray[$key] as $tag) {
                    echo '<a href="'.$tagURL.'/'.$tag['Tag'].'">'.$tag['Tag'].'</a> ';
                }
                echo '</p><p style="clear: left;"></p></li>';
            }
        }
        ?>
        </ul>
    </div>
</div>
