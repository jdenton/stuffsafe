<div id="exportContainer">
    <h4 class="icon_export"><?=lang('common_export_inventory'); ?></h4>
    <div style="margin-left: 18px;">
        <p class="icon_file_pdf_list" style="margin-bottom: 6px;"><a href="#" id="exportPDF" data-exportType="inventory" data-itemID="<?=$placeID; ?>"><?=lang('common_pdf'); ?></a></p>
        <p class="icon_file_xls_list" style="margin-bottom: 6px;"><a href="#" id="exportExcel" data-exportType="inventory" data-itemID="<?=$placeID; ?>"><?=lang('common_excel'); ?></a></p>
        <!--<p class="icon_file_doc_list" style="margin-bottom: 6px;"><a href="#" id="exportWord" data-exportType="inventory" data-itemID="<?=$placeID; ?>"><?=lang('common_word'); ?></a></p>
        <p class="icon_file_csv_list" style="margin-bottom: 6px;"><a href="#" id="exportCSV" data-exportType="inventory" data-itemID="<?=$placeID; ?>"><?=lang('common_csv'); ?></a></p>-->
    </div>
</div>