<ul class="rightColMenu">
    <li>
        <a href="<?=site_url('blog/BlogAdmin'); ?>">StuffSafe Blog</a>
        <ul>
            <li><a href="<?=BASE_URL; ?>blog/blogUpdate">New blog post</a></li>
        </ul>
    </li>
    <li><a href="<?=site_url('adminusers'); ?>">User Manager</a></li>
    <li><a href="<?=site_url('adminusermetrics'); ?>">User Metrics</a></li>
    <li><a href="<?=site_url('bugs'); ?>">Bugs</a></li>
    <li><a href="<?=site_url('admintranslation'); ?>">Translation</a></li>
</ul>
