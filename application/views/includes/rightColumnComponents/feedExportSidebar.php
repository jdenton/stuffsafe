<div id="feedExportContainer" class="hide">
    <h4 class="icon_export" style="margin-bottom: 12px;"><?=lang('common_export_options'); ?></h4>
    <?
    foreach($rssFeeds as $rssFeed) {
        echo '<a href="'.$rssFeed['Link'].'" type="rss" class="linkFeed linkRSS icon_rss_small" style="margin: 3px;">'.$rssFeed['Title'].'</a>';
    }
    foreach($icalFeeds as $icalFeed) {
        echo '<a href="'.$icalFeed['Link'].'" type="ical" class="linkFeed linkIcal icon_ical_small" style="margin: 3px;">'.$icalFeed['Title'].'</a>';
    }
    if ($exports) {
        foreach($exports as $export) {
            echo '<a href="'.$export['Link'].'" id="'.$export['ID'].'" type="'.$export['Type'].'" class="linkExcel icon_excel_small hide" style="margin: 3px;">'.$export['Title'].'</a>';
        }
    }
    ?>
</div>
