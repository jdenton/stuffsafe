<div style="width: 45%; margin-right: 24px; float: left;">
    <ul class="check">
        <li><strong>Unlimited</strong> number of properties</li>
        <li><strong>Unlimited</strong> inventory items per property</li>
        <li><strong>50 GB</strong> of file and document storage</li>
        <li>Mobile applications FREE!</li>
        <li>Invite other users to update or view your inventory</li>
    </ul>
</div>
<div style="width: 45%; float: left;">
    <ul class="check">
        <li>Drag-and-drop multiple file upload</li>
        <li>Inventory export in PDF or Excel format</li>
        <li>Insurance policy manager</li>
        <li>File manager for viewing all your photos and files</li>
        <li>Inventory reports</li>
    </ul>
</div>
<div style="clear: left;"></div>