<div class="whiteBoxContent_300">
    <a href="http://www.mycpohq.com/rss/Rss/productBlogFeed" class="buttonGrabRSSFeed" title="Grab the RSS Feed" style="margin: 6px 0 12px 55px;"></a>

    <h2 class="brown mBottom">Topics</h2>
    <ul class="arrow1 mBottom" style="margin-left: 25px;">
    <?
    foreach($blogNavTopics as $navTopic) {
        echo '<li><a href="'.BASE_URL.'blog/tag/'.$navTopic['TagURL'].'">'.$navTopic['TagName'].'</a> <span class="gray2">[ '.$navTopic['tagCount'].' ]</span></li>';
    }
    ?>
    </ul>

    <h2 class="brown mBottom">Recent Entries</h2>
    <ul class="arrow1 mBottom" style="margin-left: 25px;">
    <?
    foreach($blogRecentEntries as $navEntry) {
        echo '<li><a href="'.BASE_URL.'blog/entry/'.$navEntry['EntryURL'].'">'.$navEntry['Title'].'</a></li>';
    }
    ?>
    </ul>

    <!--<h2 class="brown mBottom">Recent Updates</h2>
    <a href="<?=BASE_URL; ?>updates">See a list of recent bug fixes and feature upgrades.</a>-->
</div>
