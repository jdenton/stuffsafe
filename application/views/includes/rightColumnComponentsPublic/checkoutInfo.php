<div class="whiteBoxContent_300">
    <h2 class="brown" style="margin: 0 0 12px 6px;">Thank you for choosing StuffSafe!</h2>
    <div style="margin-left: 9px; width: 280px;">
        <h3 class="mBottom">How does billing work?</h3>
        <p class="mBottom">Paid plans are free for the first 15 days. After that, your credit card will be billed once per year. You can
        cancel your subscription at any time. We will not collect your credit card information until your
        15 day trial has expired.</p>

        <h3 class="mBottom">Do I need anything special?</h3>
        <p class="mBottom">Just a web browser and an internet connection. We officially support Internet Explorer 8+, Firefox 4+,
        Safari 4+, and Google Chrome. Other browsers may work but are not supported. You'll also need:
        <ul class="mBottom" style="margin-left: 18px;">
            <li>JavaScript enabled</li>
            <li>Cookies enabled</li>
            <li>A monitor resolution of 1024 x 768 or better</li>            
        </ul>        
        </p>

        <h3 class="mBottom">Cancel at any time.</h3>
        <p class="mBottom">You can cancel your StuffSafe plan at any time by simply clicking a button in your
            control panel. If you cancel before the trial period is over, you will
            never be billed for anything.
        </p>

        <h3 class="mBottom">What if I need help?</h3>
        <p class="mBottom">StuffSafe is designed to be easy to use. We'd rather spend time coding new
            features than writing help manuals that never get read. However, we understand that sometimes users
            get stuck. Our <strong><a href="http://blog.stuffsafe.com">blog</a></strong> will usually have answers to most common problems. You can always contact us
            by email at <strong><a href="maito:<?=MAIL_SUPPORT; ?>"><?=MAIL_SUPPORT; ?></a></strong>. You will normally receive a reply within hours.
        </p>
    </div>
</div>