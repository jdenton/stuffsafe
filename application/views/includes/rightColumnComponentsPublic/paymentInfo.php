<div class="whiteBoxContent_300">
    <? if (!isset($page) || $page != 'selfhostedbuy') { ?>
    <h2 class="brown" style="margin: 0 0 12px 6px;">Why am I being asked for my credit card information?</h2>
    <? } ?>
    <div style="margin-left: 9px; width: 280px;">

        <? if ($this->session->userdata('whyPay') == 'newAccount') { ?>
        <h3 class="mBottom">Your 30 day free trial has ended</h3>
        <p class="mBottom">
            Your 30 free trial has ended. Since we don't require
            users to enter a credit card number when they sign up, we ask for payment information
            when the 30 day trial is over.
        </p>
        <? } elseif ($this->session->userdata('whyPay') == 'changeAccount') { ?>
        <h3 class="mBottom">You have changed your account level</h3>
        <p class="mBottom">            
            You have decided to upgrade or downgrade your account. For your security, we do not store your
            credit card information on our servers.
        </p>
        <p class="mBottom">
            So, in order to update your billing information
            with our payment handling service, we need to send them your credit card information again.
        </p>
        <? } elseif ($this->session->userdata('whyPay') == 'paymentError') { ?>
        <h3 class="mBottom">There was a problem with your billing transaction</h3>
        <p class="mBottom">
            We tried to collect your payment for the month but there was an error of some sort. You
            should have received an email message with the subject "StuffSafe Payment Error" that
            describes in more detail what exactly happened.
        </p>
        <p class="mBottom">
            If you have any questions about this, please <a href="<?=INSECURE_URL; ?>/contact">contact us</a>.
        </p>
        <? } ?>

        <h3 class="mBottom">What about security?</h3>
        <p class="mBottom">Your payment information is encrypted as it travels to our payment handling
        service. You should see a little padlock at the bottom of your browser indicating a secure
        connection.</p>

        <p class="mBottom">
            We never store any of your credit card details on our servers.  This means that nobody will
            be able to 'hack' StuffSafe and get your credit card number. We <u>do</u> store your
            billing address for your convenience in case you want to change your account level in the future.
        </p>

        <div id="siteseal" style="float: left; margin: 14px 12px 0 0;">
            <script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=ICbtp8kTIFbULoydJMPyKdSUhQg52FgcjKRioUgHIpnM8IujJsJfChHQ"></script>
        </div>
        <div style="float: left;">
            <a href="https://www.paypal.com/verified/pal=info@upstart-productions.com" title="PayPal Verified" target="_blank"><img src="<?=SECURE_URL; ?>images/paypal_verified.png" alt="PayPal Verified" border="0" /></a>
        </div>
        <div style="clear: left;"></div>
    </div>
</div>