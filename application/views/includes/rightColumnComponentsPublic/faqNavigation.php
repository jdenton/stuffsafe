<div class="whiteBoxContent_300">
    <h2 class="brown mBottom">Creating a Home Inventory</h2>
    <ul class="arrow1 mBottom" style="margin-left: 25px;">
        <li><a href="#1">What is a home inventory and why do I need one?</a></li>
        <li><a href="#2">How do I start making my home inventory?</a></li>
    </ul>

    <h2 class="brown mBottom">Using StuffSafe</h2>
    <ul class="arrow1 mBottom" style="margin-left: 25px;">
        <li><a href="#3">Why should I use an online service like StuffSafe?</a></li>
        <li><a href="#4">How do I get started?</a></li>
        <li><a href="#5">How do I access my inventory if there is an emergency?</a></li>
        <li><a href="#6">Is my inventory really secure online? Can it get deleted?</a></li>
        <li><a href="#7">Can I let other people view my inventory?</a></li>
        <li><a href="#8">What about multiple properties?</a></li>
        <li><a href="#9">If I cancel my StuffSafe account, can I still get my inventory?</a></li>
    </ul>
</div>