<div style="float: left; width: 140px;">
    <ul class="check">
        <li>Perpetual license</li>
        <li>Open source code</li>
        <li>Unlimited users</li>
        <li>Unlimited projects</li>
        <li>Unlimited file storage</li>
        <li>1 year of support &amp; updates</li>
    </ul>
</div>
<div style="float: right; text-align: center;">
    <a href="<?=SECURE_URL; ?>selfhostedbuy/index/selfhosted" class="selfHostedPricePanel" title="Buy a CreativePro Office self-hosted license for $599.00"></a>
</div>
<div style="clear: both; height: 32px;"></div>
