<h2 class="brown" style="margin: 0 0 18px 9px;">What people are saying.</h2>
<div class="customerLogoContainer">
    <div style="margin: 12px 12px 0 12px;">
        <p class="callout">
            StuffSafe seems like a wonderful way to document your stuff in case of theft.<br />
            <img src="<?=BASE_URL; ?>images/logoLifehacker.png" title="Lifehacker" alt="Lifehacker" style="margin-top: 6px;" />
        </p>
        <p class="callout mTop18">
            Taking an inventory of your stuff seems like a huge undertaking, but StuffSafe makes the
            task a lot easier.<br />
            <img src="<?=BASE_URL; ?>images/logoAppScout.png" title="PC Magazine's AppScout" alt="PC Magazine's AppScout" style="margin: 6px 0 12px 0;" />
        </p>
        <p class="callout mTop18">
            Kim Komando Cool Site of the Day.</a><br />
            <a href="http://www.komando.com/coolsites/index.aspx?id=8384" title="Kim Komando Cool Site of the Day">
                <img src="<?=BASE_URL; ?>images/logoKKCoolSiteOfTheDay.png" style="border: none;" />
            </a>
        </p>
    </div>
</div>