<h2 class="brown" style="margin: 0 0 18px 9px;">Who uses CreativePro Office?</h2>
<div class="customerLogoContainer">
    <?
    $a=0;
    foreach($logoArray as $logo) {
        if ($a%2) {
            $column = 'column2';
        } else {
            $column = '';
        }
        echo '<a href="'.$logo['CompanyURL'].'" title="'.$logo['CompanyName'].'" target="_blank" class="'.$column.'"><img src="'.BASE_URL.'images/customerLogos/'.$logo['LogoFilename'].'" alt="'.$logo['CompanyName'].'" /></a>';
        $a++;
    }
    ?>

    <div style="margin: 42px 0 0 6px;">
        <p class="callout">
            We are wanting to switch all our book keeping to CPO from Freshbooks, so we can use CPO for
            everything. Thanks for a great product.<br />
            <strong>John Pack ~ Baja Good Life Club</strong>
        </p>
        <p class="callout mTop18">
            Hi! I've been trying out your software and have to congratulate you. I've tried some of your competitors
            and CPO is by far the most intuitive and easy to use. The price is hard to beat as well.<br />
            <strong>Helgi Mar Hallgrímsson</strong>
        </p>
        <p class="callout mTop18">Hi! Awesome job with this app! Beats all the others I´ve been looking at!<br />
        <strong>Ignasi Perez</strong></p>

        <p class="callout mTop18">I love this software! You guys rock!<br />
        <strong>Buddy Rigotti ~ Big Buddy Media</strong></p>
    </div>
</div>