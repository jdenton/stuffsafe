<div id="formInventoryContainer" class="hide" style="padding: 12px 0 12px 12px;" title="<?=lang('inventory_add_item'); ?>">
    <input type="hidden" id="invAction" value="add" />
    <input type="hidden" id="invID" />
    <input type="hidden" id="originalRoomID" />
    <div style="float: left;">
        <div style="margin-bottom: 12px;"><input type="text" id="invName" value="<?=lang('common_item_name'); ?>" tabindex="1" class="bigText" style="width: 546px;" /></div>

        <div style="float: left; width: 320px; margin-right: 12px;">
            <p><strong><?=lang('common_select_room'); ?></strong><br />
                <input type="text" id="invRoom" tabindex="2" style="width: 312px;" />
                <input type="hidden" id="invRoomID" />
            </p>
            <p><strong><?=lang('common_select_category'); ?></strong><br />
                <select id="invCategory" tabindex="3" style="float: left; width: 278px; margin-right: 6px;">
                <option></option>
                <?
                foreach($categories as $category) {
                    echo '<option value="'.$category['CatID'].'"';
                    echo '>'.$category['MainCat'].'</option>';
                }
                ?>
                </select>
                <div style="float: left; margin: 0;"><button class="smallButton popUpMenuClick" id="buttonCategoryEdit" title="<?=lang('button_edit'); ?>"><span class="edit single"></span></button></div>
                <div style="clear: left;"></div>
            </p>
            <p><strong><?=lang('common_description'); ?></strong><br />
                <textarea id="invDescription" tabindex="4" style="width: 309px;" rows="5" cols="10"></textarea>
            </p>
        </div>
        <div style="float: left; width: 230px;">
            <div style="float: left; margin-right: 6px;">
                <p><strong><?=lang('common_make'); ?></strong><br />
                    <input type="text" tabindex="5" style="width: 100px;" id="invMake" />
                </p>
            </div>
            <div style="float: left;">
                <p><strong><?=lang('common_model'); ?></strong><br />
                    <input type="text" tabindex="6" style="width: 100px;" id="invModel" />
                </p>
            </div>
            <div style="clear: left;"></div>

            <p><strong><?=lang('common_serial_number'); ?></strong><br />
                <input type="text" id="invSN" tabindex="7" style="width: 214px;" />
            </p>

            <div style="float: left; margin-right: 6px;">
                <p><strong><?=lang('common_purchase_date'); ?></strong><br />
                    <input type="text" class="dateField" tabindex="8" style="width: 100px;" id="invDate" />
                </p>
            </div>
            <div style="float: left;">
                <p><strong><?=lang('common_condition'); ?></strong><br />
                    <select id="invCondition" tabindex="9" style="width: 108px;">
                        <option value="excellent"><?=lang('inventory_condition_excellent'); ?></option>
                        <option value="good"><?=lang('inventory_condition_good'); ?></option>
                        <option value="fair"><?=lang('inventory_condition_fair'); ?></option>
                        <option value="poor"><?=lang('inventory_condition_poor'); ?></option>
                    </select>
                </p>
            </div>
            <div style="clear: left;"></div>

            <div style="float: left; margin-right: 6px;">
                <p><strong><?=lang('common_purchase_price'); ?></strong><br />
                    <input type="text" tabindex="10" style="width: 100px; text-align: right;" id="invPricePurchase" />
                </p>
            </div>
            <div style="float: left;">
                <p><strong><?=lang('common_replacement_price'); ?></strong><br />
                    <input type="text" tabindex="11" style="width: 100px; text-align: right;" id="invPriceReplace" />
                </p>
            </div>
            <div style="clear: left;"></div>

            <p><strong><?=lang('common_tags'); ?></strong><br />
                <input type="text" id="invTags" tabindex="12" style="float: left; width: 173px; margin-right: 6px;" />
                <div style="float: left; margin: 0;"><button class="smallButton popUpMenuClick" id="buttonTagEdit" tabindex="13" title="<?=lang('button_edit'); ?>"><span class="edit single"></span></button></div>
                <div style="clear: left;"><span class="subText"><?=lang('help_tag_format'); ?></span></div>
            </p>
            <div style="margin: 12px 0 0 -3px;">
                <button class="buttonExpand blueGray" id="buttonSave" tabindex="20" style="margin-right: 80px;"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator save"><?=lang('button_save'); ?></span></span></button>
                <button class="buttonExpand yellow" id="buttonInventoryFormCancel" tabindex="21"><span class="buttonOuterSpan yellow"><span class="buttonDecorator cancel"><?=lang('button_close'); ?></span></span></button>
                <div style="clear: left;"></div>
                <div id="invMessage"></div>
            </div>
        </div>
        <div style="clear: left;"></div>
    </div>
    <div style="float: left; width: 340px;">
        <button class="smallButton fileUploadButton" id="invFile" tabindex="13" itemID="0" itemType="inventory"><span class="upload"><?=lang('files_attach'); ?></span></button>
        <div id="fileUploadingContainer">
            <div id="dropFilesHere" class="fileUploadBox"><?=lang('common_drop_files'); ?></div>
            <div id="invFile_fileContainer"></div>
        </div>
    </div>
    <div style="clear: both;"></div>
</div>
