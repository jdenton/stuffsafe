<div class="boxYellow" id="boxPlan">
    <div style="float: left; width: 150px;">
    <p>
        <div class="plan <?=$planDataArray['classBasic']; ?> planSelect basic">Basic</div>
        <div class="plan <?=$planDataArray['classPremium']; ?> planSelect premium">Premium</div>
        <div class="plan <?=$planDataArray['classMax']; ?> planSelect max">Max</div>
    </p>
    </div>
    <div style="float: left; margin: 3px 0 0 24px;" id="planDescription"><?=$planDataArray['acctDesc']; ?></div>
    <div style="float: right; margin: 9px 9px 0 0;">
        <? if (!empty($linkSelectNewPlan)) { ?>
        <a href="<?=$linkSelectNewPlan; ?>" id="buttonSelectAnotherPlan" title="Select another plan." class="blueButton" style="margin-right: 18px;"><span>Select another plan</span></a>
        <? } ?>
    </div>
    <div style="clear: left;"></div>
</div>
<div class="hSpacer_12"></div>