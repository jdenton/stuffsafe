<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <h1 class="mBottom">
        <? if ($whyPay == 'newAccount' || $whyPay == 'paymentError') { ?>
        You have just paid for the <?=strtoupper($rate_plan); ?> plan. 
        <? } elseif ($whyPay == 'changeAccount') { ?>
        Your account is changed to the <?=strtoupper($rate_plan); ?> plan.
        <? } ?>
    </h1>

    <p class="mBottom">
        We appreciate your business with StuffSafe. You should receive a confirmation email
        shortly at <strong><?=$this->session->userdata('userEmail'); ?></strong>. Please <strong><a href="/contact">contact us</a></strong> if you have any
        questions with your account or payment.
    </p>

    <? $this->load->view('includes/planDetailBox'); ?>
    <a href="<?=INSECURE_URL; ?>places" id="buttonGoToAccount" class="greenButton" style="margin-right: 18px;"><span>Go to your account.</span></a>

</div>
<div class="rightPane">
</div>
<? $this->load->view('includes/footerPublic'); ?>
