<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="siteSearch">
<?
$searchResultsNumber = 0;
    
$moduleArray = array('Places','Rooms','Items');
foreach($moduleArray as $module) {
    switch ($module) {
        case 'Places':
            $langCode = 'place_properties';
            break;
        case 'Rooms':
            $langCode = 'common_rooms';
            break;
        case 'Items':
            $langCode = 'inventory_items';
            break;
    }
    if (count($searchResults[$module])>0) {
        echo '<div class="barBlue bottom clickable searchModuleHeader"><h2 style="padding: 9px;">'.lang($langCode).' <span class="lightBlue">('.count($searchResults[$module]).')</span></h2></div>';
        echo '<div class="searchModuleContainer">';
        echo '<table class="dataTable" style="background: none;">';
        foreach($searchResults[$module] as $item) {
            echo '<tr class="rowLines" style="border-top: none;">';
            if ($module == 'Places') {
                echo '<td><a href="'.site_url('place/getPlace/'.$item['PlaceID']).'">'.$item['PlaceName'].'</a></td>';
                echo '<td>'.$item['Address'].' '.$item['City'].' '.$item['State'].' '.$item['Zip'].' '.$item['Country'].'</td>';
            } elseif ($module == 'Rooms') {
                echo '<td><a href="'.site_url('place/getPlace/'.$item['RoomID']).'">'.$item['RoomName'].'</a></td>';
                echo '<td>'.$item['PlaceName'].'</td>';
            } elseif ($module == 'Items') {
                echo '<td style="width: 70%"><a href="'.site_url('inventory/showInventoryItem/'.$item['InventoryID']).'">'.$item['ItemName'].'</a></td>';
                echo '<td style="width: 15%;">'.$item['ItemMake'].' '.$item['ItemModel'].'</td>';
                echo '<td style="width: 15%; text-align: right;">'.getCurrencyMark($this->session->userdata('currency')).$item['PriceReplace'].'</td>';
            }
            echo '</tr>';
            $searchResultsNumber++;
        }
        echo '</table>';
        echo '</div>';
    }
}
if ($searchResultsNumber == 0) {
    echo '<div class="infoMessageBig">'.sprintf(lang('help_no_search_results'),$searchTerm).'</div>';
}
?>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>