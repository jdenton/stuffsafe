<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <div class="homePageFeatureBox left"><div class="simple">
        <h2>Simple &amp; fast</h2>
        <p>Update your home inventory with your smartphone, drag-and-drop photos from your desktop
            into our web app. Creating your inventory has never been easier!</p>
    </div></div>
    <div class="homePageFeatureBox"><div class="nosoftware">
        <h2>No software to install</h2>
        <p>StuffSafe keeps your home inventory securely online. There's nothing to install on your computer
        and nothing to get lost if your computer crashes or is destroyed.</p>
    </div></div>

    <div class="homePageFeatureBox left"><div class="security">
        <h2>Data security</h2>
        <p>Your home inventory photos and data are protected by the same secure network used by Amazon.com. Only
        people you add to your account can see your inventory.</p>
    </div></div>
    <div class="homePageFeatureBox"><div class="reports">
        <h2>Reports</h2>
        <p>How much is all your furniture worth? View easy to read charts about your home inventory value by
            category, room, or property.</p>
    </div></div>

    <div class="homePageFeatureBox left"><div class="export">
        <h2>Exports &amp; downloads</h2>
        <p>Need to send your home inventory to your insurance agent? No problem. Just
        export it as a PDF or Excel file and you're ready to go. </p>
    </div></div>
    <div class="homePageFeatureBox"><div class="insurance">
        <h2>Insurance</h2>
        <p>Keep your insurance company and agent contact information close at hand with the
        Insurance Policy manager.</p>
    </div></div>

    <div class="homePageFeatureBox left"><div class="search">
        <h2>Search</h2>
        <p>Locate anything in your home inventory quickly including properties, rooms, and items by using the
            account wide search feature.</p>
    </div></div>
    <div class="homePageFeatureBox"><div class="trash">
        <h2>Recycle bin &amp; restore</h2>
        <p>Never accidentally lose data again! Everything you delete in StuffSafe first goes to the recycle
            bin where it can be easily restored.</p>
    </div></div>

    <div style="clear: both;" class="boxGrey">
        <p>...and a whole lot more.  <a href="tour">Take a tour</a> or <a href="pricing">See our plans and pricing.</a></p>
    </div>
    <div class="contentSpacer"></div>
    
    <div style="width: 50%; float: left;">
        <a href="http://blog.<?=SITE_DOMAIN; ?>" class="h2link brown iconBlogBlock" style="margin: 0 0 12px 6px;">Recent blog entries</a>
        <ul class="arrow1 mBottom" style="margin-left: 25px;">
        <?
        foreach($blogRecentEntries as $navEntry) {
            echo '<li><a href="'.BASE_URL.'blog/entry/'.$navEntry['EntryURL'].'">'.$navEntry['Title'].'</a></li>';
        }
        ?>
        </ul>
    </div>
    <div style="width: 48%; float: left;">
        <a href="http://twitter.com/stuffsafe" target="_blank" class="h2link brown iconTwitterBlock" style="margin: 0 0 12px 6px;">Follow us on Twitter!</a>
        <?
        $a = 0;
        if (is_array($tweets) && count($tweets)>0) {
            foreach($tweets as $tweet) {
                if ($a<4) {
                    echo '<p style="margin: 0 0 12px 6px;"><img src="'.$tweet->user->profile_image_url.'" style="float: left; padding: 0 6px 0 0; width: 27px; height: 27px;" />';
                    $tweet = twitterReplaceHashTags(twitterReplaceMention($tweet->text)).'</p>';
                    echo auto_link($tweet,'url');
                    $a++;
                }
            }
        }
        ?>
    </div>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<div style="clear: both;"></div>

<? $this->load->view('includes/footerPublic'); ?>
