<? $this->load->view('includes/headerPublic'); ?>
<div style="width: 710px;">
    <div class="tourLeft">
        <h2 class="mBottom12">What is StuffSafe?</h2>
        <p>Got about 5 minutes? Get acquainted with StuffSafe and see just how simple
        it can to be create a home or business inventory.</p>
    </div>
    <div class="tourRight">
        <a href="http://www.youtube.com/embed/l9KLpLxrQQ4?hl=en_US&amp;rel=0&amp;wmode=transparent" class="video">
            <img src="<?=SITE_URL; ?>/images/tourImages/tourTourVideoSmall.png" alt="" />
        </a>
    </div>
    <div class="tourClear"></div>

    <div class="tourLeft">
        <h2 class="mBottom12">Property details</h2>
        <p>The properties screen is your starting point within StuffSafe. From here you can add
        rooms and inventory items to each of your properties. </p>
    </div>
    <div class="tourRight">
        <a href="<?=SITE_URL; ?>/images/tourImages/tourPropertyBig.png" title="The properties screen is your starting point within StuffSafe." class="thumbs">
            <img src="<?=SITE_URL; ?>/images/tourImages/tourPropertySmall.png" alt="The properties screen is your starting point within StuffSafe." />
        </a>
    </div>
    <div class="tourClear"></div>

    <div class="tourLeft">
        <h2 class="mBottom12">Drag-n-drop file upload</h2>
        <p>Attaching files to your inventory items for things like documentation, receipts, and product photos is
        a cinch.  Just drag the files from your desktop into the browser and they start uploading immediately.<br />
        <span class="subText">(This feature
        works with HTML5 compliant browsers like Firefox, Safari, and Chrome...but not Internet Explorer...yet)</span></p>
    </div>
    <div class="tourRight">
        <a href="<?=SITE_URL; ?>/images/tourImages/tourUploadBig.png" title="Drag-n-drop file uploads are sweet!" class="thumbs">
            <img src="<?=SITE_URL; ?>/images/tourImages/tourUploadSmall.png" alt="Drag-n-drop file uploads are sweet!" />
        </a>
    </div>
    <div class="tourClear"></div>

    <div class="tourLeft">
        <h2 class="mBottom12">Go mobile</h2>
        <p>Creating your inventory can be as simple as walking around your house with your smart phone and the
            StuffSafe mobile app for iPhone or Android. Take pictures of your items, enter some details for
            each item, and click SAVE! No need to download a bunch of pictures from your camera to your computer
            before making your inventory.</p>
    </div>
    <div class="tourRight">
        <a href="<?=SITE_URL; ?>/images/tourImages/tourPhoneBig.png" title="Use your smart phone to create your inventory." class="thumbs">
            <img src="<?=SITE_URL; ?>/images/tourImages/tourPhoneSmall.png" alt="se your smart phone to create your inventory." />
        </a>
        <a href="http://itunes.apple.com/us/app/stuffsafe/id477460603?mt=8" title="Go to the App Store!" style="margin-left: 5px;" class="appStore"></a>
        <div class="android" style="margin-left: 12px;" title="Android app coming soon."></div>
        <div style="clear: left; height: 12px;"></div>
    </div>
    <div class="tourClear"></div>

    <div class="tourLeft">
        <h2 class="mBottom12">Inventory charts</h2>
        <p>Quickly see how your inventory value is distributed. How much is your furniture or jewelry worth? Find
        out with our inventory category charts.</p>
    </div>
    <div class="tourRight">
        <a href="<?=SITE_URL; ?>/images/tourImages/tourChartsBig.png" title="Quickly see how your inventory value is distributed." class="thumbs">
            <img src="<?=SITE_URL; ?>/images/tourImages/tourChartsSmall.png" alt="Quickly see how your inventory value is distributed." />
        </a>
    </div>
    <div class="tourClear"></div>

    <!--
    <div class="tourLeft">
        <h2 class="mBottom12">Inventory value at a glance</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat metus, pulvinar sed vehicula
            nec, bibendum sed erat. Nunc nec tellus et arcu rutrum vulputate a ut enim. Duis ante ante,
            sodales vitae consequat ac, fringilla a nibh. Praesent in eleifend libero. Nullam luctus
            mauris eget dui gravida tristique.</p>
    </div>
    <div class="tourRight">
        <img src="<?=SITE_URL; ?>/images/tourImages/tourInventoryValueSmall.png" alt="" />
    </div>
    <div class="tourClear"></div>
    -->

    <div class="tourLeft">
        <h2 class="mBottom12">Nice PDF and Excel exports</h2>
        <p>If you ever need to send your insurance agent a copy of your inventory, you can export
        your entire inventory to a PDF file or Excel spreadsheet.  This is a good way to back up your
        inventory should you decide to cancel your account.</p>
    </div>
    <div class="tourRight">
        <a href="<?=SITE_URL; ?>/images/tourImages/tourPDFBig.png" title="Export your entire inventory to a PDF file or Excel spreadsheet." class="thumbs">
            <img src="<?=SITE_URL; ?>/images/tourImages/tourPDFSmall.png" alt="Export your entire inventory to a PDF file or Excel spreadsheet." />
        </a>
    </div>
    <div class="tourClear"></div>

    <div class="tourLeft">
        <h2 class="mBottom12">Search everything</h2>
        <p>Your entire inventory is searchable.  Search by room name, property name, category, tag, or item name. Quickly
        find that one item that needs to be updated or deleted.</p>
    </div>
    <div class="tourRight">
        <a href="<?=SITE_URL; ?>/images/tourImages/tourSearchBig.png" title="Your entire inventory is searchable." class="thumbs">
            <img src="<?=SITE_URL; ?>/images/tourImages/tourSearchSmall.png" alt="Your entire inventory is searchable." />
        </a>
    </div>
    <div class="tourClear"></div>
</div>
<? $this->load->view('includes/footerPublic'); ?>