<? $this->load->view('includes/header'); ?>
<input type="hidden" id="tag" value="<?=$tag; ?>" />
<input type="hidden" id="maxPlaces" value="<?=$maxPlaces; ?>" />

<div id="pageLeftColumn" pageID="viewAllPlaces">
    <div id="tabsPlaces">
		<ul class="ui-tabs-nav">
	        <li class="ui-tabs-selected"><a href="#windowViewPlaces"><span><?=lang('place_my_places'); ?></span></a></li>
	        <li><a href="#windowInventoryReports"><span><?=lang('inventory_reports'); ?></span></a></li>
	    </ul>
	    <div style="margin-top: -1px; min-height: 200px;" class="windowFrame">
	    	<div id="windowViewPlaces">
                <!--
		        <div class="barYellow bottom" id="placeSearchSort"><div style="padding: 6px;">
		        	<div style="float: left; margin-right: 12px;">
			        	<input type="text" style="width: 250px;" class="search" id="sPlaceAutocompleter" />
						<input type="hidden" id="placeID" />
					</div>
					<div style="float: left; margin-right: 12px;">
						<select id="selectPlaceSort" class="sort" style="width: 150px;">
                            <option value="title"><?=lang('project_form_title'); ?></option>
						</select>
					</div>
                    <div style="clear: both;"></div>
		        </div></div>
                -->
		        <div id="placeListContainer" class="activityMessageBig"> <?=lang('common_loading'); ?></div>
			</div>
	    	<div id="windowInventoryReports" class="ui-tabs-hide">
                <div class="barYellow bottom"><div style="padding: 6px;">
                    <div>
                        <select id="selectReportType" style="width: 300px; margin-right: 6px;" class="chart">
                            <option value="repInventoryProperty"><?=lang('reports_inventory_by_property'); ?></option>
                            <option value="repInventoryCategory"><?=lang('reports_inventory_by_category'); ?></option>
                            <option value="repInventoryPurchaseReplacementCategory"><?=lang('reports_inventory_purchase_replacement_category'); ?></option>
                            <option value="repInventoryPurchaseReplacementProperty"><?=lang('reports_inventory_purchase_replacement_property'); ?></option>
                        </select>
                    </div>
                </div></div>
                <div style="margin: 6px;"><div id="chartContainer"></div></div>
                <div style="margin: 18px 6px 6px 6px;"><div id="gridContainer"></div></div>
	   		</div>
		</div>
	</div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>