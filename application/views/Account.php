<? $this->load->view('includes/headerSecureSite'); ?>
<div class="leftPane">
    <div style="width: 615px; float: left;">
        <div class="whiteBoxTop_615"></div>
        <div class="whiteBoxContentContainter_615" style="padding-left: 5px;">
            <table class="pricing">
                <tr>
                    <th class="<?=$class1; ?>">
                        <h2>BASIC</h2>
                        <sup>$</sup><span class="biggerText white"><?=$currentPrices[0]; ?></span> yearly</th>
                    <th class="<?=$class2; ?>">
                        <h2>PREMIUM</h2>
                        <sup>$</sup><span class="biggerText white"><?=$currentPrices[1]; ?></span> monthly</th>
                    <th class="<?=$class3; ?>" style="border-right: none;">
                        <h2>MAX</h2>
                        <sup>$</sup><span class="biggerText white"><?=$currentPrices[2]; ?></span> monthly</th>
                </tr>
                <tr>
                    <td class="<?=$class1; ?>"><span class="bigText white">1</span><br />Location</td>
                    <td class="<?=$class2; ?>"><span class="bigText white">6</span><br />Locations</td>
                    <td class="<?=$class3; ?>" style="border-right: none;"><span class="bigText white">Unlimited</span><br />Locations</td>
                </tr>
                <tr>
                    <td class="<?=$class1; ?>"><span class="bigText white">1 GB</span><br />Storage space</td>
                    <td class="<?=$class2; ?>"><span class="bigText white">10 GB</span><br />Storage space</td>
                    <td class="<?=$class3; ?>" style="border-right: none;"><span class="bigText white">Unlimited</span><br />Storage space</td>
                </tr>
                <tr>
                    <td class="<?=$class1; ?>"><span class="bigText white">3</span><br />User accounts</td>
                    <td class="<?=$class2; ?>"><span class="bigText white">10</span><br />User accounts</td>
                    <td class="<?=$class3; ?>" style="border-right: none;"><span class="bigText white">Unlimited</span><br />User accounts</td>
                </tr>
                <tr style="height: 55px;">
                    <? if ($buttonState1 == 'none') { ?>
                        <td class="green"><h2 class="white" style="margin: 0 0 0 45px;">Your plan</h2></td>
                    <? } else { ?>
                        <td class="black"><a href="<?=SECURE_URL; ?>signup/changeAccount/basic" class="orangeButton" style="margin: 0 0 0 60px;"><span>Select</span></a></td>
                    <? } ?>

                    <? if ($buttonState2 == 'none') { ?>
                        <td class="green"><h2 class="white" style="margin: 0 0 0 45px;">Your plan</h2></td>
                    <? } else { ?>
                        <td class="black"><a href="/signup/changeAccount/premium" class="orangeButton" style="margin: 0 0 0 60px;"><span>Select</span></a></td>
                    <? } ?>

                    <? if ($buttonState3 == 'none') { ?>
                        <td class="green"><h2 class="white" style="margin: 0 0 0 45px;">Your plan</h2></td>
                    <? } else { ?>
                        <td class="black"><a href="/signup/changeAccount/max" class="orangeButton" style="margin: 0 0 0 60px;"><span>Select</span></a></td>
                    <? } ?>
                </tr>
                <tr>
                    <td colspan="3" class="footer">
                        <? $this->load->view('includes/plansInclude'); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="whiteBoxBottom_615"></div>
    </div>
    <div style="clear: left; height: 24px;"></div>
</div>
<div class="rightPane">
</div>
<? $this->load->view('includes/footerSecureSite'); ?>