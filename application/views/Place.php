<? $this->load->view('includes/header'); ?>
<input type="hidden" id="placeID" value="<?=$placeID; ?>" />
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
    var rooms = <?=$rooms; ?>;
    var placeData = <?=$placeInformation; ?>;
/* ]]> */
</script>
<div id="pageLeftColumn" pageID="viewPlace">
    <div id="tabsPlace">
		<ul class="ui-tabs-nav">
	        <li class="ui-tabs-selected"><a href="#windowViewPlace"><span><?=lang('place_property_info'); ?></span></a></li>
	        <li><a href="#windowInventory"><span><?=lang('inventory'); ?></span></a></li>
	    </ul>
	    <div style="margin-top: -1px; min-height: 200px;" class="windowFrame">
	    	<div id="windowViewPlace">
		        <div id="placeInformationContainer" class="activityMessageBig"> <?=lang('common_loading'); ?></div>
			</div>
	    	<div id="windowInventory" class="ui-tabs-hide">
                <div class="barYellow bottom">
					<div style="padding: 6px;">
                        <div style="float: left;"></div>
						<div style="float: right;">
                            <button class="buttonExpand blueGray taskCreate" id="buttonAddInventory"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator boxAdd"><?=lang('inventory_add_item'); ?></span></span></button>
                        </div>
                        <div style="clear: both;"></div>
					</div>
				</div>
				<div id="inventoryInformationContainer">
                    <div class="activityMessageBig"> <?=lang('common_loading'); ?></div>
                </div>
	   		</div>
		</div>
	</div>
</div>
<? $this->load->view('includes/formInventory'); ?>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>