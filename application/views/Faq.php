<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <article>
        <h1>Creating a Home Inventory</h1>
        <a name="1"></a>
        <p><strong>What is a home inventory and why do I need one?</strong></p>
        <p>A home inventory is simply a detailed list of the stuff you own and should be a big part of your theft or
            disaster recovery plan.  Your up-to-date inventory will help you purchase the correct
            amount of home owner insurance protection, get your insurance claim settled faster if you
            suffer property loss or damage and verify losses for your tax returns.
        </p>

        <a name="2"></a>
        <p><strong>How do I start making my home inventory?</strong></[>
        <p>Cataloging all the stuff you own can be a daunting challenge so start with the big ticket items like
            appliances, computers, home electronics, jewelry, furniture and power tools.  Gradually
            work to the less expensive items like books, linens and clothing. For each item, give a
            brief description, date purchased, make, model, serial number (if appropriate) and
            replacement value.  A photo of the item is helpful too.</p>
        <p>Creating your inventory with a software solution like StuffSafe is ideal; however, you can also
            take photos of your stuff and write the details on the back of the prints or create a video
            inventory.</p>

        <a name="3"></a>
        <h1>Using StuffSafe to Create Your Inventory</h1>
        <p><strong>Why should I use an online service like StuffSafe?</strong></p>
        <p>The best home inventory is not worth much if you can&#39;t find it after a fire, flood or other
            disaster.  And keeping your inventory on your home computer is fine until it gets stolen or destroyed.</p>
        <p>StuffSafe keeps your inventory securely online where you or your insurance agent can get quick access.</p>

        <a name="4"></a>
        <p><strong>How do I get started?</strong></p>
        <p>Creating a StuffSafe account is <strong><a href="<?=SITE_URL; ?>/pricing">fast and easy</a></strong>!  We only ask for your email address and
            name to get started - no phone number, address or billing information.  Your account is free
            for 15 days and you can cancel your account at any time.</p>

        <p>Once you create an account, just follow the instructions for adding locations, rooms and
            items to your inventory.</p>

        <a name="5"></a>
        <p><strong>How do I access my inventory if there is an emergency?</strong></p>
        <p>On any computer with an internet connection, you simply log into your StuffSafe account.  Then, you can
            print or download your entire inventory.</p>
        <p>Also, you can let your insurance agent, partner or friend have read-only access to your StuffSafe
            account with separate passwords.  Read-only access means that the user will not be able to add,
            edit or delete anything in your inventory - the user can only view, print or download your inventory.</p>

        <a name="6"></a>
        <p><strong>Is my inventory really secure online? Can it get deleted?</strong></p>
        <p>The StuffSafe website and your inventory data are housed on servers using industry standard firewall,
            encryption and redundant <a href="http://mozy.com">backup</a> security technology.  Your passwords are encrypted with strong encryption
            algorithms. It is highly unlikely that a hacker would discover your data or that your inventory would be
            lost due to a server crash.</p>

        <p>However, we always strongly recommend that you perform a regular backup of your inventory.  This can be
            done by simply clicking the Excel and/or PDF export links in your inventory view.</p>

        <a name="7"></a>
        <p><strong>Can other people view my inventory?</strong></p>
        <p>Yes, you can allow any number of people to have access to your inventory by creating new accounts
            in the Account Profile area.  Accounts can be full access or read only.  A read only account will
            not be able to add, edit or delete anything in your inventory. This option allows you to give access
            to a friend or insurance agent in case of an incident.</p>

        <a name="8"></a>
        <p><strong>I have a home, office, summer cabin and a storage unit. Will my StuffSafe account handle
                all of these places?</strong></p>
        <p>Yes, if you purchase our <strong><a href="<?=SITE_URL; ?>/pricing">Premium or Max</a></strong> plans, you can add multiple properties to your StuffSafe account.</p>

        <a name="9"></a>
        <p><strong>If I cancel my StuffSafe account, can I still get my inventory?</strong></p>
        <p>Yes. Simply click the Excel or PDF export links located in the inventory view for each property to 
            backup your inventory and save it to your computer.</p>
    </article>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>
