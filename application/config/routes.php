<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

if(preg_match("/^([a-zA-Z0-9\-_]+)\.([a-zA-Z0-9\-_]+)\.([a-zA-Z]{2,5})$/",$_SERVER["SERVER_NAME"],$matches)) {
	list($domain,$subdomain,$tld) = $matches;
}

/*
if ($subdomain == 'www') {
	$route['default_controller'] = "appinit";
} elseif ($subdomain == '') {
    header('Location: '.BASE_URL);
} else {
    $route['default_controller'] = "login";
}
*/

if ($subdomain == 'm') {
    $route['default_controller'] = "mobile/mobile";
} elseif (!empty($subdomain) && $subdomain != 'www' && $subdomain != 'dev') {
    $route['default_controller'] = "login/index/0/".urlencode($subdomain);
} else {
    $route['default_controller'] = "appinit";
}
$route['blog'] = "blog";
$route['blog/entry'] = "blog/entry";
$route['blog/entry/(:any)'] = "blog/entry/$1";
$route['blog/archive'] = "blog/archive";
$route['blog/archive/(:any)/(:any)'] = "blog/archive/$1/$2";
$route['blog/tag'] = "blog/tag";
$route['blog/tag/(:any)'] = "blog/tag/$1";

$route['forum'] = "forum/forum";
$route['mobile'] = "mobile/mobile";

$route['404_override']  = 'error/index';

$route['scaffolding_trigger'] = "";
/* End of file routes.php */
/* Location: ./system/application/config/routes.php */