<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('VER_NUM',   '2000');
define('VER_HUMAN', '2.0.0.0');
define('IS_OFFLINE', FALSE);

define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 					'ab');
define('FOPEN_READ_WRITE_CREATE', 				'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 			'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
|---------------------------------------------------------------
| CUSTOM APPLICATION CONSTANTS (CPO)
|---------------------------------------------------------------
|
*/

/*
 * Determing http/https
 */
$http = 'http://';
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 1) {
    $http = 'https://';
}

define('SITE_URL',          $http.$_SERVER['HTTP_HOST']);
define('SITE_DOMAIN',       'stuffsafe.com');
define('BASE_URL',          $http.$_SERVER['HTTP_HOST'].'/');

define('INSECURE_URL',      'http://www.stuffsafe.com/');
define('SECURE_URL',        'https://www.stuffsafe.com/');

define('BASE_JS_URL',       BASE_URL);
define('MAIL_SUPPORT',      'Support@StuffSafe.com');
define('MAIL_NOBODY',       'mailsender@StuffSafe.com');
define('ROOT_DIR',          $_SERVER['DOCUMENT_ROOT']);
define('FONT_DIR',          APPPATH.'libraries/fonts/');
define('ADMIN_CHART_DIR',   ROOT_DIR.'/images/adminCharts/');
define('ADMIN_CHART_PATH',  BASE_URL.'images/adminCharts/');
define('IMAGES_PATH',       BASE_URL.'images/');
define('IMAGES_PATH_EMAIL', INSECURE_URL.'images/emailImages/');
define('CHART_BG_COLOR',    '#FBFDED');
define('CHART_TMP_DIR',     APPPATH.'libraries/pchart/temp/');
define('USER_DIR',          ROOT_DIR.'/user_files/');
define('USER_DIR_REL',      BASE_URL.'user_files/');
define('SITE_TITLE',        'Online Home Inventory Software | StuffSafe.com');
define('EMAIL_SETTINGS_LINK',SITE_URL.'/settings');
define('SITE_CODE',         '4$3dS,<@!dSa');
define('LANGUAGE_FILE_PATH',ROOT_DIR.'/system/language/');
define('SWF_PATH',          BASE_URL.'flash/');
define('EMAIL_SUPPORT_FROM', 'StuffSafe');
define('PAY_PAL_URL',       'https://www.paypal.com/cgi-bin/webscr');

define('SS_BLOG_URL',      'http://blog.stuffsafe.com');
define('SS_BLOG_TITLE',    'StuffSafe Blog');
define('SSV1_URL', 'http://stuffsafe.upstart-productions.com');

define('GOOGLE_SHOPPING_API_KEY', 'AIzaSyA8DsYhbamDGXBJhG1IUhEcbGKGtgg8R2c');

define('REMEMBER_ME_EXPIRE',2592000); // Remember me exipiration (30 days = 2592000)

/*
 ***** PERMISSISONS AND ROLES *****
 */
define("USER_TYPE_OWNER",  1);
define("USER_TYPE_TEAM",   2);
define("USER_TYPE_CLIENT", 3);
define("USER_TYPE_GOD",    100);

define("MAX_LOGO_UPLOAD_SIZE","2048000"); // 2MB in bytes
define("MAX_FILE_UPLOAD_SIZE","512000000"); // 500MB in bytes
define("ACCOUNT_FILE_FOLDER","accountFiles");

/*
 ***** API KEYS AND STUFF ***** 
 */
//define('GOOGLE_MAP_KEY', 'ABQIAAAA-4dpn0xgLICHe7OEQYMdqBSnKir9O_CiB9DBFG40hySkRjZpLhSFDq7h8IYwc6YEsYBWqFpZ6P3s3A'); // stuffsafe.jeffdenton.com
//define('GOOGLE_MAP_KEY', 'ABQIAAAA-4dpn0xgLICHe7OEQYMdqBQ5Bj_HJrS9gAQocU1Ci8bIWYzhCxRHdTRD2-xA-Uoz7iYqwSsvL5wgPQ'); // dev.StuffSafe.com
define('GOOGLE_MAP_KEY', 'ABQIAAAA-4dpn0xgLICHe7OEQYMdqBTOjmwnLt1M6oErbh7jJtMU1fB3LxSys1paWlubJNIZt7Y6TVyFb3wsRg'); // www.StuffSafe.com
define("GOOGLE_OAUTH_CONSUMER_KEY", "www.mycpohq.com");
define("GOOGLE_OAUTH_CONSUMER_SECRET", "TNMzRTjEaSM3tweHgZibiGYa");

define("AWS_ACCESS_KEY", "0TRVYJ15ER5NMW4K1XG2");
define("AWS_SECRET_KEY", "AE7yEtjTvat7r97I7+5i7EHCdjgqNr2/t/2ZGDbW");
define("AWS_BUCKET",     "stuffsafe");
define("AWS_URL_ROOT",   "http://stuffsafe.s3.amazonaws.com");
define("AWS_BUCKET_NAME","stuffsafe");
define("AWS_ACCOUNT_LIMIT_PAID","3000000000");  // <--  3 GB
define("AWS_LINK_EXPIRE","28800");               // 8 hours: Time after which AWS links to files will expire unless user logs in again.

define("BOX_NET_KEY",    "11h169d4jvsqhsi7armevoiprp2sp08m");

define('TWITTER_CONSUMER_KEY', 'DciJb70VrFz1uQs5eb1mg');
define('TWITTER_CONSUMER_SECRET', 'Jz9CRBctvttrFpyisKS0uvgGvh3xzEp63EG4TQ7AEo');

define('DROPBOX_CONSUMER_KEY', 'bcoep12c9m5j9v1');
define('DROPBOX_CONSUMER_SECRET', 'swyml9bctq59q03');

/*
 ***** PAY-PAL STUFF *****
 */
define('PAYPAL_ENDPOINT_LIVE',    'https://api-3t.paypal.com/nvp');
define('PAYPAL_ENDPOINT_SANDBOX', 'https://api-3t.sandbox.paypal.com/nvp');
define('PAYPAL_ENVIRONMENT',      'live');
//define('PAYPAL_ENVIRONMENT',      'sandbox');
define('PAYPAL_API_VERSION',      '51.0');

/*
 * PAYPAL: These are the sandbox credentials.
 */
/*
define('PAYPAL_USER_NAME',        'chiefupstart+paypal_api1.gmail.com');
define('PAYPAL_PASSWORD',         'B2533C3EN6XW3HT8');
define('PAYPAL_SIGNATURE',        'AOlX34MekN0cvudVEEMixvczf0cHAZmy9YEOn1qAP7PBUBnIam3aUvNa');
*/
/*
 * PAYPAL: These are the live credentials.
 */
define('PAYPAL_USER_NAME',        'info_api1.upstart-productions.com');
define('PAYPAL_PASSWORD',         'Q3SSBWPZ56HSKES6');
define('PAYPAL_SIGNATURE',        'AFNQ2LsmzfhzbX9ayaILlRTwBBY7A96wAqqPv.4U5.Hgj1qrMTMyjZiO');

/*
 ***** SOME GENERAL ARRAYS *****
 */
$monthNameArray = array('January','February','March','April','May','June','July','August','September','October','November','December');
$alphaArray = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
$colorCSSArray = array(
						'light-blue'   => '#AFD8F8',
						'med-blue'     => '#0099FF',
						'dark-blue'    => '#1941A5',
						'light-green'  => '#B2DD32',
						'med-green'    => '#669966',
						'green-swatch' => '#8BBA00',
						'dark-green'   => '#006F00',
						'light-purple' => '#CCCCFF',
						'med-purple'   => '#7C7CB4',
						'purple'       => '#A66EDD',
						'dark-purple'  => '#9900FF',
						'light-red'    => '#F984A1',
						'pink'         => '#FF66CC',
						'red-swatch'   => '#FF0000',
						'orange'       => '#F6BD0F',
						'dark-orange'  => '#FF9933',
						'light-brown'  => '#e0ad4d',
						'brown'        => '#BF9A55',
						'dark-brown'   => '#ad7f2a',
						'gold'         => '#CCCC00',
						'light-gray'   => '#DEDDDD',
						'dark-gray'    => '#999999',
						'teal'         => '#0099CC',
						'light-teal'   => '#99FFCC'
					);

$colorChartArray = array(
                    '#0662B0',
					'#E48701',
					'#A5bC4E',
					'#1B95D9',
					'#CACA9E',
					'#6693B0',
					'#F05E27',
					'#86D1E4',
					'#E4F9A0',
					'#FFD512',
					'#75B000',
					'#F984A1',
					'#FF66CC',
					'#FF0000',
					'#F6BD0F',
					'#FF9933',
					'#e0ad4d',
					'#BF9A55',
					'#ad7f2a',
					'#CCCC00',
					'#DEDDDD',
					'#999999',
					'#0099CC',
					'#99FFCC'
					);

/*
 ***** HTML EMAIL STYLE SETTINGS *****
 */
$emailStyleArray = array(
    'H1' => 'style="font-size: 24px; font-family: arial; font-weight: bold;"',
    'H2' => 'style="font-size: 18px; font-family: arial; font-weight: bold;"',
    'H3' => 'style="font-size: 15px; font-family: arial; font-weight: bold;"',
    'P'  => 'style="font-size: 12px; font-family: arial; margin: 0px; padding: 0px;"',
    'SMALL_GRAY' => 'style="font-size: 11px; font-family: arial; color: #CCCCCC;"',
    'BLUE' => 'style="font-size: 12px; font-family: arial; color: #003399;"',
    'BOX_YELLOW' => 'style="border: 3px solid #fcfc80; background: #FFFFD2; padding: 6px;"',
    'BOX_BLUE'   => 'style="border: 3px solid #bddbf9; background: #ebf4fd; padding: 6px;"'
);

/*
 ***** DISALLOWED SUBDOMAINS *****
 */
$disallowedSubdomains = array(
  'blog',
  'forum',
  'dev',
  'code',
  'www',
  'secure',
  'm',
  'mobile',
  'help'
);

/*
 ***** PRODUCT PRICING
 */
$productArray = array(
    'selfhosted' => array(
        'name'        => 'Self-hosted source code',
        'price'       => '599.00',
        'description' => 'StuffSafe self-hosted source code with 1 year of free updates and email support.'
    ),
    'updates'    => array(
        'name'        => '1 year self-hosted update and email support subscription',
        'price'       => '199.00',
        'description' => '1 year subscription: free updates and email suppport for StuffSafe self-hosted source code.'
    )
);


/*
 ***** ROLES: Places-Rooms-Inventory-Settings-Team-Contacts-Trash
 * admin: has all priveledges.
 * user:  can add/edit/delete all items but cannot update account settings.
 * user_view_only: can view all items - cannot add/edit/delete anything.
 */
$rolesArray = array(
    'admin'          => '11111-1111-1111-1-1111-1111-10',
    'user'           => '11111-1111-1111-0-1111-1111-10',
    'user_view_only' => '10000-1000-1000-0-1000-1000-00'

);

define("PERM_ADMIN",      "11111-1111-1111-1-1111-10");

/*
 ***** ACCOUNT LEVEL INFORMATION    
 */
$accountPricePrev = array('30','10','49');
$accountPriceNext = array('30','10','49');
$datePriceChange  = '2011-01-01';

$accountLevelArray = array(
    '0' => array(
        'name'          => 'basic',
        'teamMembers'   => '10000',
        'locations'     => '10000',
        'extraFeatures' => '1',
        'cost'          => '29',
        'term'          => 'Year',
        'fileSpace'     => '53687091200', // <-- 50GB
        'support'       => 'Forum only'
    ),
    '1' => array(
        'name'          => 'premium',
        'teamMembers'   => '10',
        'locations'     => '6',
        'extraFeatures' => '1',
        'cost'          => '10',
        'term'          => 'Month',
        'fileSpace'     => '10737418240', // <-- 10GB
        'support'       => 'Priority email'
    ),
    '2' => array(
        'name'          => 'max',
        'teamMembers'   => '100000',
        'locations'     => '100000',
        'extraFeatures' => '1',
        'cost'          => '49',
        'term'          => 'Month',
        'fileSpace'     => 'unlimited',
        'support'       => 'Priority email'
    )
);
/* End of file constants.php */
/* Location: ./system/application/config/constants.php */