<?
$config = array(
			'placeupdate/savePlace' => array(
				array(
				        'field' => 'placeName',
				        'label' => lang('form_required_place_name'),
				        'rules' => 'trim|required|xss_clean|max_length[200]'
				     )
			),
            'ProjectUpdate/saveProject' => array(
				array(
				        'field' => 'clientID',
				        'label' => lang('project_form_select_client'),
				        'rules' => 'trim|required|xss_clean'
				     ),
				array(
				        'field' => 'projectTitle',
				        'label' => lang('project_form_title'),
				        'rules' => 'trim|required|xss_clean|max_length[200]'
				     )
			),
            'InvoiceUpdate/saveInvoice' => array(
				array(
				        'field' => 'clientID',
				        'label' => lang('project_form_select_client'),
				        'rules' => 'trim|required|xss_clean'
				     ),
                array(
				        'field' => 'invoiceNumber',
				        'label' => lang('finance_invoice_number'),
				        'rules' => 'trim|required|xss_clean|max_length[50]'
				     ),
				array(
				        'field' => 'invoiceTitle',
				        'label' => lang('project_form_title'),
				        'rules' => 'trim|required|xss_clean|max_length[200]'
				     )
			),
            'ExpenseUpdate/saveExpense' => array(
				array(
				        'field' => 'expenseTitle',
				        'label' => lang('form_required_expense_title'),
				        'rules' => 'trim|required|xss_clean'
				     ),
                array(
				        'field' => 'expenseAmount',
				        'label' => lang('form_required_expense_amount'),
				        'rules' => 'trim|required|xss_clean'
				     )
			),
            'SettingsOwners/saveCompanyInformation' => array(
				array(
				        'field' => 'accountCompanyName',
				        'label' => lang('form_required_company_name'),
				        'rules' => 'trim|required|xss_clean|max_length[200]'
				     ),
                array(
				        'field' => 'accountEmail',
				        'label' => lang('form_required_company_email'),
				        'rules' => 'trim|required|xss_clean|max_length[200]'
				     )
			),
            'payment/sendPayment' => array(
                array(
                        'field' => 'firstName',
				        'label' => 'First Name',
				        'rules' => 'trim|required|xss_clean|max_length[200]'
                ),
                array(
                        'field' => 'lastName',
				        'label' => 'Last Name',
				        'rules' => 'trim|required|xss_clean|max_length[200]'
                ),
                array(
                        'field' => 'address',
				        'label' => 'Street Address',
				        'rules' => 'trim|required|xss_clean|max_length[1000]'
                ),
                array(
                        'field' => 'city',
				        'label' => 'City',
				        'rules' => 'trim|required|xss_clean|max_length[300]'
                ),
                array(
                        'field' => 'zip',
				        'label' => 'Zip/Postal',
				        'rules' => 'trim|required|xss_clean|max_length[50]'
                ),
                array(
                        'field' => 'cardtype',
				        'label' => 'Credit Card Type',
				        'rules' => 'trim|required|xss_clean|max_length[30]'
                ),
                array(
                        'field' => 'cardnumber',
                        'label' => 'Credit Card Number',
				        'rules' => 'trim|required|xss_clean|max_length[16]|checkValidCreditCard'
                ),
                array(
                        'field' => 'cvv2number',
                        'label' => 'Credit Card Security Code (CVV2)',
				        'rules' => 'trim|required|xss_clean|max_length[4]|checkValidCVV2Number'
                )
            ),
            'payment/selfHostedSendPayment' => array(
                array(
                        'field' => 'firstName',
				        'label' => 'First Name',
				        'rules' => 'trim|required|xss_clean|max_length[200]'
                ),
                array(
                        'field' => 'lastName',
				        'label' => 'Last Name',
				        'rules' => 'trim|required|xss_clean|max_length[200]'
                ),
                array(
                        'field' => 'address',
				        'label' => 'Street Address',
				        'rules' => 'trim|required|xss_clean|max_length[1000]'
                ),
                array(
                        'field' => 'city',
				        'label' => 'City',
				        'rules' => 'trim|required|xss_clean|max_length[300]'
                ),
                array(
                        'field' => 'zip',
				        'label' => 'Zip/Postal',
				        'rules' => 'trim|required|xss_clean|max_length[50]'
                ),
                array(
				        'field' => 'email',
				        'label' => 'Email Address',
				        'rules' => 'trim|valid_email|xss_clean|max_length[150]'
				     ),
                array(
                        'field' => 'cardtype',
				        'label' => 'Credit Card Type',
				        'rules' => 'trim|required|xss_clean|max_length[30]'
                ),
                array(
                        'field' => 'cardnumber',
                        'label' => 'Credit Card Number',
				        'rules' => 'trim|required|xss_clean|max_length[16]|checkValidCreditCard'
                ),
                array(
                        'field' => 'cvv2number',
                        'label' => 'Credit Card Security Code (CVV2)',
				        'rules' => 'trim|required|xss_clean|max_length[4]|checkValidCVV2Number'
                )
            )
);
?>