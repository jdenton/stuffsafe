<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Terms extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('public_site','',true);
	}

    function _init() {
		$data['child']           = TRUE;
        $data['headerTextImage'] = '';
        $data['pageTitle']       = 'Terms of Use';
        $data['jsFileArray']     = array();
        return $data;
    }

    function index($popup=NULL) {
		$data = Terms::_init();
        $this->load->view('Terms',$data);        
    }

    function popup() {
        $data = Terms::_init();
        $this->load->view('TermsPopup',$data);
    }
}
?>