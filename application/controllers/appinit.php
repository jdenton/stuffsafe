<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Appinit extends CI_Controller {
     function _init() {
        $this->load->model('public_site','',true);
        $this->load->model('Blog_model','',true);
        $this->load->library('twitter');

        $data['child'] = FALSE;
        $data['jsFileArray'] = array();

        /*
         * Get latest tweets
         */
        $data['tweets']   = $this->twitter->call('statuses/user_timeline', array('id' => 'stuffsafe'));

        /*
         * Get latest blog posts
         */
        $data['blogRecentEntries']    = $this->Blog_model->getLatestEntries(7);

        $data['rightColumnComponents'] = array('goodPress');
        $data['headerTextImage']       = '';
        $data['showBlogLatest']        = TRUE;
        $data['jsFileArray']           = array();

        noCache();
        return $data;
	}

	function index() {
        $data = Appinit::_init();
        $this->load->view('AppInit',$data);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
