<?php
class AppContacts extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        
		$this->load->helper('download');
		$this->load->model('contacts','Contacts',true);
		$this->load->model('user_login','UserLogin',true);
        $this->load->model('trash','',true);
        $this->load->model('app_data','AppData',TRUE);
        $this->load->model('team','',TRUE);

		$this->load->library('application/CommonTeam');
        $this->load->library('application/CommonContacts');
		$this->load->library('application/CommonEmail');
        $this->load->library('application/CommonAppData');
	}
	
	function _init($clientID=NULL) {
		
	}
	
	function makeVcardForm() {
		$this->load->view('includes/vCardForm');
	}
	
	function makeVcard() {
		$vCardParams = $_POST['vCardParams'];
		parse_str($vCardParams,$vCardParam);
		$fn      = $vCardParam['fn'];
		$title   = $vCardParam['title'];
		$org     = $vCardParam['org'];
		$address = $vCardParam['address'];
		$city    = $vCardParam['city'];
		$region  = $vCardParam['region'];
		$zip     = $vCardParam['zip'];
		$country = $vCardParam['country'];
		$email   = $vCardParam['email'];
		$url     = $vCardParam['url'];
		$phone   = $vCardParam['phone'];

		$fnArray = explode(' ',$fn);
		if (isset($fnArray[1])) {
			$fullName = $fnArray[1].';'.$fnArray[0];
		} else {
			$fullName = $fnArray[0];
		}

		$emailArray = explode('|',$email);
		$phoneArray = explode('||',$phone);
		
		$data  = 'BEGIN:VCARD'.Chr(10);
		$data .= 'VERSION:3.0'.Chr(10);
		$data .= 'N:'.$fullName.Chr(10);
		$data .= 'FN:'.$fn.Chr(10);
		$data .= 'ORG:'.$org.Chr(10);
		$data .= 'TITLE:'.$title.Chr(10);
		foreach($phoneArray as $phone) {
			if (!empty($phone)) {
				$itemArray = explode('|',$phone);
				$itemType = 'WORK';
				if (isset($itemArray[1])) {
					$itemType = strtoupper($itemArray[1]);
				}
				if ($itemType == 'MOBILE') {
					$itemType = 'CELL';
				}
				$data .= 'TEL;TYPE='.$itemType.',VOICE:'.$itemArray[0].Chr(10);
			}
		}
		if (isset($emailArray[0])) {
			$data .= 'EMAIL;TYPE=PREF,INTERNET:'.$emailArray[0].Chr(10);
		}
		$data .= 'ADR;TYPE=WORK:;;'.$address.';'.$city.';'.$region.';'.$zip.';'.$country.Chr(10);
		$data .= 'END:VCARD';
		$filename = cleanString($fn).'.vcf';
		
		force_download($filename, $data); 		
	}

    function saveContact() {
        global $rolesArray;

        $contactArray['accountUserid']  = $this->session->userdata('accountUserid');
		$contactArray['itemID']         = $_POST['itemID'];
        $contactArray['action']         = $_POST['action'];
        $contactArray['contactID']      = $_POST['contactID'];
        $contactArray['UID']            = $_POST['contactUID'];
        $contactArray['itemType']       = $_POST['contactType'];
		$contactArray['teamMember']     = $_POST['teamMember'];
        $contactArray['vendor']         = $_POST['vendor'];
        $contactArray['contact']        = $_POST['contact'];
        $contactArray['nameFirst']      = fieldToDB($this->input->post('NameFirst', TRUE));
	    $contactArray['nameLast']       = fieldToDB($this->input->post('NameLast', TRUE));
	    $contactArray['title']          = fieldToDB($this->input->post('ContactTitle', TRUE));
	    $contactArray['company']        = fieldToDB($this->input->post('Company', TRUE));
	    $contactArray['address']        = fieldToDB($this->input->post('Address', TRUE), TRUE,TRUE);
        $contactArray['city']           = fieldToDB($this->input->post('City', TRUE));
	    $contactArray['state']          = fieldToDB($this->input->post('State', TRUE));
	    $contactArray['zip']            = fieldToDB($this->input->post('Zip', TRUE));
	    $contactArray['country']        = $_POST['Country'];
        $contactArray['timezone']       = $_POST['Timezone'];
		$contactArray['language']       = $_POST['Language'];
        $contactArray['phoneData']      = fieldToDB($this->input->post('contactPhoneData', TRUE));
        $contactArray['emailData']      = fieldToDB($this->input->post('contactEmailData', TRUE));
        $contactArray['urlData']        = fieldToDB($this->input->post('contactURLData', TRUE));
        $contactArray['imData']         = fieldToDB($this->input->post('contactIMData', TRUE));
		$contactArray['avatarFilename'] = fieldToDB($this->input->post('avatarFilename', TRUE),TRUE,FALSE,TRUE);
        $contactArray['peopleTags']     = fieldToDB($this->input->post('Tags', TRUE));
		$contactArray['roleID']         = $_POST['roleID'];

        /*
         * Save tags
         */
        if (!empty($contactArray['peopleTags'])) {
            $this->commonappdata->saveTags($contactArray['peopleTags'],'people');
        }

        /*
         * Shave trailing ||
         */
        $contactArray['phoneData'] = substr($contactArray['phoneData'],0,-2);
        $contactArray['emailData'] = substr($contactArray['emailData'],0,-2);
        $contactArray['urlData']   = substr($contactArray['urlData'],0,-2);
        $contactArray['imData']    = substr($contactArray['imData'],0,-2);

        $contactArray['phone'] = array();
		if (!empty($contactArray['phoneData'])) {
            $dataArray = explode('||',$contactArray['phoneData']);
            $a=0;
            foreach($dataArray as $data) {
                $subDataArray = explode('|',$data);
                if (!empty($subDataArray[0])) {
                    $contactArray['phone']['data'][$a] = $subDataArray[0];
                    $contactArray['phone']['type'][$a] = $subDataArray[1];
                    $a++;
                }
            }
        }

		$firstEmail = '';
        $contactArray['email'] = array();
        if (!empty($contactArray['emailData'])) {
            $dataArray = explode('||',$contactArray['emailData']);
            $a=0;
            foreach($dataArray as $data) {
                $subDataArray = explode('|',$data);
                if (!empty($subDataArray[0])) {
                    $contactArray['email']['data'][$a] = $subDataArray[0];
                    $contactArray['email']['type'][$a] = $subDataArray[1];

					if (empty($firstEmail)) {
						$firstEmail = $subDataArray[0];
					}
                    $a++;
                }
            }
        }

        $contactArray['url'] = array();
        if (!empty($contactArray['urlData'])) {
            $dataArray = explode('||',$contactArray['urlData']);
            $a=0;
            foreach($dataArray as $data) {
                $subDataArray = explode('|',$data);
                if (!empty($subDataArray[0])) {
                    $contactArray['url']['data'][$a] = prep_url($subDataArray[0]);
                    $contactArray['url']['type'][$a] = $subDataArray[1];
                    $a++;
                }
            }
        }

        $contactArray['im'] = array();
        if (!empty($contactArray['imData'])) {
            $dataArray = explode('||',$contactArray['imData']);
            $a=0;
            foreach($dataArray as $data) {
                $subDataArray = explode('|',$data);
                if (!empty($subDataArray[0])) {
                    $contactArray['im']['data'][$a] = $subDataArray[0].'|'.$subDataArray[1];
                    $contactArray['im']['type'][$a] = $subDataArray[2];
                    $a++;
                }
            }
        }

		/*
		 * Are we saving a team member?
		 * If so, enter their login information and dashboard widgets first.
		 */
		if ($contactArray['teamMember'] == 1) {
            $loginArray['action']        = $contactArray['action'];
            $loginArray['UID']           = $contactArray['UID'];
			$loginArray['Userid']        = $firstEmail;
            $password = random_string();
			$loginArray['Password']      = sha1($password.SITE_CODE);
			$loginArray['RoleID']        = $contactArray['roleID'];
			$loginArray['UserType']      = USER_TYPE_TEAM;
			$loginArray['UseridAccount'] = $this->session->userdata('accountUserid');
            $loginArray['AuthString']    = random_string('alnum',20);

			$loginUID = $this->UserLogin->saveLoginInformation($loginArray);
            if ($contactArray['action'] == 'add') {
                /*
                 * Create authString and update
                 */
                $authString              = sha1($loginUID.SITE_CODE);
                $authArray['UID']        = $loginUID;
                $authArray['action']     = 'updateAuthString';
                $authArray['authString'] = $authString;
                $this->UserLogin->saveLoginInformation($authArray);

                
            }
		} else {
			$loginUID = 0;
		}
		$contactArray['loginUID'] = $loginUID;

        if (
            empty($contactArray['nameFirst']) &&
            empty($contactArray['nameLast'])  &&
            empty($contactArray['title'])     &&
            empty($contactArray['company'])
        )  {
            $newContactID = 'error';
        } else {
			$newContactID = $this->Contacts->saveContactPerson($contactArray);
            if ($contactArray['teamMember'] == 1 && $contactArray['action'] == 'add') {
                /*
                 * Send email to team member with user name and password information.
                 */
                $messageArray = $this->commonemail->createSystemMessage('newTeamMember',$contactArray['language']);
                $message = $messageArray['message'];
                $subject = $messageArray['subject'];
                
                $roleConfig = $_POST['roleID'];
                $roleName = array_search($roleConfig, $rolesArray);
                $messageIntro = sprintf($message,$this->session->userdata('fullName'),lang('role_'.$roleName),$firstEmail,$password);
                $footer = $this->commonemail->createSystemMessage('messageFooter',$contactArray['language']);

                $finalMessage = DBToField($messageIntro.Chr(10).Chr(10).$footer['message'],TRUE);                
                $this->commonemail->sendEmail($finalMessage,$loginUID,'team',$subject,'');
            }
        }
        return $newContactID;
    }

    function getContactsTest() {
        $contactArray = $this->Contacts->getContactsTest($this->session->userdata('accountUserid'));
        echo '{"contacts":'.DBToField(json_encode($contactArray)).'}';
    }

    function getContacts($itemType=NULL,$itemID=NULL,$teamMember=FALSE,$vendor=FALSE,$renderType='array',$return=0) {
        if ($renderType == 'array') {
			return $this->commoncontacts->getContacts($this->session->userdata('accountUserid'),$itemType,$itemID,$teamMember,$vendor,'json',1);
		} elseif ($renderType == 'json') {
            $contactJSON = $this->commoncontacts->getContacts($this->session->userdata('accountUserid'),$itemType,$itemID,$teamMember,$vendor,'json',1);
            if ($return == 1) {
                return $contactJSON;
            } else {
                echo $contactJSON;
            }
		}
    }

    /*
     * This method returns a limited data set for mobile apps.
     */
    function getContactsLimited($itemType=NULL,$itemID=NULL,$teamMember=FALSE,$vendor=FALSE,$renderType='array',$return=0) {
        $contactArray = $this->Contacts->getContactsLimited($this->session->userdata('accountUserid'),$itemType,$itemID,$teamMember,$vendor);

        if ($renderType == 'array') {
			return $contactArray;
		} elseif ($renderType == 'json') {
            $contactJSON = json_encode($contactArray);
            if ($return == 1) {
                return $contactJSON;
            } else {
                echo $contactJSON;
            }
		}
    }

    function getContact($personID,$renderType='json',$return=0,$edit=0) {
        $contactArray = $this->Contacts->getContactPerson($personID,NULL);
        if ($edit == 1) {
            $contactArray['Address'] = DBToField($contactArray['Address'],TRUE);
        }
        if ($renderType == 'json') {
            if ($return == 1) {
                return json_encode($contactArray);
            } else {
                echo json_encode($contactArray);
            }
        } elseif ($renderType == 'array') {
            return $contactArray;
        } elseif ($renderType == 'html') {
            if (empty($contactArray['AvatarURL'])) {
                $avatarURL = BASE_URL.'images/avatars/stockAvatar46.png';
            } else {
                $avatarURL = $contactArray['AvatarURL'];
            }


            $htmlOut  = '<div class="vcard" itemID="'.$contactArray['PID'].'" id="contactHolder'.$contactArray['PID'].'">';
            $htmlOut .= '<p><img src="'.$avatarURL.'" class="avatar46" style="padding: 0 6px 6px 0; float: left;" /><span class="fn"><strong>'.$contactArray['NameFirst'].' '.$contactArray['NameLast'].'</strong></span></p>';
            $htmlOut .= '<p style="min-height: 40px; margin: 0 0 6px 52px;">';
            if (!empty($contactArray['Title'])) {
                $htmlOut .= '<span class="title">'.$contactArray['Title'].'</span>';
            }
            if (!empty($contactArray['Title']) && !empty($contactArray['Company'])) {
                $htmlOut .= ', ';
            }
            if (!empty($contactArray['Company'])) {
                $htmlOut .= '<span class="org">'.$contactArray['Company'].'</span>';
            }
            $roleName = '';
            if (isset($contactArray['RoleName']) && !empty($contactArray['RoleName']) && $contactArray['RoleName'] != '0') {
                $roleName .= ', '.$contactArray['RoleName'];
            }

            if ($contactArray['AccountCreator'] == 1) {
                $htmlOut .= '<span class="subText icon_badge_gold">'.lang('common_account_creator').$roleName.'</span>';
            } elseif ($contactArray['TeamMember'] == 1) {
                $htmlOut .= '<span class="subText icon_badge_blue">'.lang('team_member').$roleName.'</span>';
            } elseif ($contactArray['Vendor'] == 1) {
                $htmlOut .= '<span class="subText icon_badge_green">'.lang('common_vendor').'</span>';
            }
            $htmlOut .= '</p>';

            foreach($contactArray['ContactItems'] as $contactItem) {
                if ($contactItem['ContactType'] == 'phone') {
                    $vCardType = 'tel';
                } else {
                    $vCardType = $contactItem['ContactType'];
                }
                $htmlOut .= '<p class="icon_'.$contactItem['ContactType'].' contactItem"><span class="'.$vCardType.'" location="'.$contactItem['ContactLocation'].'">';
                
                if ($contactItem['ContactType'] == 'email') {
                    $htmlOut .= '<a href="mailto:'.$contactItem['ContactData'].'">'.$contactItem['ContactData'].'</a>';
                } elseif ($contactItem['ContactType'] == 'url') {
                    $htmlOut .= '<a href="'.$contactItem['ContactData'].'">'.$contactItem['ContactData'].'</a>';
                } elseif ($contactItem['ContactType'] == 'im') {
                    $imArray = explode('|',$contactItem['ContactData']);
                    $imUserName = $imArray[0];
                    $imProtocol = $imArray[1];
                    $htmlOut .= $imUserName.' ['.$imProtocol.']';
                } else {
                    $htmlOut .= $contactItem['ContactData'];
                }
                $htmlOut .= '</span> <span class="subText">'.$contactItem['ContactLocation'].'</span></p>';
                $htmlOut .= '</p>';
            }

            $htmlOut .= '</div>';
            
            header("Cache-Control: no-cache, must-revalidate");
            echo $htmlOut;
        }
    }

    function deleteContact($personID,$tagForDelete=TRUE) {
        /*
         * First get some information about this contact
         */
        $personArray = $this->Contacts->getContactPerson($personID);
        $this->Contacts->deleteContact($personID,$tagForDelete,$this->session->userdata('userid'));
        if ($personArray['TeamMember'] == 1) {
            $loginUserid = $personArray['LoginUserid'];
            $this->UserLogin->deleteLoginInformation($loginUserid,$tagForDelete);}
        $this->commontrash->countItemsInTrash();
    }

    function restoreContact($personID) {
        /*
         * First get some information about this contact
         */
        $personArray = $this->Contacts->getContactPerson($personID);
        $this->Contacts->restoreContact($personID,$tagForDelete,$this->session->userdata('userid'));
        if ($personArray['TeamMember'] == 1) {
            $loginUserid = $personArray['LoginUserid'];
            $this->UserLogin->restoreLoginInformation($loginUserid);

            /*
             * Rebuild team member JSON string and put into session
             */
            $sessionData = array('teamArray'=>$this->commoncontacts->buildTeamMemberArray($this->session->userdata('accountUserid')));
            $this->session->set_userdata($sessionData);
        }
        $this->commontrash->countItemsInTrash();
    }

    function searchContacts() {
		$q = cleanStringTag(strtolower($this->input->post('q', TRUE)));
        $clients  = $_POST['clients'];
        $contacts = $_POST['contacts'];
        $team     = $_POST['team'];
        $vendors  = $_POST['vendors'];
		if (!$q) return;
		$contactList = $this->Contacts->getContactsForAutoCompleter($q,$this->session->userdata('accountUserid'),$clients,$contacts,$team,$vendors);
		echo $contactList;
	}

    function associatePersonWithItem($contactID,$itemID,$itemType) {
        /*
         * Are we associating a team member with a project?
         * If so, send team member an email message.
         */
        if ($itemType == 'P') {
            $contactArray = $this->Contacts->getContactPerson($contactID);

            /*
             * Get some project information.
             */
            $projectArray = $this->project_view->getProjectInformation($itemID);
            if ($contactArray['TeamMember'] == 1) {
                $messageType = 'assignToProject';
                $messageTypeEmail = 'emailReceiveProject';
                $teamMemberUID      = $contactArray['LoginUserid'];
                $teamMemberLanguage = $contactArray['Language'];

                $messageArray = $this->commonemail->createSystemMessage($messageType,$teamMemberLanguage);
                $subject = DBToField(sprintf($messageArray['subject'],$projectArray['Title']));
                $message = sprintf($messageArray['message'],$this->session->userdata('userCompany'),$projectArray['Title']);

                $footer = $this->commonemail->createSystemMessage('messageFooter',$teamMemberLanguage);
                $finalMessage = DBToField($message.Chr(10).Chr(10).$footer['message'],TRUE);
                $this->commonemail->sendEmail($finalMessage,$teamMemberUID,'team',$subject,$messageTypeEmail);
            }
        }
        $this->Contacts->associatePersonWithItem($contactID,$itemID,$itemType);
    }

    function removePersonFromItem($contactID,$itemID,$itemType) {
        $this->Contacts->removePersonFromItem($contactID,$itemID,$itemType);
    }
}	
?>