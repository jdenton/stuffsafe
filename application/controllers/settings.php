<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        
		$this->load->model('settings_update','',true);
        $this->load->model('user_login','',true);
        $this->load->model('contacts','',true);
        $this->load->model('accounts','',true);
        $this->load->model('public_site','',true);
        $this->load->model('data_import','',true);

        $this->load->library('application/CommonFileManager');
		$this->load->library('application/FileManagerS3');
        $this->load->helper('dataImport');
        $this->load->helper('payment');
	}

	function _init($status=NULL,$forWho=NULL) {
        global $rolesArray;

		$data['page']              = 'settings';
		$data['pageTitle']         = lang('settings_page_title');
		$data['wysiwyg']           = 0;
		$data['map']               = 0;
        $data['validate']          = 1;
		$data['jsFileArray']       = array('jsSettings.js','jsContacts.js');
		$data['pageIconClass']     = 'iconPageSettings';
		$data['pageLayoutClass']   = 'withRightColumn';
        $data['rolesPermissions']  = $rolesArray;

        if ($forWho == 'client') {
            $data['rightColumnComponents'] = array();
        } else {
            $data['rightColumnComponents'] = array();
        }
		
        /*
         * Try to get account logo thumbnail
         */
        $logoSettingLoResArray = $this->settings_update->getSettings($this->session->userdata('accountUserid'),'logoLowRes');
        $data['logoThumbFileLoRes'] = $logoSettingLoResArray['SettingValue'];

        $logoSettingHiResArray = $this->settings_update->getSettings($this->session->userdata('accountUserid'),'logoHiResThumb');
        $data['logoThumbFileHiRes'] = $logoSettingHiResArray['SettingValue'];
        $data['accountInformation'] = $this->accounts->getAccountInformation($this->session->userdata('accountUserid'));

        /*
         * Get account level email settings
         */
        $emailSettingsArray   = $this->settings_update->getSettings($this->session->userdata('accountUserid'),NULL,NULL,'email');
        $emailArray = array();

        if ($emailSettingsArray != FALSE) {
            foreach($emailSettingsArray as $setting) {
                $emailArray[$setting['Setting']] = $setting['SettingValue'];
            }
        }

        /*
         * Get user level email settings
         */
        $emailSettingsArray   = $this->settings_update->getSettings($this->session->userdata('userid'),NULL,NULL,'email');
        
        if ($emailSettingsArray != FALSE) {
            foreach($emailSettingsArray as $setting) {
                $emailArray[$setting['Setting']] = $setting['SettingValue'];
            }
        }

        $data['emailArray'] = $emailArray;

        $accountSettingsArray = $this->settings_update->getSettings($this->session->userdata('accountUserid'),NULL,NULL,'account');
        $accountArray = array();

        if ($accountSettingsArray != FALSE) {
            foreach($accountSettingsArray as $setting) {
                $accountArray[$setting['Setting']] = $setting['SettingValue'];
            }
        }
        $data['accountArray'] = $accountArray;

		$success = NULL;
		$statusMessage = '';
		if (isset($status)) {
			switch ($status) {
				case 'success':
					$success = '1';
					$statusMessage = lang('settings_success');
					break;
                case 'webAddressDisallowed':
					$success = '0';
					$statusMessage = 'You cannot use this web address. Please choose another.';
					break;
                case 'webAddressInUse':
					$success = '0';
					$statusMessage = 'Your web address in already in use. Please choose another.';
					break;
                case 'webAddressBadChars':
					$success = '0';
					$statusMessage = 'Your web address contains bad characters.';
					break;
			}
		}
		$data['success']       = $success;
		$data['statusMessage'] = $statusMessage;

        /*
         * Check for existing ImportGUID
         */
        $importGUIDArray = $this->data_import->checkForImportGUID($this->session->userdata('accountUserid'));
        if ($importGUIDArray != FALSE) {
            $importGUID = $importGUIDArray[0]['ImportGUID'];
            $this->session->set_userdata('importGUID',$importGUID);
        }

        /*
         * Account plan information
         */
        global $accountLevelArray;
        $accountDesc = array();
        foreach ($accountLevelArray as $account) {
            if ($account['fileSpace'] == 0) {
                $storageSpace = 'None';
            } else {
                $storageSpace = byte_format($account['fileSpace']);
            }

            if ($account['cost'] == 0) {
                $costPer = 'Free!';
            } else {
                //$costPerMonth = '$'.$account['cost'].'.00';
                $costPer = '$'.$account['cost'].'.00 '.$account['term'];
            }

            if ($account['locations'] == 100000) {
                $account['locations'] = 'Unlimited';
            }
            if ($account['teamMembers'] == 100000) {
                $account['teamMembers'] = 'Unlimited';
            }

            $accountDescTxt  = '<ul class="check">';
            $accountDescTxt .= '<li>'.lang('plan_cost').': <strong>'.$costPer.'</strong></li>';
            $accountDescTxt .= '<li>'.lang('plan_locations').': <strong>'.$account['locations'].'</strong></li>';
            $accountDescTxt .= '<li>'.lang('plan_team').': <strong>'.$account['teamMembers'].'</strong></li>';
            $accountDescTxt .= '<li>'.lang('plan_storage').': <strong>'.$storageSpace.'</strong></li>';
            $accountDescTxt .= '</ul>';
            $accountDesc[$account['name']] = $accountDescTxt;
        }
        $data['accountDescriptions'] = $accountDesc;

        switch ($this->session->userdata('accountLevel')) {
            case 0:
                $data['accountDesc'] = $data['accountDescriptions']['basic'];
                $data['accountName'] = 'Basic';
                break;
            case 1:
                $data['accountDesc'] = $data['accountDescriptions']['premium'];
                $data['accountName'] = 'Premium';
                break;
            case 2:
                $data['accountDesc'] = $data['accountDescriptions']['max'];
                $data['accountName'] = 'Max';
                break;
        }

        $messageArray = array(
						'accountCompanyName' => lang('form_required_company_name'),
						'accountEmail'       => lang('form_required_company_email'),
						'accountEmail'       => array(
												'required' => lang('form_required_company_email'),
												'minlength'=> lang('form_minlength_clientEmail')
												)
						);
        $data['validationJSON'] = json_encode($messageArray);
        $data['permissions'] = $this->session->userdata('permissions');

        noCache();
		return $data;
	}

	function index($status=NULL) {
		$data = Settings::_init($status);
		$this->load->view('Settings',$data);
	}

    function getSettings($setting=NULL,$group=NULL,$section=NULL,$account=1,$renderType='json',$return=0) {
        if ($account == 1) {
            $userid = $this->session->userdata('accountUserid');
        } else {
            $userid = $this->session->userdata('userid');
        }
        $settingArray = $this->settings_update->getSettings($userid,$setting,$group,$section);

        if ($renderType == 'json') {
            if ($return == 1) {
                return json_encode($settingArray);
            } else {
                echo json_encode($settingArray);
            }
        } else {
            return $settingArray;
        }
    }

	function saveSettings() {
		$settingsArray['value'] = fieldToDB($this->input->post('settingValue', TRUE));

        $typeArray = explode('_',$_POST['settingType']);
        $settingsArray['type']      = $typeArray[0];
		$settingsArray['group']     = $typeArray[1];
        $settingsArray['section']   = $typeArray[2];
        if (isset($typeArray[3])) {
            $settingsArray['value'] = $settingsArray['value'].'_'.$typeArray[3];
        }
        $this->settings_update->saveSettings($this->session->userdata('accountUserid'),$this->session->userdata('userid'),$settingsArray);
	}

    function saveSettingsMultiple() {
        foreach ($_POST as $key => $value) {
            $settingsArray['value'] = fieldToDB($this->input->post($key, TRUE));
            $typeArray = explode('_',$key);
            $settingsArray['type']      = $typeArray[0];
            $settingsArray['group']     = $typeArray[1];
            $settingsArray['section']   = $typeArray[2];
            if (isset($typeArray[3])) {
                $settingsArray['value'] = $settingsArray['value'].'_'.$typeArray[3];
            }
            $this->settings_update->saveSettings($this->session->userdata('accountUserid'),$this->session->userdata('userid'),$settingsArray);
        }
    }

    function saveNewPassword($renderType='json',$return=0) {
        $currentPassword = $this->input->post('currentPassword', TRUE);
        $newPassword     = $this->input->post('newPassword', TRUE);
        
        $error = FALSE;
        $status = 'success';
        $hashPass        = sha1($newPassword.SITE_CODE);
        $hashPassCurrent = sha1($currentPassword.SITE_CODE);
        if (empty($currentPassword) || empty($newPassword)) {
            $error = TRUE;
            $message = lang('error_password_missing_field');
        }
        if ($hashPassCurrent != $this->session->userdata('password')) {
            $error = TRUE;
            $message = lang('error_password_incorrect');
        }
        
        if ($error == FALSE) {
            /*
             * See if new password is already taken
             */
            $accountInfoExits = $this->user_login->tryLogin($this->session->userdata('userName'),$hashPass);
            if ($accountInfoExits != FALSE) {
                /*
                 * Then there is an account with this user name and password already.
                 */
                $error = TRUE;
                $message = lang('error_account_exists');                
            }
        }

        if ($error == FALSE) {
            /*
             * Save the new password and add to session
             */
            $this->user_login->changePassword($this->session->userdata('userid'),$hashPass);
            $message = lang('success_password_saved');
            $this->session->set_userdata('password',$hashPass);
        }
        if ($error == TRUE) {
            $status = 'error';
        }
        if ($return == 1) {
            $returnArray = array($status,$message);
            return $returnArray;
        } else {
            if ($renderType == 'json') {
                echo '{"Status":"'.$status.'","Message":"'.$message.'"}';
            }
        }
    }

    function uploadLogo($resolution) {
        $fileName = $this->commonfilemanager->uploadLocalFile(USER_DIR,$_FILES['Filedata'],MAX_LOGO_UPLOAD_SIZE);        
        /*
         * See if we have an error
         */
        $responseArray = explode('|',$fileName);
        if ($responseArray[0] == 'error') {
            $responseMessage = $responseArray[1];
            echo '{"Result":"error","Message":"'.$responseMessage.'"}';
        } else {
			/*
			 * Get existing logo file and delete it if they exist
			 */
			$accountSettingsArray = $this->settings_update->getSettings($this->session->userdata('accountUserid'),NULL,'account');

            if ($accountSettingsArray != FALSE) {
                foreach($accountSettingsArray as $setting) {
                    if ($setting['Setting'] == 'logoHiRes' && $resolution == 'hi') {
                        $this->commonfilemanager->deleteLocalFile(USER_DIR,$setting['SettingValue']);
                    }
                    if ($setting['Setting'] == 'logoLoRes' && $resolution == 'low') {
                        $this->commonfilemanager->deleteLocalFile(USER_DIR,$setting['SettingValue']);
                    }
                }
            }
            
            if ($resolution == 'hi') {
                /*
                 * Save hi-res logo url in settings
                 */
                $fileURL = AWS_URL_ROOT.'/'.ACCOUNT_FILE_FOLDER.'/'.$fileName;
                $settingsArray = array('type'=>'logoHiRes','group'=>'account','value'=>$fileURL,'section'=>'user');
                $this->settings_update->saveSettings($this->session->userdata('accountUserid'),$this->session->userdata('userid'),$settingsArray);
                /*
                 * Create logo thumbnail for use on the settings page.
                 */
                $logoPath = USER_DIR.$fileName;
                $logoSize = getimagesize($logoPath);
                
                $config['source_image']   = $logoPath;
                $config['create_thumb']   = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['master_dim']     = 'width';
                $config['width']          = 200;
                $config['height']         = 200;

                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $fileNameArray = $this->image_lib->explode_name($fileName);
                $thumbFileName = $fileNameArray['name'].'_thumb'.$fileNameArray['ext'];

                $logoThumbPath = USER_DIR.$thumbFileName;
                $logoThumbURL = AWS_URL_ROOT.'/'.ACCOUNT_FILE_FOLDER.'/'.$thumbFileName;
                $settingsArray = array('type'=>'logoHiResThumb','group'=>'account','value'=>$logoThumbURL,'section'=>'user');
                $this->settings_update->saveSettings($this->session->userdata('accountUserid'),$this->session->userdata('userid'),$settingsArray);

                /*
                 * Move logo files to S3 and delete from local file system
                 */
                $this->filemanagers3->moveFileToS3($logoPath,$fileName,ACCOUNT_FILE_FOLDER);
                $this->commonfilemanager->deleteLocalFile(USER_DIR,$fileName);
                
                $this->filemanagers3->moveFileToS3($logoThumbPath,$thumbFileName,ACCOUNT_FILE_FOLDER);
                $this->commonfilemanager->deleteLocalFile(USER_DIR,$thumbFileName);

                echo '{"Result":"success","Message":"'.$logoThumbURL.'","Width":"'.$logoSizeArray[0].'","Height":"'.$logoSizeArray[1].'"}';
            } elseif ($resolution == 'low') {
                /*
                 * Make sure we're uploading an image with height <= 92px
                 */
                $logoPath = USER_DIR.$fileName;
                $logoSize = getimagesize($logoPath);
                if ($logoSize[1]>92) {
                    $config['source_image']   = $logoPath;
                    $config['create_thumb']   = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['master_dim']     = 'height';
                    $config['height']         = 92;
                    $config['width']          = 400;

                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    $fileNameArray = $this->image_lib->explode_name($fileName);
                    $lowResFileName = $fileNameArray['name'].'_thumb'.$fileNameArray['ext'];
                } else {
                    $lowResFileName = $fileName;
                }
                $lowResLogoPath = USER_DIR.$lowResFileName;

                /*
                 * Save low-res logo url in settings
                 */
                $lowResLogoURL = AWS_URL_ROOT.'/'.ACCOUNT_FILE_FOLDER.'/'.$lowResFileName;
                $settingsArray = array('type'=>'logoLowRes','group'=>'account','value'=>$lowResLogoURL,'section'=>'user');
                $this->settings_update->saveSettings($this->session->userdata('accountUserid'),$this->session->userdata('userid'),$settingsArray);
                $this->session->set_userdata('logoLowRes',$lowResLogoURL);
                /*
                * Move logo files to S3 and delete from local file system
                */
                $this->filemanagers3->moveFileToS3($lowResLogoPath,$lowResFileName,ACCOUNT_FILE_FOLDER);
                $this->commonfilemanager->deleteLocalFile(USER_DIR,$lowResFileName);

                $logoSizeArray = imageResize($this->session->userdata('logoLowRes'),NULL,92,FALSE);
                echo '{"Result":"success","Message":"'.$lowResLogoURL.'","Width":"'.$logoSizeArray[0].'","Height":"'.$logoSizeArray[1].'"}';
            }            
        }
    }

	/*
	 * $type  'account' = the avatar that shows next to the company name in the header
	 *        'member'  = the avatar associated with an individual user or team member
	 */
	function uploadAvatar($type) {
        $fileName = $this->commonfilemanager->uploadLocalFile(USER_DIR,$_FILES['Filedata'],MAX_LOGO_UPLOAD_SIZE);
        /*
         * See if we have an error
         */
        $responseArray = explode('|',$fileName);
        if ($responseArray[0] == 'error') {
            $responseMessage = $responseArray[1];
            echo '{"Result":"error","Message":"'.$responseMessage.'"}';
        } else {
			/*
			 * Get existing avatar file and delete it
			 */
			if ($type == 'account') {
				$userid = $this->session->userdata('accountUserid');
				$settingName = 'avatarAccount';
				$group = 'account';
                $section = 'user';
			} else {
				$userid = $this->session->userdata('userid');
				$settingName = 'avatarMember';
				$group = 'member';
                $section = 'user';
			}
			$accountAvatarArray = $this->settings_update->getSettings($userid,$settingName,NULL);
			$this->commonfilemanager->deleteLocalFile(USER_DIR,$accountAvatarArray['SettingValue']);
			
            /*
             * Resize avatar if necessary
             */
            $avatarPath = USER_DIR.$fileName;
            $avatarSize = getimagesize($avatarPath);
            if ($avatarSize[0]>80) {
                $this->load->library('image_lib');

                $config['source_image']   = $avatarPath;
                $config['create_thumb']   = FALSE;
				$config['thumb_marker']   = '';
                $config['maintain_ratio'] = FALSE;
                $config['master_dim']     = 'auto';
                $config['width']          = 80;
                $config['height']         = 80;
                
                //$this->image_lib->initialize($config);
                $this->load->library('image_lib');
                $this->image_lib->initialize($config);
                $this->image_lib->resizeThenCrop();
            }
			/*
			 * Save avatar url in settings
			 */
			$avatarURL = AWS_URL_ROOT.'/'.ACCOUNT_FILE_FOLDER.'/'.$fileName;
			$settingsArray = array('type'=>$settingName,'group'=>$group,'value'=>$avatarURL,'section'=>$section);
			$this->settings_update->saveSettings($userid,$this->session->userdata('userid'),$settingsArray);

			/*
			 * Save avatar to session variable
			 */
			$sessionData = array($settingName=>$fileName);
			$this->session->set_userdata($sessionData);

			/*
			 * Move avatar file to S3 and delete from local file system
			 */
			$this->filemanagers3->moveFileToS3($avatarPath,$fileName,ACCOUNT_FILE_FOLDER);
			$this->commonfilemanager->deleteLocalFile(USER_DIR,$fileName);
            echo '{"Result":"success","Message":"'.$avatarURL.'"}';
        }
	}

    function saveCompanyInformation() {
        if (isset($_POST['save']) && $_POST['save'] == 1) {
            /*
             * Save data
             * fieldToDB($string,$escape=TRUE,$linebreak=FALSE,$prepURL=FALSE,$autoLink=FALSE,$autoLinkNewWindow=TRUE)
             */
            $settingArray['accountID']          = $_POST['accountID'];
            $settingArray['accountCompanyName'] = fieldToDB($this->input->post('accountCompanyName', TRUE));
            $settingArray['accountAddress1']    = fieldToDB($this->input->post('accountAddress1', TRUE));
            $settingArray['accountAddress2']    = fieldToDB($this->input->post('accountAddress2', TRUE));
            $settingArray['accountCity']        = fieldToDB($this->input->post('accountCity', TRUE));
            $settingArray['accountState']       = fieldToDB($this->input->post('accountState', TRUE));
            $settingArray['accountZip']         = fieldToDB($this->input->post('accountZip', TRUE));
            $settingArray['accountCountry']     = $_POST['accountCountry'];
            $settingArray['accountCurrency']    = $_POST['accountCurrency'];
            $settingArray['accountPhoneOffice'] = fieldToDB($this->input->post('accountPhoneOffice', TRUE));
            $settingArray['accountMobile']      = fieldToDB($this->input->post('accountMobile', TRUE));
            $settingArray['accountFax']         = fieldToDB($this->input->post('accountFax', TRUE));
            $settingArray['accountWebAddress']  = fieldToDB($this->input->post('accountWebAddress', TRUE));
            $settingArray['accountEmail']       = fieldToDB($this->input->post('accountEmail', TRUE));
            $settingArray['accountURL']         = fieldToDB($this->input->post('accountURL', TRUE), TRUE,FALSE,TRUE);
            $settingArray['accountIndustry']    = $_POST['accountIndustry'];

            /*
             * Check web address first.
             */
            $error = false;
            if (!empty($settingArray['accountWebAddress'])) {
                $webAddressStatus = SettingsOwners::checkWebAddress($settingArray['accountWebAddress']);
                if ($webAddressStatus != 'success') {
                    $error = true;
                    header('Location: '.BASE_URL.'settings/SettingsOwners/index/'.$webAddressStatus);
                }
            }

            /*
             * Update currency session var
             */
            if ($error == false) {
                $this->session->unset_userdata('currency');
                $sessionData = array('currency' => $settingArray['accountCurrency']);
                $this->session->set_userdata($sessionData);
                $this->settings_update->saveCompanyInformation($settingArray);
                header('Location: '.BASE_URL.'settings/SettingsOwners/index/success');
            }
        } else {
			header('Location: '.BASE_URL.'settings/SettingsOwners');
		}
    }

    function checkWebAddress($webAddress) {
        global $disallowedSubdomains;

        /*
         * Check that the subdomain is not in our disallow list.
         */
        foreach($disallowedSubdomains as $disSub) {
            if ($webAddress == $disSub) {
                return 'webAddressDisallowed';
            }
        }

        /*
         * Check for illegal characters first
         */

        if (alphaNumOnly($webAddress)) {
            /*
             * We're ok so far....
             * Now check that this web address does not already exist
             */
            $status = $this->public_site->checkWebAddress($webAddress,$this->session->userdata('accountUserid'));
            if ($status == FALSE) {
                return 'webAddressInUse';
            } else {
                return 'success';
            }
        } else {
            /*
             * Letters and numbers only kids, try again!
             */
            return 'webAddressBadChars';
        }
    }

    function changeLanguage($langCode) {
        $this->settings_update->changeLanguage($langCode,$this->session->userdata('userid'));
		$this->session->set_userdata('language',$langCode);
    }

    function cancelAccount() {
        $profileID = $this->session->userdata('paymentProfileID');
        if (!empty($profileID)) {
            /*
             * Cancel PayPal recurring payment profile.
             */
            $note = urlencode('StuffSafe account canceled.');
            $profileID = urlencode($this->session->userdata('paymentProfileID'));
            $nvpStr =	"&PROFILEID=$profileID&ACTION=Cancel&NOTE=$note";
            $httpParsedResponseAr = PPHttpPost('ManageRecurringPaymentsProfileStatus', $nvpStr);
        }
        $this->Accounts->cancelAccount($this->session->userdata('accountUserid'));
    }

    function dataImportVersion10() {
        global $accountLevelArray;
        function fix_latin1_mangled_with_utf8_maybe_hopefully_most_of_the_time($str)
        {
            return preg_replace_callback('#[\\xA1-\\xFF](?![\\x80-\\xBF]{2,})#', 'utf8_encode_callback', $str);
        }

        function utf8_encode_callback($m)
        {
            return utf8_encode($m[0]);
        }
        
        $xmlURL  = SSV1_URL.'/widgetSSDataImport.php?';
        $xmlURL .= 'userid='.$_POST['userName'].'&';
        $xmlURL .= 'pw='.$_POST['password'];

        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $xmlURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $response = utf8_encode(curl_exec($ch));
        
        $response = preg_replace('/[\x00-\x09\x0B\x0C\x0E-\x1F\x7F]/', '', $response);

        if (!$xmlData = simplexml_load_string($response)) {
            $error = 1;
            $message = 'This operation requires that <i>allow_url_fopen</i> be set to 1 in your PHP.ini file.';
        } else {            
            curl_close($ch);
            /*
             * What's our status?
             */
            $statusType    = $xmlData->status->statusType;
            $statusMessage = $xmlData->status->statusMessage;
            if ($statusType == 'error' && $statusMessage == 'badLogin') {
                echo '{"Status":"error","Message":"'.lang('login_message_noAccount2').'"}';
            } elseif ($statusType == 'success') {                
                /*
                 * Check for existing ImportGUID
                 */
                $newGUID = random_string('alnum',50);
                $data['accountUserid'] = $this->session->userdata('accountUserid');
                $data['importGUID']    = $newGUID;
                $data['action']        = 'add';

                $existingImportGUID = $this->session->userdata('importGUID');
                if (!empty($existingImportGUID)) {
                    /*
                     * If an import GUID exists, delete previous import data with this GUID
                     * and assign a new GUID to this account.
                     */
                    $this->data_import->deletePreviousImportData($this->session->userdata('importGUID'));
                    $data['action']        = 'edit';
                }
                $this->data_import->saveImportGUID($data);

                /*
                 * Put new importGUID into session
                 */
                $this->session->set_userdata('importGUID',$newGUID);               

                /*
                 * Start moving data over.
                 */
                $loginArray             = $xmlData->logins->login;
                $userDir                = $xmlData->userDir;
                $clientArray            = $xmlData->clients->client;
                $teamArray              = $xmlData->team_members->team_member;
                $projectArray           = $xmlData->projects->project;
                $projectContactsArray   = $xmlData->project_contacts->project_contact;
                $projectTeamMemberArray = $xmlData->project_team_members->project_team_member;
                $taskArray              = $xmlData->tasks->task;
                $invoiceArray           = $xmlData->invoices->invoice;
                $invoiceItemsArray      = $xmlData->invoice_items->invoice_item;
                $categoryArray          = $xmlData->categories->category;                
                $timeArray              = $xmlData->times->time;
                $eventsArray            = $xmlData->events->event;
                $vendorsArray           = $xmlData->vendors->vendor;
                $notesArray             = $xmlData->notes->note;
                $tagsArray              = $xmlData->tags->tag;
                $feedsArray             = $xmlData->feeds->feed;
                $apiArray               = $xmlData->apis->api;                
                $expensesArray          = $xmlData->expenses->expense;
                $settingsInvoiceArray   = $xmlData->settings->setting;
                $filesArray             = $xmlData->files->file;

                cpo_main_cat($categoryArray);
                cpo_login($loginArray);
                $countClients  = cpo_clients($clientArray);
                $countProjects = cpo_projects($projectArray);
                cpo_project_contacts($projectContactsArray);
                $countTeam       = cpo_team($teamArray);
                $countTasks      = cpo_project_tasks($taskArray);
                $countTimesheets = cpo_project_time($timeArray);
                $countInvoices   = cpo_invoices($invoiceArray);
                cpo_invoice_items($invoiceItemsArray);
                $countNotes      = cpo_notes($notesArray);
                $countEvents     = cpo_events($eventsArray);
                $countVendors    = cpo_vendors($vendorsArray);
                cpo_tags($tagsArray);
                cpo_RSSRead($feedsArray);
                cpo_APILogin($apiArray);
                $countExpenses = cpo_expenses($expensesArray);
                
                if ($accountLevelArray[$this->session->userdata('accountLevel')]['fileSpace'] > 0) {
                    $countFiles = cpo_files($filesArray,$userDir);
                }

                $importSummary  = '<h3>'.lang('common_import_summary').'</h3>';
                $importSummary .= '<ul class=plain>';
                $importSummary .= '<li>'.lang('menu_clients').': <strong>'.$countClients.'</strong></li>';
                $importSummary .= '<li>'.lang('menu_projects').': <strong>'.$countProjects.'</strong></li>';
                $importSummary .= '<li>'.lang('team_members').': <strong>'.$countTeam.'</strong></li>';
                $importSummary .= '<li>'.lang('widget_tasks').': <strong>'.$countTasks.'</strong></li>';
                $importSummary .= '<li>'.lang('timesheets_entries').': <strong>'.$countTimesheets.'</strong></li>';
                $importSummary .= '<li>'.lang('finance_invoices').': <strong>'.$countInvoices.'</strong></li>';
                $importSummary .= '<li>'.lang('widget_notes').': <strong>'.$countNotes.'</strong></li>';
                $importSummary .= '<li>'.lang('calendar_events').': <strong>'.$countEvents.'</strong></li>';
                $importSummary .= '<li>'.lang('common_vendors').': <strong>'.$countVendors.'</strong></li>';
                $importSummary .= '<li>'.lang('finance_expenses').': <strong>'.$countExpenses.'</strong></li>';

                if ($accountLevelArray[$this->session->userdata('accountLevel')]['fileSpace'] > 0) {
                    $importSummary .= '<li>'.lang('files_files').': <strong>'.$countFiles.'</strong></li>';
                }
                $importSummary .= '</ul>';

                echo '{"Status":"success","Message":"'.lang('success_data_import').'","Summary":"'.$importSummary.'"}';
            }
        }
    }

    function dataImportVersion10SelfHosted() {
        $error = FALSE;
        $importSummary = '';

        function fix_latin1_mangled_with_utf8_maybe_hopefully_most_of_the_time($str)
        {
            return preg_replace_callback('#[\\xA1-\\xFF](?![\\x80-\\xBF]{2,})#', 'utf8_encode_callback', $str);
        }

        function utf8_encode_callback($m)
        {
            return utf8_encode($m[0]);
        }

        $dbaseHost     = $_POST['dbaseHostname'];
        $dbaseName     = $_POST['dbaseDatabase'];
        $dbaseUserName = $_POST['dbaseUserName'];
        $dbasePassword = $_POST['dbasePassword'];
        $filePath      = $_POST['filePath'];

        $dbaseConn = checkDatabaseConnection($dbaseHost,$dbaseName,$dbaseUserName,$dbasePassword);

        /*
         * Check our database connection
         */
        if ($dbaseConn == FALSE) {
            $error = TRUE;
            $message = 'There is a problem with your database connection or the database <strong>'.$dbaseName.'</strong> does not exist. Please check your database settings and try again.';
            $status  = 'error';
        }

        if (!empty($filePath)) {
            /*
             * Check our file path.
             */
            if ( ! file_exists( $filePath )) {
                $error = TRUE;
                $message = 'Your version 1 file directory '.$filePath.' does not exist.';
                $status  = 'error';
            }

            /*
             * Check our version 2 user directory to make sure it's writable.
             */
            if ( ! is_writable( USER_DIR.$this->session->userdata('userDir') ) ) {
                $error = TRUE;
                $message = 'Your version 2 file upload directory '.USER_DIR.$this->session->userdata('userDir').' is not writable. Please make it writable (CHMOD 0777 or give it write permissions).';
                $status  = 'error';
            }
        }

        if ($error == FALSE) {
            /*
             * Proceed with the import.
             */
            $dbaseArray['dbaseHost']     = $dbaseHost;
            $dbaseArray['dbaseUserName'] = $dbaseUserName;
            $dbaseArray['dbasePassword'] = $dbasePassword;
            $dbaseArray['dbaseName']     = $dbaseName;

            $data = utf8_encode(getXMLFromLocalDatabase($dbaseArray));
            $data = preg_replace('/[\x00-\x09\x0B\x0C\x0E-\x1F\x7F]/', '', $data);

            if (!$xmlData = simplexml_load_string($data,NULL, LIBXML_NOERROR)) {
                $status = 'error';
                $message = 'This operation requires that <i>allow_url_fopen</i> be set to 1 in your PHP.ini file.';
                $error = TRUE;
            }
            if ($error == FALSE) {
                /*
                 * Check for existing ImportGUID
                 */
                $newGUID = random_string('alnum',50);
                $data['accountUserid'] = $this->session->userdata('accountUserid');
                $data['importGUID']    = $newGUID;
                $data['action']        = 'add';

                $existingImportGUID = $this->session->userdata('importGUID');
                if (!empty($existingImportGUID)) {
                    /*
                     * If an import GUID exists, delete previous import data with this GUID
                     * and assign a new GUID to this account.
                     */
                    $this->data_import->deletePreviousImportData($this->session->userdata('importGUID'));
                    $data['action']        = 'edit';
                }
                $this->data_import->saveImportGUID($data);

                /*
                 * Put new importGUID into session
                 */
                $this->session->set_userdata('importGUID',$newGUID);

                /*
                 * Start moving data over.
                 */
                $loginArray             = $xmlData->logins->login;
                $userDir                = $xmlData->userDir;
                $clientArray            = $xmlData->clients->client;
                $teamArray              = $xmlData->team_members->team_member;
                $projectArray           = $xmlData->projects->project;
                $projectContactsArray   = $xmlData->project_contacts->project_contact;
                $projectTeamMemberArray = $xmlData->project_team_members->project_team_member;
                $taskArray              = $xmlData->tasks->task;
                $invoiceArray           = $xmlData->invoices->invoice;
                $invoiceItemsArray      = $xmlData->invoice_items->invoice_item;
                $categoryArray          = $xmlData->categories->category;
                $timeArray              = $xmlData->times->time;
                $eventsArray            = $xmlData->events->event;
                $vendorsArray           = $xmlData->vendors->vendor;
                $notesArray             = $xmlData->notes->note;
                $tagsArray              = $xmlData->tags->tag;
                $feedsArray             = $xmlData->feeds->feed;
                $apiArray               = $xmlData->apis->api;
                $expensesArray          = $xmlData->expenses->expense;
                $settingsInvoiceArray   = $xmlData->settings->setting;
                $filesArray             = $xmlData->files->file;

                cpo_main_cat($categoryArray);
                cpo_login($loginArray);
                $countClients  = cpo_clients($clientArray);
                $countProjects = cpo_projects($projectArray);
                cpo_project_contacts($projectContactsArray);
                $countTeam       = cpo_team($teamArray);
                $countTasks      = cpo_project_tasks($taskArray);
                $countTimesheets = cpo_project_time($timeArray);
                $countInvoices   = cpo_invoices($invoiceArray);
                cpo_invoice_items($invoiceItemsArray);
                $countNotes      = cpo_notes($notesArray);
                $countEvents     = cpo_events($eventsArray);
                $countVendors    = cpo_vendors($vendorsArray);
                cpo_tags($tagsArray);
                cpo_RSSRead($feedsArray);
                cpo_APILogin($apiArray);
                $countExpenses = cpo_expenses($expensesArray);

                if (!empty($filePath)) {
                    $countFiles = cpo_filesSelfHosted($filesArray,$userDir,$filePath);
                }

                $importSummary  = '<h3>'.lang('common_import_summary').'</h3>';
                $importSummary .= '<ul class=plain>';
                $importSummary .= '<li>'.lang('menu_clients').': <strong>'.$countClients.'</strong></li>';
                $importSummary .= '<li>'.lang('menu_projects').': <strong>'.$countProjects.'</strong></li>';
                $importSummary .= '<li>'.lang('team_members').': <strong>'.$countTeam.'</strong></li>';
                $importSummary .= '<li>'.lang('widget_tasks').': <strong>'.$countTasks.'</strong></li>';
                $importSummary .= '<li>'.lang('timesheets_entries').': <strong>'.$countTimesheets.'</strong></li>';
                $importSummary .= '<li>'.lang('finance_invoices').': <strong>'.$countInvoices.'</strong></li>';
                $importSummary .= '<li>'.lang('widget_notes').': <strong>'.$countNotes.'</strong></li>';
                $importSummary .= '<li>'.lang('calendar_events').': <strong>'.$countEvents.'</strong></li>';
                $importSummary .= '<li>'.lang('common_vendors').': <strong>'.$countVendors.'</strong></li>';
                $importSummary .= '<li>'.lang('finance_expenses').': <strong>'.$countExpenses.'</strong></li>';

                if ($accountLevelArray[$this->session->userdata('accountLevel')]['fileSpace'] > 0) {
                    $importSummary .= '<li>'.lang('files_files').': <strong>'.$countFiles.'</strong></li>';
                }
                $importSummary .= '</ul>';

                $message = lang('success_data_import');
                $status  = 'success';
            }
        }

        echo '{"Status":"'.$status.'","Message":"'.$message.'","Summary":"'.$importSummary.'"}';
    }

    function checkForUpdates() {
        /*
         * Get version number
         */
        include(APPPATH.'version.php');
        if (ini_get('allow_url_fopen')) {
            $context = stream_context_create( array(
                    'http' => array(
                        'method'    => 'POST',
                        'content'   => http_build_query( array(
                                'orderNumber'   => $this->session->userdata('orderNumber'),
                                'installKey'    => $this->session->userdata('installKey'),
                                'versionNumber' => $version
                        ) ),
                        'timeout'	=> 5 )
                ) );

            $updateStatus = @file_get_contents( 'http://www.mycpohq.com/selfhosted/checkForUpdates', null, $context );
            echo $updateStatus;
        } else {
            echo '{"message":"'.lang('cannot_check_for_updates').'<br />'.lang('updates_download_manual').'","status":"error"}';
        }
    }

    function updateAccountInformation() {
        $data['userid']        = $this->session->userdata('userid');
        $data['accountUserid'] = $this->session->userdata('accountUserid');
        $data['accountID']     = $_POST['accountID'];
        $data['nameFirst']     = fieldToDB($this->input->post('nameFirst', TRUE));
        $data['nameLast']      = fieldToDB($this->input->post('nameLast', TRUE));
        $data['company']       = fieldToDB($this->input->post('company', TRUE));
        $data['address1']      = fieldToDB($this->input->post('address1', TRUE));
        $data['address2']      = fieldToDB($this->input->post('address2', TRUE));
        $data['city']          = fieldToDB($this->input->post('city', TRUE));
        $data['state']         = fieldToDB($this->input->post('state', TRUE));
        $data['zip']           = fieldToDB($this->input->post('zip', TRUE));
        $data['country']       = $_POST['country'];
        $data['currency']      = $_POST['currency'];
        $data['phone']         = fieldToDB($this->input->post('phone', TRUE));
        $data['email']         = fieldToDB($this->input->post('email', TRUE));

        if (empty($data['nameFirst']) || empty($data['nameLast']) || empty($data['email'])) {
            echo 'error';
        } else {
            $this->accounts->updateAccountInformation($data);
            echo 'success';
        }
    }
}
?>