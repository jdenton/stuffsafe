<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class SiteSearch extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		checkAuthentication($_POST);
        loadLanguageFiles();

        $this->load->model('site_search','',true);
	}

    function _init($searchTerm=NULL,$tag=null) {
        $data['page']            = 'finances';
		$data['pageTitle']       = lang('common_search_stuff');
		$data['pageSubTitle']    = '';
		$data['wysiwyg']         = 0;
		$data['map']             = 0;
		$data['jsFileArray']     = array('jsSiteSearch.js');
		$data['pageIconClass']   = 'iconPageSearch';
		$data['pageLayoutClass'] = 'withRightColumn';
        $data['rightColumnComponents'] = array();

        $data['searchTerm'] = $searchTerm;
        $data['tag']        = $tag;

        $accountUserid = $this->session->userdata('accountUserid');
        $userid = $this->session->userdata('userid');
        $data['tagSearch'] = FALSE;
        if (!empty($searchTerm) && $searchTerm != NULL) {
            $data['searchResults'] = $this->site_search->performSiteSearch($searchTerm,$accountUserid,$userid);
        } else if (!empty($tag) && $tag != NULL) {
            $data['tagSearch']     = TRUE;
            $data['pageTitle']     = lang('common_search_tags').' ('.$tag.')';
            $data['searchResults'] = $this->site_search->searchTags($tag,$accountUserid,$userid);
        } else {
            $data['searchResults'] = NULL;
        }

        return $data;
    }

    function index() {
        $searchTerm = $this->input->post('appSearch', TRUE);
        $data = SiteSearch::_init($searchTerm);
		$this->load->view('SiteSearch',$data);
    }

    function searchTags($tag) {
        $data = SiteSearch::_init(null,$tag);
		$this->load->view('SiteSearch',$data);
    }

    function siteSearchAutocomplete() {
        $q = strtolower($this->input->post('term', TRUE));
		if (!$q) return;
        $accountUserid = $this->session->userdata('accountUserid');
        $userid        = $this->session->userdata('userid');
		$searchArray    = $this->site_search->getSearchResultsForAutoCompleter($q,$accountUserid,$userid);
		echo json_encode($searchArray);
    }

    function inventorySearch() {
        $q = strtolower($this->input->post('term', TRUE));
		if (!$q) return;
        $accountUserid = $this->session->userdata('accountUserid');
        $userid        = $this->session->userdata('userid');
		$searchArray    = $this->site_search->inventorySearch($q,$accountUserid,$userid);
		echo json_encode($searchArray);
    }
}
?>