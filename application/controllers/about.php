<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class About extends CI_Controller {
    function _init() {
        $data = array();
        $data['mainMenu'] = makeMainMenu();
        return $data;
    }

	public function index($text=null)
	{
        $data = About::_init();
        $data['content'] = $text;
		$this->load->view('about',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
