<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Payment extends CI_Controller {

    function __construct(){
		parent::__construct();
                
		$this->load->model('public_site','',true);
        $this->load->model('accounts','',true);

        $this->load->library('application/CommonEmail');

        $this->load->helper('account');
        $this->load->helper('date');
        $this->load->helper('payment');
        $this->load->helper('templates');
	}

    function _init($plan=NULL,$isPaymentSuccess=FALSE) {
        global $accountLevelArray, $monthNameArray, $accountPricePrev, $accountPriceNext, $datePriceChange;

        $data['child'] = TRUE;
        if ($isPaymentSuccess == TRUE) {
            $data['headerTextImage']       = 'txtThanksHeader_public.png';
        } else {
            $data['headerTextImage']       = 'txtPaymentHeader_public.png';
        }
        $data['rightColumnComponents'] = array('paymentInfo');
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'Payment';

        $planCost = '';
        $a=0;
        foreach ($accountLevelArray as $accountLevel) {
            if ($accountLevel['name'] == $plan) {
                $planCost = $accountPricePrev[$a];
                $planTerm = $accountLevel['term'];
                if (days_between_dates(strtotime($datePriceChange),time())>0) {
                    $planCost = $accountPriceNext[$a];
                }
                $planNumber = $a;
            }
            $a++;
        }
        $data['planTerm']   = $planTerm;
        $data['rate_plan']  = $plan;
        $data['planNumber'] = $planNumber;
        $data['rate_price'] = $planCost;
        $data['planDataArray'] = createPlanData($plan);

        if ($this->session->userdata('logged') != TRUE) {
            header('Location: '.site_url('error'));
        }

        $data['currentMonth']  = date('m');
        $data['currentYear']   = date('Y');

        $data['months'] = $monthNameArray;

        return $data;
    }

    function paymentInfo($accountUserid,$plan=NULL) {
        global $accountLevelArray;

        /*
         * Retrieve account data and put it into session.
         */        
        $accountArray = spinUpAccountSession($accountUserid);
        
        if ($accountArray === false) {
            header('Location: http://www.stuffsafe.com');
        }
        
        $paymentProfileID = $this->session->userdata('paymentProfileID');
        $isPaid           = $this->session->userdata('isPaid');
        $accountLevel     = $this->session->userdata('accountLevel');

        /*
         * Are we paying for the first time?
         */        
        $linkSelectNewPlan = '/account/changeAccount/'.$accountUserid;
        if (empty($paymentProfileID)) {
            $whyPay = 'newAccount';
        } elseif (!empty($paymentProfileID) && $isPaid == TRUE) {
            $whyPay = 'changeAccount';
        } elseif (!empty($paymentProfileID) && $isPaid == FALSE) {
            $whyPay = 'paymentError';
        }
        $this->session->set_userdata('whyPay',$whyPay);

        if (empty($plan)) {
            foreach ($accountLevelArray as $key => $value) {
                if ($key == $accountLevel) {
                    $plan = $value['name'];
                }
            }
        }
        
        $data = Payment::_init($plan);
        $data['linkSelectNewPlan'] = $linkSelectNewPlan;
        
        /*
         * Load up our fields
         */
        $data['firstName'] = $accountArray['BillingNameFirst'];
        $data['lastName']  = $accountArray['BillingNameLast'];
        $data['address']   = $accountArray['BillingAddress'];
        $data['city']      = $accountArray['BillingCity'];
        $data['state']     = $accountArray['BillingStateRegion'];
        $data['zip']       = $accountArray['BillingZipPostalCode'];
        $data['country']   = $accountArray['BillingCountry'];
        $data['currency']  = $accountArray['BillingCurrency'];
        $data['cardtype']  = NULL;
        $data['expMonth']  = date('m');
        $data['expYear']   = date('Y');
        $data['description'] = 'StuffSafe '.$plan.' subscription for $'.$data['rate_price'].'.00 per month.';
        $data['whyPay']    = $whyPay;

        $whyPayArray = determineWhyPay();
        $data['submitButtonClass'] = $whyPayArray['buttonClass'];

        $this->load->view('Payment',$data);
    }

    function sendPayment() {
        global $monthNameArray;
        $validPayment = TRUE;

        $plan = $_POST['plan'];
        $data = Payment::_init($plan);
        $billingPeriod = $data['planTerm'];

        $data['linkSelectNewPlan'] = '/account/changeAccount/'.$this->session->userdata('accountUserid');

        $data['rate_price']  = $_POST['amount'];
        $data['description'] = $_POST['description'];
        $data['rate_plan']   = $plan;
        $data['currentMonth']= $_POST['currentMonth'];
        $data['currentYear'] = $_POST['currentYear'];

        $data['firstName']   = $_POST['firstName'];
        $data['lastName']    = $_POST['lastName'];
        $data['address']     = $_POST['address'];
        $data['city']        = $_POST['city'];

        if (!empty($_POST['stateRegion'])) {
            $data['state'] = fieldToDB($this->input->post('stateRegion', TRUE));
        } else {
            $data['state'] = $_POST['stateUS'];
        }

        $data['zip']         = $_POST['zip'];
        $data['country']     = $_POST['country'];
        $data['currency']    = $_POST['currency'];
        $data['cardtype']    = $_POST['cardtype'];
        $data['expMonth']    = $_POST['expmonth'];
        $data['expYear']     = $_POST['expyear'];

        $whyPayArray = determineWhyPay();
        $data['submitButtonClass'] = $whyPayArray['buttonClass'];

        if ($this->form_validation->run() == FALSE)	{ 
            $this->load->view('Payment',$data);
        } else {
            $sendPayment = TRUE;
            
            /*
             * Set request-specific fields.
             */
            $paymentType      = urlencode('Sale');				// or 'Sale'
            $description      = urlencode($_POST['description']);

            $firstName        = urlencode(fieldToDB($this->input->post('firstName', TRUE)));
            $lastName         = urlencode(fieldToDB($this->input->post('lastName', TRUE)));
            $address          = urlencode(fieldToDB($this->input->post('address', TRUE)));
            $city             = urlencode(fieldToDB($this->input->post('city', TRUE)));
            $state            = urlencode($data['state']);
            $creditCardType   = urlencode($_POST['cardtype']);

            /*
             * Pull numberic portion of credit card number out
             */
            $creditCardInput = fieldToDB($this->input->post('cardnumber', TRUE));
            $creditCardNumeric = '';
            for($a=0;$a<=strlen($creditCardInput)-1;$a++) {
                if (is_numeric($creditCardInput[$a])) {
                    $creditCardNumeric .= $creditCardInput[$a];
                }
            }
            $creditCardNumber = urlencode($creditCardNumeric);
            $creditCardNoSecure = 'XXXX-XXXX-XXXX-'.substr($creditCardNumeric,-4,4);

            $expDateMonth     = urlencode($_POST['expmonth']);
            $expDateYear      = urlencode($_POST['expyear']);
            $cvv2Number       = urlencode(fieldToDB($this->input->post('cvv2number', TRUE)));
            $zip              = urlencode(fieldToDB($this->input->post('zip', TRUE)));
            $country          = urlencode($_POST['country']);
            $amount           = urlencode($_POST['amount']);
            $currencyID       = urlencode($_POST['currency']);	// Or other PayPal supported currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
            $now = time();
            $profileStartDate = standard_date('DATE_PAYPAL',$now);
            
            if ($amount != '30' && $amount != '10' && $amount != '49') {
                $sendPayment = FALSE;
            }

            /*
             * Add request-specific fields to the request string.
             */
            $nvpStr  = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                        "&EXPDATE=$expDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                        "&COUNTRYCODE=$country&STATE=$state&STREET=$address&CITY=$city&ZIP=$zip&CURRENCYCODE=$currencyID".
                        "&PROFILESTARTDATE=$profileStartDate&BILLINGPERIOD=$billingPeriod&BILLINGFREQUENCY=1&DESC=$description".
                        "&MAXFAILEDPAYMENTS=3";
            $method =   "CreateRecurringPaymentsProfile";
            
            if ($validPayment == TRUE) {
                if ($this->session->userdata('whyPay') != 'newAccount') {
                    /*
                     * Cancel existing PayPal recurring billing profile first.
                     */
                    $profileID = urlencode($this->session->userdata('paymentProfileID'));
                    $note = urlencode('StuffSafe account updated. Need new profile.');
                    $nvpStrCancel =	"&PROFILEID=$profileID&ACTION=Cancel&NOTE=$note";
                    $httpParsedResponseArCancel = PPHttpPost('ManageRecurringPaymentsProfileStatus', $nvpStrCancel);
                }
                
                /*
                 * Now create the new PayPal profile.
                 */
                
                if ($sendPayment === TRUE) {
                    $accountArray['profileID'] = NULL;
                    if ($plan != 'free' && $data['rate_price']>0) {
                        $httpParsedResponseAr = PPHttpPost($method, $nvpStr);
                        $data['payPalResponseArray'] = $httpParsedResponseAr;

                        if(strtoupper($httpParsedResponseAr["ACK"]) == "SUCCESS" || strtoupper($httpParsedResponseAr["ACK"]) == "SUCCESSWITHWARNING") {
                            $validPayment = TRUE;
                        } else {
                            $validPayment = FALSE;
                        }
                        $accountArray['profileID'] = urldecode($data['payPalResponseArray']['PROFILEID']);
                    }
                }
            }

            /*
             * Load the required view based on payment success or failure.
             */
            if ($validPayment == TRUE) {
                /*
                 * Update account table for this account with billing address and
                 * PayPal Profile ID.
                 */
                $accountArray['accountID'] = $this->session->userdata('accountUserid');
                $accountArray['isPaid']    = 1;
                $accountArray['billingNameFirst'] = urldecode($firstName);
                $accountArray['billingNameLast']  = urldecode($lastName);
                $accountArray['billingAddress']   = urldecode($address);
                $accountArray['billingCity']      = urldecode($city);
                $accountArray['billingState']     = urldecode($state);
                $accountArray['billingZip']       = urldecode($zip);
                $accountArray['billingCountry']   = $_POST['country'];
                $accountArray['billingCurrency']  = $_POST['currency'];
                $accountArray['accountPrice']     = $_POST['amount'];
                $accountArray['accountLevel']     = $data['planNumber'];

                $this->accounts->updateAccountWithPaymentInformation($accountArray);

                /*
                 * Send thank you confirmation email to account owner.
                 */
                $whyPayArray = determineWhyPay();
                $messageSubject = $whyPayArray['emailSubject'];

                $templateData['whyPay']      = $this->session->userdata('whyPay');
                $templateData['plan']        = $plan;
                $templateData['cost']        = $data['planDataArray']['acctVars']['cost'];
                $templateData['locations']   = $data['planDataArray']['acctVars']['locations'];
                $templateData['teamMembers'] = $data['planDataArray']['acctVars']['teamMembers'];
                $templateData['storage']     = $data['planDataArray']['acctVars']['storage'];
                $templateData['term']        = $data['planDataArray']['acctVars']['term'];
                $templateData['billingInfo']  = $accountArray['billingNameFirst'].' '.$accountArray['billingNameLast'].'<br>'.Chr(10);
                $templateData['billingInfo'] .= $accountArray['billingAddress'].'<br>'.Chr(10);
                $templateData['billingInfo'] .= $accountArray['billingCity'].' '.$accountArray['billingState'].' '.$accountArray['billingZip'].'<br>'.Chr(10);
                $templateData['billingInfo'] .= 'Card No: '.$creditCardNoSecure.'<br>'.Chr(10);
                $templateData['billingInfo'] .= 'Expires on: '.$_POST['expmonth'].'/'.$_POST['expyear'];


                $messageHTML = templatePaymentEmail($templateData);

                $messageSent = $this->commonemail->sendPaymentEmail($messageSubject,$messageHTML,$this->session->userdata('email'),NULL);
               
                /*
                 * Redirect to Thank You page in insecure area.
                 */
                header('Location: '.INSECURE_URL.'payment/paymentSuccess/'.$plan.'/'.$this->session->userdata('whyPay'));
            } else {
                /*
                 * Let's format some error messages
                 */
                $errorCode = urldecode($data['payPalResponseArray']['L_ERRORCODE0']);
                $errorMessage = getHumanTransactionErrorMessage($errorCode);

                /*
                 * Log error to database
                 */
                $errorArray['errorType']     = 'PayPal';
                $errorArray['errorCode']     = $errorCode;
                $errorArray['accountUserid'] = $this->session->userdata('accountUserid');
                $errorMessage = urldecode($data['payPalResponseArray']['L_LONGMESSAGE0']);
                $errorArray['errorMessage']  = fieldToDB($errorMessage);
                $errorArray['errorPage']     = NULL;
                $errorLogID = $this->accounts->enterPaymentErrorLog($errorArray);

                /*
                 * Send email notification to CPO support
                 */
                $messageAdmin  = 'Someone encountered an error while paying.'.Chr(10);
                $messageAdmin .= 'Account ID: '.$this->session->userdata('accountUserid').Chr(10);
                $messageAdmin .= 'Error Code: '.$errorCode.Chr(10);
                $messageAdmin .= 'Error Message: '.$errorArray['errorMessage'];
                $subjectAdmin = "StuffSafe: Account payment problem";
                $this->commonemail->sendEmailToAdmin($messageAdmin,$subjectAdmin);

                $data['errorMessage'] = $errorMessage;
                $this->load->view('Payment',$data);
            }
        }
    }    

    function paymentSuccess($plan,$whyPay=NULL) {
        $data = Payment::_init($plan,TRUE);
        $accountLevelStatusArray = $this->accounts->checkAccountLevelStatus($this->session->userdata('accountUserid'));
        if ($accountLevelStatusArray['IsPaid'] == TRUE) {
            $this->session->set_userdata('isPaid',TRUE);
        } else {
            $this->session->set_userdata('isPaid',FALSE);
        }
        $this->session->set_userdata('accountLevel',$accountLevelStatusArray['AccountLevel']);
        $this->session->set_userdata('paymentProfileID',$accountLevelStatusArray['PaymentProfileID']);

        $data['whyPay'] = $whyPay;
        $data['linkSelectNewPlan'] = NULL;

        $this->load->view('Payment_success',$data);
    }
}
?>