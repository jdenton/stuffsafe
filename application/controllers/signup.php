<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Signup extends CI_Controller {
	function __construct()
	{
        parent::__construct();
		$this->load->model('public_site','',true);

        $this->load->helper('number');
        $this->load->helper('templates');

        $this->load->library('application/CommonLogin');
        $this->load->library('application/CommonEmail');
	}

    function _init($plan=NULL) {
        global $accountLevelArray;

        $data['child']           = TRUE;
        $data['headerTextImage'] = 'txtSignupHeader_public.png';
        $data['jsFileArray']     = array();
        $data['pageTitle']       = 'Signup';
        
        $data['rightColumnComponents'] = array('checkoutInfo');
		
        /*
         * Create account descriptions
         */
        $data['planDataArray'] = createPlanData($plan);
        $data['linkSelectNewPlan'] = '/pricing';

        /*
         * Calculate payment date
         */
        $data['billingDate'] = new_date_from_days(date('Y-m-d'),15,'F j, Y');

        noCache();
        return $data;
    }

    function index($plan=NULL) {
		$data = Signup::_init($plan);
        $this->load->view('Signup',$data);
    }
    
    function basic() {
		$data = Signup::_init('basic');
        $this->load->view('Signup',$data);
    }

    function premium() {
		$data = Signup::_init('premium');
        $this->load->view('Signup',$data);
    }

    function max() {
		$data = Signup::_init('max');
        $this->load->view('Signup',$data);
    }

    function changeAccount($plan=NULL) {
        $data = Signup::_init($plan);
        $data['headerTextImage'] = 'txtAccountHeader_public.png';
        $data['plan'] = $plan;
        $data['linkSelectNewPlan'] = SECURE_URL.'account/changeAccount/'.$this->session->userdata('accountUserid');
        $paymentProfileID = $this->session->userdata('paymentProfileID');
        
        $daysFromSignup = days_between_dates(strtotime($this->session->userdata('dateSignup')),time());

        //if ($daysFromSignup >= 31 && $this->session->userdata('userType') == USER_TYPE_GOD) {
        if ($daysFromSignup >= 31) {
            /*
             * Redirect to payment screen because user will need to confirm
             * account change by entering credit card number.
             * Or if they've had a free plan for > 30 days and they're upgrading
             * to a paid plan, they'll need to enter their CC info.
             */
            header('Location: '.SECURE_URL.'payment/paymentInfo/'.$this->session->userdata('accountUserid').'/'.$plan);
        } else {
            $accountConfirm = TRUE;
        }

        if ($accountConfirm == TRUE) {
            /*
             * Don't force the user to complete any payment info yet
             * because they're still within their 30 day trial period.
             */
            $daysLeftInTrial = 30-$daysFromSignup;
            $dateTrialExpires = '';
            if ($daysLeftInTrial>0) {
                $dateTrialExpires = new_date_from_days(date('Y-m-d'),$daysLeftInTrial,'F j, Y');
            }
            $data['dateTrialExpires'] = $dateTrialExpires;
            $data['daysLeftInTrial']  = $daysLeftInTrial;
            $data['paymentProfileID'] = $this->session->userdata('paymentProfileID');
            $data['accountUserid']    = $this->session->userdata('accountUserid');
            $this->load->view('ConfirmAccountChange',$data);
        }
    }

    function changeAccountConfirm($accountUserid=NULL,$plan=NULL) {
        global $accountLevelArray;
        /*
         * We don't get here unless:
         * 1.  We are still in our 30 day trial period or
         * 2.  We have a successful change payment profile ID
         *     through the payment form.
         */
        switch ($plan) {
			case 'basic':
                $newAccountLevel = 0;
                break;
            case 'premium':
                $newAccountLevel = 1;
                break;
            case 'max':
                $newAccountLevel = 2;
                break;
        }
        $newAccountPrice = $accountLevelArray[$newAccountLevel]['cost'];
        $this->session->set_userdata('accountLevel',$newAccountLevel);
        /*
         * Save new account level to database
         */
        $this->public_site->saveNewAccountLevel($accountUserid,$newAccountLevel,$newAccountPrice);
    }

    function createAccount() {
        global $accountLevelArray, $rolesArray;
        $this->lang->load('email_en','en');
        
        if (!isset($_POST['createAccount'])) {
            $data = Signup::_init();
            $this->load->view('Signup',$data);
        } else {
            /*
             * Get our variables
             */
            $signupArray['selectedPlan']       = $_POST['selectedPlan'];
            if (!empty($signupArray['selectedPlan'])) {
                $planArray = explode('_',$signupArray['selectedPlan']);
                $signupArray['selectedPlanNumber'] = $planArray[1];
                $signupArray['planCost'] = $accountLevelArray[$signupArray['selectedPlanNumber']]['cost'];
                $signupArray['dateExp']  = new_date_from_days(date('Y-m-d'),15,'F j, Y');

            }

            $signupArray['companyName']        = fieldToDB($this->input->post('companyName', TRUE));
            $signupArray['firstName']          = fieldToDB($this->input->post('firstName', TRUE));
            $signupArray['lastName']           = fieldToDB($this->input->post('lastName', TRUE));
            $signupArray['email']              = fieldToDB($this->input->post('email', TRUE));
            $signupArray['password']           = fieldToDB($this->input->post('password', TRUE));
            $signupArray['passwordConfirm']    = fieldToDB($this->input->post('passwordConfirm', TRUE));
            $signupArray['authStringAccount']  = random_string('alnum',20);
            $signupArray['authStringPersonal'] = random_string('alnum',20);
            $signupArray['permissions']        = $rolesArray['admin'];

            /*
             * Get a new account number
             */
            $maxAccountNo = $this->public_site->getLastAccountNumber();
            $signupArray['newAccountNumber'] = $maxAccountNo['MaxAccountNo']+1;
            $hashPass = sha1($signupArray['password'].SITE_CODE);
            $signupArray['hashPassword']    = $hashPass;
            
            /*
             * Do some error checking
             */
            $error = FALSE;
            if (empty($signupArray['selectedPlan'])) {
                $error = TRUE;
            }
            if (empty($signupArray['firstName'])) {
                $error = TRUE;
            }
            if (empty($signupArray['password'])) {
                $error = TRUE;
            }
            if ($signupArray['password'] != $signupArray['passwordConfirm']) {
                $error = TRUE;
            }
            if (Signup::checkEmailPassword($signupArray['email'],$signupArray['password'],1) == FALSE) {
                $error = TRUE;
            }
            $p         = $signupArray['selectedPlanNumber'];
            $plan      = $accountLevelArray[$p]['name'];
            $planPrice = $accountLevelArray[$p]['cost'];

            if ($error == FALSE) {
                /*
                 * Save new user account information
                 */
                $this->public_site->saveNewAccount($signupArray);

                /*
                 * Send welcome email to user.
                 */        
                if ($planPrice == 0) {
                    $planPrice = '$'.$planPrice.' per year';
                } else {
                    $planPrice = '$'.$planPrice.' per month';
                }
                $subject = lang('email_new_account_subject');
                
                $planData = createPlanData($plan);
                $templateData['plan']        = $plan;
                $templateData['cost']        = $planData['acctVars']['cost'];
                $templateData['locations']   = $planData['acctVars']['locations'];
                $templateData['teamMembers'] = $planData['acctVars']['teamMembers'];
                $templateData['storage']     = $planData['acctVars']['storage'];
                $templateData['term']        = $planData['acctVars']['term'];
                $templateData['accountNo']   = $signupArray['newAccountNumber'];
                $templateData['userid']      = $signupArray['email'];
                $templateData['expDate']     = $signupArray['dateExp'];

                $messageHTML = templateNewAccountEmail($templateData);
                $messageSent = $this->commonemail->sendNewAccountEmail($subject,$messageHTML,$signupArray['email']);

                /*
                 * Send email to administrator
                 */
                $messageAdmin = $signupArray['firstName']." ".$signupArray['lastName']." signed up for the StuffSafe ".$accountLevelArray[$p]['name']." plan.".Chr(10);
                $subjectAdmin = "New StuffSafe Account: ".$accountLevelArray[$p]['name'];
                $this->commonemail->sendEmailToAdmin($messageAdmin,$subjectAdmin);

                /*
                 * Log this person into the application and redirect
                 * to dashboard screen
                 */
                $this->commonlogin->tryLogin($signupArray['email'],$signupArray['password'],NULL,NULL,NULL,NULL,FALSE,TRUE,NULL);
            } else {
                /*
                 * If there was an error, do something here.
                 */
                $data = Signup::_init($plan);
                $this->load->view('Signup',$data);
            }
        }        
    }

    function checkWebAddress($webAddress=NULL,$return=0) {
        global $disallowedSubdomains;
        $noGood = false;
        if (empty($webAddress)) {
            $webAddress = $this->input->post('webAddress', TRUE);
        }

        /*
         * Check that the subdomain is not in our disallow list.
         */
        foreach($disallowedSubdomains as $disSub) {
            if ($webAddress == $disSub) {
                if ($return == 1) {
                    return FALSE;
                } else {
                    echo '{"Status":"false","Message":"This web address is not allowed. Please choose another one."}';
                }
                $noGood = true;
            }
        }

        /*
         * Check for illegal characters first
         */
        if ($noGood == false) {
            if (alphaNumOnly($webAddress)) {
                /*
                 * We're ok so far....
                 * Now check that this web address does not already exist
                 */
                $status = $this->public_site->checkWebAddress($webAddress);
                if ($status == FALSE) {
                    if ($return == 1) {
                        return FALSE;
                    } else {
                        echo '{"Status":"false","Message":"This web address is being used by another account."}';
                    }
                } else {
                    if ($return == 1) {
                        return TRUE;
                    } else {
                        echo '{"Status":"true","Message":""}';
                    }
                }
            } else {
                /*
                 * Letters and numbers only kids, try again!
                 */
                if ($return == 1) {
                    return FALSE;
                } else {
                    echo '{"Status":"false","Message":"Your web address contains bad characters. Letters and numbers only."}';
                }
            }
        }
    }

    function checkEmailAddress($emailAddress) {
        
    }

    function checkEmailPassword($emailAddress=NULL,$password=NULL,$return=0) {
        if (empty($emailAddress) || empty($password)) {
            $emailAddress = $this->input->post('email', TRUE);
            $password     = $this->input->post('password', TRUE);
        }

        /*
         * See if this email/password combination already exists in the login table
         */
        $hashPass = sha1($password.SITE_CODE);
        $validCombo = $this->public_site->checkEmailPassword($emailAddress,$hashPass);
        if ($validCombo == FALSE) {
            if ($return == 1) {
                return FALSE;
            } else {
                echo '{"Status":"false","Message":"Your email address and password are in use by another account."}';
            }
        } else {
            if ($return == 1) {
                return TRUE;
            } else {
                echo '{"Status":"true","Message":""}';
            }
        }

    }    
}
?>
