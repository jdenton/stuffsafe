<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Apptrash extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		checkAuthentication($_POST);
        loadLanguageFiles();

		$this->load->model('trash','',true);
	}

    function getItemsInTrash($itemType=NULL,$renderType='json',$return=0) {
        $permissions = $this->session->userdata('permissions');
        $userid = $this->session->userdata('userid');
        $accountUserid = 0;
        if ($permissions['trashViewAll'] == 1) {
            $accountUserid = $this->session->userdata('accountUserid');
        }
        $trashItemArray = $this->trash->getItemsInTrashByType($itemType,$userid,$accountUserid);

        noCache();
        if ($renderType == 'json') {

            $jsonString = json_encode($trashItemArray);
            if ($return == 1) {
                return $jsonString;
            } else {
                echo decode_utf8($jsonString);
            }
        }
    }

    function getAllItemsInTrash($countOnly=0,$renderType='json',$return=0) {
        $trashClientsArray  = Apptrash::getItemsInTrash('inventory');
        $trashProjectsArray = Apptrash::getItemsInTrash('place');
    }

    function getItemCountInTrash($renderType='json',$return=0) {
        $permissions = $this->session->userdata('permissions');
        $userid = $this->session->userdata('userid');
        $accountUserid = 0;
        if ($permissions['trashViewAll'] == 1) {
            $accountUserid = $this->session->userdata('accountUserid');
        }
        $trashCountArray['inventory']['label']   = lang('inventory_items');
        $trashCountArray['inventory']['iconCSS'] = 'icon_inventory_small';
        $trashCountArray['inventory']['count']   = $this->Trash->getNumberOfItemsInTrashByType('inventory',$userid,$accountUserid);

        $trashCountArray['property']['label']   = lang('place_properties');
        $trashCountArray['property']['iconCSS'] = 'icon_property_small';
        $trashCountArray['property']['count']   = $this->Trash->getNumberOfItemsInTrashByType('property',$userid,$accountUserid);

        $trashCountArray['people']['label']   = lang('team_members');
        $trashCountArray['people']['iconCSS'] = 'icon_client_small';
        $trashCountArray['people']['count']   = $this->Trash->getNumberOfItemsInTrashByType('people',$userid,$accountUserid);

        $trashCountArray['room']['label']   = lang('common_rooms');
        $trashCountArray['room']['iconCSS'] = 'icon_room_small';
        $trashCountArray['room']['count']   = $this->Trash->getNumberOfItemsInTrashByType('rooms',$userid,$accountUserid);


        noCache();
        if ($renderType == 'json') {
            $jsonString = json_encode($trashCountArray);
            if ($return == 1) {
                return $jsonString;
            } else {
                echo $jsonString;
            }
        }
    }
}
?>