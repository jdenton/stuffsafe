<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FileManager extends CI_Controller {

	function __construct()
	{
        parent::__construct();
        checkPermissionsForController($this->router->fetch_class());
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

        $this->load->model('file_maintenance');
        $this->load->model('settings_update');
		$this->load->helper('file');
		$this->load->helper('download');
        $this->load->helper('number');
        $this->load->helper('zend');
		$this->load->library('application/FileManagerS3');
        $this->load->library('application/CommonFileManager');
        $this->load->library('application/CommonAppData');
	}

    function _init($viewFile=FALSE) {
        global $subMenuArray;
		$data['page']                  = 'files';
		$data['pageTitle']             = lang('files_file_manager');
		$data['pageSubTitle']          = '';
		$data['wysiwyg']               = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array();
		$data['pageIconClass']         = 'iconPageFile';
		$data['pageLayoutClass']       = 'withRightColumn';
		$data['rightColumnComponents'] = array();
		$data['tagRenderArray']        = $this->commonappdata->renderTags('file','array');
        if ($viewFile == FALSE) {
            $data['pageButtons']       = array(
                                          /*
                                           * Button text, button title, visual style, extra className, itemID
                                           */
                                           'upload'    => array(lang('files_upload_files'),lang('files_upload_files'),'blueGray','lnkOpenFileUpload',''),
                                           'folderNew' => array(lang('files_new_folder'),lang('files_new_folder'),'blueGray','','')
                                        );
        } else {
            $data['pageButtons'] = array();
        }
        noCache();
        return $data;
    }

    function index() {
        $data = FileManager::_init();
		$this->load->view('FileManager',$data);
    }

    function viewFile($itemType,$itemID) {
        $data = FileManager::_init(TRUE);
        $data['itemType'] = $itemType;
        $data['itemID']   = $itemID;
        $data['fileLinkID'] = '';
        if ($itemType == 'attachedFile') {
            /*
             * Get file ID
             */
            $fileLinkArray = $this->file_maintenance->getFileLink($itemID);
            $data['fileID'] = $fileLinkArray['FileID'];
            $data['fileLinkID'] = $itemID;

            $projectID = null;
            $data['projectInformation'] = '';
            if ($fileLinkArray['ItemType'] == 'project') {
                $this->load->model('projects/Project_view','',true);
                $this->load->library('application/CommonProject');

                $projectID = $fileLinkArray['ItemID'];
                $projectArray = $this->Project_view->getProjectInformation($projectID);
                $data['projectInformation'] = $this->commonproject->renderProject($projectArray,'json');
            }
        } else {
            $data['fileID'] = $itemID;
        }

		$this->load->view('FileView',$data);
    }

     /**
      * uploadFileHandler
      * This method abstracts our file handling to a common
      * library file...in this case FileManagerS3. If we ever
      * wanted to swtich from S3 to Box.net or local storage,
      * we can just swap out the library file and keep on trucking.      
      */
     function uploadFileHandler($itemID=NULL,$itemType=null) {
        $fromMobile = FALSE;
        if (isset($_POST['fromMobile']) && $_POST['fromMobile'] == '1') {
            $fromMobile = TRUE;
        }

        $fileInfoArray = $this->filemanagers3->uploadFileHandler();
        $fileID    = $fileInfoArray[0];
        $fileTypes = 'local';
        if ($fromMobile == TRUE) {
            FileManager::attachFilesToItem($itemID,$itemType,$fileTypes,$fileID,$fromMobile);
            $fileName    = $fileInfoArray[3];
            $thumbFileS3 = $fileInfoArray[4];
            $fileSpaceJSON = FileManager::getFileSpaceUsage('json',1);
            echo '{"InventoryID":"'.$itemID.'","LinkID":"","FileID":"'.$fileID.'","Filename":"'.$fileName.'","Thumbfilename":"'.$thumbFileS3.'","Filespace":['.$fileSpaceJSON.']}';
        } elseif ($itemID != null) {
            FileManager::attachFilesToItem($itemID,$itemType,$fileTypes,$fileID,$fromMobile);
        }  else {
            FileManager::getFileItems($fileInfoArray[2]);
        }
     }

     function uploadAvatar($type=null) {
        $fileName = $this->commonfilemanager->uploadLocalFile(USER_DIR,$_FILES['Filedata'],MAX_LOGO_UPLOAD_SIZE);
        /*
         * See if we have an error
         */
        $responseArray = explode('|',$fileName);
        if ($responseArray[0] == 'error') {
            $responseMessage = $responseArray[1];
            echo '{"Result":"error","Message":"'.$responseMessage.'"}';
        } else {
			/*
			 * Get existing avatar file and delete it
			 */
			if ($type == 'user') {
				$userid = $this->session->userdata('userid');
				$settingName = 'avatarMember';
				$group = 'member';
                $section = 'user';
			} elseif ($type == 'property') {
				$userid = $this->session->userdata('userid');
				$settingName = 'avatarMember';
				$group = 'member';
                $section = 'user';
			}
			$accountAvatarArray = $this->settings_update->getSettings($userid,$settingName,NULL);
			$this->commonfilemanager->deleteLocalFile(USER_DIR,$accountAvatarArray['SettingValue']);

            /*
             * Resize avatar if necessary
             */
            $avatarPath = USER_DIR.$fileName;
            $avatarSize = getimagesize($avatarPath);
            if ($avatarSize[0]>80) {
                $this->load->library('image_lib');

                $config['source_image']   = $avatarPath;
                $config['create_thumb']   = FALSE;
				$config['thumb_marker']   = '';
                $config['maintain_ratio'] = FALSE;
                $config['master_dim']     = 'auto';
                $config['width']          = 80;
                $config['height']         = 80;

                //$this->image_lib->initialize($config);
                $this->load->library('image_lib');
                $this->image_lib->initialize($config);
                $this->image_lib->resizeThenCrop();
            }
			/*
			 * Save avatar url in settings
			 */
			$avatarURL = AWS_URL_ROOT.'/'.ACCOUNT_FILE_FOLDER.'/'.$fileName;
			$settingsArray = array('type'=>$settingName,'group'=>$group,'value'=>$avatarURL,'section'=>$section);
			$this->SettingsUpdate->saveSettings($userid,$this->session->userdata('userid'),$settingsArray);

			/*
			 * Save avatar to session variable
			 */
			$sessionData = array($settingName=>$fileName);
			$this->session->set_userdata($sessionData);

			/*
			 * Move avatar file to S3 and delete from local file system
			 */
			$this->filemanagers3->moveFileToS3($avatarPath,$fileName,ACCOUNT_FILE_FOLDER);
			$this->commonfilemanager->deleteLocalFile(USER_DIR,$fileName);
            echo '{"Result":"success","Message":"'.$avatarURL.'"}';
        }
	}

    function updateFileDescription($fileID) {
        if (is_numeric($fileID) && $fileID>0) {
            $fileDescription = fieldToDB(strip_tags($this->input->post('fileDescription', TRUE)), TRUE,TRUE,FALSE,TRUE);
            $this->file_maintenance->updateFileDescription($fileID,$fileDescription);
        }
    }

	 /**
      * getFileItems : gets all records to create the initial tree state
	  *
      * @access	public
	  * @param  string $renderType how to render the results json|array
      * @return mixed  JSON string or PHP array
      */
	 function getFileItems($nodeID=NULL,$parentID=NULL) {
        if (isset($_POST['id'])) {
            $nodeID = $_POST['id'];
        } elseif ($nodeID == null) {
            $nodeID = 0;
        }
        if (isset($_POST['parentID'])) {
            $parentID = $_POST['parentID'];
        } elseif ($parentID == null) {
            $parentID = 0;
        }
		$renderType = 'json';
		$fileArray = $this->file_maintenance->getFileItems($nodeID,$this->session->userdata('accountUserid'));
		if ($renderType == 'json') {
            $jsonString = '';
			foreach($fileArray as $file) {
				if (!empty($file['FileNameActual'])) {
					if ($file['Folder'] == 0) {
						$fileIconArray = get_mime_icon_by_extension($file['FileNameActual']);
						$file['iconSmall'] = $fileIconArray[0];
						$file['iconBig']   = $fileIconArray[1];
						$state     = '"state": "closed", ';
						$file['fileExt']   = $fileIconArray[2];
                        $file['thumbnailLink'] = '';

                        $fileExt = strtolower($file['fileExt']);
                        if ($fileExt == 'jpg' || $fileExt == 'jpeg' || $fileExt == 'gif' || $fileExt == 'png') {
                            /*
                             * See if we can get the thumbnail
                             */
                             $fileNameParts = pathinfo($file['FileNameActual']);
                             $extension = $fileNameParts['extension'];
                             $fileNameNoExtension = $fileNameParts['filename'];
                             $thumbFileName = $fileNameNoExtension.'_thumb.'.$extension;
                             $file['thumbnailLink'] = $this->filemanagers3->createFileLink($thumbFileName);
                        }

						$file['fileLink']  = $this->filemanagers3->createFileLink($file['FileNameActual']);
					} else {
						$file['iconSmall'] = 'f.png';
						$file['iconBig']   = '';
						$state     = '"state": "closed", ';
						$file['fileExt']   = 'folder';
						$file['fileLink']  = '';

                        if ($file['FileService'] == 1) {
                            switch ($file['FileTitle']) {
                                case 'Google Docs':
                                    $file['ParentID'] = 'google';
                                    break;
                                case 'Dropbox':
                                    $file['ParentID'] = 'dropbox';
                                    break;
                            }
                        }
					}
                    $file['UploaderName'] = $file['NameFirst'].' '.$file['NameLast'];
                    $file['FileSize'] = byte_format($file['FileSize']);
                    $file['DateEntry'] = convertMySQLToGMT($file['DateUpdate'],'pull','M j, Y');
                    $jsonString .= json_encode($file).',';
				}
                
			}

            $jsonString = substr($jsonString,0,-1);

            /*
             * Get folder path
             */

             /*
            $pathArray = array();
            if ($parentID>0 && $nodeID>0) {
                $pathArray = FileManager::getFolderPath($parentID);
            } elseif ($nodeID>0 && $parentID == 0) {
                $pathArray[0] = array('FolderName' => lang('files_all_files'), 'NodeID' => 0, 'ParentID' => 0);
            }
              *
              */

            $pathArray = FileManager::getFolderPath($parentID,$nodeID);
			$jsonString = '{"Path":'.json_encode($pathArray).',"Files":['.$jsonString.']}';
			if (count($fileArray) == 0) {
				$jsonString = '{"Path":'.json_encode($pathArray).',"Files":[]}';
			}
			echo $jsonString;
		} else {

		}
	 }

     function getFolderPath($parentID,$nodeID) {
         $filePathArray = $this->file_maintenance->getFolderPath($parentID,$nodeID,$this->session->userdata('accountUserid'));
         /*
          * Loop over results and create path
          */
         $pathArray = array();
         $a = 0;
         foreach ($filePathArray as $nodeArray) {
            if ($nodeArray['FileID'] == $parentID || ($nodeArray['FileID'] == $nodeID && $nodeID > 0) ) {
                $folderName = $nodeArray['FileNameActual'];
                $nodeID     = $nodeArray['FileID'];
                $parentID   = $nodeArray['ParentID'];
                
                $pathArray[$a] = array('FolderName' => $folderName, 'NodeID' => $nodeID, 'ParentID' => $parentID);
                $a++;
            }
         }
         $pathArray[$a] = array('FolderName' => lang('files_all_files'), 'NodeID' => 0, 'ParentID' => 0);
         return array_reverse($pathArray);
     }

     function getNumberOfItemsInFolder($nodeID) {
         $numberOfItems = $this->file_maintenance->getNumberOfItemsInFolder($nodeID,$this->session->userdata('accountUserid'));
         echo $numberOfItems;
     }

     function setNodeIDInSession($nodeID) {
         $this->session->set_userdata('fileTreeNodeID',$nodeID);
     }

	 /**
      * getFileItem : gets information for 1 file
	  *
      * @access	public
	  * @param  int    $itemID ID of file we're looking for
	  * @param  string $renderType how to render the results json|array
      * @return mixed  JSON string or PHP array
      */
	 function getFileItem($fileID,$renderType='json',$return=0) {
		$fileArray = $this->file_maintenance->getFileItemInformation($fileID);

        if ($fileArray['FileService'] == 1) {
            $fileIconArray = get_mime_icon_by_fileservice_path($fileArray['FileServicePath']);
            $fileArray['fileLink'] = $fileArray['FileServicePath'];
        } else {
            $fileIconArray = get_mime_icon_by_extension($fileArray['FileNameActual']);
            $fileArray['fileLink'] = $this->filemanagers3->createFileLink($fileArray['FileNameActual']);
        }
        $fileArray['FileClassName'] = 'icon_file_'.$fileIconArray[2];
        $fileArray['iconSmall']     = $fileIconArray[0];
		$fileArray['iconBig']       = $fileIconArray[1];
		$fileArray['fileExt']       = $fileIconArray[2];
		$fileArray['UploaderName']  = $fileArray['NameFirst'].' '.$fileArray['NameLast'];
        $fileArray['FileSize']      = byte_format($fileArray['FileSize']);
        $fileArray['DateEntry']     = convertMySQLToGMT($fileArray['DateUpdate'],'pull','M j, Y g:i A');
		if ($renderType == 'json') {
            $jsonString = json_encode($fileArray);

            if ($return == 0) {
                echo $jsonString;
            } else {
                return $jsonString;
            }
		} else {
			
		}
	 }

	 /**
      * createFolder : gets all records to create the initial tree state
	  *
      * @access	public
	  * @param  string $renderType how to render the results json|array
      * @return mixed  JSON string or PHP array
      */
	 function createNewFolder() {
         $parentNodeID    = $_POST['parentNodeID'];
         $currentParentID = $_POST['currentParentID'];
         $folderName      = fieldToDB($this->input->post('folderName', TRUE));
         $action          = $_POST['action'];
         $folderID = 0;
         if (isset($_POST['folderID'])) {
            $folderID        = $_POST['folderID'];
         }
         if (!empty($folderName)) {
             $newFolderID     = $this->file_maintenance->saveFolder($parentNodeID,$folderName,$action,$folderID);
             /*
              * Pull down the new file view
              */
             FileManager::getFileItems($parentNodeID,$currentParentID);
         }
	 }

	 /**
      * createFolder : gets all records to create the initial tree state
	  *
      * @access	public
	  * @param  string $renderType how to render the results json|array
      * @return mixed  JSON string or PHP array
      */
	 function renameFolder($folderName,$folderID) {
		 if (!empty($folderName)) {
			$this->file_maintenance->renameFolder($this->input->xss_clean($folderName),$folderID);
		 }
	 }

	 /**
      * dropFile : update file order when droped from a drag-and-drop operation
	  *
      * @access	public
	  * @param  int    $itemNodeID ID of item we're moving
	  * @param  int    $targetNodeID ID of node we're moving the item to
      * @return void
      */
	 function dropFileItem($itemNodeID,$targetNodeID) {
		 $this->file_maintenance->dropFileItem($itemNodeID,$targetNodeID);
	 }	 

	 function serveFileForDownload() {
         $itemID = $_POST['itemID'];
         $fileURL = AWS_URL_ROOT.'/'.$this->session->userdata('userDir').'/'.$hashFileName;
		 $this->filemanagers3->serveFileForDownload($hashFileName);
		 
		 /*
		 header('GET /2011.jpg HTTP/1.1');
		 header('Host: '.AWS_URL_ROOT);
		 header('Date: Wed, 01 Mar  2006 12:00:00 GMT');
		 header('Authorization: AWS '.AWS_ACCESS_KEY.':'.AWS_SECRET_KEY);
		 header('Content-Disposition: attachment; filename="'.$hashFileName.'"');
		 var_dump(apache_response_headers());
		 
         header('Location: '.$fileURL);
         force_download_file('newFile.doc',$fileURL,'20000','doc');
		 */
	 }

     function makeDownloadForm() {
		$this->load->view('includes/downloadForm');
	}

	/**
      * deleteItem : delete folder or file. If file, then delete from S3
	  *
      * @access	public
	  * @param  int    $itemNodeID Database ID of item (folder or file)
	  * @param  string $itemType   Folder or File
      * @return float  $spaceLeft  Amount of file space left for this account.
      */
	 function deleteItem($itemNodeID,$itemParentID=NULL,$itemType='file',$deleteContents=FALSE) {
         $fileDeleteInfoArray = $this->file_maintenance->deleteItem($itemNodeID,$itemParentID,$itemType,$deleteContents);
		 /*
		  * Now remove any files from S3
		  */
         $this->filemanagers3->deleteItemFromS3($fileDeleteInfoArray);
         /*
          * Recalculate file usage
          */
         FileManager::getFileSpaceUsage('json',0);
	 }

	 /**
      * attachFilesToItem : attach files to an item like a project, task, message, etc.
	  *
      * @access	public
	  * @param  int    $itemID       ID of the item to which we are attaching files.
	  * @param  string $itemType     Type of item to which we are attaching files (project, task, message, etc.)
	  * @return void                 For now
      */
	 function attachFilesToItem($itemID,$itemType,$fileTypes=null,$fileIDString=null,$fromMobile=null) {
        if ($fileTypes == null) {
            $fileTypes = $_POST['fileTypes'];
        }
        if ($fileIDString == null) {
            $fileIDString = $_POST['fileIDString'];
        }

        if ($fileTypes == 'local') {
            $fileIDArray = explode('|',$fileIDString);
        } elseif ($fileTypes == 'fileservice') {
            $fileIDArray = json_decode($fileIDString,TRUE);
        }
        $fileLinkIDString = '';
		if (is_array($fileIDArray)) {
			foreach($fileIDArray as $fileID) {
                if ($fileTypes == 'local') {
                    if (!empty($fileID) && $this->file_maintenance->checkForAttachedFile($fileID,$itemID,$itemType)) {
                        $fileLinkID = $this->file_maintenance->attachFilesToItem($fileID,$itemID,$itemType,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
                    }
                } elseif ($fileTypes == 'fileservice') {
                    /*
                     * See if this file is already in the database
                     */
                     $fileName = $fileID['FileName'];
                     $filePath = $fileID['FilePath'];
                     $fileType = $fileID['FileType'];
                     $existingFileID = $this->file_maintenance->checkForDuplicateFileServiceFile($fileName,$filePath,$this->session->userdata('accountUserid'));
                     if ($existingFileID == FALSE) {
                         /*
                          * Insert this file into the database
                          */
                        $fileDataArray['action']          = 'add';
                        $fileDataArray['ParentID']        = 0;
                        $fileDataArray['accountUserid']   = $this->session->userdata('accountUserid');
                        $fileDataArray['FileNameActual']  = $fileName;
                        $fileDataArray['FileServicePath'] = $filePath;
                        $fileDataArray['FileSize']        = 0;
                        $fileDataArray['FileType']        = $fileType;
                        $fileDataArray['FileService']     = 1;
                        $fileDataArray['userid']          = $this->session->userdata('userid');
                        $fileID = $this->file_maintenance->insertFileData($fileDataArray);
                        $fileLinkID = $this->file_maintenance->attachFilesToItem($fileID,$itemID,$itemType,$this->session->userdata('userid'));

                     } else {
                        $fileLinkID = $this->file_maintenance->attachFilesToItem($existingFileID,$itemID,$itemType,$this->session->userdata('userid'));
                     }
                }
                $fileLinkIDString .= $fileLinkID.'|';
			}
            $outString = $fileLinkIDString;
		} else {
            $outString = '0';
        }

        if ($fromMobile != TRUE) {
            echo $outString;
        }
	 }

	 /**
      * attachFilesToItem : attach files to an item like a project, task, message, etc.
	  *
      * @access	public
	  * @param  int    $itemID       ID of the item to which we are attaching files.
	  * @param  string $itemType     Type of item to which we are attaching files (project, task, message, etc.)
	  * @return void                 For now
      */
	 function removeAttachedFileFromItem($fileLinkID=NULL,$fileID=NULL,$itemID=NULL,$itemType=NULL) {
		$this->file_maintenance->removeAttachedFileFromItem($fileLinkID,$fileID,$itemID,$itemType);
	 }
     
     function removeAttachedFilesFromItem($fileLinkIDString) {
         if (!empty($_POST['fileLinkIDString'])) {
             $fileLinkIDString = $_POST['fileLinkIDString'];
         }
        $fileLinkIDArray = explode('|',$fileLinkIDString);
        foreach($fileLinkIDArray as $fileLinkID) {
            if ($fileLinkID>0) {
                $this->file_maintenance->removeAttachedFileFromItem($fileLinkID);
            }
        }
         
		$this->file_maintenance->removeAttachedFileFromItem($fileLinkID,$fileID,$itemID,$itemType);
	 }

     function removeStrayFileLinks() {
         $this->file_maintenance->removeStrayFileLinks($this->session->userdata('userid'),$this->session->userdata('accountUserid'));
     }

	 function getFilesForSomething($itemID,$itemType,$renderType='json',$return=0) {
        $fileArray = $this->commonfilemanager->getFilesForSomething($itemID,$itemType,'array',1);
        
        if ($renderType == 'json') {
			$return = '{"Files":'.json_encode($fileArray).'}';
		} elseif ($renderType == 'array') {
			$return = $fileArray;
		}

		if ($return == 0) {
			echo $return;
		} else {
			return $return;
		}
	}

    function getFileSpaceUsage($renderType='json',$return=0) {
        global $accountLevelArray;
        $fileUseOver = '0';
        $fileUseTotal = $this->file_maintenance->getFileSpaceUsage($this->session->userdata('accountUserid'));
        $fileAccountMax = $accountLevelArray[$this->session->userdata('accountLevel')]['fileSpace'];

        $fileUsePercentage = '0';
        if ($fileAccountMax != 0) {
            $fileUsePercentage = number_format(ceil(($fileUseTotal/$fileAccountMax)*100),0);
        }

        $fileUseTotalHuman   = byte_format($fileUseTotal);
        $fileAccountMaxHuman = byte_format($fileAccountMax);

        if ($fileAccountMax == 'unlimited') {
            $fileAccountMaxHuman = 'unlimited';
        }

        if ($fileUsePercentage>=100) {
            $fileUseOver = '1';
        }

        $fileUseArray = array(
            'FileUseTotal'      => $fileUseTotalHuman,
            'AccountMax'        => $fileAccountMaxHuman,
            'FileUsePercentage' => $fileUsePercentage,
            'FileUseRaw'        => $fileUseTotal,
            'FileUseOver'       => $fileUseOver
        );

        if ($renderType == 'json') {
            if ($return == 0) {
                echo json_encode($fileUseArray);
            } else {
                return json_encode($fileUseArray);
            }
        } else {
            return $fileUseArray;
        }
    }

    function oAuthFileService($fileService,$return=0) {
        $thisPage = $this->uri->uri_string();
        $folderID = NULL;
        if (isset($_POST['thisPage']) && !empty($_POST['thisPage'])) {
            $thisPage = $_POST['thisPage'];
        }
        if (isset($_POST['folderID']) && !empty($_POST['folderID'])) {
            $folderID = $_POST['folderID'];
        }
        $returnURI = SITE_URL.$thisPage;

        if ($fileService == 'google') {
            $googleToken = $this->session->userdata('googleToken');
            if (!empty($googleToken)) {
                $this->googledocs->getGoogleDocs($folderID);
            } else {
                $scope = 'http://docs.google.com/feeds/';
                $secure = false;
                $session = true;
                $authLink = Zend_Gdata_AuthSub::getAuthSubTokenUri($returnURI, $scope, $secure, $session);
                if ($return == 1) {
                    return $authLink;
                } else {
                    echo '{"AuthLink":"'.$authLink.'"}';
                }
            }
        } elseif ($fileService == 'dropbox') {
            $dc = new DropboxClient(DROPBOX_CONSUMER_KEY,DROPBOX_CONSUMER_SECRET);
            $dropboxToken = $this->session->userdata('dropboxToken');
            if(empty($dropboxToken)) {
                $dc->authorize($returnURI);
            } else {
                $token = $dc->accessToken($_REQUEST['oauth_token']);
                /*
                Now you can save the access token
                $token is an array with two elements 'token' and 'secret'
                */
            }

        }
    }

    function updateFileShareStatus($fileLinkID) {
        $newShareStatus = $this->file_maintenance->updateFileShareStatus($fileLinkID);
        echo '{"ShareStatus":"'.$newShareStatus.'"}';
    }
}
?>