<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pricing extends CI_Controller {
	function _init() {
        $data['child']           = TRUE;
        $data['headerTextImage'] = 'txtPricingSignupHeader_public.png';
        $data['rightColumnComponents'] = array('goodPress');
        $data['jsFileArray']           = array();
        $data['pageTitle']             = '15 Day Free Trial';

        return $data;
    }

    function index() {
        $this->load->model('public_site','',true);
        global $accountPricePrev,$accountPriceNext,$datePriceChange;
		$data = Pricing::_init();

        $data['currentPrices'] = $accountPricePrev;
        if (days_between_dates(strtotime($datePriceChange),time())>0) {
            $data['currentPrices'] = $accountPriceNext;
        }

        $this->load->view('Pricing',$data);
    }   
}
?>