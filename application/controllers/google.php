<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Google extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

		
        $this->load->library('application/CommonMessages');
		$this->load->library('application/CommonEmail');

		$this->load->helper('zend');
        
        Zend_Loader::loadClass('Zend_Gdata_AuthSub');
        Zend_Loader::loadClass('Zend_Gdata_Docs');
	}

	function _init() {
		$data['page']                  = 'finances';
		$data['pageTitle']             = 'Google Doc Test';
		$data['pageSubTitle']          = '';
		$data['wysiwyg']               = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsGoogle.js');
		$data['pageIconClass']         = '';
		$data['pageLayoutClass']       = 'withRightColumn';
		return $data;
	}   

	function index() {        
        $gdataDocs = new Zend_Gdata_Docs();
		$data = Google::_init();
        $data['authLink'] = GoogleDocs::getAuthSubUrl();
		$this->load->view('google/GoogleDocTest',$data);
	}

    function getAuthSubUrl($return=0) {
        $googleToken = $this->session->userdata('googleToken');
        if (!empty($googleToken)) {
            Google::getGoogleDocs();
        } else {
            $thisPage = $this->uri->uri_string();
            if (isset($_POST['thisPage']) && !empty($_POST['thisPage'])) {
                $thisPage = $_POST['thisPage'];
            }
            $next = SITE_URL.$thisPage;
            $scope = 'http://docs.google.com/feeds/';
            $secure = false;
            $session = true;
            $authLink = Zend_Gdata_AuthSub::getAuthSubTokenUri($next, $scope, $secure, $session);
            if ($return == 1) {
                return $authLink;
            } else {
                echo '{"AuthLink":"'.$authLink.'"}';
            }
        }
    }

    function getGoogleDocs() {
        $httpClient = Zend_Gdata_AuthSub::getHttpClient($this->session->userdata('googleToken'));
		$gdClient = new Zend_Gdata_Docs($httpClient);

		$docFeed = $gdClient->getDocumentListFeed();
		$a=0;
		foreach ($docFeed->entries as $entry) {
			/*
			 *  Find the URL of the HTML view of the document.
			 */
			$alternateLink = '';
			$docExtension = '';
			foreach($entry->link as $link) {
                if ($link->getRel() === 'alternate') {
					$alternateLink = $link->getHref();
					if (preg_match('/spreadsheet/',$alternateLink)) {
						$docExtension = 'xls';
					} elseif (preg_match('/present/',$alternateLink)) {
						$docExtension = 'ppt';
					} elseif (preg_match('/docs/',$alternateLink)) {
						$docExtension = 'doc';
					}
				}
			}
            $author = $entry->getAuthor();            

            $docArray[$a]['FileID']         = 0;
            $docArray[$a]['ParentID']       = 'fileservice';
			$docArray[$a]['FileTitle']      = $entry->title->text;
            $docArray[$a]['FileNameActual'] = $entry->title->text;
            $docArray[$a]['FileSize']       = '';
            $docArray[$a]['FileType']       = $docExtension;
            $docArray[$a]['FileDesc']       = '';
            $docArray[$a]['Tags']           = '';
            $docArray[$a]['Public']         = 0;
            $docArray[$a]['Folder']         = 0;
            $docArray[$a]['FolderState']    = 0;
            $docArray[$a]['ImportFolder']   = 0;
            $docArray[$a]['DateEntry']      = convertIcalToHuman($entry->published->text,'M j, Y g:i A');
            $docArray[$a]['DateUpdate']     = convertIcalToHuman($entry->updated->text,'M j, Y g:i A');
            $docArray[$a]['PID']            = '';
            $docArray[$a]['UploaderName']   = $author[0]->getName()->getText();
            $docArray[$a]['iconSmall']      = '';
            $docArray[$a]['iconBig']        = '';
			$docArray[$a]['fileLink']       = $alternateLink;
			$docArray[$a]['fileExt']        = $docExtension;
            $docArray[$a]['FileService']    = 1;
			$a++;
		}
		
        $jsonString = '{"Path":[{"FolderName":"All files","NodeID":0,"ParentID":0},{"FolderName":"Google Docs","NodeID":"0","ParentID":"google"}],"Files":';
        $jsonString .= json_encode($docArray);
        $jsonString .= '}';
		echo $jsonString;
   }

	function testGoogleEmail() {
		$data = Google::_init();
		$this->load->view('GoogleEmailTest',$data);
	}

	function getGmail() {
        $mailArray = array(
			'host'     => 'pop.1and1.com',
			'user'     => 'task@creativeprooffice.com',
			'password' => 'addyG0@SE'
		);

		Zend_Loader::loadClass('Zend_Mail_Storage_Pop3');
		$gmail = new Zend_Mail_Storage_Pop3($mailArray);
		$numberOfMessages = $gmail->countMessages();

		$messageID = $numberOfMessages;
		for($i=0; $i<$numberOfMessages; $i++) {
			$message = $gmail->getMessage($messageID);

            foreach ($message->getHeaders() as $name => $value) {
                echo $name.' '.$value.'<br>';
                if ($name == 'to') {
                    echo 'To: '.$value;
                }
            }

			echo 'Subject: '.$message->subject;
            echo 'Content: '.$message->getContent();
            echo 'From: '.htmlentities($message->from);
            
			$messageID = $messageID-1;
		}
	}
}
?>