<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include(APPPATH.'libraries/excel/excel_xml.php');

class ExportExcel extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		checkAuthentication($_POST);
        loadLanguageFiles();

		$this->load->model('app_activity','',true);
		$this->load->model('user_information','',TRUE);
        $this->load->model('inventory_view','',TRUE);
        $this->load->model('places_view','',TRUE);
		$this->load->model('contacts','',TRUE);

	}

	function _init() {

	}

    function exportinventory($placeID) {
        $excel = new excel_xml();

        $currencyMark   = getCurrencyMark($this->session->userdata('currency'));
        $placeArray     = $this->places_view->getPlaceInformation($placeID);
        $inventoryArray = $this->inventory_view->getInventoryForPlace($placeID,$this->session->userdata('accountUserid'));
        $exportFileName = lang('inventory').'_'.$placeArray['PlaceName'].'.xls';
        /*
		 * Make header
		 */
		$header_style = array(
			'bold' => 1,
			'size' => '12',
			'color' => '#FFFFFF',
			'bgcolor' => '#4F81BD'
		);
		$room_style = array(
			'bold' => 1,
			'size' => '10',
			'color' => '#000000',
			'bgcolor' => '#CCCCCC'
		);
		$excel->add_style('header', $header_style);
		$excel->add_style('room', $room_style);

        /*
		 * Add row and attach header style
		 */
		$excel->add_row(array(
			lang('common_item_name'),
			lang('common_description'),
			lang('common_category'),
			lang('common_make'),
			lang('common_model'),
            lang('common_serial_number'),
            lang('common_purchase_date'),
            lang('common_condition'),
            lang('common_purchase_price'),
            lang('common_replacement_price'),
            lang('common_tags')
			), 'header');

        foreach($inventoryArray as $room) {
            
			$excel->add_row(array(
				$room['RoomName'],
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
                '',
                ''
			), 'room');

			foreach($room['Inventory'] as $item) {
                $excel->add_row(array(
                    $item['ItemName'],
                    DBToField(strip_tags($item['ItemDesc'])),
                    $item['MainCat'],
                    $item['ItemMake'],
                    $item['ItemModel'],
                    $item['ItemSerial'],
                    $item['DatePurchaseHuman'],
                    $item['ItemCondition'],
                    $item['PricePurchase'],
                    $item['PriceReplace'],
                    $item['Tags']
                ));
            }
        }
        $excel->create_worksheet(lang('inventory'));
		$xml = $excel->generate($this->session->userdata('fullName'),$this->session->userdata('userCompany'));
		$excel->download($exportFileName);

		/*
		 * Log activity: $activity=NULL,$itemID=NULL,$itemType=NULL,$userType=NULL
		 */
		$this->App_activity->logAppActivity('export',0,'inventory',$this->session->userdata('userType'));
    }

    function exportTasks($varString) {
		$excel = new excel_xml();

		/*
		 * Parse $varString and get our timesheet report array
		 */
		$varArray = explode('---',$varString);
		$projectID   = $varArray[0];
        $milestoneID = $varArray[1];
        $dateStart   = $varArray[2];
		$dateEnd     = $varArray[3];
		$userID      = $varArray[4];
		$seeOnlyMyTasks         = $varArray[5];
		$seeOnlyCompleteTasks   = $varArray[6];
		$seeOnlyIncompleteTasks = $varArray[7];


		$milestoneArray = $this->Task_view->getMilestonesForProject($projectID,1);
		$projectInfoArray = $this->Project_view->getProjectInformation($projectID);
		$userNameFilename = '';
		if ($seeOnlyMyTasks == 1) {
			$userInformationArray  = $this->Contacts->getContactPerson(NULL,$this->session->userdata('userid'));
			$userName = $userInformationArray['NameFirst'].' '.$userInformationArray['NameLast'];
			$userNameFilename = '_'.$userInformationArray['NameFirst'].'_'.$userInformationArray['NameLast'];
		}

		$exportFileName = lang('task_tasks').'_'.$projectInfoArray['Title'].$userNameFilename.'.xls';

		/*
         * Let's create a new array filtered by our variables
         */
         $a=0;
         $b=0;
         $c=0;

         $newMilestoneArray = array();
		 foreach($milestoneArray as $milestone) {
			$milestone = $this->commontask->renderMilestone($milestone,'array');
			$milestone['Tasks'] = $this->commontask->renderTasks($milestone['Tasks'],'array');
			$taskArray = $milestone['Tasks'];
            foreach($taskArray as $task) {
				$keep = 0;
                $teamMemberArray = $task['TeamMembers'];
                foreach($teamMemberArray as $teamMember) {
                    if ($seeOnlyMyTasks == 1 && $teamMember['UID'] == $this->session->userdata('userid')) {
                        $keep = 1;
                    }
                }
				if ($seeOnlyCompleteTasks == 1) {
					if ($task['Status'] == 2) {
						$keep = 1;
					} else {
						$keep = 0;
					}
				}
				if ($seeOnlyIncompleteTasks == 1) {
					if ($task['Status'] != 2) {
						$keep = 1;
					} else {
						$keep = 0;
					}
				}
                if ($seeOnlyMyTasks == 0 && $seeOnlyCompleteTasks == 0 && $seeOnlyIncompleteTasks == 0) {
                    $keep = 1;
                }

				if ($keep == 1) {
					$newMilestoneArray[$a]['Tasks'][$b] = $task;
					$b++;
				}
            }
			/*
			 * Should we even render this milestone?
			 */
			if (isset($newMilestoneArray[$a]['Tasks']) && count($newMilestoneArray[$a]['Tasks'])>0) {
				$newMilestoneArray[$a]['Title']       = $milestone['Title'];
				$newMilestoneArray[$a]['Description'] = $milestone['Description'];
				$newMilestoneArray[$a]['Date_Start']  = $milestone['Date_Start'];
				$newMilestoneArray[$a]['Date_End']    = $milestone['Date_End'];
				$a++;
			}
        }

		/*
		 * Make header
		 */
		$header_style = array(
			'bold' => 1,
			'size' => '12',
			'color' => '#FFFFFF',
			'bgcolor' => '#4F81BD'
		);
		$milestone_style = array(
			'bold' => 1,
			'size' => '10',
			'color' => '#000000',
			'bgcolor' => '#CCCCCC'
		);
		$excel->add_style('header', $header_style);
		$excel->add_style('milestone', $milestone_style);

		/*
		 * Add row and attach header style
		 */
		$excel->add_row(array(
			lang('task_task'),
			lang('common_date_start'),
			lang('common_date_end'),
			lang('task_description'),
			lang('task_estimated_time'),
			lang('task_hours_complete'),
			lang('common_priority'),
			lang('common_status'),
			lang('team_members'),
			lang('files_files')
			), 'header');


		for($c=0;$c<=($a-1);$c++) {
			$milestone = $newMilestoneArray[$c];

			$excel->add_row(array(
				$milestone['Title'],
				$milestone['Date_Start'],
				$milestone['Date_End'],
				'',
				'',
				'',
				'',
				'',
				'',
				''
			), 'milestone');

			foreach($milestone['Tasks'] as $task) {
				/*
				 * Create team members string
				 */
				$teamString = '';
				foreach($task['TeamMembers'] as $teamMember) {
					$teamString .= $teamMember['NameFirst'].' '.$teamMember['NameLast'].', ';
				}
				$teamString = substr($teamString,0,-2);
				/*
				 * Create files string
				 */
				$fileString = '';
				foreach($task['Files'] as $file) {
					$fileString .= $file['FileNameActual'].', ';
				}
				$fileString = substr($fileString,0,-2);
				$excel->add_row(array(
					$task['Title'],
					$task['Date_Start'],
					$task['Date_End'],
					DBToField(strip_tags($task['Description'])),
					$task['HoursEstimated'],
					$task['HoursActual'],
					$task['PriorityText'],
					$task['StatusHuman'],
					$teamString,
					$fileString
				));
			}
		}
		$excel->create_worksheet(lang('task_tasks'));
		$xml = $excel->generate($this->session->userdata('fullName'),$this->session->userdata('userCompany'));
		$excel->download($exportFileName);

		/*
		 * Log activity: $activity=NULL,$itemID=NULL,$itemType=NULL,$userType=NULL
		 */
		$this->App_activity->logAppActivity('export',0,'tasks',$this->session->userdata('userType'));
	}

    function exportFinanceReport($varString) {
        $excel = new excel_xml();

		$currencyMark = getCurrencyMark($this->session->userdata('currency'));
        $reportArray = array();

        $reportArray = explode('---',$varString);
        $reportType = $reportArray[0];
        $reportYear = $reportArray[1];
        $reportDateStart = $reportArray[2];
        $reportDateEnd   = $reportArray[3];
        $chartType = NULL;

        switch ($reportType) {
            case 'repInvoicedReceived':
                $fullTitle = lang('reports_invoiced_received').' ('.$reportYear.')';
                $workSheetTitle = lang('reports_invoiced_received');
                $reportArray['Data'] = $this->Finance_reports->getReportDataInvoicedReceived($this->session->userdata('accountUserid'),$reportYear);
                $reportArray['BarLabels'] = array(lang('reports_invoiced'),lang('reports_received'));
                $reportArray['ReportType'] = $reportType;
                $newReportArray = ExportExcel::createFinanceReportBarGrid($reportArray);
                $chartType = 'bar';
                break;
            case 'repIncomeExpenses':
                $fullTitle = lang('reports_income_expenses').' ('.$reportYear.')';
                $workSheetTitle = lang('reports_income_expenses');
                $reportArray['Data'] = $this->Finance_reports->getReportDataIncomeExpenses($this->session->userdata('accountUserid'),$reportYear);
                $reportArray['BarLabels'] = array(lang('finance_expenses'),lang('reports_income'));
                $reportArray['ReportType'] = $reportType;
                $newReportArray = ExportExcel::createFinanceReportBarGrid($reportArray);
                $chartType = 'bar';
                break;
            case 'repInvoicedByProject':
                $fullTitle = lang('reports_invoiced_by_project').' ('.MySQLDateToJS($reportDateStart).' to '.MySQLDateToJS($reportDateEnd).')';
                $workSheetTitle = lang('reports_invoiced_by_project');
                $reportArray['Data'] = $this->Finance_reports->getReportDataInvoicedByProjectClient($this->session->userdata('accountUserid'),$reportDateStart,$reportDateEnd,'project');
                $newReportArray = ExportExcel::createFinanceReportPieGrid($reportArray);
                $chartType = 'pie';
                break;
            case 'repInvoicedByClient':
                $fullTitle = lang('reports_invoiced_by_client').' ('.MySQLDateToJS($reportDateStart).' to '.MySQLDateToJS($reportDateEnd).')';
                $workSheetTitle = lang('reports_invoiced_by_client');
                $reportArray['Data'] = $this->Finance_reports->getReportDataInvoicedByProjectClient($this->session->userdata('accountUserid'),$reportDateStart,$reportDateEnd,'client');
                $newReportArray = ExportExcel::createFinanceReportPieGrid($reportArray);
                $chartType = 'pie';
                break;
            case 'repExpensesByProject':
                $fullTitle = lang('reports_expenses_by_project').' ('.MySQLDateToJS($reportDateStart).' to '.MySQLDateToJS($reportDateEnd).')';
                $workSheetTitle = lang('reports_expenses_by_project');
                $reportArray['Data'] = $this->Finance_reports->getReportDataExpensesBySegment($this->session->userdata('accountUserid'),$reportDateStart,$reportDateEnd,'project');
                $newReportArray = ExportExcel::createFinanceReportPieGrid($reportArray);
                $chartType = 'pie';
                break;
            case 'repExpensesByClient':
                $fullTitle = lang('reports_expenses_by_client').' ('.MySQLDateToJS($reportDateStart).' to '.MySQLDateToJS($reportDateEnd).')';
                $workSheetTitle = lang('reports_expenses_by_client');
                $reportArray['Data'] = $this->Finance_reports->getReportDataExpensesBySegment($this->session->userdata('accountUserid'),$reportDateStart,$reportDateEnd,'client');
                $newReportArray = ExportExcel::createFinanceReportPieGrid($reportArray);
                $chartType = 'pie';
                break;
            case 'repExpensesByCategory':
                $fullTitle = lang('reports_expenses_by_category').' ('.MySQLDateToJS($reportDateStart).' to '.MySQLDateToJS($reportDateEnd).')';
                $workSheetTitle = lang('reports_expenses_by_category');
                $reportArray['Data'] = $this->Finance_reports->getReportDataExpensesBySegment($this->session->userdata('accountUserid'),$reportDateStart,$reportDateEnd,'category');
                $newReportArray = ExportExcel::createFinanceReportPieGrid($reportArray);
                $chartType = 'pie';
                break;
            case 'repExpensesByVendor':
                $fullTitle = lang('reports_expenses_by_vendor').' ('.MySQLDateToJS($reportDateStart).' to '.MySQLDateToJS($reportDateEnd).')';
                $workSheetTitle = lang('reports_expenses_by_vendor');
                $reportArray['Data'] = $this->Finance_reports->getReportDataExpensesBySegment($this->session->userdata('accountUserid'),$reportDateStart,$reportDateEnd,'vendor');
                $newReportArray = ExportExcel::createFinanceReportPieGrid($reportArray);
                $chartType = 'pie';
                break;
        }
        //print_r($newReportArray);
        $exportFileName = cleanString($workSheetTitle).'.xls';

		/*
		 * Make header
		 */
		$header_style = array(
			'bold' => 1,
			'size' => '12',
			'color' => '#FFFFFF',
			'bgcolor' => '#4F81BD'
		);
		$excel->add_style('header', $header_style);

        if ($chartType == 'pie') {
            $data = $newReportArray['Data'];
            $excel->add_row(array(
                $fullTitle,
                '',
                ''
                ), 'header');
            $excel->add_row(array(
                'Project',
                'Percentage',
                'Total'
                ), 'header');
            foreach($data as $row) {
                $excel->add_row(array(
                    $row['Label'],
                    $row['Percentage'],
                    $row['Total']
                ));
                $a++;
            }
            $excel->add_row(array(
                'Total',
                '',
                $newReportArray['Total']
            ), 'header');
         } elseif ($chartType == 'bar') {
            $data = $newReportArray;
            $excel->add_row(array(
                $fullTitle,
                '',
                ''
                ), 'header');
            $excel->add_row(array(
                lang('common_month'),
                $reportArray['BarLabels'][0],
                $reportArray['BarLabels'][1]
            ), 'header');

            for ($a=0;$a<=11;$a++) {
                $excel->add_row(array(
                    $data['Series1'][$a]['Month'],
                    $data['Series1'][$a]['Total'],
                    $data['Series2'][$a]['Total']
                ));
            }
             /*
              * Total row
              */
            $excel->add_row(array(
                lang('common_total'),
                $data['TotalSeries1'],
                $data['TotalSeries2']
            ), 'header');
         }

        $excel->create_worksheet($workSheetTitle);
		$xml = $excel->generate($this->session->userdata('fullName'),$this->session->userdata('userCompany'));
		$excel->download($exportFileName);

		/*
		 * Log activity: $activity=NULL,$itemID=NULL,$itemType=NULL,$userType=NULL
		 */
		$this->App_activity->logAppActivity('export',0,'finance',$this->session->userdata('userType'));
    }

    function createFinanceReportPieGrid($reportArray) {
        $grandTotal = 0;
        $reportData = $reportArray['Data'];
        foreach($reportData as $reportItem) {
            $grandTotal += $reportItem['Total'];
        }

        $b = 0;
        foreach($reportData as $reportItem) {
            $total = floatval($reportItem['Total']);
            $itemPercentage = number_format(($reportItem['Total']/$grandTotal)*100,1);
            $reportArray['Data'][$b]['Percentage'] = $itemPercentage;
            $reportArray['Data'][$b]['Total'] = number_format($total,2,'.','');
            $reportArray['Data'][$b]['Label'] = stripslashes(entities_to_quotes($reportItem['Label']));

            $b++;
        }
        $reportArray['Total'] = number_format($grandTotal,2,'.','');
        return $reportArray;
    }

    function createFinanceReportBarGrid($reportArray) {
        $reportType = $reportArray['ReportType'];
        if ($reportType == 'repInvoicedReceived') {
            $series1Array = $reportArray['Data']['Invoiced'];
            $series2Array = $reportArray['Data']['Received'];
        } else {
            $series1Array = $reportArray['Data']['Expenses'];
            $series2Array = $reportArray['Data']['Income'];
        }

        $series1ChartData = array();
        $series2ChartData = array();

        $resultsArray = array();
        $b=0;

        $series1Total = 0;
        $series2Total = 0;
        for($a=1;$a<=12;$a++) {
            $gotMonth = FALSE;
            $resultsArray['Series1'][$b]['MonthNum'] = $a;
            foreach($series1Array as $data) {
                if ($data['MonthNum'] == $a) {
                    if ($reportType == 'repInvoicedReceived') {
                        $totalDataNum = $data['TotalInvoiced'];
                        $resultsArray['Series1'][$b]['Total'] = $data['TotalInvoiced'];
                        $series1ChartData[$b] = floatval($data['TotalInvoiced']);
                    } else {
                        $totalDataNum = $data['TotalExpenses'];
                        $resultsArray['Series1'][$b]['Total'] = $data['TotalExpenses'];
                        $series1ChartData[$b] = floatval($data['TotalExpenses']);
                    }

                    $gotMonth = TRUE;
                }
            }

            if ($gotMonth == FALSE) {
                $resultsArray['Series1'][$b]['Total'] = "0";
                $series1ChartData[$b] = 0;
            }
            $series1Total += $totalDataNum;
            $resultsArray['Series1'][$b]['Month'] = date('M',mktime(0, 0, 0, $a, 1, date('Y')));
            $resultsArray['Series1'][$b]['Month'] = lang('cal_'.strtolower($resultsArray['Series1'][$b]['Month']));
            $b++;
        }

        $b=0;
        $totalDataNum = 0;
        for($a=1;$a<=12;$a++) {
            $gotMonth = FALSE;
            $resultsArray['Series2'][$b]['MonthNum'] = $a;
            foreach($series2Array as $data) {
                if ($data['MonthNum'] == $a) {
                    if ($reportType == 'repInvoicedReceived') {
                        $totalDataNum = $data['TotalReceived'];
                        $resultsArray['Series2'][$b]['Total'] = $data['TotalReceived'];
                        $series2ChartData[$b] = floatval($data['TotalReceived']);
                    } else {
                        $totalDataNum = $data['TotalIncome'];
                        $resultsArray['Series2'][$b]['Total'] = $data['TotalIncome'];
                        $series2ChartData[$b] = floatval($data['TotalIncome']);
                    }
                    $gotMonth = TRUE;
                }
            }

            if ($gotMonth == FALSE) {
                $resultsArray['Series2'][$b]['Total'] = "0";
                $series2ChartData[$b] = 0;
            }
            $series2Total += $totalDataNum;
            $resultsArray['Series2'][$b]['Month'] = date('M',mktime(0, 0, 0, $a, 1, date('Y')));
            $resultsArray['Series2'][$b]['Month'] = lang('cal_'.strtolower($resultsArray['Series2'][$b]['Month']));
            $b++;
        }
        $resultsArray['TotalSeries1'] = number_format($series1Total,2,'.','');
        $resultsArray['TotalSeries2'] = number_format($series2Total,2,'.','');
        return $resultsArray;
    }
}
?>