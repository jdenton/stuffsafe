<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Privacy extends CI_Controller {
	function __construct()
	{
		parent::__construct();
        $this->load->model('public_site','',true);
	}

    function _init() {
        $data['child']           = TRUE;
        $data['headerTextImage'] = '';
        $data['pageTitle']       = 'Privacy Policy';
        $data['jsFileArray']     = array();
        return $data;
    }

    function index() {
		$data = Privacy::_init();
        $this->load->view('Privacy',$data);
    }

    function popup() {
        $data = Privacy::_init();
        $this->load->view('PrivacyPopup',$data);
    }
}
?>