<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reports extends CI_Controller {
    function __construct() {
        parent::__construct();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

		$this->load->model('Inventory_reports','',true);
        include(APPPATH.'libraries/OpenFlashCharts/open-flash-chart.php');

        $this->load->helper('chart');
    }

    function getReportPurchaseReplace($type='category',$renderType='json',$return=0) {
        $colorChartArray = colorChartArray();
        $currencyMark = getCurrencyMark($this->session->userdata('currency'));

        $reportArray = $this->Inventory_reports->getReportPurchaseReplace($this->session->userdata('accountUserid'),$type);
        $purchaseArray = $reportArray['Purchase'];
        $replaceArray  = $reportArray['Replace'];

        $purchaseOFCData = array();
        $replaceOFCData = array();
        $xLabelOFCData   = array();

        $resultsArray = array();
        $b=0;

        $totalPurchase = 0;
        $totalReplace  = 0;
        for($a=0;$a<=count($purchaseArray)-1;$a++) {
            $purchaseOFCData[$a] = floatval($purchaseArray[$a]['TotalPurchase']);
            $replaceOFCData[$a]  = floatval($replaceArray[$a]['TotalReplace']);
            $xLabelOFCData[$a]   = DBToField($purchaseArray[$a]['LabelPurchase']);
            if (empty($xLabelOFCData[$a])) {
                $xLabelOFCData[$a] = '(N/A)';
            }
        }
        $resultsArray['Title'] = lang('reports_invoiced_received');
        $resultsArray['Legend'] = array(lang('common_purchase_cost'),lang('common_replace_cost'));
        $resultsArray['TotalPurchase'] = $currencyMark.number_format($totalPurchase,2);
        $resultsArray['TotalReplace']  = $currencyMark.number_format($totalReplace,2);

        /*
         * Open Flash Charts
         */
        $chart = new open_flash_chart();
        if ($type == 'category') {
            $title = new title(lang('reports_inventory_purchase_replacement_category'));
        } elseif ($type == 'property') {
            $title = new title(lang('reports_inventory_purchase_replacement_property'));
        }
        $title->set_style( "{font-family: arial; font-size: 20px; color: #333; text-align: center;}" );
        $chart->set_title( $title );
        $chart->set_bg_colour('#FFFFFF');

        $bar = new bar_glass();
        $bar->colour($colorChartArray[0]);
        $bar->key(lang('common_purchase_cost'), 12);
        $bar->set_values($purchaseOFCData);
        $bar->set_tooltip($currencyMark.'#val# '.lang('common_purchase_cost'));
        $bar->set_on_show(new bar_on_show('fade-in',0,0));

        $bar2 = new bar_glass();
        $bar2->colour($colorChartArray[1]);
        $bar2->key(lang('common_replacement_cost'), 12);
        $bar2->set_values($replaceOFCData);
        $bar2->set_tooltip($currencyMark.'#val# '.lang('common_replacement_cost'));
        $bar2->set_on_show(new bar_on_show('fade-in',0,0));

        $x_labels = new x_axis_labels();
        $x_labels->set_colour( '#333' );

        $x = new x_axis();
        $x->set_labels_from_array($xLabelOFCData);
        $x->set_grid_colour('#eeeeee');
        $chart->set_x_axis( $x );

        $chart->add_element( $bar );
        $chart->add_element( $bar2 );

        $x_labels = new x_axis_labels();

        $y = new y_axis();
        $yMaxPurchase = max($purchaseOFCData);
        $yMaxReplace  = max($replaceOFCData);

        if ($yMaxPurchase>$yMaxReplace) {
            $yMax = $yMaxPurchase+20;
        } else {
            $yMax = $yMaxReplace+20;
        }
        $yInc = ceil($yMax/10);

        $y->set_range(0,$yMax,$yInc);
        $y->set_grid_colour('#eeeeee');
        $y->set_label_text( "#val#" );

        $t = setChartTooltipStyle();

        $chart->set_y_axis($y);
        $chart->set_tooltip($t);
        $resultsArray['HeaderLabels'] = lang('common_month').'|'.lang('reports_invoiced').'|'.lang('reports_received');
        if ($renderType == 'json') {
            if ($return == 0) {
                $outToClient  = '{"ChartData":'.$chart->toString();
                $outToClient .= ',"GridData":'.json_encode($resultsArray).'}';
                echo utf8_encode($outToClient);
            } else {
                return json_encode($resultsArray);
            }
        } elseif ($renderType == 'array') {
            return $resultsArray;
        }
    }

    function getReportValueByProperty($renderType='json',$return=0) {
        global $colorCSSArray, $colorChartArray;
        $currencyMark = getCurrencyMark($this->session->userdata('currency'));
        $reportArray = $this->Inventory_reports->getReportValueByProperty($this->session->userdata('accountUserid'));
        $titleText = lang('reports_inventory_by_property');
        $title = new title($titleText);
        $title->set_style( "{font-family: arial; font-size: 20px; color: #333; text-align: center;}" );

        $pieData = array();
        $newColorArray = array();
        $a=0;
        $b=0;
        $colorIndex=0;

        $grandTotal = 0;
        foreach($reportArray as $reportItem) {
            $grandTotal += $reportItem['Total'];
        }

        $otherTotal = null;
        $otherLabel = '<br>';
        foreach($reportArray as $reportItem) {
            $total = floatval($reportItem['Total']);
            $itemPercentage = number_format(($reportItem['Total']/$grandTotal)*100,1);
            $reportArray[$b]['Percentage'] = $itemPercentage.'%';
            $reportArray[$b]['Total'] = $currencyMark.number_format($total,2);

            if ($total>0) {
                if (strlen($reportItem['Label'])>20) {
                    $label = substr($reportItem['Label'],0,17).'...';
                } else {
                    $label = $reportItem['Label'];
                }
                //$label = stripslashes(entities_to_quotes($label)).' ('.$currencyMark.number_format($total,2).')';
                $label = stripslashes(entities_to_quotes($label)).' ('.number_format($total,2).')';
                if (sizeof($reportArray)>5 && $itemPercentage<5) {
                    $otherTotal += $total;
                    $otherLabel .= $label.'<br>';
                } else {
                    $tmp = new pie_value($total,"");
                    $tmp->set_label($label,'#333333',11);
                    $tmp->set_tooltip('#label#<br>'.$currencyMark.'#val# of '.$currencyMark.$grandTotal.' ('.$itemPercentage.'%)');
                    if (!isset($colorChartArray[$colorIndex])) {
                        $colorIndex=0;
                    }
                    $newColorArray[$a] = $colorChartArray[$colorIndex];
                    $pieData[] = $tmp;
                    $a++;
                    $colorIndex++;
                }
            }
            $b++;
        }

        $pie = new pie();
        if ($otherTotal != null) {
            $tmp = new pie_value($otherTotal,"");
            $tmp->set_label(lang('common_other'),'#333333',11);
            $tmp->set_tooltip($otherLabel);
            $newColorArray[$a] = $colorChartArray[$colorIndex];
            $pieData[] = $tmp;
        }
        $pie->set_start_angle(60);
        $pie->gradient_fill();
        $pie->add_animation( new pie_fade() );
        $pie->colours($newColorArray);
        $pie->set_values($pieData);

        $t = setChartTooltipStyle();

        $chart = new open_flash_chart();
        $chart->set_title( $title );
        $chart->add_element( $pie );
        $chart->set_tooltip($t);
        $chart->set_bg_colour('#FFFFFF');

        if ($renderType == 'json') {
            if ($return == 0) {
                $outToClient  = '{"ChartData":'.$chart->toString();
                $outToClient .= ',"GridData":'.json_encode($reportArray);
                $outToClient .= ',"GrandTotal":"'.$currencyMark.number_format($grandTotal,2).'"}';
                echo $outToClient;
            } else {
                return json_encode($reportArray);
            }
        } elseif ($renderType == 'array') {
            return $reportArray;
        }
    }

    function getReportValueByCategory($renderType='json',$return=0) {
        global $colorCSSArray, $colorChartArray;
        $currencyMark = getCurrencyMark($this->session->userdata('currency'));
        $reportArray = $this->Inventory_reports->getReportValueByCategory($this->session->userdata('accountUserid'));
        $titleText = lang('reports_inventory_by_category');
        $title = new title($titleText);
        $title->set_style( "{font-family: arial; font-size: 20px; color: #333; text-align: center;}" );

        $pieData = array();
        $newColorArray = array();
        $a=0;
        $b=0;
        $colorIndex=0;

        $grandTotal = 0;
        foreach($reportArray as $reportItem) {
            $grandTotal += $reportItem['Total'];
        }

        $otherTotal = null;
        $otherLabel = '<br>';
        foreach($reportArray as $reportItem) {
            $total = floatval($reportItem['Total']);
            $itemPercentage = number_format(($reportItem['Total']/$grandTotal)*100,1);
            $reportArray[$b]['Percentage'] = $itemPercentage.'%';
            $reportArray[$b]['Total'] = $currencyMark.number_format($total,2);

            if ($total>0) {
                if (strlen($reportItem['Label'])>20) {
                    $label = substr($reportItem['Label'],0,17).'...';
                } else {
                    $label = $reportItem['Label'];
                }
                //$label = stripslashes(entities_to_quotes($label)).' ('.$currencyMark.number_format($total,2).')';
                $label = stripslashes(entities_to_quotes($label)).' ('.number_format($total,2).')';
                if (sizeof($reportArray)>5 && $itemPercentage<5) {
                    $otherTotal += $total;
                    $otherLabel .= $label.'<br>';
                } else {
                    $tmp = new pie_value($total,"");
                    $tmp->set_label($label,'#333333',11);
                    $tmp->set_tooltip('#label#<br>'.$currencyMark.'#val# of '.$currencyMark.$grandTotal.' ('.$itemPercentage.'%)');
                    if (!isset($colorChartArray[$colorIndex])) {
                        $colorIndex=0;
                    }
                    $newColorArray[$a] = $colorChartArray[$colorIndex];
                    $pieData[] = $tmp;
                    $a++;
                    $colorIndex++;
                }
            }
            $b++;
        }

        $pie = new pie();
        if ($otherTotal != null) {
            $tmp = new pie_value($otherTotal,"");
            $tmp->set_label(lang('common_other'),'#333333',11);
            $tmp->set_tooltip($otherLabel);
            $newColorArray[$a] = $colorChartArray[$colorIndex];
            $pieData[] = $tmp;
        }
        $pie->set_start_angle(60);
        $pie->gradient_fill();
        $pie->add_animation( new pie_fade() );
        $pie->colours($newColorArray);
        $pie->set_values($pieData);

        $t = setChartTooltipStyle();

        $chart = new open_flash_chart();
        $chart->set_title( $title );
        $chart->add_element( $pie );
        $chart->set_tooltip($t);
        $chart->set_bg_colour('#FFFFFF');

        if ($renderType == 'json') {
            if ($return == 0) {
                $outToClient  = '{"ChartData":'.$chart->toString();
                $outToClient .= ',"GridData":'.json_encode($reportArray);
                $outToClient .= ',"GrandTotal":"'.$currencyMark.number_format($grandTotal,2).'"}';
                echo $outToClient;
            } else {
                return json_encode($reportArray);
            }
        } elseif ($renderType == 'array') {
            return $reportArray;
        }
    }
}
?>