<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Places extends CI_Controller {
    function __construct()
	{
        parent::__construct();
        checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        $this->load->model('places_view','',true);
        $this->load->model('inventory_view','',true);
        $this->load->library('application/CommonAppData');
    }

    function _init($tag=null) {
        $data['page']              = 'places';
		$data['pageTitle']         = lang('place_my_places');
		$data['wysiwyg']           = 0;
		$data['map']               = 1;
		$data['jsFileArray']       = array('jsPlaces.js','jsInventory.js','jsInventoryReports.js');
		$data['pageIconClass']     = 'iconplaces';
		$data['pageLayoutClass']   = 'withRightColumn';
		$data['rightColumnComponents'] = array('assetTotalSidebar', 'tagRender');

        $data['tag'] = $tag;
        $data['maxPlaces'] = 10;
        $data['pageButtons']           = array(
                                      /*
                                       * Button text, button title, visual style, extra className, itemID, buttonID, initialState
                                       */
									   'add'        => array(lang('menu_new_place'),lang('menu_new_place'),'blueGray','buttonNewMain placeUpdate hide','','newProperty')
									);

        $data['tagRenderArray']        = $this->commonappdata->renderTags('property','array');
        return $data;
    }

    function index($tag=null) {
        $data = places::_init($tag);
        $this->load->view('Places',$data);
    }

    function getPlaces($tag=NULL,$renderType=NULL,$return=1) {
        $permArray = $this->session->userdata('permissions');
        $currencyMark = getCurrencyMark($this->session->userdata('currency'));
        $viewAll = TRUE;
        if ($permArray['placeViewOwn'] == 1 && $permArray['placeViewAll'] == 0) {
            $viewAll = FALSE;
        }
        $placeArray = $this->places_view->getPlaces($this->session->userdata('accountUserid'),$this->session->userdata('userid'),null,$tag,$viewAll,true);
        $j=0;
        foreach($placeArray as $place) {
            $placeTotalArray = $this->inventory_view->getInventoryTotalsForPlace($place['PlaceID'],$this->session->userdata('accountUserid'));
            $placeArray[$j]['TotalPricePurchase'] = $placeTotalArray['TotalPricePurchase'];
            $placeArray[$j]['TotalPriceReplace']  = $placeTotalArray['TotalPriceReplace'];
            $placeArray[$j]['TotalPricePurchaseRender'] = $currencyMark.number_format($placeTotalArray['TotalPricePurchase'],2);
            $placeArray[$j]['TotalPriceReplaceRender']  = $currencyMark.number_format($placeTotalArray['TotalPriceReplace'],2);
            if (!empty($place['Category'])) {
                switch ($placeArray[$j]['Category']) {
                    case 'single family house':
                        $placeArray[$j]['AvatarClass'] = 'house';
                        break;
                    case 'apartment unit':
                        $placeArray[$j]['AvatarClass'] = 'apartment';
                        break;
                    case 'multi family':
                        $placeArray[$j]['AvatarClass'] = 'multi';
                        break;
                    case 'multi family':
                        $placeArray[$j]['AvatarClass'] = 'multi';
                        break;
                    case 'townhouse':
                        $placeArray[$j]['AvatarClass'] = 'townhouse';
                        break;
                    case 'condominium':
                        $placeArray[$j]['AvatarClass'] = 'condominium';
                        break;
                    case 'business':
                        $placeArray[$j]['AvatarClass'] = 'business';
                        break;
                    case 'boat':
                        $placeArray[$j]['AvatarClass'] = 'boat';
                        break;
                    case 'plane':
                        $placeArray[$j]['AvatarClass'] = 'plane';
                        break;
                    default:
                        $placeArray[$j]['AvatarClass'] = 'house';
                }
                $placeArray[$j]['Category'] = lang('cat_'.str_replace(' ','_',$place['Category']));
            } else {
                $placeArray[$j]['Category'] = '';
                $placeArray[$j]['AvatarClass'] = 'default';
            }

            $placeItemTotal = '0';
            foreach($placeArray[$j]['Rooms'] as $room) {
                $placeItemTotal = $placeItemTotal + $room['RoomItemCount'];
            }
            $placeArray[$j]['PlaceItemCount'] = $placeItemTotal;
            $j++;
        }

        if ($renderType == 'json') {
            $newPlaceArray['Places'] = $placeArray;
            if ($return == '1') {
                return json_encode($newPlaceArray);
            } else {
                echo json_encode($newPlaceArray);
            }
        } else {
            return $placeArray;
        }
    }
}
?>