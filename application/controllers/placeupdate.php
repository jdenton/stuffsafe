<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PlaceUpdate extends CI_Controller {
    function __construct()
	{
        parent::__construct();
        checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        $this->load->model('app_data','',TRUE);
        $this->load->model('places_update','',true);
        $this->load->model('places_view','',true);

        $this->load->library('application/CommonAppData');
    }

    function _init($placeID=null) {
        global $subMenuArray;
		$data['page']            = 'places';
        $data['pageTitle']   = lang('place_add_place');
		$data['wysiwyg']         = 1;
		$data['map']             = 0;
		$data['validate']        = 1;
		$data['jsFileArray']     = array('jsPlaces.js');
		$data['subMenuPage']     = $subMenuArray['places'];
		$data['pageIconClass']   = 'iconPagePlaces';
		$data['pageLayoutClass'] = 'withRightColumn';

        /*
		 * Create tag string for places
		 */
		$tagArray = $this->app_data->getTags($this->session->userdata('accountUserid'),'place');
		$data['tags'] = makeTagJSON($tagArray);

        if ($placeID>0) {
            /*
             * We're editing, retrieve and format place data
             */
            if (!$this->places_view->checkForValidPlace($placeID,$this->session->userdata('accountUserid'))) {
                header('Location: '.site_url('places'));
            } else {
                $data['action'] = 'edit';
                $data['placeInformation'] = PlaceUpdate::getPlaceDetails($placeID);
            }
            $data['pageTitle'] = lang('place_edit_place');
            $data['action'] = 'edit';
        } else {
            $data['placeInformation']['PlaceID']     = '';
            $data['placeInformation']['PlaceName']   = '';
            $data['placeInformation']['Category']    = '';
            $data['placeInformation']['Address']     = '';
            $data['placeInformation']['City']        = '';
            $data['placeInformation']['State']       = '';
            $data['placeInformation']['Zip']         = '';
            $data['placeInformation']['Country']     = '';
            $data['placeInformation']['Description'] = '';
            $data['placeInformation']['Tags']        = '';
            $data['placeInformation']['Area']        = '';
            $data['placeInformation']['AreaUnit']    = '';
            $data['placeInformation']['Category']    = '';
            $data['action'] = 'add';
        }

        $messageArray = array(
						'placeName' => lang('form_required_place_name')
						);
        $data['validationJSON'] = json_encode($messageArray);

        return $data;
    }

    function index($placeID=null) {
        /*
         * Are we supposed to be editing
         */
		$data = PlaceUpdate::_init($placeID);
		$this->load->view('PlaceUpdate',$data);
    }

    function savePlace() {
        if (isset($_POST['save']) && $_POST['save'] == 1) {
			if ($this->form_validation->run() == FALSE)	{
				$data = PlaceUpdate::_init();
				$this->load->view('PlaceUpdate',$data);
			} else {
                $placeArray['placeID']          = $_POST['placeID'];
                $placeArray['userid']           = $this->session->userdata('userid');
                $placeArray['accountUserid']    = $this->session->userdata('accountUserid');
                $placeArray['action']           = $_POST['action'];
                $placeArray['placeName']        = fieldToDB($this->input->post('placeName', TRUE));
                $placeArray['placeType']        = $_POST['placeType'];
                $placeArray['placeAddress']     = fieldToDB($this->input->post('placeAddress', TRUE), TRUE,TRUE);
                $placeArray['placeCity']        = fieldToDB($this->input->post('placeCity', TRUE));
                $placeArray['placeState']       = fieldToDB($this->input->post('placeState', TRUE));
                $placeArray['placeZip']         = fieldToDB($this->input->post('placeZip', TRUE));
                $placeArray['placeCountry']     = $_POST['placeCountry'];
                $placeArray['placeDescription'] = fieldToDB(strip_tags($this->input->post('placeDescription', TRUE)), TRUE,TRUE,FALSE,TRUE);
                $placeArray['placeArea']        = fieldToDB($this->input->post('placeArea', TRUE));
                if ($placeArray['placeType'] == '0') {
                    $placeArray['placeType'] = '';
                }

                $placeArray['placeAreaUnit'] = '';
                if (isset($_POST['placeAreaUnit'])) {
                    $placeArray['placeAreaUnit']    = $_POST['placeAreaUnit'];
                }
                
                $placeArray['placeTags']        = fieldToDB($this->input->post('placeTags', TRUE));
                $newPlaceID = $this->places_update->savePlace($placeArray);

                /*
                 * Save tags
                 */
                if (!empty($placeArray['placeTags'])) {
                    $this->commonappdata->saveTags($placeArray['placeTags'],'property');
                }
				header('Location: '.BASE_URL.'place/getPlace/'.$newPlaceID);
            }
        }
    }

    function getPlaceDetails($placeID) {
        $placeArray = $this->places_view->getPlaceInformation($placeID);
        return $placeArray;        
    }

    function deletePlace($placeID,$tagForDelete=TRUE) {
        $this->places_update->deletePlace($placeID,$tagForDelete,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
        $this->commontrash->countItemsInTrash();
    }

    function restorePlace($placeID) {
        $this->places_update->restorePlace($placeID,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
        $this->commontrash->countItemsInTrash();
    }
}
?>