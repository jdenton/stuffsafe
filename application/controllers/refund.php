<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Refund extends CI_Controller {
	function __construct()
	{
		parent::__construct();
        $this->load->model('public_site','',true);
	}

    function _init() {
        $data['child']           = TRUE;
        $data['headerTextImage'] = '';
        $data['pageTitle']       = 'Refund Policy';
        $data['jsFileArray']     = array();
        return $data;
    }

    function index() {
		$data = Refund::_init();
        $this->load->view('Refund',$data);
    }

    function popup() {
        $data = Refund::_init();
        $this->load->view('RefundPopup',$data);
    }
}
?>