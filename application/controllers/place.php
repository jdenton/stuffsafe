<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Place extends CI_Controller {
    function __construct()
	{
        parent::__construct();
        checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        $this->load->model('app_data','AppData',TRUE);
        $this->load->model('places_view','',true);
        $this->load->model('rooms_view','',true);
        $this->load->model('inventory_view','',true);

        $this->load->library('application/CommonAppData');
    }

    function _init($placeID=null) {
        $data['page']              = 'place';
		$data['pageTitle']         = lang('place_my_places');
        $data['pageTitleLink']     = site_url('places');
		$data['wysiwyg']           = 1;
		$data['map']               = 1;
		$data['jsFileArray']       = array('jsPlaces.js','jsInventory.js');
		$data['pageIconClass']     = 'iconplaces';
		$data['pageLayoutClass']   = 'withRightColumn';
		$data['rightColumnComponents'] = array('assetTotalSidebar','exportSidebar', 'tagRender');
        $data['placeInformation']      = Place::getPlaceDetail($placeID,'json',1);
        $jsonArray                     = json_decode($data['placeInformation'], true);
        $data['pageSubTitle']          = $jsonArray['PlaceName'];
        $data['rooms'] = json_encode($this->rooms_view->getRoomsForSelect($this->session->userdata('accountUserid'),NULL,true));

        $data['categories'] = $this->AppData->getCategories($this->session->userdata('accountUserid'),'inventory');
        $data['catJSON']    = makeCategoryJSON($data['categories']);
        $data['placeID']    = $placeID;

        $tagArray = $this->AppData->getTags($this->session->userdata('accountUserid'),'inventory');
        $data['tags']           = makeTagJSON($tagArray);
        $data['tagRenderArray'] = $this->commonappdata->renderTags('inventory','array');
        return $data;
    }

    function index($placeID=null) {
        if (!is_numeric($placeID)) {
			header('Location: '.BASE_URL.'places');
		}
        $data = place::_init($placeID);
        $this->load->view('Place',$data);
    }

    function getPlace($placeID=null) {
        if (!is_numeric($placeID)) {
			header('Location: '.BASE_URL.'places');
		}
        $data = place::_init($placeID);
        $this->load->view('Place',$data);
    }

    function getPlaceDetail($placeID,$renderType=NULL,$return=1) {
        if (!$this->places_view->checkForValidPlace($placeID,$this->session->userdata('accountUserid')) || empty($placeID)) {
			/*
			 * Send them back to this users' project view page.
			 */
			header('Location: '.site_url('places'));
		} else {
            $currencyMark = getCurrencyMark($this->session->userdata('currency'));
            $placeArray = $this->places_view->getPlaceInformation($placeID);

            $placeTotalArray = $this->inventory_view->getInventoryTotalsForPlace($placeID,$this->session->userdata('accountUserid'));
            $placeArray['TotalPricePurchase'] = $placeTotalArray['TotalPricePurchase'];
            $placeArray['TotalPriceReplace']  = $placeTotalArray['TotalPriceReplace'];
            $placeArray['TotalPricePurchaseRender'] = $currencyMark.number_format($placeTotalArray['TotalPricePurchase'],2);
            $placeArray['TotalPriceReplaceRender']  = $currencyMark.number_format($placeTotalArray['TotalPriceReplace'],2);
            if (!empty($placeArray['Category'])) {
                $placeArray['Category'] = lang('cat_'.str_replace(' ','_',$placeArray['Category']));
            }

            $placeArray['DateEnteredHuman'] = date('M j, Y', strtotime($placeArray['DateEntered']));
            $placeArray['EnteredBy']        = $placeArray['NameFirst'].' '.$placeArray['NameLast'];
            $placeArray['Rooms']            = $this->rooms_view->getRoomsForPlace($placeID);
			if ($renderType == 'array') {
				return $placeArray;
			} elseif ($renderType == 'json') {
				if ($return == 1) {
					return json_encode($placeArray);
				} else {
					echo json_encode($placeArray);
				}
			}
        }
    }
}
?>