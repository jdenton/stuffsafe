<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventory extends CI_Controller {
    function __construct()
	{
        parent::__construct();
        checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        $this->load->model('inventory_update','',true);
        $this->load->model('inventory_view','',true);
        $this->load->model('rooms_update','',true);
        $this->load->model('rooms_view','',true);
        $this->load->model('file_maintenance','',true);
        $this->load->model('app_data','',TRUE);

        $this->load->library('application/CommonAppData');
        $this->load->library('application/CommonFileManager');
    }

    function saveInventoryItem() {
        if (isset($_POST['save']) && $_POST['save'] == 1) {
            $roomID = $_POST['invRoomID'];
            /*
             * Check for newly added room.
             * Does this room exist for this place yet? It may exit
             * for another place but not this one.
             */
            $roomName   = fieldToDB($this->input->post('invRoomNew', TRUE));
            if (empty($roomName)) {
                $roomName = lang('inventory_default_room');
            }
            $roomExists = $this->rooms_view->checkRoomExistsForPlace($roomName,$_POST['placeID']);
            $newRoom = FALSE;

            if ($roomExists == FALSE && !empty($roomName)) {
                 $roomArray['action']        = 'add';
                 $roomArray['accountUserid'] = $this->session->userdata('accountUserid');
                 $roomArray['userid']        = $this->session->userdata('userid');
                 $roomArray['placeID']       = $_POST['placeID'];
                 $roomArray['roomName']      = $roomName;
                 $roomArray['tags']          = '';
                 $roomID = $this->rooms_update->saveRoom($roomArray);
                 $newRoom = TRUE;
            } else {
                $roomID = $roomExists;
            }

            $invArray['invID']        = $_POST['invID'];
            $invArray['action']       = $_POST['action'];
            $invArray['accountUserid']= $this->session->userdata('accountUserid');
            $invArray['userid']       = $this->session->userdata('userid');
            $invArray['roomID']       = $roomID;
            $invArray['catID']        = $_POST['invCategory'];
            $invArray['invName']      = fieldToDB($this->input->post('invName', TRUE));
            $invArray['invDesc']      = fieldToDB($this->input->post('invDescription', TRUE),TRUE,FALSE,FALSE,FALSE,TRUE,FALSE);
            $invArray['invMake']      = fieldToDB($this->input->post('invMake', TRUE));
            $invArray['invModel']     = fieldToDB($this->input->post('invModel', TRUE));
            $invArray['invSN']        = fieldToDB($this->input->post('invSN', TRUE));
            $invArray['invPricePurchase'] = fieldToDB($this->input->post('invPricePurchase', TRUE));
            $invArray['invPriceReplace']  = fieldToDB($this->input->post('invPriceReplace', TRUE));
            $invArray['invCondition']     = $_POST['invCondition'];
            $invArray['invDate']          = jsDateToMySQL(trim($_POST['invDate']));
            $invArray['invTags']          = $this->input->post('invTags', TRUE);

            $invArray['invPricePurchase'] = str_replace(',','',$invArray['invPricePurchase']);
            $invArray['invPriceReplace']  = str_replace(',','',$invArray['invPriceReplace']);

            /*
             * Save new tags
             */
            $this->commonappdata->saveTags($invArray['invTags'],'inventory');
            $inventoryID = $this->inventory_update->saveInventoryItem($invArray);

            /*
             * Save files (only if we're adding)
             */
            if ($invArray['action'] == 'add') {
                /*
                 * Link files to this inventory item
                 */
                $this->file_maintenance->attachFilesToNewItem($inventoryID,'inventory',$this->session->userdata('accountUserid'));
            }
            
            $catName = $this->app_data->getCategoryName($invArray['catID']);

            $jsonArray = array(
                'InventoryID'   => $inventoryID,
                'RoomID'        => $roomID,
                'PlaceID'       => $_POST['placeID'],
                'CatID'         => $invArray['catID'],
                'MainCat'       => $catName,
                'ItemName'      => $invArray['invName'],
                'ItemDesc'      => $invArray['invDesc'],
                'ItemMake'      => $invArray['invMake'],
                'ItemModel'     => $invArray['invModel'],
                'ItemSerial'    => $invArray['invSN'],
                'PricePurchase' => $invArray['invPricePurchase'],
                'PriceReplace'  => $invArray['invPriceReplace'],
                'ItemCondition' => $invArray['invCondition'],
                'DatePurchase'  => $invArray['invDate'],
                'DatePurchaseHuman' => date('M j, Y', strtotime($invArray['invDate'])),
                'Tags'          => $invArray['invTags'],
                'DateEntered'   => date('Y-m-d'),
                'PID'           => $this->session->userdata('pid'),
                'EnteredBy'     => $this->session->userdata('fullName'),
                'DateEnteredHuman' => date('M j, Y'),
                'Files'            => $this->commonfilemanager->getFilesForSomething($inventoryID,'inventory','array',1)
            );
            $files = $jsonArray['Files'];

            if ($newRoom == TRUE) {
                $roomArray = $this->rooms_view->getRoom($roomID);
                $newArray['RoomID']    = $roomArray['RoomID'];
                $newArray['RoomName']  = $roomArray['RoomName'];
                $newArray['RoomTags']  = $roomArray['Tags'];
                $newArray['EnteredBy'] = '';
                $newArray['Inventory'][0] = $jsonArray;
                $jsonArray = $newArray;
            }

            /*
             * Get avatar thumbnail
             */
            $gotThumbnail = FALSE;
            $thumbnailPath = '';
            
            if (count($files)>0) {
                foreach($files as $file) {
                    if ($file['FileType'] == 'image/png' || $file['FileType'] == 'image/jpeg' || $file['FileType'] == 'image/gif' || $file['FileType'] == 'image/jpg') {
                        $gotThumbnail = TRUE;
                        $thumbnailPath = $file['thumbnailLink'];
                    }
                }
            }
            $jsonArray['AvatarURL'] = $thumbnailPath;
            echo json_encode($jsonArray);
        }
    }

    function getInventoryForPlace($placeID,$renderType=NULL,$return=0) {
        if ($placeID == 0) {
            $placeID = $_POST['placeID'];
        }
        $inventoryArray = $this->inventory_view->getInventoryForPlace($placeID,$this->session->userdata('accountUserid'));

        if ($renderType == 'json') {
            if ($return == 0) {
                echo json_encode($inventoryArray);
            } else {
                return json_encode($inventoryArray);
            }
        } else {
            return $inventoryArray;
        }
    }
    
    function getInventoryForRoom($roomID,$renderType=NULL,$return=0) {
        if ($roomID == 0) {
            $roomID = $_POST['roomID'];
        }
        $inventoryArray = $this->inventory_view->getInventoryForRoom($roomID,$this->session->userdata('accountUserid'));

        if ($renderType == 'json') {
            if ($return == 0) {
                echo json_encode($inventoryArray);
            } else {
                return json_encode($inventoryArray);
            }
        } else {
            return $inventoryArray;
        }
    }
    
    function getInventoryAvatarImages($placeID=NULL,$renderType='json',$return=0) {
        $resultArray = $this->inventory_view->getInventoryForPlace($placeID,$this->session->userdata('accountUserid'));
        
        $newArray = array();
        $a = 0;
        foreach($resultArray as $room) {
            $newArray[$a]['RoomID'] = $room['RoomID'];
            
            $inventoryArray = $room['Inventory'];
            $avatarArray = array();
            $b = 0;
            foreach($inventoryArray as $item) {
                $avatarArray[$b]['AvatarURL'] = $item['AvatarURL'];
                $avatarArray[$b]['FileID'] = $item['FileID'];
                
                /*
                $bigImage = str_replace('_thumb', '', $item['AvatarURL']);
                $avatarArray[$b]['BigImage'] = $bigImage;
                
                $bigImageSize = @getimagesize($bigImage);
                $avatarArray[$b]['BigImageW'] = $bigImageSize[0];
                $avatarArray[$b]['BigImageH'] = $bigImageSize[1];
                */
                
                $b++;
            }
            $newArray[$a]['Avatars'] = $avatarArray;
            
            $a++;
        }
        if ($renderType == 'json') {
            if ($return == 0) {
                echo json_encode($newArray);
            } else {
                return json_encode($newArray);
            }
        } else {
            return $newArray;
        }
    }

    function getInventoryItem($inventoryID,$renderType=NULL,$return=0) {
        $inventoryArray = $this->inventory_view->getInventoryItem($inventoryID,$this->session->userdata('accountUserid'));
        $inventoryArray['DatePurchase'] = MySQLDateToJS($inventoryArray['DatePurchase']);
        if ($renderType == 'json') {
            if ($return == 0) {
                echo json_encode($inventoryArray);
            } else {
                return json_encode($inventoryArray);
            }
        } else {
            return $inventoryArray;
        }
    }

    function showInventoryItem($inventoryID) {
        /*
         * For now, just get the place that this item is associated with
         * and redirect to the inventory view on that place page.
         */
        $itemArray = $this->inventory_view->getInventoryItem($inventoryID,$this->session->userdata('accountUserid'));
        $placeID = $itemArray['PlaceID'];
        if ($placeID>0) {
            header('Location: '.site_url('place/getPlace/'.$placeID.'#windowInventory'));
        } else {
            header('Location: '.site_url('places'));
        }
    }

    function searchInventory() {
        
    }

    function moveInventoryToRoom($roomFrom,$roomTo) {
        if ($roomFrom > 0) {
            $this->inventory_update->moveInventoryToRoom($roomFrom,$roomTo);
        }
    }

    function deleteRoom($roomID,$withInventory=0,$tagForDelete=TRUE) {
        /*
         * If withInventory = 1 then we delete all inventory items
         * in this room as well.
         */
        if ($roomID > 0) {
            $this->rooms_update->deleteRoom($roomID,$withInventory,$tagForDelete,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
            $this->commontrash->countItemsInTrash();
        }
	}

    function getRoomsForPlace($placeID,$renderType=NULL,$return=0) {
        if ($placeID == 0) {
            $placeID = $_POST['placeID'];
        }
        $roomArray = $this->rooms_view->getRoomsForPlace($placeID,$this->session->userdata('accountUserid'));

        if ($renderType == 'json') {
            if ($return == 0) {
                echo json_encode($roomArray);
            } else {
                return json_encode($roomArray);
            }
        } else {
            return $roomArray;
        }
    }

    function getInventoryCategories() {
        $categories = $this->app_data->getCategories($this->session->userdata('accountUserid'),'inventory');
        echo makeCategoryJSON($categories);
    }

    function getNumberOfItemsForRoom($roomID) {
        $itemCount = $this->inventory_view->getNumberOfItemsForRoom($roomID);
        echo $itemCount['ItemCount'];
    }

    function updateRoomName() {
        $roomID   = $_POST['roomID'];
        $roomName = fieldToDB($this->input->post('roomName', TRUE));
        $this->rooms_update->updateRoomName($roomID,$roomName);
    }

    function swapInventoryItemRooms($inventoryItemID,$newRoomID) {
        $this->inventory_update->swapInventoryItemRooms($inventoryItemID,$newRoomID);
    }

    function deleteInventoryItem($inventoryID,$tagForDelete=TRUE) {
        $this->inventory_update->deleteInventoryItem($inventoryID,$tagForDelete,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
        $this->file_maintenance->removeAttachedFileFromItem(NULL,NULL,$inventoryID,'inventory');
        $this->commontrash->countItemsInTrash();
    }

    function restoreRoom($roomID) {
        $this->rooms_update->restoreRoom($roomID,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
        $this->commontrash->countItemsInTrash();
    }

    function restoreInventoryItem($inventoryID) {
        $this->inventory_update->restoreInventoryItem($inventoryID,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
        $this->commontrash->countItemsInTrash();
    }
}