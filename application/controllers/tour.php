<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tour extends CI_Controller {
    function _init() {
        $data['child']           = TRUE;
        $data['headerTextImage'] = 'txtTourHeader_public.png';
        $data['pageTitle']       = 'StuffSafe Tour';
        $data['jsFileArray']     = array();

        return $data;
    }

    function index() {
        $data = Tour::_init();
        $this->load->view('Tour',$data);
    }

    function video($videoName=NULL,$videoTitle=NULL) {
        if ($videoName == NULL) {
            //Tour::index();
            $data = Tour::_init();
            $this->load->view('TourVideos',$data);
        } else {
            $data = Tour::_init();
            $data['videoName']  = $videoName;
            $data['videoTitle'] = $videoTitle;
            $this->load->view('TourVideo',$data);
        }
    }
}
?>