<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('public_site','',true);
        $this->load->library('application/CommonEmail');
	}

    function _init($mailSent) {
		$data['child']           = TRUE;
        $data['headerTextImage'] = 'txtContactHeader_public.png';
        $data['jsFileArray']     = array();
        $data['pageTitle']       = 'Contact Us';

        /*
         * Get some customer logos
         */
        $data['mailSent'] = $mailSent;
        $data['rightColumnComponents'] = array('loginBox','goodPress');
        return $data;
    }

    function index($mailSent = false) {
		$data = Contact::_init($mailSent);
        $this->load->view('Contact',$data);
    }

    function sendMessage() {
        $this->commonemail->sendContactEmail($_POST);
        Contact::index(true);
    }
}
?>