<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {
    function __construct()
	{
        parent::__construct();
        $this->load->model('User_login','',TRUE);
		$this->load->model('User_information','',TRUE);
		$this->load->model('Team','',TRUE);
        $this->load->model('Trash','',TRUE);
		$this->load->model('Settings_update','',TRUE);
        $this->load->model('Accounts','',TRUE);

        $this->load->library('encrypt');
        $this->load->library('application/CommonLogin');
        $this->load->library('application/CommonEmail');
    }

    function _init($who=NULL) {
        $authKey = $this->session->userdata('authKey');
		if ($this->session->userdata('logged') == TRUE && !isset($authKey)) {
            header('Location: '.site_url('Places'));
		}
        loadLanguageFiles();

		$data['page']            = 'login';
		if ($who == 'client') {
			$data['pageTitle'] = lang('login_client_page_title');
		} else {
			$data['pageTitle'] = lang('login_page_title');
		}
        $data['child']           = TRUE;
        $data['siteName']        = 'StuffSafe';
		$data['jsFileArray']     = array('jsLogin.js');
        $data['headerTextImage'] = 'txtLoginHeader_public.png';
        $data['pageTitle']       = 'Login';
    	return $data;
	}

	function index($message=null,$subdomain=null) {
		switch ($message) {
			case 'logout':
			    $message = '<div><strong>'.lang('login_message_logout').'</strong></div>';
			    break;
			case 'inactiveAccount':
			    $message = '<div class="errorMessageSmall">'.lang('login_message_inactiveAccount').'</div>';
			    break;
            case 'missingSubdomain':
			    $message = '<div class="errorMessageSmall">We could not locate your account area.</div>';
			    break;
			case 'missingField':
			    $message = '<div class="errorMessageSmall">'.lang('login_message_missingField').'</div>';
			    break;
			case 'noAccount':
			    $message = '<div class="errorMessageSmall">We cannot find your account. Perhaps your email or password are incorrect or your account has been deactivated. Please contact <a href="mailto:'.MAIL_SUPPORT.'">'.MAIL_SUPPORT.'</a> with any questions.</div>';
			    break;
			case 'expired':
			    $message = '<div class="infoMessageSmall">'.lang('login_message_expired').'</div>';
			    break;
            case 'roleMissing':
			    $message = '<div class="errorMessageSmall">'.lang('login_message_roleMissing').'</div>';
			    break;
			default:
                $message = '<div class="bigText">Welcome! Please login to your StuffSafe account below.</div>';
		}
		$data = login::_init();
		$data['message'] = $message;
        if (!empty($subdomain)) {
            $accountArray = $this->Accounts->getAccountInfoFromSubdomain($subdomain);
            $data['pageTitle'] = $accountArray['Company'].': Please login';
            if ($accountArray != false) {
                $logoArray = $this->Settings_update->getSettings($accountArray['AccountID'],'logoLowRes');
                if (!empty($logoArray['SettingValue'])) {
                    $data['logoValue'] = '<img src="'.$logoArray['SettingValue'].'" alt="'.$accountArray['Company'].'" />';
                } else {
                    $data['logoValue'] = $accountArray['Company'];
                }

                $data['subdomain'] = $subdomain;
                $this->load->view('public_site/LoginSubdomain',$data);
            } else {
                //header('Location: '.INSECURE_URL.'login/index/missingSubdomain');
            }

        } else {
            $this->load->view('login',$data);
        }
	}

    function tryLogin($apiAuth=NULL,$authKey=NULL,$userType=NULL) {
        $userid     = $this->input->post('userid', TRUE);
	$password   = $this->input->post('password', TRUE);
        $remember   = FALSE;

        if ($this->input->post('remember') == 1) {
            $remember = TRUE;
        }

        $subdomain = '0';
        if (isset($_POST['subdomain']) && !empty($_POST['subdomain'])) {
            $subdomain = $this->input->post('subdomain', TRUE);
        }

        $this->commonlogin->tryLogin($userid,$password,$authKey,$userType,$apiAuth,NULL,$remember,TRUE,$subdomain);
    }

    function logout($cancel=NULL) {
        if ($cancel == 'cancel') {
            $redirect = site_url('cancelAccount');
        } else {
            $redirect = site_url('');
        }
        /*
         * Kill our session
         */
        $this->session->sess_destroy();

        /*
         * Kill the rememberMe cookie
         */
        delete_cookie('rememberMe');
            header('Location: '.$redirect);
    }

	function findPassword() {
            $email = $this->input->post('email',TRUE);
            if (isset($_POST['subdomain']) && !empty($_POST['subdomain'])) {
                $subdomain = $this->input->post('subdomain', TRUE);
            }

        /*
         * Do we even have this email address on file?
         */
        $data = login::_init();
        $accounts = $this->User_login->checkUserEmail($email);
        if ($accounts == FALSE) {
            $data['message'] = '<div class="errorMessageSmall">Oops! We cannot find the email address '.$email.'.</div>';
        } else {
            /*
             * Now reset password and send to user.
             * User may have multiple account under that same email address
             * so we'll need to reset all of them.
             */
            $a=0;
            $message = '';
            $mailMessageArray = $this->commonemail->createSystemMessage('resetPassword','en');
            $subject = $mailMessageArray['subject'];
            $messageHeader = $mailMessageArray['message'];
            $footer = $this->commonemail->createSystemMessage('messageFooter','en');
            foreach($accounts as $account) {
                $UID           = $account['UID'];
                $accountUserid = $account['UseridAccount'];
                $userType      = $account['User_Type'];
                $accountInfo = $this->Accounts->getAccountInformation($accountUserid,1);
                if (isset($accountInfo['Company'])) {
                    $companyName = $accountInfo['Company'];
                } else {
                    $companyName = $accountInfo['NameFirst'].' '.$accountInfo['NameLast'];
                }

                /*
                 * Create and update password
                 */
                $password = random_string();
                $hashPassword = sha1($password.SITE_CODE);
                $this->User_login->changePassword($UID,$hashPassword);

                if ($userType == 1 || $userType == 100) {
                    /*
                     * Then we're resetting an owner account.
                     */
                    $message .= 'Your StuffSafe account password has been reset to: '.$password.Chr(10).Chr(10);
                } elseif ($userType == 2) {
                    /*
                     * Then we're resetting a team member account.
                     */
                    $message .= 'Your Team Member account password for '.$companyName.' has been reset to: '.$password.Chr(10).Chr(10);
                } elseif ($userType == 3) {
                    /*
                     * Then we're resetting a client account.
                     */
                    $message .= 'Your Client account password for '.$companyName.' has been reset to: '.$password.Chr(10).Chr(10);
                }
            }
            $finalMessage = $messageHeader.Chr(10).Chr(10).$message.$footer['message'];
            $this->commonemail->sendResetPasswordEmail($finalMessage,$subject,$email);
            $data['message'] = '<div class="successMessageSmall">Your new password has been sent to '.$email.'.</div>';
        }

        if (!empty($subdomain)) {
            $accountArray = $this->Accounts->getAccountInfoFromSubdomain($subdomain);
            $data['pageTitle'] = $accountArray['Company'].': Please login';
            if ($accountArray != false) {
                $logoArray = $this->Settings_update->getSettings($accountArray['AccountID'],'logoLowRes');
                if (!empty($logoArray['SettingValue'])) {
                    $data['logoValue'] = '<img src="'.$logoArray['SettingValue'].'" alt="'.$accountArray['Company'].'" />';
                } else {
                    $data['logoValue'] = $accountArray['Company'];
                }

                $data['subdomain'] = $subdomain;
                $this->load->view('public_site/LoginSubdomain',$data);
            } else {
                header('Location: '.INSECURE_URL.'login/index/missingSubdomain');
            }

        } else {
            $this->load->view('login',$data);
        }
	}

    function adminLoginToUserAccount($accountUserid) {
        /*
         * Before we commence with login, make sure user making request is an administrator.
         */
        if ($this->session->userdata('userType') != USER_TYPE_GOD || $this->session->userdata('logged') != TRUE) {
            $this->session->sess_destroy();
            header('Location: '.site_url('login'));
        }

        /*
         * Get userid and password and log em in.
         */
        $userCredsArray = $this->User_login->getLoginInformation(NULL,$accountUserid,NULL);
        /*
         * Logout first, then try logging in as user
         */
        $this->commonlogin->tryLogin($userCredsArray[0]['Userid'],$userCredsArray[0]['Password'],NULL,NULL,NULL,TRUE);
    }

    function APIAuthentication($userid,$password) {

    }
}
?>