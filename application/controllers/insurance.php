<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Insurance extends CI_Controller {
    function __construct()
	{
        parent::__construct();
        checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        $this->load->model('insurance_model','',true);
    }

    function getPolicies($render='json',$return=0) {
        $policyArray = $this->insurance_model->getPolicies($this->session->userdata('accountUserid'));

        if ($render == 'json') {
            if ($return == 0) {
                echo json_encode($policyArray);
            } else {
                return json_encode($policyArray);
            }
        } else {
            return $policyArray;
        }
    }

    function savePolicy() {
        if (isset($_POST['save']) && $_POST['save'] == 1) {

            $insArray['action']        = $_POST['action'];
            $insArray['policyID']      = $_POST['policyID'];
            $insArray['accountUserid'] = $this->session->userdata('accountUserid');
            $insArray['userid']        = $this->session->userdata('userid');
            $insArray['companyName']   = fieldToDB($this->input->post('companyName', TRUE));
            $insArray['companyURL']    = fieldToDB($this->input->post('companyURL', TRUE));
            $insArray['policyNumber']  = fieldToDB($this->input->post('policyNumber', TRUE));
            $insArray['policyDate']    = jsDateToMySQL(trim($_POST['policyDate']));
            $insArray['agentNameFirst']= fieldToDB($this->input->post('agentNameFirst', TRUE));
            $insArray['agentNameLast'] = fieldToDB($this->input->post('agentNameLast', TRUE));
            $insArray['agentEmail']    = fieldToDB($this->input->post('agentEmail', TRUE));
            $insArray['agentPhone']    = fieldToDB($this->input->post('agentPhone', TRUE));
            $insArray['agentFax']      = fieldToDB($this->input->post('agentFax', TRUE));

            $policyID = $this->insurance_model->savePolicy($insArray);
            Insurance::getPolicies();
        }
    }

    function getPolicy($policyID,$render='json',$return=0) {
        $policyArray = $this->insurance_model->getPolicy($policyID);
        $policyArray['DatePolicy'] = MySQLDateToJS($policyArray['DatePolicy']);

        if ($render == 'json') {
            if ($return == 0) {
                echo json_encode($policyArray);
            } else {
                return json_encode($policyArray);
            }
        } else {
            return $policyArray;
        }
    }

    function deletePolicy($policyID,$tagForDelete=TRUE) {
        $this->insurance_model->deletePolicy($policyID,$tagForDelete,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
        $this->commontrash->countItemsInTrash();
    }

    function restorePolicy($policyID) {
        $this->insurance_model->restorePlace($policyID,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
        $this->commontrash->countItemsInTrash();
    }
}
?>