<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Eula extends Controller {
	function __construct()
	{
		parent::Controller();
        $this->load->model('common/public_site','',true);
	}

    function _init() {
        $data['child']           = TRUE;
        $data['headerTextImage'] = '';
        $data['pageTitle']       = 'End user license agreement';
        $data['jsFileArray']     = array();

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        return $data;
    }

    function index() {
		$data = Eula::_init();
        $this->load->view('public_site/Eula',$data);
    }

    function popup() {
        $data = Eula::_init();
        $this->load->view('public_site/EulaPopup',$data);
    }
}
?>