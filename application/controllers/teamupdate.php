<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class TeamUpdate extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        
		$this->load->model('team','',true);
		$this->load->model('contacts','',true);
        $this->load->model('app_data','AppData',TRUE);

        $this->load->library('application/CommonContacts');
	}
	
	function _init() {
        global $rolesArray;
		$data['page']              = 'team';
		$data['pageTitle']         = lang('team_my_team');
		$data['wysiwyg']           = 0;
		$data['map']               = 0;
		$data['jsFileArray']       = array('jsTeam.js','jsContacts.js','jsSettings.js');
		$data['pageIconClass']     = 'iconPageTeam';
		$data['pageLayoutClass']   = 'withRightColumn';
        $data['teamMemberInfo']    = $this->commoncontacts->getContacts($this->session->userdata('accountUserid'),NULL,NULL,TRUE,FALSE,FALSE,'json',1);
		
        $data['rolesPermissions']  = $rolesArray;

        /*
		 * Create tag string for people
		 */
		$tagArray = $this->AppData->getTags($this->session->userdata('accountUserid'),'people');
		$data['tags'] = makeTagJSON($tagArray);

        /*
         * Can we even add any more team members?
         */
        $data['maxTeamMembers'] = 0;
        $teamCountArray = $this->contacts->getNumberOfContacts($this->session->userdata('accountUserid'),'team');
        $teamCount = $teamCountArray['Contacts'];
        if ($teamCount >= getAccountLevelSetting('teamMembers')) {
            $data['maxTeamMembers'] = 1;
        }

        noCache();
		return $data;
	}

	function index() {
		$data = TeamUpdate::_init();
		$this->load->view('TeamUpdate',$data);
	}
}	
?>