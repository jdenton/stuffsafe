<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('public_site','',true);
        $this->load->model('accounts','',true);

        $this->load->library('application/CommonEmail');

        $this->load->helper('account');
        $this->load->helper('payment');
	}

    function _init() {
		$data['child']           = TRUE;
        $data['headerTextImage'] = 'txtAccountHeader_public.png';
        $data['rightColumnComponents'] = array();
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'Change your plan';

        $data['currentAccountLevel'] = $this->session->userdata('accountLevel');

        switch ($data['currentAccountLevel']) {
            case 0:
                $class1 = 'green1';
                $class2 = 'blue2';
                $class3 = 'blue3';
                $buttonState1 = 'none';
                $buttonState2 = '';
                $buttonState3 = '';
                break;
            case 1:
                $class1 = 'blue1';
                $class2 = 'green1';
                $class3 = 'blue3';
                $buttonState1 = '';
                $buttonState2 = 'none';
                $buttonState3 = '';
                break;
            case 2:
                $class1 = 'blue1';
                $class2 = 'blue2';
                $class3 = 'green1';
                $buttonState1 = '';
                $buttonState2 = '';
                $buttonState3 = 'none';
                break;
        }
        $data['class1'] = $class1;
        $data['class2'] = $class2;
        $data['class3'] = $class3;
        $data['buttonState1'] = $buttonState1;
        $data['buttonState2'] = $buttonState2;
        $data['buttonState3'] = $buttonState3;
        return $data;
    }

    function changeAccount($accountUserid) {
        global $accountPricePrev,$accountPriceNext,$datePriceChange;
        $data = Account::_init();

        /*
         * We are only supposed to be here if we have an accountUserid.
         */
        if (empty($accountUserid)) {
            header('Location: '.site_url('pricing'));
        }
        $data['currentPrices'] = $accountPricePrev;
        if (days_between_dates(strtotime($datePriceChange),time())>0) {
            $data['currentPrices'] = $accountPriceNext;
        }

        /*
         * Retrieve account data and put it into session.
         */
        $accountArray = spinUpAccountSession($accountUserid);
        $this->load->view('Account',$data);
    }

    function checkPayPalProfiles() {
        $profileArray = $this->accounts->getAllAccountProfiles();
        foreach($profileArray as $profile) {
            /*
             * Go check PayPal status
             */
            $profileID = urlencode($profile['PaymentProfileID']);
            $httpParsedResponseAr = PPHttpPost('GetRecurringPaymentsProfileDetails', '&PROFILEID='.$profileID);

            if(strtoupper($httpParsedResponseAr["ACK"]) == "SUCCESS" || strtoupper($httpParsedResponseAr["ACK"]) == "SUCCESSWITHWARNING") {
                $status = urldecode($httpParsedResponseAr['STATUS']);            
                if ($status == 'Suspended') {
                    /*
                     * Log error to database
                     */
                    $errorArray['errorType']     = 'PayPal';
                    $errorArray['errorCode']     = 'SUSPENDED ACCOUNT';
                    $errorArray['accountUserid'] = $profile['AccountID'];
                    $errorArray['errorMessage']  = 'Account suspended by PayPal: Max failed billing attempts reached.';
                    $errorArray['errorPage']     = NULL;
                    $errorLogID = $this->accounts->enterPaymentErrorLog($errorArray);

                    /*
                     * Send email notification to CPO support
                     */
                    $messageAdmin  = 'Someone encountered an error while paying.'.Chr(10);
                    $messageAdmin .= 'Account ID: '.$profile['AccountID'].Chr(10);
                    $messageAdmin .= 'Profile ID: '.$profile['PaymentProfileID'].Chr(10);
                    $messageAdmin .= 'Error Code: '.$errorArray['errorCode'].Chr(10);
                    $messageAdmin .= 'Error Message: '.$errorArray['errorMessage'];
                    $subjectAdmin = "StuffSafe: Account payment problem";
                    $this->commonemail->sendEmailToAdmin($messageAdmin,$subjectAdmin);

                    /*
                     * Update paid status in account table
                     */
                    $this->accounts->changeAccountPaidStatus($profile['AccountID'],'0');

                    /*
                     * Send email to account owner with subject line of
                     * "StuffSafe Payment Error"
                     */
                }
            }
        }
    }
}
?>
