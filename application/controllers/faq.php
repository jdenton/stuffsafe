<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Faq extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('public_site','',true);
	}

    function _init() {
		$data['child']                 = TRUE;
        $data['headerTextImage']       = 'txtFAQHeader_public.png';
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'StuffSafe FAQ';
        $data['rightColumnComponents'] = array('faqNavigation');
        return $data;
    }

    function index() {
		$data = Faq::_init();
        $this->load->view('Faq',$data);
    }
}
?>