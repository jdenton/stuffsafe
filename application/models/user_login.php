<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_login extends CI_Model {
    function tryLogin($userid=NULL,$hashPass=NULL,$authString=NULL) {
        if (!empty($authString)) {
            $where = "L.AuthString = ".$this->db->escape($authString);
        } else {
            $where = "L.Userid = ".$this->db->escape($userid)." AND L.Password = ".$this->db->escape($hashPass);
        }

    	$sql = "SELECT
                    L.UID,L.Userid,L.Password,L.UseridAccount,L.User_Type,L.AuthString,L.Permissions
                FROM ss_login L
                LEFT JOIN ss_account_profile P ON P.AccountID = L.UseridAccount
                WHERE ".$where." AND P.IsActive = 1";
    	$query = $this->db->query($sql); 
    	if ($query->num_rows()<1) {
    		return false;
    	} else {
    		return $query->row_array();
    	}	
    }

    function tryLoginClient($authString) {
    	$sql = "SELECT * from ss_login WHERE
				AuthString = '$authString' AND User_Type = '".USER_TYPE_CLIENT."' AND Active = 1";
    	$query = $this->db->query($sql);
    	if ($query->num_rows()<1) {
    		return false;
    	} else {
    		return $query->row_array();
    	}
    }
    
    function updateLoginStats($userid) {
    	$sql = "UPDATE ss_login SET Logins = (Logins+1), LastLogin = '".date('Y-m-d G:i:s')."' WHERE UID = '$userid'";
		$query = $this->db->query($sql); 		
    }

    function checkAuthKey($decryptAuthKey) {
        $sql = "SELECT
                    L.UID,L.Userid,L.Password,L.UseridAccount,L.User_Type,L.Permissions,L.AuthString,L.Assignment
                FROM ss_login L
                WHERE
                L.UID = '$decryptAuthKey' AND L.Active = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
    		return $query->row_array();
    	}	
    }

    function checkAuthString($authString,$type) {
        if ($type == 'account') {
            $sql = "SELECT * FROM ss_account_profile WHERE AuthString = '$authString'";
        } elseif ($type == 'personal') {
            /*
             * NOT for clients...team members only
             */
            $sql = "SELECT 
                    L.UID,L.UseridAccount,L.Password,L.User_Type,L.RoleID,L.Assignment,L.AuthString,
                    P.Userid,P.Country,P.Timezone,P.DaylightSavingsTime,P.Language
                    FROM ss_login L
                    LEFT JOIN ss_people P ON P.LoginUserid = L.UID
                    WHERE 
                        L.AuthString = '$authString'
                        AND L.Active = 1
                        AND (L.User_Type < ".USER_TYPE_CLIENT." OR L.User_Type = ".USER_TYPE_GOD.")";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();
    	}
    }

	function saveLoginInformation($data) {
        if ($data['action'] == 'edit') {
            $sql = "UPDATE ss_login SET
                    Permissions = '".$data['RoleID']."'
                    WHERE
                        UID = '".$data['UID']."'";
            $query = $this->db->query($sql);
            $uID   = $data['UID'];
        } elseif ($data['action'] == 'add') {
            $prevUID = '';
            if (isset($data['PrevUID'])) {
                $prevUID = $data['PrevUID'];
            }

            $sql = "INSERT INTO ss_login (
                    PrevUID,
                    Userid,
                    UseridAccount,
                    Password,
                    User_Type,
                    Permissions,
                    ImportGUID
                    ) values (
                    '$prevUID',
                    '".$data['Userid']."',
                    '".$data['UseridAccount']."',
                    '".$data['Password']."',
                    '".$data['UserType']."',
                    '".$data['RoleID']."',
                    '".$this->session->userdata('importGUID')."'
                    )";
            $query = $this->db->query($sql);
            $uID = $this->db->insert_id();
        } elseif ($data['action'] == 'updateAuthString') {
            $sql = "UPDATE ss_login
                    SET AuthString = '".$data['authString']."'
                    WHERE
                        UID = '".$data['UID']."'";

            $query = $this->db->query($sql);
            $uID = $data['UID'];
        }
        return $uID;
	}

    function deleteLoginInformation($userid,$tagForDelete=TRUE) {
        if ($tagForDelete == TRUE) {
            $sql = "UPDATE ss_login SET Active = 0 WHERE UID = '$userid'";
        } else {
            $sql = "DELETE from ss_login WHERE UID = '$userid'";
        }
        $query = $this->db->query($sql);
    }

    function restoreLoginInformation($userid) {
        $sql = "UPDATE ss_login SET Active = 1 WHERE UID = '$userid'";
        $query = $this->db->query($sql);
    }

    function checkIfPasswordExists($hashPassword) {
        $sql = "SELECT UID from ss_login WHERE Password = '$hashPassword'";
        $query = $this->db->query($sql);

        if ($query->num_rows()<1) {
    		return FALSE;
    	} else {
    		return TRUE;
    	}
    }

    function changePassword($uID,$hashPassword) {
        $sql = "UPDATE ss_login SET Password = '$hashPassword' WHERE UID = '$uID'";
        $query = $this->db->query($sql);
    }

    function checkUserEmail($email) {
        $sql = "SELECT * FROM ss_login WHERE Userid = '$email' ORDER BY User_Type";
        $query = $this->db->query($sql);

        if ($query->num_rows()<1) {
    		return FALSE;
    	} else {
    		return $query->result_array();
    	}
    }

    function getLoginInformation($userid=NULL,$accountUserid=NULL,$userName=NULL) {
        if ($userid > 0) {
            /*
             * Then get login information by userid
             */
            $sql = "SELECT * from ss_login WHERE UID = '$userid'";
        } elseif ($accountUserid>0 && empty($userName)) {
            /*
             * Get login information by accountUserid only
             */
            $sql = "SELECT L.UID,L.Userid AS Userid,L.UseridAccount,L.Password,L.User_Type,L.Assignment,
                    L.Logins,L.LastLogin,L.AuthString,L.Active
                    FROM ss_login L
                    LEFT JOIN ss_people P ON P.LoginUserid = L.UID
                    WHERE P.Userid = '$accountUserid' AND P.AccountCreator = 1";
        } else {
            /*
             * Get login information by userName and accountUserid
             */
            $sql = "SELECT * from ss_login WHERE Userid = '$userName' AND UseridAccount = '$accountUserid'";
        }
        $query = $this->db->query($sql);

        if ($query->num_rows()<1) {
    		return FALSE;
    	} else {
    		return $query->result_array();
    	}
    }

    function getLoginFromPrevUID($prevUID,$accountUserid) {
        $sql = "SELECT * from ss_login WHERE PrevUID = '$prevUID' AND UseridAccount = '$accountUserid'";
        $query = $this->db->query($sql);
		if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();
    	}
    }

    function saveRememberMeGUID($uID,$expireDate,$guid) {
        $sql = "UPDATE ss_login SET
                KeepActiveGUID = '$guid',
                KeepActiveExpire = '$expireDate'
                WHERE UID = '$uID'";
        $query = $this->db->query($sql);
    }

    function getRememberMeGUID($uID) {
        $sql = "SELECT KeepActiveGUID,KeepActiveDate from ss_login WHERE UID = '$uID'";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();
    	}
    }
}
?>