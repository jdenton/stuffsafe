<?php
class Admin_users extends CI_Model {

    function Admin_users() {
        parent::__construct();
    }
    
    function getUsersForAutoCompleter($q) {
    	$sql = "SELECT AP.AccountID,AP.Company,AP.Email,P.NameFirst,P.NameLast
				FROM ss_account_profile AP
                LEFT JOIN ss_people P ON P.Userid = AP.AccountID
				WHERE 
                    (
                        P.NameFirst LIKE '$q%' OR P.NameFirst LIKE '% $q%' OR
                        P.NameLast LIKE '$q%' OR P.NameLast LIKE '% $q%' OR
                        AP.Email LIKE '$q%' OR AP.Email LIKE '% $q%' OR
                        AP.Company LIKE '$q%' OR AP.Company LIKE '% $q%' OR
                        AP.PaymentProfileID LIKE '$q%' OR AP.PaymentProfileID LIKE '% $q%'
                    )
                AND P.AccountCreator = 1
				ORDER BY AP.Company";
		$query = $this->db->query($sql);
        $count = 0;
        if ($query->num_rows()>0) {
            foreach ($query->result_array() as $row) {
                $fullDisplay = '';
                if (!empty($row['NameFirst']) || !empty($row['NameLast'])) {
                    $fullDisplay .= $row['NameFirst'].' '.$row['NameLast'].', ';
                }
                $fullDisplay .= $row['Company'].' ['.$row['Email'].']';

                $resultArray[$count]['id']    = $row['AccountID'];
                $resultArray[$count]['label'] = $fullDisplay;
                $count++;
            }
        }
		return $resultArray;
    }  

    function getUserInformation($accountUserid) {
    	$sql = "SELECT * 
				FROM ss_account_profile AP
                LEFT JOIN ss_people P ON P.Userid = AP.AccountID
				WHERE AP.AccountID = '$accountUserid' AND P.AccountCreator = 1";
		$query = $this->db->query($sql); 		
		
		return $query->row_array();
    }
    
    function getNumberUserProperties($accountUserid) {
    	$sql = "SELECT COUNT(PlaceID) AS Places from ss_places WHERE Userid = '$accountUserid'";
		$query = $this->db->query($sql); 		
		
		return $query->row_array();
    }
    
	function getNumberUserItems($accountUserid) {
    	$sql = "SELECT COUNT(InventoryID) AS Items from ss_inventory WHERE Userid = '$accountUserid'";
		$query = $this->db->query($sql); 		
		
		return $query->row_array();
    }
    
	function getNumberUserTeam($accountUserid) {
    	$sql = "SELECT COUNT(UID) AS TeamMembers from ss_login
				WHERE 
					User_Type = 2 AND
                    Active = 1 AND
					UseridAccount = '$accountUserid'";
		$query = $this->db->query($sql); 		
		
		return $query->row_array();
    }
    
	function getUserLoginInfo($accountUserid) {
    	$sql = "SELECT Userid,Logins,LastLogin from ss_login
				WHERE UseridAccount = '$accountUserid'";
		$query = $this->db->query($sql); 		
		
		return $query->row_array();
    }
    
	function toggleUserStatus($accountID) {
    	$sql = "UPDATE ss_account_profile SET IsActive = 1 - IsActive
				WHERE AccountID = '$accountID'";
		$query = $this->db->query($sql); 
    }
}
?>