<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Insurance_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getPolicies($accountUserid) {
        $sql = "SELECT * from ss_insurance WHERE Userid = '$accountUserid' AND Deleted = 0 ORDER BY DatePolicy";
        $query = $this->db->query($sql);
        $insuranceArray = $query->result_array();

        $j=0;
        foreach($insuranceArray as $insuranceItem) {
            if ($insuranceItem['DatePolicy'] != '0000-00-00') {
                $insuranceArray[$j]['DatePolicy'] = date('M j, Y', strtotime($insuranceItem['DatePolicy']));
            } else {
                $insuranceArray[$j]['DatePolicy'] = '';
            }
            $j++;
        }

        return $insuranceArray;
    }

    function savePolicy($data) {
        if ($data['action'] == 'add') {
            $sql = "INSERT into ss_insurance (
                    Userid,
                    Company,
                    URL,
                    AgentNameFirst,
                    AgentNameLast,
                    AgentPhone,
                    AgentFax,
                    AgentEmail,
                    PolicyNo,
                    DatePolicy,
                    DateEntered,
                    UseridUpdate,
                    Deleted
                    ) values (
                    '".$data['accountUserid']."',
                    '".$data['companyName']."',
                    '".$data['companyURL']."',
                    '".$data['agentNameFirst']."',
                    '".$data['agentNameLast']."',
                    '".$data['agentPhone']."',
                    '".$data['agentFax']."',
                    '".$data['agentEmail']."',
                    '".$data['policyNumber']."',
                    '".$data['policyDate']."',
                    '".date('Y-m-d')."',
                    '".$data['userid']."',
                    '0'
                    )";
            $query = $this->db->query($sql);
    		$policyID = $this->db->insert_id();
        } else {
            $sql = "UPDATE ss_insurance SET
                        Company        = '".$data['companyName']."',
                        URL            = '".$data['companyURL']."',
                        AgentNameFirst = '".$data['agentNameFirst']."',
                        AgentNameLast  = '".$data['agentNameLast']."',
                        AgentPhone     = '".$data['agentPhone']."',
                        AgentFax       = '".$data['agentFax']."',
                        AgentEmail     = '".$data['agentEmail']."',
                        PolicyNo       = '".$data['policyNumber']."',
                        DatePolicy     = '".$data['policyDate']."',
                        UseridUpdate   = '".$data['userid']."'
                    WHERE
                    PolicyID = '".$data['policyID']."'";
            $query = $this->db->query($sql);
            $policyID = $data['policyID'];
        }

        return $policyID;
    }

    function getPolicy($policyID) {
        $sql = "SELECT * from ss_insurance WHERE PolicyID = $policyID";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function deletePolicy($policyID,$tagForDelete=TRUE,$userid,$accountUserid) {
        if ($tagForDelete == TRUE) {
            $sql = "UPDATE ss_insurance SET UseridUpdate = '$userid', Deleted = 1
                    WHERE PolicyID = '$policyID' AND Userid = '$accountUserid'";
            $query = $this->db->query($sql);
        } else {
            $sql = "DELETE FROM ss_insurance WHERE PolicyID = '$policyID'";
            $query = $this->db->query($sql);
        }
    }

    function restorePolicy($policyID,$userid,$accountUserid) {
        $sql = "UPDATE ss_insurance SET UseridUpdate = '$userid', Deleted = 0
                WHERE PolicyID = '$policyID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
    }
}
?>