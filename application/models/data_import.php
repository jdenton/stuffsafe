<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Data_import extends CI_Model {
    function checkForImportGUID($accountUserid) {
        $sql = "SELECT * FROM ss_import_guid WHERE Userid = '$accountUserid'";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
    		return $query->result_array();
    	}
    }

    function deletePreviousImportData($importGUID) {
        $tableArray = array(
            'ss_contact',
            'ss_insurance',
            'ss_inventory',
            'ss_login',
            'ss_main_cat',
            'ss_people',
            'ss_people_contact_info',
            'ss_places',
            'ss_room',
            'ss_files',
            'ss_linkPeopleItemType',
            'ss_linkFileItemType'
        );

        for($a=0;$a<=count($tableArray)-1;$a++) {
            $sql = "DELETE from ".$tableArray[$a]." WHERE ImportGUID = '$importGUID'";
            $query = $this->db->query($sql);
        }
    }

    function saveImportGUID($data) {
        if ($data['action'] == 'add') {
            $sql = "INSERT into ss_import_guid (
                    ImportGUID,
                    Userid,
                    DateCreated
                    ) values (
                    '".$data['importGUID']."',
                    '".$data['accountUserid']."',
                    '".date('Y-m-d G:i:s')."'
                    )";
        } else {
            $sql = "UPDATE ss_import_guid SET
                    ImportGUID  = '".$data['importGUID']."',
                    DateCreated = '".date('Y-m-d G:i:s')."'
                    WHERE
                    Userid = '".$data['accountUserid']."'";
        }
        $query = $this->db->query($sql);
    }
}