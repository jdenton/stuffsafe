<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Places_view extends CI_Model {
    function __construct() {
        parent::__construct();
		$this->load->model('rooms_view','',true);
    }

    function getPlaces($accountUserid=null,$userid=null,$acdc=null,$tag=null,$viewAll=true,$roomsAlso=false) {
        $from = " ss_places P";
        $viewAllJoin = "";
        if ($viewAll == FALSE) {
            $from  = " ss_linkPeopleItemType IT";
            $viewAllJoin = " LEFT JOIN ss_places P ON P.PlaceID = IT.ItemID";
        }
		$sql = "SELECT
					P.PlaceID,P.PlaceName,P.Address,P.City,P.State,P.Zip,P.Country,P.Description,
                    P.Category,P.Area,P.AreaUnit,P.Tags,P.DateEntered,P.UseridUpdate,PP.PID,PP.NameFirst,PP.NameLast
                FROM $from
                LEFT JOIN ss_people PP ON PP.LoginUserid = P.UseridUpdate
                $viewAllJoin
				WHERE
                    P.Deleted = 0 AND
                    P.Userid = '".$accountUserid."'
                    GROUP BY P.PlaceID
                    ORDER BY P.PlaceName";
		$query = $this->db->query($sql);
		$returnArray = $query->result_array();

        if ($roomsAlso == true) {
            $j=0;
            foreach ($query->result_array() as $row) {
                $returnArray[$j]['PlaceID']      = $row['PlaceID'];
                $returnArray[$j]['PlaceName']    = $row['PlaceName'];
                $returnArray[$j]['Address']      = $row['Address'];
                $returnArray[$j]['City']         = $row['City'];
                $returnArray[$j]['State']        = $row['State'];
                $returnArray[$j]['Zip']          = $row['Zip'];
                $returnArray[$j]['Country']      = $row['Country'];
                $returnArray[$j]['Category']     = $row['Category'];
                $returnArray[$j]['Area']         = $row['Area'];
                $returnArray[$j]['AreaUnit']     = $row['AreaUnit'];
                $returnArray[$j]['Tags']         = $row['Tags'];
                $returnArray[$j]['Description']  = $row['Description'];
                $returnArray[$j]['DateEntered']  = $row['DateEntered'];
                $returnArray[$j]['DateEnteredHuman'] = date('M j, Y', strtotime($row['DateEntered']));
                $returnArray[$j]['UseridUpdate'] = $row['UseridUpdate'];
                $returnArray[$j]['Rooms']        = $this->rooms_view->getRoomsForPlace($row['PlaceID']);
                $returnArray[$j]['EnteredBy']    = $row['NameFirst'].' '.$row['NameLast'];
                $j++;
            }
        }
        return $returnArray;
    }

    function getPlaceInformation($placeID) {
        $sql = "SELECT
					P.PlaceID,P.PlaceName,P.Address,P.City,P.State,P.Zip,P.Country,P.Description,
                    P.Category,P.Area,P.AreaUnit,P.Tags,P.DateEntered,P.UseridUpdate,PP.NameFirst,PP.NameLast
                FROM ss_places P
                LEFT JOIN ss_people PP ON PP.LoginUserid = P.UseridUpdate
				WHERE
                    P.PlaceID = $placeID";
		$query = $this->db->query($sql);
		return $query->row_array();
    }

    function checkForValidPlace($placeID,$accountUserid) {
          $sql = "SELECT PlaceID
                    FROM ss_places
                    WHERE
					PlaceID = '$placeID' AND
					Userid = '".$accountUserid."'";
          $query = $this->db->query($sql);
          if ($query->num_rows()<1) {
               return FALSE;
          }
          return TRUE;
     }
}
?>