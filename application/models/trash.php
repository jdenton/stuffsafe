<?
class Trash extends CI_Model {
    /**
	 * getNumberOfItemsInTrash : gets number of items tagged for deletion.
	 *
	 * @access public
	 * @return int    $trashItems the number of items tagged for deletion.
	 */
    function getNumberOfItemsInTrash($userid=NULL,$accountUserid=NULL) {
         $trashItems = 0;

         $trashTableArray = array(
                    'ss_places',
                    'ss_room',
                    'ss_inventory',
                    'ss_people'
         );

         foreach($trashTableArray as $table) {
             $sql = "SELECT COUNT(Deleted) AS Deleted from $table WHERE Userid = '$accountUserid' AND Deleted = 1";
             $query = $this->db->query($sql);
             $row = $query->row_array();
             $trashItems = $trashItems+$row['Deleted'];
         }
         return $trashItems;
    }

    function getNumberOfItemsInTrashByType($itemType,$userid=NULL,$accountUserid=NULL) {
        $sql = '';
        switch ($itemType) {
            case 'inventory':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from ss_inventory WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from ss_inventory WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }                
                break;
            case 'property':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from ss_places WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from ss_places WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }                
                break;
            case 'people':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from ss_people WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from ss_people WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }                
                break;
            case 'rooms':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from ss_room WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from ss_room WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }                
                break;
            case 'invoice':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_invoice WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_invoice WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }
                break;
            case 'message':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_messages WHERE UIDFrom = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_messages WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }                
                break;
            case 'note':
                $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_notes WHERE Userid = '$userid' AND Deleted = 1";
                break;
            case 'contact':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_people WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_people WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }                
                break;
            case 'task':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_project_tasks WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_project_tasks WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }
                break;
            case 'project':
                if ($accountUserid > 0) {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_project WHERE Userid = '$accountUserid' AND Deleted = 1";
                } else {
                    $sql = "SELECT COUNT(Deleted) AS Deleted from cpo_project WHERE UseridUpdate = '$userid' AND Deleted = 1";
                }
                break;
        }
        $query = $this->db->query($sql);
        $row = $query->row_array();

        return $row['Deleted'];
    }

    function getItemsInTrashByType($itemType,$userid=NULL,$accountUserid=NULL) {
        $sql = '';
        switch ($itemType) {
            case 'inventory':
                if ($accountUserid > 0) {
                    $sql = "SELECT InventoryID AS ID,ItemName AS Title from ss_inventory WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT InventoryID AS ID,ItemName AS Title from ss_inventory WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'property':
                if ($accountUserid > 0) {
                    $sql = "SELECT PlaceID AS ID,PlaceName AS Title from ss_places WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT PlaceID AS ID,PlaceName AS Title from ss_places WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'people':
                if ($accountUserid > 0) {
                    $sql = "SELECT PID AS ID,CONCAT(NameFirst,' ',NameLast) AS Title from ss_people WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT PID AS ID,CONCAT(NameFirst,' ',NameLast) AS Title from ss_people WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'rooms':
                if ($accountUserid > 0) {
                    $sql = "SELECT ExpenseID AS ID,Title from ss_room WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT ExpenseID AS ID,Title from ss_room WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'invoice':
                if ($accountUserid > 0) {
                    $sql = "SELECT InvoiceID AS ID,Title from cpo_invoice WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT InvoiceID AS ID,Title from cpo_invoice WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'message':
                if ($accountUserid > 0) {
                    $sql = "SELECT MID AS ID,Message AS Title from cpo_messages WHERE UIDFrom = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT MID AS ID,Message AS Title from cpo_messages WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'note':
                $sql = "SELECT NID AS ID,Note AS Title from cpo_notes WHERE Userid = '$userid' AND Deleted = 1 ORDER BY Title";
                break;
            case 'contact':
                if ($accountUserid > 0) {
                    $sql = "SELECT PID AS ID,CONCAT(NameFirst,' ',NameLast,' ',Company) AS Title from cpo_people WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY NameLast";
                } else {
                    $sql = "SELECT PID AS ID,CONCAT(NameFirst,' ',NameLast,' ',Company) AS Title from cpo_people WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY NameLast";
                }
                break;
            case 'task':
                if ($accountUserid > 0) {
                    $sql = "SELECT TaskID AS ID, Title from cpo_project_tasks WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT TaskID AS ID, Title from cpo_project_tasks WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
            case 'project':
                if ($accountUserid > 0) {
                    $sql = "SELECT ProjectID AS ID,Title from cpo_project WHERE Userid = '$accountUserid' AND Deleted = 1 ORDER BY Title";
                } else {
                    $sql = "SELECT ProjectID AS ID,Title from cpo_project WHERE UseridUpdate = '$userid' AND Deleted = 1 ORDER BY Title";
                }
                break;
        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
?>