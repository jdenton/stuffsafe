<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Accounts extends CI_Model {
    function getAccountInformation($accountID,$isActive=null) {
        $sql = "SELECT * from ss_account_profile
                WHERE AccountID = '$accountID'";
        if ($isActive == 1) {
    		$sql .= " AND IsActive = '1'";
    	}
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();
        }
    }

    function getAllAccountProfiles() {
        $sql = "SELECT AccountID,Email,AccountLevel,AccountPrice,Account_No,PaymentProfileID
                FROM ss_account_profile WHERE
                PaymentProfileID IS NOT NULL AND
                PaymentProfileID <> '000000' AND
                IsActive = 1";
        $query = $this->db->query($sql);
		return $query->result_array();
    }

    function updateAccountWithPaymentInformation($data) {
        $sql = "UPDATE ss_account_profile SET
                    AccountLevel         = '".$data['accountLevel']."',
                    AccountPrice         = '".$data['accountPrice']."',
                    PaymentProfileID     = '".$data['profileID']."',
                    IsPaid               = '1',
                    BillingNameFirst     = '".$data['billingNameFirst']."',
                    BillingNameLast      = '".$data['billingNameLast']."',
                    BillingAddress       = '".$data['billingAddress']."',
                    BillingCity          = '".$data['billingCity']."',
                    BillingStateRegion   = '".$data['billingState']."',
                    BillingZipPostalCode = '".$data['billingZip']."',
                    BillingCountry       = '".$data['billingCountry']."',
                    BillingCurrency      = '".$data['billingCurrency']."',
                    DatePaid             = '".date('Y-m-d')."'
                WHERE
                AccountID = '".$data['accountID']."'";
        $query = $this->db->query($sql);
    }

    function enterPaymentErrorLog($errorData) {
        $sql = "INSERT into ss_errors (
                DateError,
                AccountUserid,
                ErrorType,
                ErrorCode,
                ErrorMessage,
                ErrorPage
                ) values (
                '".date('Y-m-d G:i:s')."',
                '".$errorData['accountUserid']."',
                '".$errorData['errorType']."',
                '".$errorData['errorCode']."',
                '".$errorData['errorMessage']."',
                '".$errorData['errorPage']."'
                )";
        $query = $this->db->query($sql);
    	return $this->db->insert_id();
    }

    function checkAccountLevelStatus($accountUserid) {
        $sql = "SELECT AccountLevel,PaymentProfileID,IsPaid from ss_account_profile WHERE AccountID = '$accountUserid'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function changeAccountPaidStatus($accountUserid,$newStatus) {
        $sql = "UPDATE ss_account_profile SET IsPaid = '$newStatus' WHERE AccountID = '$accountUserid'";
        $query = $this->db->query($sql);
    }

    function cancelAccount($accountUserid) {
        /*
         * Deactivate account owner login
         */
        $sql = "UPDATE ss_login SET Active = '0' WHERE UID = '$accountUserid'";
        $query = $this->db->query($sql);

        /*
         * Deactivate other account members
         */
        $sql = "UPDATE ss_login SET Active = '0' WHERE UseridAccount = '$accountUserid'";
        $query = $this->db->query($sql);

        /*
         * Deactivate account profile
         */
        $sql = "UPDATE ss_account_profile SET IsActive = '0' WHERE AccountID = '$accountUserid'";
        $query = $this->db->query($sql);
    }

    function getAccountInfoFromSubdomain($subdomain) {
        $sql = "SELECT A.AccountID,A.Company,A.Email,A.URL
                FROM ss_account_profile A
                WHERE A.SubDomain = '".$subdomain."'";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();
        }
    }

    function getProductAccountInformation($accountID) {
        $sql = "SELECT * from ss_account_profile_product WHERE AccountID = $accountID";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getNewSelfHostedOrderNumber() {
        $sql = "SELECT MAX(OrderNumber) AS MaxOrderNumber from ss_account_profile_product";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['MaxOrderNumber']+1;
    }

    function checkSubscriptionStatus($accountID,$datePaid) {
        $sql = "SELECT * from ss_product_payment
                WHERE
                    AccountID = '$accountID' AND
                    IsUpdate  = 1 AND
                    DatePaid >= '$datePaid'";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();;
        }
    }

    function updateAccountInformation($data) {
        $sql = "UPDATE ss_account_profile SET
                    NameFirst   = '".$data['nameFirst']."',
                    NameLast    = '".$data['nameLast']."',
                    Company     = '".$data['company']."',
                    Address1    = '".$data['address1']."',
                    Address2    = '".$data['address2']."',
                    City        = '".$data['city']."',
                    State       = '".$data['state']."',
                    Zip         = '".$data['zip']."',
                    PhoneOffice = '".$data['phone']."',
                    Email       = '".$data['email']."',
                    Country     = '".$data['country']."',
                    Currency    = '".$data['currency']."'
                WHERE
                AccountID = ".$data['accountID'];
        $query = $this->db->query($sql);
    }
}
?>
