<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rooms_view extends CI_Model {
    
    function getRoomsForPlace($placeID,$accountUserd=null) {
        $sql = "SELECT
                R.RoomID,R.RoomName,R.Tags,R.DateEntered,R.UseridUpdate,
                (SELECT SUM(I.PriceReplace) FROM ss_inventory I WHERE I.RoomID = R.RoomID AND I.Deleted = 0) AS RoomTotal,
                (SELECT COUNT(I.InventoryID) FROM ss_inventory I WHERE I.RoomID = R.RoomID AND I.Deleted = 0) AS RoomItemCount,
                PP.PID,CONCAT(PP.NameFirst,' ',PP.NameLast) AS EnteredBy
                FROM ss_room R
                LEFT JOIN ss_people PP ON PP.LoginUserid = R.UseridEntered
                WHERE R.PlaceID = '$placeID' AND R.Deleted = 0
                ORDER BY RoomName";
        $query = $this->db->query($sql);
		return $query->result_array();
    }

    function getRoomsForSelect($accountUserid,$placeID=NULL,$autocomplete=false) {
        if ($placeID>0) {
            $sql = "SELECT RoomID,RoomName from ss_room WHERE PlaceID = $placeID AND Deleted = 0";
        } else {
            $sql = "SELECT DISTINCT(RoomName),RoomID from ss_room WHERE Userid = $accountUserid AND Deleted = 0 GROUP BY RoomName";
        }
        $query = $this->db->query($sql);
        $results = $query->result_array();
        $resultArray = array();
        if ($autocomplete == true) {
            $a=0;
            foreach($results as $result) {
                $resultArray[$a]['label'] = $result['RoomName'];
                $resultArray[$a]['value'] = $result['RoomID'];
                $a++;
            }
            return $resultArray;
        }

		return $results;
    }

    function checkRoomExistsForPlace($roomName,$placeID) {
        $sql = "SELECT RoomID from ss_room WHERE RoomName = '$roomName' AND PlaceID = '$placeID'";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
           return FALSE;
        }
        $results = $query->row_array();
        return $results['RoomID'];
    }

    function getRoom($roomID) {
        $sql = "SELECT * from ss_room WHERE RoomID = $roomID";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
            return FALSE;
        }
        return $query->row_array();
    }
}
?>