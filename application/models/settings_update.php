<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings_update extends CI_Model {
	function saveSettings($accountUserid,$userid,$data) {
		/*
		 * Is this a member or account setting?
		 */
		if ($data['group'] == 'member') {
			$saveUserid = $userid;
		} else {
			$saveUserid = $accountUserid;
		}

		/*
		 * Check to see if this setting already has a value in the database
		 */
		$sql = "SELECT * FROM ss_settings
				WHERE
					Userid = '$saveUserid'
					AND Setting = '".$data['type']."'";
		$query = $this->db->query($sql);

		if ($query->num_rows()<1) {
			/*
			 * We're adding a new record
			 */
			$sql = "INSERT into ss_settings (
					Userid,
					Setting,
					SettingValue,
                    SettingGroup,
                    SettingSection,
					UseridUpdate,
					DateUpdate
					) values (
					'".$saveUserid."',
					'".$data['type']."',
					'".$data['value']."',
                    '".$data['group']."',
                    '".$data['section']."',
					'".$userid."',
					'".date('Y-m-d')."'
					)";
			$query = $this->db->query($sql);
		} else {
			/*
			 * We're editing an existing record
			 */
			$sql = "UPDATE ss_settings SET
					Setting      = '".$data['type']."',
					SettingValue = '".$data['value']."',
					UseridUpdate = '".$userid."',
					DateUpdate   = '".date('Y-m-d')."'
					WHERE
						Userid = '$saveUserid'
						AND Setting = '".$data['type']."'";
			$query = $this->db->query($sql);
		}
	}

	function getSettings($userid,$setting=NULL,$group=NULL,$section=NULL) {
        $where = '';
		if (!empty($setting)) {
			$where .= "Setting = '$setting' AND ";
		}
        if (!empty($group)) {
			$where .= "SettingGroup = '$group' AND ";
		}
        if (!empty($section)) {
			$where .= "SettingSection = '$section' AND ";
		}
		$sql = "SELECT * from ss_settings
				WHERE
				$where
				Userid = '$userid'";
		$query = $this->db->query($sql);
		if ($query->num_rows() == 1 && $setting != NULL) {
			return $query->row_array();
        } elseif ($query->num_rows() < 1) {
            return FALSE;
		} else {
            return $query->result_array();
		}
	}

    function saveCompanyInformation($settingArray) {
        $sql = "UPDATE ss_account_profile SET
                    Company          = '".$settingArray['accountCompanyName']."',
                    Address1         = '".$settingArray['accountAddress1']."',
                    Address2         = '".$settingArray['accountAddress2']."',
                    City             = '".$settingArray['accountCity']."',
                    State            = '".$settingArray['accountState']."',
                    Zip              = '".$settingArray['accountZip']."',
                    PhoneOffice      = '".$settingArray['accountPhoneOffice']."',
                    PhoneFax         = '".$settingArray['accountFax']."',
                    PhoneCell        = '".$settingArray['accountMobile']."',
                    Email            = '".$settingArray['accountEmail']."',
                    URL              = '".$settingArray['accountURL']."',
                    Company_Industry = '".$settingArray['accountIndustry']."',
                    Country          = '".$settingArray['accountCountry']."',
                    Currency         = '".$settingArray['accountCurrency']."',
                    SubDomain        = '".$settingArray['accountWebAddress']."'
                WHERE
                AccountID = '".$settingArray['accountID']."'";
        $query = $this->db->query($sql);
    }

    function changeLanguage($langCode,$userid) {
        $sql = "UPDATE ss_people SET Language = '$langCode' WHERE LoginUserid = '$userid'";
        $query = $this->db->query($sql);
    }    
}
?>