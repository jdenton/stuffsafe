<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rooms_update extends CI_Model {
    function saveRoom($data) {
        if ($data['action'] == 'add') {
            $sql = "INSERT into ss_room (
                    Userid,
                    PlaceID,
                    RoomName,
                    Tags,
                    DateEntered,
                    UseridEntered,
                    UseridUpdate
                    ) values (
                    '".$data['accountUserid']."',
                    '".$data['placeID']."',
                    '".$data['roomName']."',
                    '".$data['tags']."',
                    '".date('Y-m-d')."',
                    '".$data['userid']."',
                    '".$data['userid']."'
                    )";
            $query = $this->db->query($sql);
    		$roomID = $this->db->insert_id();
        } else {
            $sql = "UPDATE ss_room SET
                    RoomName     = '".$data['roomName']."',
                    Tags         = '".$data['tags']."'
                    UseridUpdate = '".$data['userid']."'
                    WHERE RoomID = ".$data['roomID'];
            $query = $this->db->query($sql);
            $roomID = $data['roomID'];
        }
        return $roomID;
    }

    function updateRoomName($roomID,$roomName) {
        $sql = "UPDATE ss_room SET RoomName = '".$roomName."' WHERE RoomID = $roomID";
        $query = $this->db->query($sql);
    }

    function deleteRoom($roomID,$withInventory,$tagForDelete=TRUE,$userid=NULL,$accountUserid=NULL) {
        if ($tagForDelete == TRUE) {
            $sql = "UPDATE ss_room SET
                        UseridUpdate = '$userid',
                        Deleted = 1
                    WHERE
                        RoomID = '$roomID' AND
                        Userid = '$accountUserid'";
            $query = $this->db->query($sql);

            if ($withInventory == 1) {
                $sql = "UPDATE ss_inventory SET
                            UseridUpdate = '$userid',
                            Deleted = 1
                        WHERE
                            RoomID = '$roomID' AND
                            Userid = '$accountUserid'";
                $query = $this->db->query($sql);
            }
        } else {
            $sql = "DELETE FROM ss_room WHERE RoomID = '$roomID' AND Userid = '$accountUserid'";
            $query = $this->db->query($sql);

            if ($withInventory == 1) {
                $sql = "DELETE from ss_inventory WHERE RoomID = '$roomID' AND Userid = '$accountUserid'";
                $query = $this->db->query($sql);
            }
        }
    }

    function restoreRoom($roomID,$userid,$accountUserid=NULL) {
        $sql = "UPDATE ss_room SET UseridUpdate = '$userid', Deleted = 0 WHERE RoomID = '$roomID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);

        $sql = "UPDATE ss_inventory SET UseridUpdate = '$userid', Deleted = 0 WHERE RoomID = '$roomID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
    }
}