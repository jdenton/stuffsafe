<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventory_view extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->model('rooms_view','',true);
        $this->load->model('file_maintenance','',true);

        $this->load->library('application/CommonFileManager');
    }

    function getInventoryForPlace($placeID,$accountUserid) {
        $roomArray = $this->rooms_view->getRoomsForPlace($placeID,$accountUserid);
        $j = 0;
        $returnArray = array();
        foreach($roomArray as $room) {
            $returnArray[$j]['RoomID']    = $room['RoomID'];
            $returnArray[$j]['RoomName']  = $room['RoomName'];
            $returnArray[$j]['RoomTags']  = $room['Tags'];
            $returnArray[$j]['EnteredBy'] = $room['EnteredBy'];
            $returnArray[$j]['Inventory'] = Inventory_view::getInventoryForRoom($room['RoomID']);
            $returnArray[$j]['RoomItemCount'] = count($returnArray[$j]['Inventory']);
            $j++;
        }
        return $returnArray;
    }

    function getInventoryTotalsForPlace($placeID,$accountUserid) {
        $roomArray = $this->rooms_view->getRoomsForPlace($placeID,$accountUserid);
        $j = 0;
        $totalPricePurchase = 0;
        $totalPriceReplace = 0;
        foreach($roomArray as $room) {
            $totalArray = Inventory_view::getInventoryTotalsForRoom($room['RoomID']);
            $totalPricePurchase = $totalPricePurchase + $totalArray['TotalPurchase'];
            $totalPriceReplace = $totalPriceReplace + $totalArray['TotalReplace'];
            $j++;
        }
        $returnArray = array('TotalPricePurchase' => $totalPricePurchase, 'TotalPriceReplace' => $totalPriceReplace);
        return $returnArray;
    }

    function getInventoryTotalsForRoom($roomID) {
        $sql = "SELECT
                SUM(PriceReplace) AS TotalReplace,
                SUM(PricePurchase) AS TotalPurchase
                from ss_inventory WHERE RoomID = $roomID AND Deleted = 0";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getInventoryForRoom($roomID) {
        $sql = "SELECT
                I.InventoryID,I.RoomID,I.CatID,I.ItemName,I.ItemDesc,I.ItemMake,I.ItemModel,I.ItemSerial,
                I.PricePurchase,I.PriceReplace,I.ItemCondition,I.DatePurchase,I.Tags,I.DateEntered,
                PP.PID,CONCAT(PP.NameFirst,' ',PP.NameLast) AS EnteredBy,
                C.MainCat,R.RoomName
                FROM ss_inventory I
                LEFT JOIN ss_people PP ON PP.LoginUserid = I.UseridUpdate
                LEFT JOIN ss_room R ON R.RoomID = $roomID
                LEFT JOIN ss_main_cat C ON C.CatID = I.CatID
                WHERE I.RoomID = $roomID AND I.Deleted = 0
                ORDER BY I.ItemName ASC";
        $query = $this->db->query($sql);
		$inventoryArray = $query->result_array();

        $j=0;
        foreach($inventoryArray as $inventoryItem) {
            $inventoryArray[$j]['DateEnteredHuman'] = date('M j, Y', strtotime($inventoryItem['DateEntered']));
            if ($inventoryItem['DatePurchase'] != '0000-00-00') {
                $inventoryArray[$j]['DatePurchaseHuman'] = date('M j, Y', strtotime($inventoryItem['DatePurchase']));
            } else {
                $inventoryArray[$j]['DatePurchaseHuman'] = '';
            }
            $inventoryArray[$j]['Files'] = $this->commonfilemanager->getFilesForSomething($inventoryItem['InventoryID'],'inventory','array',1);
            
            $gotThumbnail = FALSE;
            $thumbnailPath = '';
            $fileID = '';
            if (count($inventoryArray[$j]['Files'])>0) {
                foreach($inventoryArray[$j]['Files'] as $file) {
                    if ($file['FileType'] == 'image/png' || $file['FileType'] == 'image/jpeg' || $file['FileType'] == 'image/gif' || $file['FileType'] == 'image/jpg') {
                        $gotThumbnail = TRUE;
                        $thumbnailPath = $file['thumbnailLink'];
                        $fileID = $file['FileID'];
                    }
                }
            }
            $inventoryArray[$j]['AvatarURL'] = $thumbnailPath;
            $inventoryArray[$j]['FileID'] = $fileID;

            $j++;
        }
        return $inventoryArray;
    }

    function getInventoryAvatarImages($placeID=NULL,$roomID=NULL) {
        if ($placeID != NULL) {
            $sql = "SELECT RoomID from ss_room WHERE PlaceID = $placeID";
        } else if ($roomID != NULL) {
            
        }
        $query = $this->db->query($sql);
        $roomArray = $query->result_array();
        
        $avatarArray = array();
        $a = 0;
        foreach($roomArray as $roomID) {
            $avatarArray[$a] = $roomID['RoomID'];
            $a++;
        }        
        return $avatarArray;
    }

    function getInventoryItem($inventoryID,$accountUserid) {
        $sql = "SELECT * FROM ss_inventory I
                LEFT JOIN ss_room R ON R.RoomID = I.RoomID
                WHERE I.InventoryID = '$inventoryID' AND I.Userid = '$accountUserid'";
        $query = $this->db->query($sql);
        $inventoryArray = $query->row_array();
        $inventoryArray['Files'] = $this->commonfilemanager->getFilesForSomething($inventoryID,'inventory','array',1);
        
        $gotThumbnail = FALSE;
        $thumbnailPath = '';
        $fileID = '';
        if (count($inventoryArray['Files'])>0) {
            foreach($inventoryArray['Files'] as $file) {
                if ($file['FileType'] == 'image/png' || $file['FileType'] == 'image/jpeg' || $file['FileType'] == 'image/gif' || $file['FileType'] == 'image/jpg') {
                    $gotThumbnail = TRUE;
                    $thumbnailPath = $file['thumbnailLink'];
                    $fileID = $file['FileID'];
                }
            }
        }
        $inventoryArray['AvatarURL'] = $thumbnailPath;
        $inventoryArray['FileID'] = $fileID;
        
        return $inventoryArray;
    }

    function getNumberOfItemsForRoom($roomID) {
        $sql = "SELECT COUNT(InventoryID) AS ItemCount from ss_inventory WHERE RoomID = $roomID AND Deleted = 0";
        $query = $this->db->query($sql);
        return $query->row_array();
    }
}
