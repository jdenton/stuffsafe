<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventory_reports extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function getReportPurchaseReplace($accountUserid,$type) {
        if ($type == 'category') {
            $sqlReplace = "SELECT SUM(I.PriceReplace) AS TotalReplace,M.MainCat AS LabelReplace
                    FROM ss_inventory I
                    LEFT JOIN ss_main_cat M ON M.CatID = I.CatID
                    WHERE I.Deleted = 0 AND
                    I.Userid = '".$accountUserid."'
                    GROUP BY M.CatID";
            $sqlPurchase = "SELECT SUM(I.PricePurchase) AS TotalPurchase,M.MainCat AS LabelPurchase
                    FROM ss_inventory I
                    LEFT JOIN ss_main_cat M ON M.CatID = I.CatID
                    WHERE I.Deleted = 0 AND
                    I.Userid = '".$accountUserid."'
                    GROUP BY M.CatID";

            $queryReplace  = $this->db->query($sqlReplace);
            $queryPurchase = $this->db->query($sqlPurchase);

            $replaceArray  = $queryReplace->result_array();
            $purchaseArray = $queryPurchase->result_array();
        } elseif ($type == 'property') {
            $sql = "SELECT PlaceID,PlaceName from ss_places
                WHERE Userid = $accountUserid AND Deleted = 0";
            $query = $this->db->query($sql);
            $placeArray = $query->result_array();
            $result = array();
            $j=0;
            foreach($placeArray as $place) {
                $sql = "SELECT SUM(I.PriceReplace) AS TotalReplace
                        FROM ss_inventory I
                        LEFT JOIN ss_room R ON R.RoomID = I.RoomID
                        WHERE
                        R.PlaceID = '".$place['PlaceID']."' AND
                        I.Deleted = 0";
                $query = $this->db->query($sql);
                $row = $query->row_array();
                
                $replaceArray[$j] = array(
                    'TotalReplace' => $row['TotalReplace'],
                    'LabelReplace' => $place['PlaceName']
                );

                $sql = "SELECT SUM(I.PricePurchase) AS TotalPurchase
                        FROM ss_inventory I
                        LEFT JOIN ss_room R ON R.RoomID = I.RoomID
                        WHERE
                        R.PlaceID = '".$place['PlaceID']."' AND
                        I.Deleted = 0";
                $query = $this->db->query($sql);
                $row = $query->row_array();

                $purchaseArray[$j] = array(
                    'TotalPurchase' => $row['TotalPurchase'],
                    'LabelPurchase' => $place['PlaceName']
                );

                $j++;
            }
        }       

        $resultArray = array(
                                'Replace'=>$replaceArray,
                                'Purchase'=>$purchaseArray
                            );
        return $resultArray;
    }

    function getReportValueByProperty($accountUserid) {
        $sql = "SELECT PlaceID,PlaceName from ss_places
                WHERE Userid = $accountUserid AND Deleted = 0";
        $query = $this->db->query($sql);
        $placeArray = $query->result_array();
        $result = array();
        $j=0;
        foreach($placeArray as $place) {
            $result[$j]['Label'] = $place['PlaceName'];
            $sql = "SELECT SUM(I.PriceReplace) AS Total
                    FROM ss_inventory I
                    LEFT JOIN ss_room R ON R.RoomID = I.RoomID
                    WHERE
                    R.PlaceID = '".$place['PlaceID']."' AND
                    I.Deleted = 0";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            $result[$j]['Total'] = $row['Total'];
            $j++;
        }

        return $result;
    }

    function getReportValueByCategory($accountUserid) {
        $sql = "SELECT SUM(I.PriceReplace) AS Total,M.MainCat AS Label
                FROM ss_inventory I
                LEFT JOIN ss_main_cat M ON M.CatID = I.CatID
                WHERE I.Deleted = 0 AND
                I.Userid = '".$accountUserid."'
                GROUP BY M.CatID";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
?>