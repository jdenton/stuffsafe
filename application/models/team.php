<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Team extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    /**
	 * getTeamMembersForOwner : retrieve all team member information  
	 * for an account.
	 *
	 * @access	public
	 * @param	int    $userid Userid of account owner.
	 * @param   string $data 'full' for complete records, 'short' for only ID and Full Name
	 * @return	array
	 */	
    function getTeamMembersForOwner($userid,$data=NULL) {
    	if ($data == 'short') {
    		$sql = "SELECT 
					NameFirst AS First_Name,NameLast AS Last_Name,LoginUserid AS UID
					FROM ss_people
					WHERE
                    TeamMember = 1 AND
                    Userid = '$userid'";
    	} else {
    		$sql = "SELECT 
					First_Name,Last_Name,PhoneOffice,
					Email,DateSignup,Comments,GMTOffset,
					L.Userid,L.User_Type,L.Role,L.UID		
					FROM ss_user_profile P
					LEFT JOIN ss_login L ON P.Userid = L.UID
					WHERE
						L.User_Type = '".USER_TYPE_TEAM."' AND
						L.Parent_Userid = '$userid' AND
						P.IsActive = 1
						ORDER BY P.Last_Name";
    	}
        
    	$query = $this->db->query($sql);
    	if ($query->num_rows()>0) {
    		return $query->result_array();
    	} else {
    		return false;
    	}
    }
}    
?>