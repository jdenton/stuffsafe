<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Places_update extends CI_Model {
    function __construct() {

    }

    function savePlace($data) {
        if ($data['action'] == 'add') {
            $sql = "INSERT into ss_places (
                    Userid,
                    PlaceName,
                    Address,
                    City,
                    State,
                    Zip,
                    Country,
                    Description,
                    Category,
                    Area,
                    AreaUnit,
                    Tags,
                    DateEntered,
                    UseridUpdate
                    ) values (
                    '".$data['accountUserid']."',
                    '".$data['placeName']."',
                    '".$data['placeAddress']."',
                    '".$data['placeCity']."',
                    '".$data['placeState']."',
                    '".$data['placeZip']."',
                    '".$data['placeCountry']."',
                    '".$data['placeDescription']."',
                    '".$data['placeType']."',
                    '".$data['placeArea']."',
                    '".$data['placeAreaUnit']."',
                    '".$data['placeTags']."',
                    '".date('Y-m-d')."',
                    '".$data['userid']."'
                    )";
            $query = $this->db->query($sql);
    		$placeID = $this->db->insert_id();
        } else {
            $sql = "UPDATE ss_places SET
                    PlaceName   = '".$data['placeName']."',
                    Address     = '".$data['placeAddress']."',
                    City        = '".$data['placeCity']."',
                    State       = '".$data['placeState']."',
                    Zip         = '".$data['placeZip']."',
                    Country     = '".$data['placeCountry']."',
                    Description = '".$data['placeDescription']."',
                    Category    = '".$data['placeType']."',
                    Area        = '".$data['placeArea']."',
                    AreaUnit    = '".$data['placeAreaUnit']."',
                    Tags        = '".$data['placeTags']."',
                    UseridUpdate= '".$data['userid']."'
                    WHERE
                    PlaceID = '".$data['placeID']."'";
            $query = $this->db->query($sql);
            $placeID = $data['placeID'];
        }

        return $placeID;
    }

    function deletePlace($placeID,$tagForDelete=TRUE,$userid,$accountUserid) {
        if ($tagForDelete == TRUE) {
            $sql = "UPDATE ss_places SET UseridUpdate = '$userid', Deleted = 1
                    WHERE PlaceID = '$placeID' AND Userid = '$accountUserid'";
            $query = $this->db->query($sql);

            $sql = "UPDATE ss_room SET UseridUpdate = '$userid', Deleted = 1
                    WHERE PlaceID = '$placeID' AND Userid = '$accountUserid'";
            $query = $this->db->query($sql);

            $sql = "SELECT RoomID FROM ss_room WHERE PlaceID = '$placeID'";
            $query = $this->db->query($sql);
            $roomArray = $query->result_array();

            $j=0;
            foreach($roomArray as $room) {
                $sql = "UPDATE ss_inventory SET UseridUpdate = '$userid', Deleted = 1
                        WHERE RoomID = '".$room['RoomID']."'";
                $query = $this->db->query($sql);
            }
        } else {
            $sql = "DELETE FROM ss_places WHERE PlaceID = '$placeID'";
            $query = $this->db->query($sql);

            $sql = "SELECT RoomID FROM ss_room WHERE PlaceID = '$placeID'";
            $query = $this->db->query($sql);
            $roomArray = $query->result_array();

            $j=0;
            foreach($roomArray as $room) {
                $sql = "DELETE FROM ss_inventory WHERE RoomID = '".$room['RoomID']."'";
                $query = $this->db->query($sql);
            }

            $sql = "DELETE FROM ss_room WHERE PlaceID = '$placeID'";
            $query = $this->db->query($sql);
        }
    }

    function restorePlace($placeID,$userid,$accountUserid) {
        $sql = "UPDATE ss_places SET UseridUpdate = '$userid', Deleted = 0
                WHERE PlaceID = '$placeID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);

        $sql = "UPDATE ss_room SET UseridUpdate = '$userid', Deleted = 0
                WHERE PlaceID = '$placeID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);

        $sql = "SELECT RoomID FROM ss_room WHERE PlaceID = '$placeID'";
        $query = $this->db->query($sql);
        $roomArray = $query->result_array();

        $j=0;
        foreach($roomArray as $room) {
            $sql = "UPDATE ss_inventory SET UseridUpdate = '$userid', Deleted = 0
                    WHERE RoomID = '".$room['RoomID']."'";
            $query = $this->db->query($sql);
        }
    }
}
?>