<?php
class App_data extends CI_Model {
    /**
	* getTags : Retrieve tags for a given site area.
    *           Site areas: Places,Rooms,Inventory Items,etc.
	*
	* @access public
	* @param  int     $userid Userid of account owner
    * @param  string  $siteArea Module for which we want tags
	* @return array   PHP Array of tags
	*/
    function getTags($userid,$siteArea) {
    	$sql = "SELECT TID,Tag FROM ss_tags
				WHERE
				Userid = '$userid' AND
    			SiteArea = '$siteArea'
				ORDER BY Tag";
    	$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array('0');
		}
    }

    function getTag($tag,$userid,$siteArea) {
        $sql = "SELECT * FROM ss_tags
                WHERE
                Userid = '$userid' AND
                Tag = '$tag' AND
                SiteArea = '$siteArea'";
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
    }

 	function saveTag($userid,$tagID=null,$tag,$action,$siteArea) {
		if ($action == 'add') {
			$sql = "INSERT into ss_tags (
					Userid,
					Tag,
					SiteArea,
                    ImportGUID
					) values (
					'$userid',
					'$tag',
					'$siteArea',
                    '".$this->session->userdata('importGUID')."'
					)";
			$query = $this->db->query($sql);
			$tagID = $this->db->insert_id();
		} elseif ($action == 'edit') {
			$sql = "UPDATE ss_tags SET
					Tag = '$tag'
					WHERE
					TID = $tagID";
			$query = $this->db->query($sql);
		}
		return $tagID;
    }

    function deleteTag($tagID) {
		$sql = "DELETE FROM ss_tags
				WHERE
				TID = $tagID";
		$query = $this->db->query($sql);
    }

    /**
	* getCategories : Retrieve categories for a given site area.
    *                 Site areas: Projects,Library,Users,Expense,etc.
	*
	* @access public
	* @param  int     $userid Userid of account owner
    * @param  string  $siteArea Module for which we want cateogries
	* @return array   PHP Array of categories
	*/
    function getCategories($userid,$siteArea) {
        $sql = "SELECT CatID,MainCat FROM ss_main_cat
				WHERE
				Userid = '$userid' AND
    			SiteArea = '$siteArea'
				ORDER BY MainCat";
    	$query = $this->db->query($sql);
    	return $query->result_array();
    }
    
    function getCategoryName($catID) {
        $sql = "SELECT MainCat FROM ss_main_cat
				WHERE
                CatID = $catID";
    	$query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['MainCat'];
    }

	function deleteCategory($catID) {
		$sql = "DELETE FROM ss_main_cat
				WHERE
				CatID = $catID";
		$query = $this->db->query($sql);
	}

	function saveCategory($userid,$catID=null,$category,$action,$siteArea,$prevCatID=NULL) {
		if ($action == 'add') {
			$sql = "INSERT into ss_main_cat (
                    PrevCatID,
					Userid,
					MainCat,
					SiteArea,
					DateUpdate,
                    ImportGUID
					) values (
                    '$prevCatID',
					'$userid',
					'$category',
					'$siteArea',
					'".date('Y-m-d')."',
                    '".$this->session->userdata('importGUID')."'
					)";
			$query = $this->db->query($sql);
			$catID = $this->db->insert_id();
		} elseif ($action == 'edit') {
			$sql = "UPDATE ss_main_cat SET
					MainCat = '$category'
					WHERE
					CatID = $catID";
			$query = $this->db->query($sql);
		}
		return $catID;
	}

    function getNewCatIDFromOld($oldCatID,$accountUserid) {
        $sql = "SELECT CatID FROM ss_main_cat WHERE PrevCatID = '$oldCatID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
    }
}
?>
