<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Site_search extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model('contacts');
        $this->load->model('file_maintenance','',true);

        $this->load->library('application/CommonFileManager');
    }

    function inventorySearch($searchTerm=NULL,$accountUserid=NULL,$userid=NULL) {
        $sql = "SELECT 
                I.InventoryID,I.RoomID,I.CatID,I.ItemName,I.ItemDesc,I.ItemMake,I.ItemModel,I.ItemSerial,
                I.PricePurchase,I.PriceReplace,I.ItemCondition,I.DatePurchase,I.Tags,I.DateEntered,
                PP.PID,CONCAT(PP.NameFirst,' ',PP.NameLast) AS EnteredBy,
                C.MainCat,P.PlaceID,P.PlaceName,R.RoomName
                FROM ss_inventory I
                LEFT JOIN ss_people PP ON PP.LoginUserid = I.UseridUpdate
                LEFT JOIN ss_main_cat C ON C.CatID = I.CatID
                LEFT JOIN ss_room R ON R.RoomID = I.RoomID
                LEFT JOIN ss_places P ON P.PlaceID = R.PlaceID
                WHERE
                (
                    I.ItemName LIKE '$searchTerm%' OR I.ItemName LIKE '% $searchTerm%' OR
                    I.ItemDesc LIKE '$searchTerm%' OR I.ItemDesc LIKE '% $searchTerm%' OR
                    I.ItemMake LIKE '$searchTerm%' OR I.ItemMake LIKE '% $searchTerm%' OR
                    I.ItemModel LIKE '$searchTerm%' OR I.ItemModel LIKE '% $searchTerm%' OR
                    I.ItemSerial LIKE '$searchTerm%' OR I.ItemSerial LIKE '% $searchTerm%' OR
                    I.Tags LIKE '$searchTerm%' OR I.Tags LIKE '% $searchTerm%'
                )
                AND I.Userid = '".$accountUserid."'
                AND I.Deleted = 0
                ORDER BY I.ItemName";
        $query = $this->db->query($sql);
        $inventoryArray = $query->result_array();

        $j=0;
        foreach($inventoryArray as $inventoryItem) {
            $inventoryArray[$j]['DateEnteredHuman'] = date('M j, Y', strtotime($inventoryItem['DateEntered']));
            $inventoryArray[$j]['DatePurchaseHuman'] = date('M j, Y', strtotime($inventoryItem['DatePurchase']));
            $inventoryArray[$j]['Files'] = $this->commonfilemanager->getFilesForSomething($inventoryItem['InventoryID'],'inventory','array',1);

            $gotThumbnail = FALSE;
            $thumbnailPath = '';
            if (count($inventoryArray[$j]['Files'])>0) {
                foreach($inventoryArray[$j]['Files'] as $file) {
                    if ($file['FileType'] == 'image/png' || $file['FileType'] == 'image/jpeg' || $file['FileType'] == 'image/gif' || $file['FileType'] == 'image/jpg') {
                        $gotThumbnail = TRUE;
                        $thumbnailPath = $file['thumbnailLink'];
                    }
                }
            }
            $inventoryArray[$j]['AvatarURL'] = $thumbnailPath;
            $j++;
        }
        return $inventoryArray;
    }

    function performSiteSearch($searchTerm=NULL,$accountUserid=NULL,$userid=NULL) {
        $searchArrayPlaces = array();
        $searchArrayRooms  = array();
        $searchArrayItems  = array();

        $sql = "SELECT PlaceID,PlaceName,Address,City,State,Zip,Country
                FROM ss_places
                WHERE
                (
                    PlaceName LIKE '$searchTerm%' OR PlaceName LIKE '% $searchTerm%' OR
                    Address LIKE '$searchTerm%' OR Address LIKE '% $searchTerm%' OR
                    City LIKE '$searchTerm%' OR City LIKE '% $searchTerm%' OR
                    Description LIKE '$searchTerm%' OR Description LIKE '% $searchTerm%' OR
                    Tags LIKE '$searchTerm%' OR Tags LIKE '% $searchTerm%'
                )
                AND Userid = '".$accountUserid."'
                AND Deleted = 0
                ORDER BY PlaceName";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            $searchArrayPlaces = $query->result_array();
        }

        $sql = "SELECT R.RoomID,R.RoomName,P.PlaceName
                FROM ss_room R
                LEFT JOIN ss_places P ON R.PlaceID = P.PlaceID
                WHERE
                (
                    R.RoomName LIKE '$searchTerm%' OR R.RoomName LIKE '% $searchTerm%' OR
                    R.Tags LIKE '$searchTerm%' OR R.Tags LIKE '% $searchTerm%'
                )
                AND R.Userid = '".$accountUserid."'
                AND R.Deleted = 0
                ORDER BY R.RoomName";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            $searchArrayRooms = $query->result_array();
        }

        $sql = "SELECT I.InventoryID,I.ItemName,I.ItemMake,I.ItemModel,I.PriceReplace,P.PlaceName
                FROM ss_inventory I
                LEFT JOIN ss_room R ON R.RoomID = I.RoomID
                LEFT JOIN ss_places P ON P.PlaceID = R.PlaceID
                WHERE
                (
                    I.ItemName LIKE '$searchTerm%' OR I.ItemName LIKE '% $searchTerm%' OR
                    I.ItemDesc LIKE '$searchTerm%' OR I.ItemDesc LIKE '% $searchTerm%' OR
                    I.ItemMake LIKE '$searchTerm%' OR I.ItemMake LIKE '% $searchTerm%' OR
                    I.ItemModel LIKE '$searchTerm%' OR I.ItemModel LIKE '% $searchTerm%' OR
                    I.ItemSerial LIKE '$searchTerm%' OR I.ItemSerial LIKE '% $searchTerm%' OR
                    I.Tags LIKE '$searchTerm%' OR I.Tags LIKE '% $searchTerm%'
                )
                AND I.Userid = '".$accountUserid."'
                AND I.Deleted = 0
                ORDER BY I.ItemName";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            $searchArrayItems = $query->result_array();
        }
        
        $searchArrayResults = array(
            'Places' => $searchArrayPlaces,
            'Rooms'  => $searchArrayRooms,
            'Items'  => $searchArrayItems
        );

        return $searchArrayResults;
    }

    function searchTags($tag=null,$accountUserid=NULL,$userid=NULL) {
        $searchArrayPlaces = array();
        $searchArrayRooms  = array();
        $searchArrayItems  = array();

        $sql = "SELECT PlaceID,PlaceName,Address,City,State,Zip,Country
                FROM ss_places
                WHERE
                (
                    Tags LIKE '$tag%' OR Tags LIKE '% $tag%'
                )
                AND Userid = '".$accountUserid."'
                AND Deleted = 0
                ORDER BY PlaceName";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            $searchArrayPlaces = $query->result_array();
        }

        $sql = "SELECT R.RoomID,R.RoomName,P.PlaceName
                FROM ss_room R
                LEFT JOIN ss_places P ON R.PlaceID = P.PlaceID
                WHERE
                (
                    R.Tags LIKE '$tag%' OR R.Tags LIKE '% $tag%'
                )
                AND R.Userid = '".$accountUserid."'
                AND R.Deleted = 0
                ORDER BY R.RoomName";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            $searchArrayRooms = $query->result_array();
        }

        $sql = "SELECT I.InventoryID,I.ItemName,I.ItemMake,I.ItemModel,I.PriceReplace,P.PlaceName
                FROM ss_inventory I
                LEFT JOIN ss_room R ON R.RoomID = I.RoomID
                LEFT JOIN ss_places P ON P.PlaceID = R.PlaceID
                WHERE
                (
                    I.Tags LIKE '$tag%' OR I.Tags LIKE '% $tag%'
                )
                AND I.Userid = '".$accountUserid."'
                AND I.Deleted = 0
                ORDER BY I.ItemName";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            $searchArrayItems = $query->result_array();
        }

        $searchArrayResults = array(
            'Places' => $searchArrayPlaces,
            'Rooms'  => $searchArrayRooms,
            'Items'  => $searchArrayItems
        );

        return $searchArrayResults;
    }
    
    function getSearchResultsForAutoCompleter($q,$accountUserid=NULL,$userid=NULL) {
        $searchResults = '';
		/*
		 * First let's look for matching places
		 */
        $resultArray = array();
        $count = 0;
        $sql = "SELECT PlaceID,PlaceName
                FROM ss_places
                WHERE
                (
                    PlaceName LIKE '$q%' OR PlaceName LIKE '% $q%' OR
                    Address LIKE '$q%' OR Address LIKE '% $q%' OR
                    City LIKE '$q%' OR City LIKE '% $q%' OR
                    Description LIKE '$q%' OR Description LIKE '% $q%' OR
                    Tags LIKE '$q%' OR Tags LIKE '% $q%'
                )
                AND Userid = '".$accountUserid."'
                AND Deleted = 0
                ORDER BY PlaceName";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                $resultArray[$count]['id']    = $row['PlaceID'];
                $resultArray[$count]['label'] = $row['PlaceName'];
                $resultArray[$count]['value'] = $row['PlaceID'];
                $resultArray[$count]['icon']  = 'icon_home_small';
                $resultArray[$count]['type']  = 'place';
				//$searchResults .= '<span class="icon_invoice_small">'.$display.'</small>|place_'.$row['PlaceID']."\n";
                $count++;
			}
		}		

		/*
		 * Now, let's look for any rooms
		 */
        $sql = "SELECT RoomID,RoomName
                FROM ss_room
                WHERE
                (
                    RoomName LIKE '$q%' OR RoomName LIKE '% $q%' OR
                    Tags LIKE '$q%' OR Tags LIKE '% $q%'
                )
                AND Userid = '".$accountUserid."'
                AND Deleted = 0
                ORDER BY RoomName";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                //$searchResults .= '<span class="icon_project_small">'.$row['RoomName'].'</span>|room_'.$row['RoomID']."\n";
                $resultArray[$count]['id']    = $row['RoomID'];
                $resultArray[$count]['label'] = $row['RoomName'];
                $resultArray[$count]['value'] = $row['RoomID'];
                $resultArray[$count]['icon']  = 'icon_room_small';
                $resultArray[$count]['type']  = 'room';
                $count++;
			}
		}

		/*
		 * Now, let's look for any inventory items
		 */
        $sql = "SELECT InventoryID,ItemName
                FROM ss_inventory
                WHERE
                (
                    ItemName LIKE '$q%' OR ItemName LIKE '% $q%' OR
                    ItemDesc LIKE '$q%' OR ItemDesc LIKE '% $q%' OR
                    ItemMake LIKE '$q%' OR ItemMake LIKE '% $q%' OR
                    ItemModel LIKE '$q%' OR ItemModel LIKE '% $q%' OR
                    ItemSerial LIKE '$q%' OR ItemSerial LIKE '% $q%' OR
                    Tags LIKE '$q%' OR Tags LIKE '% $q%'
                )
                AND Userid = '".$accountUserid."'
                AND Deleted = 0
                ORDER BY ItemName";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            foreach ($query->result_array() as $row) {
                $resultArray[$count]['id']    = $row['InventoryID'];
                $resultArray[$count]['label'] = $row['ItemName'];
                $resultArray[$count]['value'] = $row['InventoryID'];
                $resultArray[$count]['icon']  = 'icon_widgets_small';
                $resultArray[$count]['type']  = 'inventory';
                $count++;
                //$searchResults .= '<span class="icon_client_small item">'.$row['ItemName'].'</small>|inventory_'.$row['InventoryID']."\n";
            }
        }

        /*
		 * Now, let's look for matching tags
		 */
        /*
		$sql = "SELECT Tag
				FROM cpo_tags
				WHERE
				(Tag LIKE '$q%' OR Tag LIKE '% $q%')
				AND Userid = '".$accountUserid."'
				ORDER BY Tag";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
				$searchResults .= '<span class="icon_tag tag">'.$row['Tag'].'</span>|tag_'.$row['Tag']."\n";
			}
		}
        */

        /*
		 * Now, let's look for any contacts that match
		 */
        /*
		 $sql = "SELECT PID,NameFirst,NameLast,Company,Language,TeamMember,Vendor,Client,Contact
				FROM cpo_people
				WHERE
				(
					NameFirst LIKE '$q%' OR NameFirst LIKE '% $q%' OR
                    NameLast  LIKE '$q%' OR NameLast  LIKE '% $q%' OR
                    Company   LIKE '$q%' OR Company   LIKE '% $q%'
				)
                AND Userid = '".$accountUserid."'
                AND Deleted = 0
                ORDER BY NameLast";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                if (!empty($row['Company'])) {
                    $company = ' <span class="subText">['.$row['Company'].']</span>';
                } else {
                    $company = '';
                }
                if ($row['TeamMember'] == 1) {
                    $icon = 'icon_team_small';
                } else {
                    $icon = 'icon_vcard';
                }
				$searchResults .= '<span class="'.$icon.' item">'.$row['NameFirst'].' '.$row['NameLast'].$company.'</small>|contact_'.$row['PID']."\n";
			}
		}
        */
		return $resultArray;
    }
}
?>