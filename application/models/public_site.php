<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Public_site extends CI_Model {
    function getStuffSafeCustomerInfo($howMany=NULL) {
        $sql = "SELECT * FROM cpo_customer_logos WHERE Active = 1 ORDER BY RAND() LIMIT $howMany";
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->result_array();
        } else {
            return FALSE;
        }
    }

    function checkWebAddress($webAddress,$accountUserid=NULL) {
        if ($accountUserid>0) {
            $sql = "SELECT AccountID FROM ss_account_profile WHERE SubDomain = '$webAddress' AND AccountID <> '$accountUserid'";
        } else {
            $sql = "SELECT AccountID FROM ss_account_profile WHERE SubDomain = '$webAddress'";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function checkEmailPassword($email,$hashPassword) {
        $sql = "SELECT UID FROM ss_login
                WHERE
                    Userid   = '$email' AND
                    Password = '$hashPassword' AND
                    Active = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function saveNewAccount($data) {
        /*
         * Create account entry
         */
        $sql = "INSERT into ss_account_profile (
                NameFirst,
                NameLast,
                Company,
                Email,
                AccountLevel,
                AccountPrice,
                Account_No,
                AuthString,
                DateSignup,
                DateExpire,
                AcceptTerms,
                GetStuffSafeEmail,
                UserDir,
                IsActive
                ) values (
                '".$data['firstName']."',
                '".$data['lastName']."',
                '".$data['companyName']."',
                '".$data['email']."',
                '".$data['selectedPlanNumber']."',
                '".$data['planCost']."',
                '".$data['newAccountNumber']."',
                '".$data['authStringAccount']."',
                '".date('Y-m-d')."',
                '".$data['dateExp']."',
                '1',
                '1',
                '".$data['authStringAccount']."',
                '1'
                )";
        $query = $this->db->query($sql);
        $accountID = $this->db->insert_id();

        /*
         * Create login entry
         */
        $sql = "INSERT into ss_login (
                Userid,
                UseridAccount,
                Password,
                User_Type,
                Permissions,
                AuthString,
                Active
                ) values (
                '".$data['email']."',
                '$accountID',
                '".$data['hashPassword']."',
                '".USER_TYPE_OWNER."',
                '".$data['permissions']."',
                '".$data['authStringPersonal']."',
                '1'
                )";
        $query = $this->db->query($sql);
        $UID = $this->db->insert_id();        

        /*
         * Create people entry
         */
        $sql = "INSERT into ss_people (
                Userid,
                LoginUserid,
                NameFirst,
                NameLast,
                Company,
                AccountCreator,
                UseridUpdate,
                DateEntry
                ) values (
                '".$accountID."',
                '".$UID."',
                '".$data['firstName']."',
                '".$data['lastName']."',
                '".$data['companyName']."',
                '1',
                '".$UID."',
                '".date('Y-m-d')."'
                )";
        $query = $this->db->query($sql);
        $personID = $this->db->insert_id();

        /*
         * Enter contact info for this person
         */
        $sql = "INSERT into ss_people_contact_info (
                PID,
                ContactType,
                ContactData,
                ContactLocation,
                UseridUpdate,
                DateUpdate
                ) values (
                '$personID',
                'email',
                '".$data['email']."',
                'work',
                '".$UID."',
                '".date('Y-m-d')."'
                )";
        $query = $this->db->query($sql);

        /*
         * Email settings
         */
        $emailSettingArray = array();

        foreach($emailSettingArray as $setting) {
            $sql = "INSERT into ss_settings
                       (Userid,Setting,SettingValue,SettingGroup,SettingSection,UseridUpdate,DateUpdate)
                values ('".$UID."','".$setting."','1','member','email','".$UID."','".date('Y-m-d')."')";
            $query = $this->db->query($sql);
        }
    }

    function saveNewAccountLevel($accountUserid,$newAccountLevel,$newAccountPrice) {
        $sql = "UPDATE ss_account_profile SET 
                AccountLevel = '$newAccountLevel',
                AccountPrice = '$newAccountPrice'
                WHERE AccountID = '$accountUserid'";
        $query = $this->db->query($sql);
    }

    function getLastAccountNumber() {
        $sql = "SELECT MAX(Account_No) AS MaxAccountNo FROM ss_account_profile";
        $query = $this->db->query($sql);
        return $query->row_array();
    }
}
?>