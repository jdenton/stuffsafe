<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventory_update extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    function saveInventoryItem($data) {
        if ($data['action'] == 'add') {
            $sql = "INSERT into ss_inventory (
                    Userid,
                    RoomID,
                    CatID,
                    ItemName,
                    ItemDesc,
                    ItemMake,
                    ItemModel,
                    ItemSerial,
                    PricePurchase,
                    PriceReplace,
                    ItemCondition,
                    DatePurchase,
                    Tags,
                    DateEntered,
                    UseridUpdate
                    ) values (
                    '".$data['accountUserid']."',
                    '".$data['roomID']."',
                    '".$data['catID']."',
                    '".$data['invName']."',
                    '".$data['invDesc']."',
                    '".$data['invMake']."',
                    '".$data['invModel']."',
                    '".$data['invSN']."',
                    '".$data['invPricePurchase']."',
                    '".$data['invPriceReplace']."',
                    '".$data['invCondition']."',
                    '".$data['invDate']."',
                    '".$data['invTags']."',
                    '".date('Y-m-d')."',
                    '".$data['userid']."'
                    )";
            $query = $this->db->query($sql);
    		$inventoryID = $this->db->insert_id();
        } else {
            $sql = "UPDATE ss_inventory SET
                    RoomID        = '".$data['roomID']."',
                    CatID         = '".$data['catID']."',
                    ItemName      = '".$data['invName']."',
                    ItemDesc      = '".$data['invDesc']."',
                    ItemMake      = '".$data['invMake']."',
                    ItemModel     = '".$data['invModel']."',
                    ItemSerial    = '".$data['invSN']."',
                    PricePurchase = '".$data['invPricePurchase']."',
                    PriceReplace  = '".$data['invPriceReplace']."',
                    ItemCondition = '".$data['invCondition']."',
                    DatePurchase  = '".$data['invDate']."',
                    Tags          = '".$data['invTags']."',
                    UseridUpdate  = '".$data['userid']."'
                    WHERE InventoryID = ".$data['invID'];
            $query = $this->db->query($sql);
            $inventoryID = $data['invID'];
        }
        return $inventoryID;
    }   

    function swapInventoryItemRooms($inventoryItemID,$newRoomID) {
        $sql = "UPDATE ss_inventory SET RoomID = $newRoomID WHERE InventoryID = $inventoryItemID";
        $query = $this->db->query($sql);
    }

    function deleteInventoryItem($inventoryID,$tagForDelete=TRUE,$userid,$accountUserid) {
        if ($tagForDelete == TRUE) {
            $sql = "UPDATE ss_inventory SET UseridUpdate = '$userid', Deleted = 1
                    WHERE InventoryID = '$inventoryID' AND Userid = '$accountUserid'";
            $query = $this->db->query($sql);
        } else {
            $sql = "DELETE FROM ss_inventory WHERE InventoryID = '$inventoryID'";
            $query = $this->db->query($sql);
        }
    }

    function restoreInventoryItem($inventoryID,$userid,$accountUserid) {
        $sql = "UPDATE ss_inventory SET UseridUpdate = '$userid', Deleted = 0
                WHERE InventoryID = '$inventoryID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
    }

    function moveInventoryToRoom($roomFrom,$roomTo) {
        $sql = "UPDATE ss_inventory SET RoomID = '$roomTo' WHERE RoomID = '$roomFrom'";
        $query = $this->db->query($sql);
    }
}