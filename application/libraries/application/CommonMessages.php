<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonMessages {
    function getMessages($itemID=NULL,$itemType=NULL,$unread=NULL,$viewAll=NULL,$toUserOnly=NULL,$renderType=NULL,$return=NULL,$userid=NULL,$accountUserid=NULL,$personID=NULL,$start=NULL,$limit=NULL,$notNotified=NULL) {
        $CI =& get_instance();
        $CI->load->model('messaging/Site_messages','',TRUE);
        $CI->load->model('files/file_maintenance','FileMaintenance',TRUE);

        $CI->load->library('application/CommonFileManager');

        $CI->load->helper('file');

        if (empty($userid)) {
            $userid = $CI->session->userdata('userid');
        }
        
		$messageArray  = $CI->Site_messages->getMessages($itemID,$itemType,$viewAll,$toUserOnly,$unread,$userid,$accountUserid,$personID,$start,$limit,$notNotified);
        $totalMessagesArray = $CI->Site_messages->getNumberOfMessagesForSomething($itemID,$itemType,$userid,$CI->session->userdata('accountUserid'),$personID,$viewAll);
        $totalMessages = $totalMessagesArray['NoOfMessagesTotal'];

        $jsonString = '{"TotalMessages":"'.$totalMessages.'","Messages":[';
		$newMessageArray = array();
        if (sizeof($messageArray)>0) {
			$a=0;
            foreach($messageArray as $message) {
                $message['Editable'] = 0;
                if ($message['UIDFrom'] == $userid) {
                    $message['Editable'] = 1;
                }
                $message['DateSent'] = convertMySQLToGMT($message['DateMessage'],'pull','M j, g:i a');
                if ($unread != 1) {
                     $message['DateRead'] = convertMySQLToGMT($message['DateRead'],'pull','M j, g:i a');
                }
                if ($message['TypeFrom'] == 'client') {
                     $from = $message['Company'];
                     $linkFrom = '<a href="'.site_url('clients/ClientDetail/index/'.$message['UIDFrom']).'">'.$from.'</a>';
                } else {
                     $from = $message['FromName'];
                     $linkFrom = '<a href="#" class="personLink">'.$from.'</a>';
                }
                /*
                 * Check for what message is about and get icon
                 */
                $message['ItemIcon'] = CommonMessages::getMessageIcon($message['ItemType'],$message['ItemID']);

                /*
                * Get attached files
                */
               $message['Files'] = $CI->commonfilemanager->getFilesForSomething($message['MID'],'message','array',1);

				/*
				 * Check for avatar image
				 */
				if (empty($message['Avatar'])) {
					$message['Avatar'] = IMAGES_PATH.'avatars/stockAvatar46.png';
				}

                $jsonString .= json_encode($message).',';
				$newMessageArray[$a] = $message;
				$a++;
           }
           $jsonString = substr($jsonString,0,-1).']}';
        } else {
           $jsonString = '{}';
        }
        if ($renderType == 'json') {
            if ($return == 1) {
                return $jsonString;
            } else {
                echo $jsonString;
            }
        } elseif ($renderType == 'array') {
            return $newMessageArray;
        }
    }

	function getMessagesForUpdateWindow($renderType='json',$return=1) {
		$CI =& get_instance();
        $CI->load->model('messaging/Site_messages','',TRUE);

		/*
		 * Can we view all messages or just ones sent to us?
		 */
		 $permArray = $CI->session->userdata('permissions');
		 $viewAll = FALSE;
		 if ($permArray['messageViewAll'] == 1) {
			 $viewAll = TRUE;
		 }
		$messageArray = $CI->Site_messages->getMessagesForUpdateWindow($CI->session->userdata('accountUserid'),$CI->session->userdata('userid'),$CI->session->userdata('pid'),$viewAll);
		$totalMessages = count($messageArray);

        $jsonString = '{"TotalMessages":"'.$totalMessages.'","Messages":[';
        foreach($messageArray as $message) {
            $message['DateSent'] = convertMySQLToGMT($message['DateMessage'],'pull','M j, g:i a');
            $message['ItemIcon'] = CommonMessages::getMessageIcon($message['ItemType'],$message['ItemID']);
            $message['DateRead'] = "";
            /*
             * Check for avatar image
             */
            if (empty($message['Avatar'])) {
                $message['Avatar'] = IMAGES_PATH.'avatars/stockAvatar46.png';
            }

            $jsonString .= json_encode($message).',';
        }
        
		$jsonString = substr($jsonString,0,-1).']}';

		echo $jsonString;
	}

    function getMessageIcon($itemType,$itemID) {
        $CI =& get_instance();

        if ($itemType == 'invoice') {
             $itemIcon = '<a href="'.site_url('finances/FinanceView/viewInvoice/'.$itemID).'" title="'.lang('finance_view_this_invoice').'" class="icon_invoice_small_right"></a>';
        } elseif ($itemType == 'project') {
             $itemIcon = '<a href="'.site_url('projects/ProjectDetail/index/'.$itemID).'" title="'.lang('project_view_this_project').'" class="icon_project_small_right"></a>';
        } elseif ($itemType == 'file') {
             $itemIcon = '<a href="'.site_url('files/FileManager/viewFile/file/'.$itemID).'" title="'.lang('files_view_this_file').'" class="icon_file_small_right"></a>';
        } elseif ($itemType == 'task') {
             $itemIcon = '<a href="'.site_url('tasks/TaskViewExternal/viewTask/'.$CI->session->userdata('persAuthString').'/'.$itemID).'" title="'.lang('task_view_task').'" class="icon_tasks_small_right"></a>';
        } elseif ($itemType == 'expense') {
             $itemIcon = '<a href="'.site_url('finances/FinanceView/viewExpense/'.$itemID).'" title="'.lang('finance_view_this_expense').'" class="icon_expense_small_right"></a>';
        } elseif ($itemType == 'attachedFile') {
             $itemIcon = '<a href="'.site_url('files/FileManager/viewFile/attachedFile/'.$itemID).'" title="'.lang('files_view_this_file').'" class="icon_file_small_right"></a>';
        } else {
            $itemIcon = '';
        }

        return $itemIcon;
    }
}
?>