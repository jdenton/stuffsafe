<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class GoogleDocs {
    function getGoogleDocs($folderURL=NULL) {
        $CI =& get_instance();
        $CI->load->helper('zend');

        $httpClient = Zend_Gdata_AuthSub::getHttpClient($CI->session->userdata('googleToken'));
		$gdClient = new Zend_Gdata_Docs($httpClient);
        if (!empty($folderURL) && $folderURL != 'undefined') {
            $folderID = GoogleDocs::getFolderIDFromURL($folderURL);
            $docFeed = $gdClient->getDocumentListFeed('http://docs.google.com/feeds/folders/private/full/folder%3A'.$folderID);
        } else {
            $docFeed = $gdClient->getDocumentListFeed('http://docs.google.com/feeds/documents/private/full/-/folder?showfolders=true');
        }
		$a=0;
		foreach ($docFeed->entries as $entry) {
			/*
			 *  Find the URL of the HTML view of the document.
			 */
			$alternateLink = '';
			$docExtension = '';
			foreach($entry->link as $link) {
                if ($link->getRel() === 'alternate') {
					$alternateLink = $link->getHref();
					if (preg_match('/spreadsheet/',$alternateLink)) {
						$docExtension = 'xls';
					} elseif (preg_match('/present/',$alternateLink)) {
						$docExtension = 'ppt';
					} elseif (preg_match('/docs/',$alternateLink)) {
						$docExtension = 'doc';
					}
				}
			}
            $author = $entry->getAuthor();
            $category = $entry->getCategory();

            $isFolder = '0';
            $iconSmall = '';
            $iconBig = '';
            foreach($category as $cat) {
                if ($cat->getLabel() == 'folder') {
                    $isFolder = '1';
                    $docExtension = 'folder';
                    $iconSmall = 'f.png';
                    $folderURI = $entry->id->text;
                }
            }

            $docArray[$a]['FileID']         = 0;
            $docArray[$a]['ParentID']       = 'google';
			$docArray[$a]['FileTitle']      = $entry->title->text;
            $docArray[$a]['FileNameActual'] = $entry->title->text;
            $docArray[$a]['FileSize']       = '';
            $docArray[$a]['FileType']       = $docExtension;
            $docArray[$a]['FileDesc']       = '';
            $docArray[$a]['Tags']           = '';
            $docArray[$a]['Public']         = 0;
            $docArray[$a]['Folder']         = $isFolder;
            $docArray[$a]['FolderState']    = 0;
            $docArray[$a]['ImportFolder']   = 0;
            $docArray[$a]['DateEntry']      = convertIcalToHuman($entry->published->text,'M j, Y g:i A');
            $docArray[$a]['DateUpdate']     = convertIcalToHuman($entry->updated->text,'M j, Y g:i A');
            $docArray[$a]['PID']            = '';
            $docArray[$a]['UploaderName']   = $author[0]->getName()->getText();
            $docArray[$a]['iconSmall']      = $iconSmall;
            $docArray[$a]['iconBig']        = $iconBig;
			$docArray[$a]['fileLink']       = $alternateLink;
			$docArray[$a]['fileExt']        = $docExtension;
            $docArray[$a]['FileService']    = 1;
            $docArray[$a]['FolderID']       = $entry->id->text;
			$a++;
		}

        $jsonString = '{"Path":[{"FolderName":"All files","NodeID":0,"ParentID":0},{"FolderName":"Google Docs","NodeID":"0","ParentID":"google"}],"Files":';
        $jsonString .= json_encode($docArray);
        $jsonString .= '}';
		echo $jsonString;
   }

   function getFolderIDFromURL($folderURL) {
       $urlArray = explode('%3A',$folderURL);
       return $urlArray[1];
   }
}