<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FileManagerS3 {
	/**
      * uploadFileHandler : upload files to Amazon S3 service
	  *
      * @access	public
	  * @param  array  $fileArray Array of files to be deleted
      * @return void
      */
	function uploadFileHandler() {
		$CI =& get_instance();
        $CI->load->model('file_maintenance');
        include(APPPATH.'libraries/s3/S3.php');
		$CI->load->helper('file');
		$CI->load->helper('download');

        // Check the upload
        $fileNameTmp = $_FILES['file']['tmp_name'];
        $fileName    = $_FILES['file']['name'];
        $fileType    = $_FILES['file']['type'];
        $fileSize    = $_FILES['file']['size'];
        $s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);

		/*
		 * Check for duplicate filename
		 */
		 $fileName = $CI->file_maintenance->checkForDuplicateFileName($fileName);
		 $fileName = cleanString($fileName);
		/*
		 * Try creating a 'folder' and then putting the file into the folder
		 */
		 $folderName = $CI->session->userdata('userDir');
		 $s3->putObject($folderName.'_$folder$', AWS_BUCKET_NAME, $folderName.'_$folder$', S3::ACL_PRIVATE);

		 //move the file
		 if ($s3->putObjectFile($fileNameTmp, AWS_BUCKET_NAME, $folderName.'/'.$fileName, S3::ACL_PUBLIC_READ,array("Content-Disposition" => "attachment"),"application/octet-stream")) {
             $status = 'Success';
			/*
			 * Enter file information into database
			 */
            $parentID = 0;
            if ($CI->session->userdata('fileTreeNodeID')) {
                $parentID = $CI->session->userdata('fileTreeNodeID');
            }

			$fileArray['FileNameActual']    = $fileName;
			$fileArray['FileSize']          = $fileSize;
			$fileArray['FileType']          = $fileType;
			$fileArray['action']            = 'add';
            $fileArray['ParentID']          = $parentID;
			$fileArray['accountUserid']     = $CI->session->userdata('accountUserid');
			$fileArray['userid']            = $CI->session->userdata('userid');

            $parentID = $fileArray['ParentID'];
			$fileID = $CI->file_maintenance->insertFileData($fileArray);
		  } else {
			$fileID = 0;
            $folderName = 0;
            $parentID = 0;
		  }

          /*
          * See if we need to create a thumbnail for this file.
          */
         $fileNameParts = pathinfo($fileName);
         $origExtension = $fileNameParts['extension'];
         $extension = strtolower($fileNameParts['extension']);
         $fileNameNoExtension = $fileNameParts['filename'];
         $thumbFileNameS3 = '';
         if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'png') {
            $imageSize = getimagesize($fileNameTmp);            
            $thumbFileName = $fileNameNoExtension.'_thumb.'.$origExtension;
            $thumbFileNamePath = USER_DIR.$thumbFileName;
            move_uploaded_file($fileNameTmp,$thumbFileNamePath);

            $config['source_image']   = $thumbFileNamePath;
            $config['create_thumb']   = FALSE;
            $config['thumb_marker']   = '';
            $config['maintain_ratio'] = FALSE;
            $config['master_dim']     = 'auto';
            $config['width']          = 100;
            $config['height']         = 100;

            //$this->image_lib->initialize($config);
            $CI->load->library('image_lib');
            $CI->image_lib->initialize($config);
            $CI->image_lib->resizeThenCrop();
            /*
             * Move thumbnail to S3
             */
            $s3->putObjectFile($thumbFileNamePath, AWS_BUCKET_NAME, $folderName.'/'.$thumbFileName, S3::ACL_PUBLIC_READ,array("Content-Disposition" => "attachment"),"application/octet-stream");
            /*
             * Remove thumbnail from local file system
             */
            unlink($thumbFileNamePath);
            $thumbFileNameS3 = AWS_URL_ROOT.'/'.$folderName.'/'.$thumbFileName;
         }

		 unset($s3);
         return array($fileID,$folderName,$parentID,$fileName,$thumbFileNameS3);
     }

	 /**
      * deleteItemFromS3 : delete files from S3 bucket
	  *
      * @access	public
	  * @param  array  $fileArray Array of files to be deleted
      * @return void
      */
	 function deleteItemFromS3($fileArray,$folderName=NULL) {
		$CI =& get_instance();
        $CI->load->model('file_maintenance');
        include(APPPATH.'libraries/s3/S3.php');
		$CI->load->helper('file');
		$CI->load->helper('download');
        if (empty($folderName)) {
            $folderName = $CI->session->userdata('userDir');
        }

		if (is_array($fileArray)) {
			
			$s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
			$a=0;
			foreach($fileArray as $file) {
                $fileName = $file['FileNameActual'];
				$s3->deleteObject(AWS_BUCKET_NAME, $folderName.'/'.$fileName);
				$a++;
			}
			unset($s3);
		 }
	 }

	 /**
      * createFileLink : creates a link to an S3 object that will expire after
	  *                  a period of time
	  *
      * @access	public
	  * @param  array  $fileName Name of the S3 object for which we want to create a link.
      * @return string $link
      */
	 function createFileLink($fileName,$userDir=NULL,$secureLink=FALSE) {
		$CI =& get_instance();
		$expires = time()+AWS_LINK_EXPIRE;
        if (empty($userDir)) {
            $userDir = $CI->session->userdata('userDir');
        }
		$string_to_sign = "GET\n\n\n{$expires}\n/stuffsafe/".$userDir."/".$fileName;
		$signature = urlencode(base64_encode((hash_hmac("sha1", utf8_encode($string_to_sign), AWS_SECRET_KEY, TRUE))));

		$authentication_params = "AWSAccessKeyId=".AWS_ACCESS_KEY;
		$authentication_params.= "&Expires={$expires}";
		$authentication_params.= "&Signature={$signature}";
        if ($secureLink == FALSE) {
            $link = AWS_URL_ROOT."/".$userDir."/".$fileName;
        } else {
            $link = AWS_URL_ROOT."/".$userDir."/".$fileName."?".$authentication_params;
        }
		return $link;
	 }

	function moveFileToS3($filePath,$fileName,$s3Folder,$security=NULL) {
		$CI =& get_instance();
        $CI->load->model('file_maintenance');
		if (!class_exists('S3')) {
			include(APPPATH.'libraries/s3/S3.php');
		}
		$CI->load->helper('file');
		$CI->load->helper('download');
		$s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);

        $s3security = S3::ACL_PUBLIC_READ;
        if ($security == 'private') {
            $s3security = S3::ACL_PRIVATE;
        } elseif ($security == 'public_read') {
            $s3security = S3::ACL_PUBLIC_READ;
        }

		$s3->putObjectFile($filePath, AWS_BUCKET_NAME, $s3Folder.'/'.$fileName, $s3security);
		unset($s3);
	}
}

?>
