<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonAppData {
    /**
	 * renderTags : renderTag list for various site modules
	 *
	 * @access	public
	 * @param	string  $tagType client,project,invoice,etc.
	 * @param   string  $renderType :  JSON, HTML, PHP array
	 * @return	mixed   PHP array, JSON string, HTML string
	 */
    function renderTags($siteArea,$renderType='json') {
        $CI =& get_instance();
		$CI->load->model('app_data','',true);
		$CI->lang->load('app',$CI->session->userdata('language'));

		$tagArray = $CI->app_data->getTags($CI->session->userdata('accountUserid'),$siteArea);
		$tagCounter = 0;
		$alphaCounter = 0;
		$alphaChar = '';
		$newTagArray['Tags']   = array();

		switch ($siteArea) {
			case 'inventory':
				$viewURL = site_url('place');
				break;
			case 'place':
				$viewURL = site_url('places');
				break;
			case 'room':
				$viewURL = site_url('rooms/tag');
				break;
            default:
                $viewURL = site_url('');
                break;
		}
		$newTagArray['tagURL'] = $viewURL;

		foreach($tagArray as $tag) {
			/*
			 * Get first letter of tag
			 */
			$tagID     = $tag['TID'];
			$tagString = $tag['Tag'];
			if (!empty($tagString)) {
				$firstChar = substr($tagString,0,1);
				if ($firstChar != $alphaChar && $firstChar != strtoupper($alphaChar)) {
					$newAlphaChar = strtoupper($firstChar);
					$newTagArray['Tags'][$newAlphaChar] = array();
					$alphaChar = $firstChar;
					$tagCounter = 0;
				}
				$newTagArray['Tags'][$newAlphaChar][$tagCounter]['TagID'] = $tagID;
				$newTagArray['Tags'][$newAlphaChar][$tagCounter]['Tag']   = $tagString;
				$tagCounter++;
			}
		}

		if ($renderType == 'json') {
			return json_encode($newTagArray);
		} elseif ($renderType == 'array') {
			return $newTagArray;
		}
    }

    function saveTags($tagString,$siteArea) {
        ini_set('display_errors',1);
        $CI =& get_instance();
		$CI->load->model('app_data','',true);
		$CI->lang->load('app',$CI->session->userdata('language'));
        $CI->load->helper('string');

        $tagArray = explode(' ',$tagString);
        foreach($tagArray as $tag) {
            $tag = cleanStringTag($tag);
            if (!empty($tag)) {
                if (!$CI->app_data->getTag($tag,$CI->session->userdata('accountUserid'),$siteArea)) {
                    /*
                     * Insert new tag
                     */
                    $CI->app_data->saveTag($CI->session->userdata('accountUserid'),null,$tag,'add',$siteArea);
                }
            }
        }
    }

	function modifyTrashCount($delta,$direction) {
        $CI =& get_instance();
		$trashTotal = $CI->session->userdata('trashTotal');
		if ($direction == 'down') {
			if ($trashTotal>0) {
				$trashTotal = $trashTotal-$delta;
			}
		} else {
			$trashTotal = $trashTotal+$delta;
		}
		$CI->session->set_userdata('trashTotal',$trashTotal);
	}
}
?>