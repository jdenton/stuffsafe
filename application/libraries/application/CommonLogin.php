<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonLogin {
    function tryLogin($userid=NULL,$password=NULL,$authKey=NULL,$userType=NULL,$apiAuth=NULL,$passwordEncrypted=NULL,$remember=FALSE,$redirect=TRUE,$subdomain=NULL) {
        $CI =& get_instance();
        $CI->load->model('User_login','',TRUE);
        
        $CI->load->library('encrypt');
		$error = 0;

        // Check for empty fields
		if ( (empty($userid) || empty($password)) && empty($authKey) ) {
			$error = 1;
			$errorType = 'missingField';
		} else {
            if (!empty($authKey)) {
                $loginArrayPersonal = $CI->User_login->checkAuthString($authKey,'personal');
                if ($loginArrayPersonal != FALSE) {
                    CommonLogin::doLogin($loginArrayPersonal,FALSE);
                    exit;
                } else {
                    $error = 1;
                    $errorType = 'noAccount';
                }
            } else {
                if ($passwordEncrypted != TRUE) {
                    $hashPass = sha1($password.SITE_CODE);
                } else {
                    $hashPass = $password;
                }
                
                /*
                 * Check for valid login information
                 */
                $loginArray = $CI->User_login->tryLogin($userid,$hashPass);
                if ($loginArray === false) {
                    $error = 1;
                    $errorType = 'noAccount';
                }
            }
		}
        if ($apiAuth == 1) {
            if ($error == 1) {
                echo '{"Error":"1","ErrorMessage":"'.$errorType.'"}';
            } else {
                $authKey = createAuthKey($loginArray['UID']);
                $sessionData = array('logged' => TRUE,'authKey' => $authKey);
                $CI->session->set_userdata($sessionData);
                echo '{"AuthKey":"'.$authKey.'","UserType":"'.$loginArray['User_Type'].'","Permissions":"'.$loginArray['Permissions'].'"}';
            }
            exit;
        } elseif ($error != 1) {
            $loginArray['subdomain'] = $subdomain;
            CommonLogin::doLogin($loginArray,$redirect);

            /*
             * Do we want to remain logged in for two weeks?
             */
            if ($remember == TRUE) {
                /*
                 * Create GUID, save in ss_login and store in cookie on client.
                 */
                $guid = random_string('alnum',30);
                $expireDate = date('Y-m-d',time()+REMEMBER_ME_EXPIRE);
                $CI->User_login->saveRememberMeGUID($loginArray['UID'],$expireDate,$guid);
                $cookieArray = array(
                               'name'   => 'rememberMe',
                               'value'  => $guid,
                               'expire' => REMEMBER_ME_EXPIRE,
                               'domain' => '.stuffsafe.com'
                            );
                set_cookie($cookieArray);
            }
        } else {
            header('Location: '.site_url('login/index/'.$errorType));
        }
    }

    function doLogin($loginArray,$redirect=FALSE) {
        $CI =& get_instance();
        $CI->load->model('Trash','',TRUE);
        $CI->load->model('Accounts','',TRUE);
        $CI->load->model('Contacts','',TRUE);
        $CI->load->model('Settings_update','',TRUE);
        $CI->load->model('User_login','',TRUE);
        $CI->load->model('User_information','',TRUE);
		$CI->load->model('Team','',TRUE);
        $CI->load->model('Admin_alerts','',TRUE);

        $CI->load->library('application/CommonContacts');
        $CI->load->library('application/CommonTrash');
        
        $password      = $loginArray['Password'];
        $userType      = $loginArray['User_Type'];
        $permissions   = createPermissionsArray($loginArray['Permissions']);
        if ($permissions === FALSE) {
            header('Location: '.site_url('login/index/roleMissing/'));
            exit;
        }
        $accountUserid = $loginArray['UseridAccount'];
        $userid        = $loginArray['UID'];
        $userName      = $loginArray['Userid'];
        $persAuthString= $loginArray['AuthString'];

        $accountArray = $CI->Accounts->getAccountInformation($accountUserid,1);

        $userCompany       = stripslashes($accountArray['Company']);
        $userDir           = $accountArray['UserDir'];
        $accountLevel      = $accountArray['AccountLevel'];
        $accountNumber     = $accountArray['Account_No'];
        $currency          = $accountArray['Currency'];
        $accountAuthString = $accountArray['AuthString'];
        $dateSignup        = $accountArray['DateSignup'];
        $paymentProfileID  = $accountArray['PaymentProfileID'];
        $companyEmail      = $accountArray['Email'];
        $companyPhone      = $accountArray['PhoneOffice'];
        $companyFAX        = $accountArray['PhoneFax'];
        $companyURL        = $accountArray['URL'];
        $isPaid = FALSE;
        if ($accountArray['IsPaid'] == 1) {
            $isPaid = TRUE;
        }

        $userInformationArray = $CI->Contacts->getContactPerson(null,$userid);

        $pid       = $userInformationArray['PID'];
        $country   = $userInformationArray['Country'];
        $gmtOffset = $userInformationArray['Timezone'];
        $dst       = $userInformationArray['DaylightSavingsTime'];
        $avatar    = $userInformationArray['AvatarURL'];
        $dst = FALSE;
        if ($dst == 1) {
            $dst = TRUE;
        }
        $language  = $userInformationArray['Language'];
        $firstName = $userInformationArray['NameFirst'];
        $fullName  = $userInformationArray['NameFirst'].' '.$userInformationArray['NameLast'];

        /*
         * Find an email address for this user
         */
        $emailArray = array();
        $emailArray[0] = '';
        $a=0;
        foreach($userInformationArray['ContactItems'] as $contactItem) {
            if ($contactItem['ContactType'] == 'email') {
                $emailArray[$a] = $contactItem['ContactData'];
                $a++;
            }
        }
        $userEmail = $emailArray[0];

        if (empty($language)) {
            $language = 'en';
        }

        if (empty($currency)) {
            $currency = 'USD';
        }

        /*
         * Check and see if we should display any alerts.
         * First look for a noCPOAlerts cookie.
         */
         if (isset($_COOKIE['noSSAlerts'])) {
             $offDate = $_COOKIE['noSSAlerts'];
         } else {
             $offDate = '';
         }
         $alertsArray = $CI->Admin_alerts->getAlerts($offDate);
            
        $sessionData = array(
            'pid'                 => $pid,
            'userid'              => $userid,
            'userName'            => $userName,
            'accountUserid'       => $accountUserid,
            'password'            => $password,
            'permissions'         => $permissions,
            'userType'            => $userType,
            'logged'              => TRUE,
            'firstName'           => $firstName,
            'fullName'            => $fullName,
            'userCompany'         => $userCompany,
            'userDir'             => $userDir,
            'country'             => $country,
            'currency'            => $currency,
            'gmtOffset'           => $gmtOffset,
            'daylightSavingsTime' => $dst,
            'language'            => $language,
            'userEmail'           => $userEmail,
            'persAuthString'      => $persAuthString,
            'accountAuthString'   => $accountAuthString,
            'avatarMember'        => $avatar,
            'importGUID'          => '',
            'companyEmail'        => $companyEmail,
            'companyPhone'        => $companyPhone,
            'companyFAX'          => $companyFAX,
            'companyURL'          => $companyURL,

            'accountLevel'        => $accountLevel,
            'accountNumber'       => $accountNumber,
            'isPaid'              => $isPaid,
            'dateSignup'          => $dateSignup,
            'paymentProfileID'    => $paymentProfileID,

            'alerts'              => $alertsArray
        );

        /*
         * Get footer widgets
         */
        $sessionData['footerWidgetArray'] = array();

        /*
         * Localization variables: timezone and currency
         */
        if (empty($currency)) {
            $currency = 'USD';
            $sessionData['currencyHTML'] = '$';
        } else {
            $currencyMark = getCurrencyMark($currency);
            $sessionData['currencyHTML'] = $currencyMark;
        }

        /*
         * 2. Some account settings
         */
        $settingsArray = $CI->Settings_update->getSettings($accountUserid,NULL,'account','user');
        if ($settingsArray != FALSE) {
            foreach($settingsArray as $setting) {
                $sessionData[$setting['Setting']] = $setting['SettingValue'];
            }
        }

        /*
         * 3. Some member settings
         */
        $settingsMemberArray = $CI->Settings_update->getSettings($accountUserid,NULL,'member','user');
        $a=0;

        if ($settingsMemberArray != FALSE) {
            foreach($settingsMemberArray as $settingMember) {
                $sessionData[$settingMember['Setting']] = $settingMember['SettingValue'];
                $a++;
            }
        }

        /*
         * Write data to session
         */
        $CI->session->set_userdata($sessionData);

        /*
         * Write trash total to session
         */
        $trashTotal = $CI->commontrash->countItemsInTrash(TRUE);
        $CI->session->set_userdata('trashTotal',$trashTotal);

        /*
         * Update login stats for this user
         */
        $CI->User_login->updateLoginStats($userid);

        /*
         ****** Redirect user *****
         * Are we coming from a third party widget (Google, Netvibes, Pageflakes)?
         */
        if ( isset($_POST['widget']) && ($_POST['widget'] == 1) ) {

        } else {
            /*
             * Team member
             */
            if ($redirect == TRUE) {
                if (!empty($loginArray['subdomain'])) {
                    header('Location: http://www.'.SITE_DOMAIN.'/Places');
                } else {
                    header('Location: '.site_url('places'));
                }
            }
        }
    }
}
?>