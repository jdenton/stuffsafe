var fileTree;
var fileModalArray = [];
var fileManagerTriggerItemID;
var fileManagerTriggerItemType;
var fileModalOpen = false;
var fileUseRaw;
var fileView = 'thumb';
var currentFolderID, currentParentID;
var attachedFilesView = false;
var fileDescriptionEditMode = false;

var getFileDataObj = {
	getFilesForSomething : function(itemID,itemType,fileContainerElement,attachEventHandlers) {
		$.ajax({
		    type:  "POST",
		    url:   siteURL+'filemanager/getFilesForSomething/'+itemID+'/'+itemType+'/json/0',
			dataType: 'json',
		    success: function(payload) {
				fileArray = payload;
                renderFileObj.renderFileWindow(payload,fileContainerElement,true);
                fileManagerObj.attachFilePlugins();
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
    getFilesForView : function(folderID,parentID,view,containerElement,fileserviceFolderID) {
        if (parentID == '') {
            parentID = '0';
        }
        if (parentID == 'google' || parentID == 'dropbox') {
            if (typeof fileserviceFolderID == 'undefined') {
                fileserviceFolderID = '';
            }

            var sendHash = {
                thisPage : thisPage,
                folderID : fileserviceFolderID
            }
            $.ajax({
                type:  "POST",
                url:   siteURL+'filemanager/oAuthFileService/'+parentID,
                data:  sendHash,
                dataType: 'json',
                success: function(payload) {
                    if (typeof payload.AuthLink != 'undefined') {
                        window.location = payload.AuthLink;
                    } else {
                        /*
                         * We probably have documents or folders coming back.
                         */
                        fileModalArray = payload;
                        if (view == 'window') {
                            renderFileObj.renderFileWindow(payload,containerElement);
                        }
                    }
                    
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });

        } else {
            currentFolderID = folderID;
            currentParentID = parentID;
            $.ajax({
                type:  "POST",
                url:   siteURL+'filemanager/getFileItems/'+folderID+'/'+parentID,
                dataType: 'json',
                success: function(payload) {
                    fileModalArray = payload;
                    if (view == 'window') {
                        renderFileObj.renderFileWindow(payload,containerElement);
                    }
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
    }
}

var fileManagerObj = {
    fileManagerButton : function() {
        $('.fileUploadButton').click(function() {
            $('#modalFileUpload').dialog('open');
            fileManagerTriggerID       = $(this).attr('id');
            fileManagerTriggerItemID   = $(this).attr('itemID');
            fileManagerTriggerItemType = $(this).attr('itemType');
            return false;
        });
        fileManagerObj.initFileManagerModal();
    },
    initFileManagerModal : function() {
        $('#modalFileUpload').dialog({
        	autoOpen:      false,
        	closeOnEscape: true,
        	width:         '930px',
        	position:      'center',
			resizable:     false,
			zIndex:        1000,
			open:          function(event,ui) {
                fileModalOpen = true;
                if (fileModalArray.length == 0) {
                    getFileDataObj.getFilesForView(0,0,'window',$('#fileListContainer'));
                    fileManagerObj.attachFilePopupEventHandlers();
                    fileManagerObj.getFileUsage();
                }
			},
            close:         function(event,ui) {
                fileModalOpen = false;
                if (inventoryModalOpen == true) {
                    $('#formInventoryContainer').dialog('open');
                }
            }
        });
	},
	uploadFileSuccess : function(jsonString) {
        var jsonObject = eval('('+jsonString+')');
        renderFileObj.renderFileWindow(jsonObject,$('#fileWindowViewContainer'));
	},
	createItemContainer : function(itemArray) {
		var fileID       = itemArray.FileID;
        var parentID     = itemArray.ParentID;
		var itemType = 'file';
        if (itemArray.Folder == 1) {
            itemType = 'folder';
        }
		var fileName      = itemArray.FileNameActual;
		var filePath      = itemArray.fileLink;
		var fileExt       = itemArray.fileExt;
        var fileDesc      = itemArray.FileDesc;
        var fileUploader  = itemArray.UploaderName;
        var fileSize      = itemArray.FileSize;
        var fileDate      = itemArray.DateEntry;
        var fileService   = itemArray.FileService;
        var fileThumbnail = itemArray.thumbnailLink;
        var shared        = itemArray.Shared;
        var linkID        = 0;

        if (itemArray.LinkID) {
            linkID = itemArray.LinkID;
        }

        var selector, fileServiceClass = '';
		if (itemType == 'file') {
			selector = 'fileContainer';
		} else {
			selector = 'folderContainer';
		}
        if (fileService == 1) {
            fileServiceClass = 'fileservice';
            linkID = itemArray.FolderID;
        }
        var itemContainerStyle = '', hasThumbnail = false;
        var containerClass = 'icon_file_'+fileExt;
        if (fileThumbnail != '' && itemType == 'file') {
             itemContainerStyle = 'style="background: url('+fileThumbnail+') no-repeat center;"';
             containerClass += ' thumbnailContainer';
             hasThumbnail = true;
        }
		var itemContainer = '<li class="'+containerClass+' '+selector+' itemContainer '+fileServiceClass+'" '+itemContainerStyle+' id="fileContainer_'+fileID+'" fileExt="'+fileExt+'" itemType="'+itemType+'" itemID="'+fileID+'" linkID="'+linkID+'" parentID="'+parentID+'" filePath="'+filePath+'" title="'+fileName+'">';
            itemContainer += '<input type="hidden" class="fileDescription" id="fileDescription_'+fileID+'" value="'+fileDesc+'" />';
            if (hasThumbnail == true) {
                itemContainer += '<img src="'+fileThumbnail+'" style="margin-right: 6px; float: left; width: 35px; height: 35px;" class="thumbnailElement hide" />';
            }

            itemContainer += '<p class="title';
            if (hasThumbnail == true) {
                itemContainer += ' thumbnailElement hide ';
            }
            itemContainer += '">'+fileName+'</p>';

            if (itemType == 'file') {
                itemContainer += '<p class="fileDetails hide subText">'+fileUploader+' '+fileDate+'<br />'+fileSize+'</p>';
            } else {
                itemContainer += '<p id="folderContainer_'+fileID+'" class="hide"></p>';
            }

        itemContainer += '</li>';
        return itemContainer;
	},
    attachFileWindowEventHandlers : function() {        
        $('body').delegate('.fileContainer', 'click', function() {
        	if ($(this).hasClass('selected')) {
				$(this).removeClass('selected');
			} else {
				$(this).addClass('selected');
			}

            if (fileModalOpen == false) {
                var html = '', fileURL = '', filePath = $(this).attr('filepath');
                if ($(this).hasClass('thumbnailContainer')) {
                    fileURL = filePath;
                } else {
                    var fileExt = $(this).attr('fileExt');
                    var fileTitle = $(this).find('p.title').html();
                    var fileDetails = $(this).find('p.fileDetails').html();
                    html = '<div class="icon_file_256_'+fileExt+'"><strong><a href="'+filePath+'">'+fileTitle+'</a></strong><br />'+fileDetails+'</div>';
                }
                var itemID  = $(this).attr('itemid');
                $.colorbox({
                    href:        fileURL,
                    html:        html,
                    width:       '800px',
                    height:      '500px',
                    scalePhotos: true,
                    scrolling:   false,
                    onComplete:  function(){
                        var fileTitle = $('#fileDescription_'+itemID).val();
                        if (fileTitle == '' || fileTitle == '0') {
                            fileTitle = 'Click to edit.';
                        }
                        $('#cboxTitle').html(fileTitle);
                        $('#cboxItemID').val(itemID);
                    },
                    onClosed: function() {
                        fileDescriptionEditMode = false;
                    }
                });
            }
		});

        $('#cboxTitle').click(function() {
            if (fileDescriptionEditMode == false) {
                var currentTitle = $(this).html();
                $(this).html('<textarea style="width: 400px; height: 50px;" id="cboxTitleEditor">'+currentTitle+'</textarea><button style="margin-left: 6px; display: inline-block;" id="cboxButtonSave" class="smallButton"><span class="save single"></span></button>');
                fileDescriptionEditMode = true;
                $('#cboxTitleEditor').focus();

                $('#cboxButtonSave').click(function() {
                    var newTitle = $('#cboxTitleEditor').val();
                    var fileID   = $('#cboxItemID').val();
                    $('#cboxTitle').html(newTitle);
                    fileDescriptionEditMode = false;

                    var formHash = {
                                fileDescription : newTitle
                    }

                    if (newTitle != '') {
                        $.ajax({
                            type:  "POST",
                            data:  formHash,
                            url:   siteURL+'filemanager/updateFileDescription/'+fileID,
                            dataType: 'json',
                            success: function(payload) {
                                $('#fileDescription_'+fileID).val(newTitle);
                            },
                            error: function (xhr, desc, exceptionobj) {
                                errorObj.ajaxError(xhr,desc,exceptionobj);
                            }
                        });
                    }

                    return false;
                });
            }
        });

        $('body').delegate('.fileContainer', 'dblclick', function() {
			var itemID = $(this).attr('itemID');
			var filePath = $(this).attr('filePath');
            location.href = filePath;
		});

        $('body').delegate('.itemContainer', 'dblclick', function() {
            if ($(this).attr('itemtype') == 'folder') {
                var folderID = $(this).attr('itemid');
                var parentID = $(this).attr('parentID');
                var linkID   = $(this).attr('linkID');
                var containerElement = $('div.fileManagerWindow');
                var folderName = $(this).find('.title').html();
                $('#fileWindowFolderNameContainer').html(folderName);
                getFileDataObj.getFilesForView(folderID,parentID,'window',containerElement,linkID);
                fileManagerNodeID = folderID;

                if ($(this).hasClass('.fileservice')) {
                    /*
                     * Turn off our folder and upload buttons
                     */
                    $('#buttonupload').toggle(false);
                    $('#buttonfolderNew').toggle(false);
                } else {
                    $('#buttonupload').toggle(true);
                    $('#buttonfolderNew').toggle(true);
                }
            }
        });        

        $('body').delegate('.buttonDelete', 'click', function() {
			var itemID    = $(this).attr('itemID');
            var itemType  = $(this).parents('li').parents('li').attr('itemType');
            var parentID  = $(this).parents('li').parents('li').attr('parentID');
            var itemTitle = $(this).parents('li').parents('li').find('.title').html();
            if (itemType == 'folder') {
                $.get(siteURL+'filemanager/getNumberOfItemsInFolder/'+itemID, function(payload) {
                    if (payload>0) {
                        updateFilesObj.deleteFolderModal(itemID, parentID, itemTitle, payload);
                    } else {
                        updateFilesObj.deleteFolder(itemID);
                    }
                });
            } else {
                updateFilesObj.deleteFileModal(itemID,itemTitle);
            }
			return false;
		});

        $('body').delegate('.buttonView', 'click', function() {
			var fileID = $(this).attr('itemID');
            var linkID = $(this).closest('li.fileContainer').attr('linkID');
            
            $('#fileViewContainer').show();
            $('#pageLeftColumn').hide();

            $.ajax({
                type:  "POST",
                url:   siteURL+'filemanager/getFileItem/'+fileID,
                dataType: 'json',
                success: function(payload) {
                    renderFileObj.renderFileDetails(payload,linkID);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
			return false;
		});

        $('#fileWindowNavContainer').delegate('.folderCrumb', 'click', function() {
            var itemID   = $(this).attr('itemID');
            var parentID = $(this).attr('parentID');
            var folderName = $(this).html();
            fileManagerNodeID = itemID;
            $('#fileWindowFolderNameContainer').html(folderName);
            var containerElement = $('div.fileManagerWindow');
            getFileDataObj.getFilesForView(itemID, parentID, 'window', containerElement);
            return false;
        });
        $('.itemContainer').liveDraggable({
            revert: 'invalid'
        });

        $('body').delegate('.buttonRemove', 'click', function() {
            var fileLinkID = $(this).closest('li.fileContainer').attr('linkID');
        	updateFilesObj.removeAttachedFileFromItem(fileLinkID, $(this).closest('.fileMasterContainer'));
            $(this).closest('li.fileContainer').fadeOut('fast', function() {
                $(this).remove();
            });
            return false;
		});

        $('#closeFileView').click(function() {
            $('#pageLeftColumn').show();
            $('#fileViewContainer').hide();
        });
    },
    attachFilePopupEventHandlers : function() {
        $('#fileTreeContainer').delegate('.folderContainer', 'click', function() {
            var itemID   = $(this).attr('itemID');
            var parentID = $(this).attr('parentID');
            var folderName = $(this).find('.title').html();
            if (fileManagerNodeID != itemID) {
                fileManagerNodeID = itemID;
                getFileDataObj.getFilesForView(itemID, parentID, 'window', '#fileListContainer');
            }
            $('#folderNameContainerPopup').html(folderName);
            if ($('#folderContainer_'+itemID).is(':visible')) {
                $('#folderContainer_'+itemID).slideUp('fast');
            } else {
                $('#folderContainer_'+itemID).slideDown('fast');
            }
            return false;
        });

        $('#fileTreeContainer').delegate('.fileContainer', 'click', function() {
            return false;
        });

        $('#fileTreeContainer').delegate('.itemContainer', 'hover', function() {
            $(this).toggleClass('hover');
        });

        $('#buttonSiteFileSelect').unbind('click').click(function() {
			var destID = $('#'+fileManagerTriggerID+'_fileContainer');
            var fileIDString = '';
            var fileNameArray = [];
            var filePathArray = [];
            var fileClassArray = [];
            var fileTypes = 'local';
			$('.selected').each(function(i) {
				var classStructure = $(this).attr('class');
                var classArray = classStructure.split(' ');

				var itemID         = $(this).attr('itemID');
				var fileName       = $(this).children('p').html();
                var filePath       = $(this).attr('filepath');
                var parentID       = $(this).attr('parentID');
                var fileType       = filesObj.getFileTypeFromClassName($(this));

                if (parentID == 'fileservice' || parentID == 'google') {
                    fileTypes = 'fileservice';
                    fileIDString += '{"FileName":"'+fileName+'","FilePath":"'+filePath+'","FileType":"'+fileType+'"},';
                } else {
                    fileIDString += itemID+'|';
                }                
                filePathArray.push(filePath);
                fileNameArray.push(fileName);
                fileClassArray.push(classArray[0]);				
			});
			$('.selected').removeClass('selected');
            fileIDString = fileIDString.slice(0, -1);

            if (fileTypes == 'fileservice') {
                fileIDString = '['+fileIDString+']';
            }
                
			var sendHash = {
                fileIDString : fileIDString,
                fileTypes    : fileTypes
            }
			$.ajax({
				type:  "POST",
				url:   siteURL+'filemanager/attachFilesToItem/'+fileManagerTriggerItemID+'/'+fileManagerTriggerItemType,
				data:  sendHash,
				success: function(payload){
                    if (payload != 0) {
                        $('#fileAttachConfirm').systemMessage({
                            status: 'success',
                            message: 'Files have been attached'
                        });
                        getFileDataObj.getFilesForSomething(fileManagerTriggerItemID,fileManagerTriggerItemType,destID,false);
                    }
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});

		});
    },
    attachFilePlugins : function() {
        var removeOption = false;
        var deleteOption = true;
        if (attachedFilesView == true) {
            removeOption = true;
            deleteOption = false;            
        }

        $('.itemContainer').hoverButtons({
			viewOption:       true,
            editOption:       false,
            deleteOption:     deleteOption,
            removeOption:     removeOption,
			width:            '25px',
			position:         'topRight',
			elementClass:     'itemContainer'
		});

        $('#fileListContainer ul li.itemContainer ul.smallButtonCluster').remove();
        
        if (attachedFilesView != true) {
            /*
             * Turn off some buttons
             */
            $('.folderContainer ul.smallButtonCluster .buttonView').hide();
        }

        $('.folderContainer').droppable({
            hoverClass: 'active',
            drop: function(event,ui) {
                var item = ui.draggable;
                var itemType   = item.attr('itemtype');
                var dragItemID = item.attr('itemid');
                var dropItemID = $(this).attr('itemid');
                $.get(siteURL+'filemanager/dropFileItem/'+dragItemID+'/'+dropItemID, function() {
                    item.fadeOut('fast');
                });
            }
        });        
    },
    attachPageEventHandlers : function() {
        $('#buttonupload').click(function() {
            $('#panelUpload').toggle(true);
            $('#panelDefault').toggle(false);
            $('#panelNewFolder').toggle(false);
        });

        $('#buttonfolderNew').click(function() {
            $('#panelUpload').toggle(false);
            $('#panelDefault').toggle(false);
            $('#panelNewFolder').toggle(true);
            $('#folderName').val('');
        });

        $('#buttonNewFolderCancel').click(function() {
            $('#panelUpload').toggle(false);
            $('#panelDefault').toggle(true);
            $('#panelNewFolder').toggle(false);
        });

        $('#buttonNewFolderSave').click(function() {
            $('#panelUpload').toggle(false);
            $('#panelDefault').toggle(true);
            $('#panelNewFolder').toggle(false);
            var parentNodeID = fileManagerNodeID;
            var folderName   = $('#folderName').val();
            var action       = $('#folderAction').val();
            updateFilesObj.updateFolder(parentNodeID,folderName,action);
        });

        $('#folderName').bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
                $('#panelUpload').toggle(false);
                $('#panelDefault').toggle(true);
                $('#panelNewFolder').toggle(false);
                var parentNodeID = fileManagerNodeID;
                var folderName   = $('#folderName').val();
                var action       = $('#folderAction').val();
                updateFilesObj.updateFolder(parentNodeID,folderName,action);
                return false;
            }
        });

        $('#thumbnailView').click(function() {
            $('#fileListUL').removeClass().addClass('fileListBlock');
            $('.fileDetails').toggle(false);
            $('.thumbnailElement').addClass('hide');
            $('.thumbnailContainer').removeClass('thumbnailContainerNoBG');
            fileView = 'thumb';
            return false;
        });

        $('#detailView').click(function() {
            $('#fileListUL').removeClass().addClass('fileListDetail');
            $('.fileDetails').toggle(true);
            $('.thumbnailElement').removeClass('hide');
            $('.thumbnailContainer').addClass('thumbnailContainerNoBG');
            fileView = 'detail';
            return false;
        });
    },
	serveFileForDownload : function(itemID,hashFileName) {
		$('#downloadFrame').contents().find('#itemID').val(itemID);
        $('#downloadFrame').contents().find('#hashFileName').val(hashFileName);
		$('#downloadFrame').contents().find('#formDownload').submit();
	},
    getFileUsage : function() {
        $.get(siteURL+'filemanager/getFileSpaceUsage/json/0', function(payload) {
            fileManagerObj.renderFileUsage(payload);
		});
    },
    renderFileUsage : function(jsonString) {
        var jsonObject   = eval('('+jsonString+')');
        var fileUseTotal = jsonObject.FileUseTotal;
        var accountMax   = jsonObject.AccountMax;
        var fileUsePercentage = jsonObject.FileUsePercentage;

        if (accountMax == 'unlimited') {
            $('.fileManagerMeter').hide();
        } else {
            fileUseRaw = parseInt(jsonObject.FileUseRaw);
            var accountMaxRaw = parseInt(numbers[6]);

            var useString = fileUseTotal+' of '+accountMax+' ('+fileUsePercentage+'%)';

            var barWidthDec = parseFloat(fileUsePercentage/100);
            var barWidthPixels = Math.round(barWidthDec*200);

            if (barWidthPixels > 200) {
                barWidthPixels = 200;
            }

            $('.fileManagerMeterText').html(useString);
            $('.fileManagerMeterBar').css('width',barWidthPixels);

            if ( (fileUseRaw >= accountMaxRaw || accountMaxRaw < 1) && swfUploader) {
                swfUploader.setButtonDisabled(true);
                $('#spanButtonPlaceHolderFake').show();
            }
        }
    }
}

var renderFileObj = {
    renderFileDetails : function(jsonObject,fileLinkID) {
        var fileArray = [];
        var fileExt       = jsonObject.fileExt;
        var filePath      = jsonObject.fileLink;
        var dateUploaded  = jsonObject.DateEntry;
        var fileName      = jsonObject.FileNameActual;
        var uploadedBy    = jsonObject.NameFirst+' '+jsonObject.NameLast;
        var fileSize      = jsonObject.FileSize;
        var fileIconClass = jsonObject.FileClassName;
        if (fileSize == '0 bytes') {
            fileSize = '';
        }

        var isImage = genericFunctions.isFileAnImage(fileExt);
        if (isImage == true) {
            fileArray.push('<img src="'+filePath+'" style="max-width: 717px;" />');
        }
        fileArray.push('<div class="fileInfoContainer '+fileIconClass+'"><h2>'+fileName+'</h2>');
        fileArray.push('<p>'+uploadedBy+' '+dateUploaded+'</p><p>'+fileSize+'</p>');
        fileArray.push('</div>');

        $('#downloadFileView').unbind().click(function() {
            location.href = filePath;
        });

        /*
         * Get messages for this file.
         */
        if (fileLinkID>0) {
            /*
             * Let's look for any messages for this file.
             */
            $('#messageListFile').html('');
            $('#newMessageFile').attr('itemID',fileLinkID);
            getMessageObj.getMessages(fileLinkID,'attachedFile',$('#messageListFile'),0,10,1,0);
        }
        var fileString = fileArray.join('');
        $('#fileViewInfomationContainer').html(fileString);
    },
	renderFileListItem : function(fileName,className,targetContainerID,fileLinkID,path,doReturn) {
		var listClass  = className+'_list';
        var fileString  = '<li class="fileLinkContainer" id="fileLink_'+fileLinkID+'">';
            fileString += '<a href="#" class="buttonDeleteSmall removeFile" title="'+commonTerms['remove']+'"></a>';
            fileString += '<a href="'+path+'" title="'+fileName+'" target="_blank" style="display: inline-block;" class="'+listClass+' linkContainer">'+fileName+'</a></li>';
		if (doReturn == true) {
            return fileString;
        } else {
            $('#'+targetContainerID).append(fileString);
        }
        renderFileObj.updateFileCount($('#'+targetContainerID), $('.fileCount'));
	},
	renderFileListItems : function(jsonObject,fileContainerID,doReturn) {
        var fileRenderArray = [];
		for(var a=0;a<=((jsonObject.length)-1);a++) {
			var fileName   = jsonObject[a].FileNameActual;
			var className  = jsonObject[a].FileClassName;
            var fileLinkID = jsonObject[a].LinkID;
            var filePath   = jsonObject[a].fileLink;
            if (jsonObject[a].FileService == 1) {
                filePath = jsonObject[a].FileServicePath;
            }

            fileRenderArray.push(renderFileObj.renderFileListItem(fileName,className,fileContainerID,fileLinkID,filePath,doReturn));
		}
        renderFileObj.updateFileCount($('#'+fileContainerID), $('.fileCount'));

        if (doReturn == true) {
            return fileRenderArray.join('');
        } else {
        }
	},
    updateFileCount : function(fileCountContainer, fileListContainer) {
        var fileCount = fileListContainer.find('ul.fileListBlock').children('.fileContainer').size();
        fileCountContainer.html('('+fileCount+')');
    },
    renderFileWindow : function(jsonObject,containerElement,attachedFiles,returnString,inventoryFormFiles) {
        if (inventoryFormFiles == null) {
            inventoryFormFiles = false;
        }
		containerElement.html('');
        var fileData = jsonObject.Files;
        var items,files;
		if (fileData != null && fileData.children) {
			items = fileData.children.length;
			files = fileData.children;
		} else {
			items = fileData.length;
			files = fileData;
		}
		var stringOut = [], className;
        if (fileView == 'thumb') {
            className = 'fileListBlock';
        } else if (fileView == 'detail') {
            className = 'fileListDetail';
        }        

		stringOut.push('<ul id="fileListUL" class="fileMasterContainer '+className+'">');
		for(var a=0;a<=(items-1);a++) {
			var itemContainer = fileManagerObj.createItemContainer(files[a]);
			stringOut.push(itemContainer);
		}
		stringOut.push('</ul>');
		var stringOutRender = stringOut.join('');
		if (items>0) {
			/*
			 * Clear panel for new icons
			 */
			containerElement.empty();
		}

        if (returnString == true) {
            return stringOutRender;
        } else {
            containerElement.append(stringOutRender);
        }

        if (containerElement.hasClass('fileAttachProject')) {
            containerElement.find('li.itemContainer').addClass('sharable');
        }
        
        $('.fileDetails').toggle(false);

        if (inventoryFormFiles != true) {
            var pathString = renderFileObj.renderFolderPath(jsonObject.Path);
            $('#fileWindowNavContainer').html(pathString);
        }

        /*
         * Attach event handlers
         */
        if (containerElement.attr('id') != '#fileListContainer') {
            $(".itemContainer").draggable("destroy");
            fileManagerObj.attachFilePlugins();
        }
	},
    renderFileTree : function(jsonObject) {
        var fileData = jsonObject.Files;
		if (fileData.children) {
			var items = fileData.children.length;
			var files = fileData.children;
		} else {
			var items = fileData.length;
			var files = fileData;
		}

        var stringOut = [];
        stringOut.push('<ul class="fileTree fileList">');
        for(var a=0;a<=(items-1);a++) {
			var itemContainer = fileManagerObj.createItemContainer(files[a]);
			stringOut.push(itemContainer);
		}
		stringOut.push('</ul>');
        var fileRenderString = stringOut.join('');
        if ($('#fileTreeContainer').children().length>0) {
            $('#folderContainer_'+fileManagerNodeID).html(fileRenderString);
            $('#folderContainer_'+fileManagerNodeID).slideDown('fast');
        } else {
            $('#fileTreeContainer').html(fileRenderString);
        }
    },
    renderFolderPath : function(pathObject) {
        var pathString = '';
        if (typeof pathObject != 'undefined') {
            for(var a=0;a<=(pathObject.length-1);a++) {
                pathString += '<a href="#" class="folderCrumb" itemID="'+pathObject[a].NodeID+'" parentID="'+pathObject[a].ParentID+'">'+pathObject[a].FolderName+'</a>';
                if (a<pathObject.length-1) {
                    pathString += ' › ';
                }
            }
            return pathString;
        }
    }
}

var updateFilesObj = {
    removeAttachedFileFromItem : function(fileLinkID, fileContainer) {
        $.ajax({
            type:  "POST",
            url:   siteURL+'filemanager/removeAttachedFileFromItem/'+fileLinkID,
            success: function(payload){
                renderFileObj.updateFileCount($('.fileCount'), fileContainer);
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    },
    removeAttachedFilesFromItem : function(fileLinkIDString) {
        var formHash = {
                    fileLinkIDString : fileLinkIDString
        }
        $.ajax({
            type:  "POST",
            data: formHash,
            url:   siteURL+'filemanager/removeAttachedFilesFromItem',
            success: function(payload){

            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    },
    updateFolder : function(parentNodeID,FolderName,action,folderID) {
        var sendHash = {
            currentParentID : currentParentID,
            parentNodeID    : parentNodeID,
            folderName      : FolderName,
            action          : action,
            folderID        : folderID
        }
        $.ajax({
            type:  "POST",
            url:   siteURL+'filemanager/createNewFolder',
            data:  sendHash,
			dataType: 'json',
            success: function(payload) {
                fileModalArray = payload;
                renderFileObj.renderFileWindow(payload,$('#fileWindowViewContainer'));
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    },
    deleteFolderModal : function(folderID,parentID,folderTitle,itemCount) {
        var modalContents = [];
        modalContents.push('<div class="errorMessageBig">'+commonTerms['deleteFolder']+': '+folderTitle+'</div>');
        modalContents.push('<p style="margin-bottom: 12px;"><strong>'+commonTerms['deleteFileNoUndo']+'</strong></p>');
        modalContents.push('<p><input type="checkbox" id="deleteFolderContents" value="1" /> '+commonTerms['deleteContentsToo']+'</p>');
        var outString = modalContents.join('');
        $('#deleteContentsContainer').html(outString);

        $('#buttonDeleteModalDelete').unbind('click').click(function(){
            var deleteContents = 0;
            if ($('#deleteFolderContents').attr('checked')) {
                deleteContents = true;
            }
            updateFilesObj.deleteFolder(folderID,parentID,deleteContents);
        });
        $('#modalDelete').dialog('open');
    },
    deleteFolder : function(deleteFolderID,deleteParentID,deleteContents) {
        /*
         * We are not going to send files to the trash. We are going to
         * delete them permanently.
         */
        if (deleteContents == true) {
            $.get(siteURL+'filemanager/deleteItem/'+deleteFolderID+'/'+deleteParentID+'/folder/1', function(payload) {
                fileManagerObj.renderFileUsage(payload);
                getFileDataObj.getFilesForView(currentFolderID,currentParentID,'window',$('#fileWindowViewContainer'));
            });
        } else {
            $.get(siteURL+'filemanager/deleteItem/'+deleteFolderID+'/'+deleteParentID+'/folder', function(payload) {
                fileManagerObj.renderFileUsage(payload);
                getFileDataObj.getFilesForView(currentFolderID,currentParentID,'window',$('#fileWindowViewContainer'));
            });
        }
        $('#modalDelete').dialog('close');        
    },
    deleteFileModal : function(fileID,fileName) {
        var modalContents = [];
        modalContents.push('<div class="errorMessageBig">'+commonTerms['deleteFile']+': '+fileName+'</div>');
        modalContents.push('<p style="margin-bottom: 12px;"><strong>'+commonTerms['deleteFileNoUndo']+'</strong></p>');
        var outString = modalContents.join('');
        $('#deleteContentsContainer').html(outString);

        $('#buttonDeleteModalDelete').unbind('click').click(function(){
            $.get(siteURL+'filemanager/deleteItem/'+fileID+'/0/file', function(payload) {
                $('#modalDelete').dialog('close');
                $('#fileContainer_'+fileID).fadeOut('fast');
                fileManagerObj.renderFileUsage(payload);
            });
        });
        $('#modalDelete').dialog('open');
    }
}

$(document).ready(function() {
    if ($('#pageLeftColumn').attr('pageID') == 'fileView') {
        getFileDataObj.getFilesForView(0,0,'window',$('#fileWindowViewContainer'));
        fileManagerObj.attachPageEventHandlers();
        fileManagerObj.getFileUsage();
        $('#footerWidgetMenu li.iconfilemanager').toggle(false);
	}
    fileManagerObj.attachFileWindowEventHandlers();

    if ($('#pageLeftColumn').attr('pageID') == 'individualFileView') {
        var fileID = $('#fileID').val();
        var linkID = $('#fileLinkID').val();
        $.ajax({
            type:  "POST",
            url:   siteURL+'filemanager/getFileItem/'+fileID,
            dataType: 'json',
            success: function(payload) {
                renderFileObj.renderFileDetails(payload,linkID);
                $('#fileViewContainer').show();
                $('#closeFileView').hide();

                var projectObj = eval('('+projectData+')');
                var messageTo = projectObj.MessageRecipients;
                initMessageBox.createMessageBox(messageTo,$('#newMessageFile'),$('#messageListFile'),false,false,false,true,true,true);
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    }
});