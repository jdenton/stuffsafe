var initTeamObj = {
	initTeam : function() {
		/*
		 * Set some initial settings
		 */
		$('#makeTeamMember').attr('checked',true);
		$('#formContactContainer').slideUp();

		/*
		 * Render team member information cards
		 */
        getContactObj.getContacts('T',0,1,0);

        /*
		 * Init contacts stuff
		 */
        helperContactsObj.initContactsContainer();
		helperContactsObj.initContactsForm('T');
	}
}

$(document).ready(function() {
	initTeamObj.initTeam();
});