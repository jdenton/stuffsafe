var insuranceArray = [];

var getSettingsDataObj = {
    getEmailSettings : function() {
        $.ajax({
		    type:  "POST",
		    url:   siteURL+'getSettings/0/account/email/1/json/0',
			dataType: 'json',
		    success: function(payload){
                for(a=0;a<=(payload.length-1);a++) {
                    var setting = payload[a].Setting;
                    var value   = payload[a].SettingValue;
                    if (value == 1) {
                        $('#'+setting).attr('checked',true);
                    } else {
                        $('#'+setting).attr('checked',false);
                    }
                }
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getInsurancePolicies : function() {
        $.ajax({
		    type:  "POST",
		    url:   siteURL+'insurance/getPolicies/json',
			dataType: 'json',
		    success: function(payload){
                insuranceArray = payload;
                renderSettingsObj.renderInsuranceGrid();
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    }
}

var renderSettingsObj = {
    renderInsuranceGrid : function() {
        if (insuranceArray.length<1) {
            $('#containerPolicyInformation')
            .removeClass('activityMessageBig')
            .empty()
            .systemMessage({
                status: 'information',
                noFade:  1,
                size:    'Big',
                message: commonTerms['noPolicies']
            });
        } else {
            var stringOut = [];
            for(a=0;a<=insuranceArray.length-1;a++) {
                var policyID      = insuranceArray[a].PolicyID;
                var policyCompany = insuranceArray[a].Company;
                var url           = insuranceArray[a].URL;
                var policyNumber  = insuranceArray[a].PolicyNo;
                var datePolicy    = insuranceArray[a].DatePolicy;
                var agentName     = insuranceArray[a].AgentNameFirst+' '+insuranceArray[a].AgentNameLast;
                var agentPhone    = insuranceArray[a].AgentPhone;
                var agentEmail    = insuranceArray[a].AgentEmail;
                var agentFax      = insuranceArray[a].AgentFax;

                stringOut.push('<div class="row policyRow" style="padding: 6px; width: 700px;" id="policyRow'+policyID+'" itemID="'+policyID+'">');
                stringOut.push('<div style="float: left; width: 45%;">');
                    if (url != '') {
                        stringOut.push('<p><strong><a href="'+url+'">'+policyCompany+'</a></strong></p>');
                    } else {
                        stringOut.push('<p><strong>'+policyCompany+'</strong></p>');
                    }
                    stringOut.push('<p>'+commonTerms['policyNumber']+': '+policyNumber+'</p>');
                    stringOut.push('<p>'+datePolicy+'</p>');
                stringOut.push('</div>');
                stringOut.push('<div style="float: left; width: 45%;">');
                    stringOut.push('<p>'+agentName+'</p>');
                    stringOut.push('<p>Phone: '+agentPhone+'</p>');
                    stringOut.push('<p>Fax: '+agentFax+'</p>');
                    if (agentEmail != '' && agentEmail != null) {
                        stringOut.push('<p><a href="mailto:'+agentEmail+'">'+agentEmail+'</a></p>');
                    }
                stringOut.push('</div>');
                stringOut.push('<div style="clear: left;"></div>');
                stringOut.push('</div>');
            }
            var stringOutFinal = stringOut.join('');

            $('#containerPolicyInformation')
                .removeClass('activityMessageBig')
                .html(stringOutFinal)
                .alternateContainerRowColors();

            /*
             * Make little edit/delete buttons (jquery.commonAppMethods.js)
             */
            $('#containerPolicyInformation').hoverButtons({
                elementClass:  'policyRow',
                editClass:     'policyUpdate',
                deleteClass:   'policyDelete',
                width:        '70px'
            });

        }
    }
}

var saveSettingsObj = {
    saveInsurancePolicy : function() {
        var formHash = {
            save           : 1,
            policyID       : $('#policyID').val(),
            action         : $('#action').val(),
            companyName    : $('#policyCompanyName').val(),
            companyURL     : $('#policyCompanyURL').val(),
            policyNumber   : $('#policyNumber').val(),
            policyDate     : $('#policyDate').val(),
            agentNameFirst : $('#policyAgenctNameFirst').val(),
            agentNameLast  : $('#policyAgenctNameLast').val(),
            agentEmail     : $('#policyAgentEmail').val(),
            agentPhone     : $('#policyAgentPhone').val(),
            agentFax       : $('#policyAgentFax').val()
        }

        $.ajax({
		    type:  "POST",
		    url:   siteURL+'insurance/savePolicy',
			data: formHash,
            dataType: 'json',
		    success: function(payload) {
                settingsSupportObj.resetInsuranceForm();
                insuranceArray = payload;
                renderSettingsObj.renderInsuranceGrid();
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    saveEmailSettings : function() {
        var settingArray = [];
        $('.settingEmail').each(function() {
            var setting = $(this).attr('value');
            var value = 0;
            if ($(this).attr('checked') == true) {
                value = 1;
            }
            /*
             * setting_group_section
             */
            if (setting == 'emailSendClientNew') {
                settingArray.push(setting+'_account_email='+value);
            } else {
                settingArray.push(setting+'_member_email='+value);
            }
        });

        var serverData = settingArray.join('&');
        $.ajax({
		    type:  "POST",
		    url:   siteURL+'settings/saveSettingsMultiple',
			data:  serverData,
		    success: function(payload){
				$('#messageSettingsEmail').systemMessage({
					status:  'success',
					size:    'Big',
					message: commonTerms['settingsSaved']
				});
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    saveAccountSettings : function() {
        var settingArray = [];
        $('.settingAccount').each(function() {
            var setting = $(this).attr('value');
            var value = 0;
            if ($(this).attr('checked') == true) {
                value = 1;
            }
            /*
             * setting_group_section
             */
            settingArray.push(setting+'_account_account='+value);
        });
        if (settingArray.length>0) {
            var serverData = settingArray.join('&');
            $.ajax({
                type:  "POST",
                url:   siteURL+'settings/saveSettingsMultiple',
                data:  serverData,
                success: function(payload){
					$('#messageSettingsAccountSecurity').systemMessage({
						status:  'success',
						size:    'Big',
						message: commonTerms['settingsSaved']
					});
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
    },
    saveAccountSettingsClient : function() {
        /*
         * Check for new password
         */
        if ($('#newPassword').val() != '') {
            saveSettingsObj.saveNewPassword();
        }
    },
    saveNewPassword : function() {
        var currentPassword = $('#currentPassword').val();
        var newPassword     = $('#newPassword').val();

        var formHash = {
            currentPassword : currentPassword,
            newPassword     : newPassword
        }

        $.ajax({
		    type:  "POST",
		    url:   siteURL+'settings/saveNewPassword',
			data: formHash,
            dataType: 'json',
		    success: function(payload) {
                if (payload.Status == 'error') {
                    $('#messageSettingsAccountPassword').removeClass('successMessageBig').addClass('errorMessageBig').text(payload.Message);

                } else {
                    $('#messageSettingsAccountPassword').removeClass('errorMessageBig').addClass('successMessageBig').text(payload.Message);
                }
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
	saveInvoiceSettings : function() {
		var settingArray = [];
        $('.settingInvoice').each(function() {
			var setting = $(this).attr('id');
            var value   = $(this).val();
			settingArray.push(setting+'_account_invoice='+value);
        });

        var serverData = settingArray.join('&');
		$.ajax({
		    type:  "POST",
		    url:   siteURL+'settings/saveSettingsMultiple',
			data:  serverData,
		    success: function(payload){
				$('#messageInvoiceSettings').systemMessage({
					status:  'success',
					size:    'Big',
					message: commonTerms['settingsSaved']
				});
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
        
	},
	saveInvoicePayPal : function() {
		var setting = 'emailPayPal';
		var value   = $('#emailPayPal').val();
		var serverData = 'settingValue='+value+'&settingType='+setting+'_account_invoice';
		$.ajax({
		    type:  "POST",
		    url:   siteURL+'settings/saveSettings',
			data:  serverData,
		    success: function(payload){
				$('#messageInvoiceSettingsPayPal').systemMessage({
					status:  'success',
					size:    'Big',
					message: commonTerms['settingsSaved']
				});
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
    saveAccountInformation : function() {
        /*
         * Check for missing data
         */
        if ($('#accountNameFirst').val() == '' || $('#accountNameFirst').val() == '' || $('#accountEmail').val() == '') {
            $('#messageAccountInfo').systemMessage({
               size:    'Big',
               message: commonTerms['error_missing_data'],
               status:  'error'
            });
        } else {
            var formHash = {
                accountID : $('#accountID').val(),
                nameFirst : $('#accountNameFirst').val(),
                nameLast  : $('#accountNameLast').val(),
                company   : $('#accountCompanyName').val(),
                address1  : $('#accountAddress1').val(),
                address2  : $('#accountAddress2').val(),
                city      : $('#accountCity').val(),
                state     : $('#accountState').val(),
                zip       : $('#accountZip').val(),
                country   : $('#accountCountry').val(),
                currency  : $('#accountCurrency').val(),
                phone     : $('#accountPhoneOffice').val(),
                email     : $('#accountEmail').val()
            }

            $.ajax({
                type:  "POST",
                url:   siteURL+'settings/updateAccountInformation',
                data:  formHash,
                success: function(payload){
                    if (payload == 'success') {
                        $('#messageAccountInfo').systemMessage({
                           size:    'Big',
                           message: 'Account information saved!',
                           status:  'success'
                        });
                    } else {
                        $('#messageAccountInfo').systemMessage({
                           size:    'Big',
                           message: commonTerms['error_missing_data'],
                           status:  'error'
                        });
                    }
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
    }
}

var renderSettingsDataObj = {
    renderLogo : function(logoFileURL,logoW,logoH,resolution) {
		var logoImg = '<img src="'+logoFileURL+'" width="'+logoW+'" height="'+logoH+'" />';
        if (resolution == 'hi') {
            $('#containerLogoHiRes').html(logoImg);
        } else {
            $('#containerLogoLowRes').html(logoImg);
            $('.accountIdentityContainer div').css('margin-top','0px');
            $('.accountIdentityContainer div').html(logoImg);
        }
    },
	renderAvatar : function(avatarFileURL) {
		var avatarImg = '<img src="'+avatarFileURL+'" class="avatar80" />';
        $('#containerAvatar').html(avatarImg);
		if ($('#pageLeftColumn').attr('pageID') == 'settingsOwner') {
			/*
			 * Set the owners' avatar in the header
			 */
			$('#userAvatarContainer').html(avatarImg);
		} else {
			$('#avatarFilename').val(avatarFileURL);
		}
		
	}
}

var updateSettingsObj = {
	uploadLogoFiles : function() {
        /*
         ***** LOGO LO RES
         */
		var buttonLogoLoRes = $('#buttonUploadLogoLoRes'), interval;
		new Ajax_upload(buttonLogoLoRes, {
			action:   siteURL+'settings/uploadLogo/low',
			name:     'Filedata',
			onSubmit: function(file, ext){
                if (ext && /^(jpg|png|jpeg|gif)$/.test(ext)) {
                    $('#buttonUploadLogoLoRes .buttonDecorator').removeClass('upload').addClass('busy');
                    this.disable();
                    interval = window.setInterval(function() {}, 200);
                }
			},
			onComplete: function(file, payload){
				 $('#buttonUploadLogoLoRes .buttonDecorator').removeClass('busy').addClass('upload');
				 window.clearInterval(interval);
				 this.enable();
                 var jsonObj = eval('('+payload+')');
                 if (jsonObj.Result == 'success') {
                     var logoURL = jsonObj.Message;
                     var logoW   = jsonObj.Width;
                     var logoH   = jsonObj.Height;
                     renderSettingsDataObj.renderLogo(logoURL,logoW,logoH,'low');
                     $('#logoErrorMessage').html('').removeClass('errorMessageSmall');
                 } else if (jsonObj.Result == 'error') {
                     $('#logoErrorMessage').html(jsonObj.Message).addClass('errorMessageSmall');
                 }
			}
	   });

       /*
         ***** LOGO HI RES
         */
       var buttonLogoHiRes = $('#buttonUploadLogoHiRes'), interval;
		new Ajax_upload(buttonLogoHiRes, {
			action:   siteURL+'settings/uploadLogo/hi',
			name:     'Filedata',
			onSubmit: function(file, ext){
                if (ext && /^(jpg|png|jpeg|gif)$/.test(ext)) {
                    $('#buttonUploadLogoHiRes .buttonDecorator').removeClass('upload').addClass('busy');
                    this.disable();
                    interval = window.setInterval(function() {}, 200);
                }
			},
			onComplete: function(file, payload) {
				 $('#buttonUploadLogoHiRes .buttonDecorator').removeClass('busy').addClass('upload');
				 window.clearInterval(interval);
				 this.enable();
                 var jsonObj = eval('('+payload+')');
                 if (jsonObj.Result == 'success') {
                     var logoURL = jsonObj.Message;
                     var logoW   = jsonObj.Width;
                     var logoH   = jsonObj.Height;
                     renderSettingsDataObj.renderLogo(logoURL,logoW,logoH,'hi');
                     $('#logoErrorMessage').html('').removeClass('errorMessageSmall');
                 } else if (jsonObj.Result == 'error') {
                     $('#logoErrorMessage').html(jsonObj.Message).addClass('errorMessageSmall');
                 }
			}
	   });
	},
	uploadAvatarFiles : function(type) {
	    var buttonAvatar = $('#buttonUploadAvatar'), interval2;
        new Ajax_upload(buttonAvatar, {
			action:   siteURL+'settings/uploadAvatar/'+type,
			name:     'Filedata',
			onSubmit: function(file, ext){
                if (ext && /^(jpg|png|jpeg|gif)$/.test(ext)) {
                    $('#buttonUploadAvatar .buttonDecorator').removeClass('upload').addClass('busy');
                    this.disable();
                    interval2 = window.setInterval(function() {}, 200);
                }
			},
			onComplete: function(file, payload){
                //console.log(payload);
				 $('#buttonUploadAvatar .buttonDecorator').removeClass('busy').addClass('upload');
				 window.clearInterval(interval2);
				 this.enable();
                 var jsonObj = eval('('+payload+')');
                 if (jsonObj.Result == 'success') {
                     var avatarURL = jsonObj.Message;
                     renderSettingsDataObj.renderAvatar(avatarURL);
                     $('#avatarErrorMessage').html('').removeClass('errorMessageSmall');
                 } else if (jsonObj.Result == 'error') {
                     $('#avatarErrorMessage').html(jsonObj.Message).addClass('errorMessageSmall');
                 }				 
			}
	   });      
	},
    cancelAccount : function() {
        $.ajax({
		    type:  "POST",
		    url:   siteURL+'settings/cancelAccount',
		    success: function(payload){
				window.location = siteURL+'login/logout/cancel';
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    }
}

var initSettingsObj = {
	initSettings : function() {
		$('#tabsSettings').tabs({
			fx: { opacity: 'toggle' },
			show: function(event,ui) {
				var panel = ui.panel.id;
				if (panel == 'windowSettingsAccount') {
                    $('#accountPlanContainer').show();
                } else if (panel == 'windowSettingsPersonal') {
                    var pID = $('#pID').val();
                    $.getJSON(siteURL+'appcontacts/getContact/'+pID+'/json/0/1', function(payload){
                        updateContactsObj.populateEditForm(payload);
                    });

                    /*
                     * Turn off some contact form controls
                     */
                    $('#contactTeamMemberRoleContainer').hide();
                    $('#buttonCancelContact').hide();
                } else if (panel == 'windowSettingsInsurance') {
                    if (insuranceArray.length == 0) {
                        getSettingsDataObj.getInsurancePolicies();
                    }
                } else {
                    $('#accountPlanContainer').hide();
                }
			}
		});

        /*
         * Attach some event handlers
         */
        $('#buttonDataImportOther').click(function() {
           window.location = siteURL+'/import/Import';
        });

        $('#buttonSaveAccountPassword').click(function() {
            saveSettingsObj.saveNewPassword();
            return false;
        });

        $('#buttonSaveAccountSettingsClient').click(function() {
            saveSettingsObj.saveAccountSettingsClient();
            return false;
        });

		$('#buttonSaveInvoiceSettings').click(function() {
            saveSettingsObj.saveInvoiceSettings();
            return false;
        });

		$('#buttonSaveInvoiceSettingsPayPal').click(function() {
            saveSettingsObj.saveInvoicePayPal();
            return false;
        });

        initSettingsObj.initDialog();

        $('#buttonAccountCancel').click(function() {
            $('#dialogCancelAccount').dialog('open');
            return false;
        });

        $('#buttonDataImport').click(function() {
            dataImportObj.dataImportVersion1();
            return false;
        });

        $('#buttonDataImportSelfHosted').click(function() {
            dataImportObj.dataImportVersion1SelfHosted();
            return false;
        });

        $('#buttonDataExport').click(function() {
            dataImportObj.dataExport();
            return false;
        });

        $('#buttonAddPolicyInformation').click(function() {
            $('#containerPolicyInformation').hide();
            $('#containerFormPolicy').slideDown('fast');
            settingsSupportObj.clearInsuranceForm();

            $('#policyDate').datepicker({
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1
            });
            return false;
        });

        $('#buttonSavePolicy').click(function() {
            saveSettingsObj.saveInsurancePolicy();
            return false;
        });

        $('#buttonCancelPolicy').click(function() {
            settingsSupportObj.resetInsuranceForm();
            return false;
        });
        
        $('#buttonAccountSave').click(function() {
            saveSettingsObj.saveAccountInformation();
            return false;
        });


        /*
         * Some form stuff
         */
        $('#accountCountry').val($('#countryHolder').val());

        $('#buttonUpdateAccount').click(function() {
           window.location = siteSecureURL+'account/changeAccount/'+numbers[1];
        });

        if (numbers[2]>0) {
            $('#buttonUpdateCCInfo').click(function() {
               window.location = siteSecureURL+'payment/paymentInfo/'+numbers[1];
            });
        } else {
            $('#changeCCContainer').hide();
        }

        $('#containerPolicyInformation').delegate('.buttonEdit', 'click', function(e) {
            var policyID = $(this).attr('itemID');
            settingsSupportObj.getPolicyForEdit(policyID);
            return false;
        });

        $('#containerPolicyInformation').delegate('.buttonDelete', 'click', function(e) {
            var policyID = $(this).attr('itemID');
            $('#policyRow'+policyID).fadeOut('slow');
            updateTrashObj.sendToTrash('policy',policyID);
            return false;
        });
	},
    initDialog : function() {
        $('#dialogCancelAccount').dialog({
        	autoOpen:      false,
        	closeOnEscape: true,
        	height:        '300',
        	width:         '490px',
        	position:      'center',
            draggable:     false,
            modal:         true,
			resizable:     false,
			zIndex:        1000,
			open:          function(event,ui) {
			}
        });
        $('#buttonCancel').click(function() {
           $('#dialogCancelAccount').dialog('close');
           return false;
        });
        $('#buttonCancelAccount').click(function() {
           updateSettingsObj.cancelAccount();
           return false;
        });
    }
}

var settingsSupportObj = {
    clearInsuranceForm : function() {
        $('#policyID').val('');
        $('#action').val('add');
        $('#policyCompanyName').val('');
        $('#policyCompanyURL').val('');
        $('#policyNumber').val('');
        $('#policyDate').val('');
        $('#policyAgenctNameFirst').val('');
        $('#policyAgenctNameLast').val('');
        $('#policyAgentEmail').val('');
        $('#policyAgentPhone').val('');
        $('#policyAgentFax').val('');
    },
    resetInsuranceForm : function() {
        settingsSupportObj.clearInsuranceForm();
        $('#containerFormPolicy').slideUp('fast');
        $('#containerPolicyInformation').show();
    },
    populateInsuranceFormForEdit : function(jsonObject) {
        $('#policyID').val(jsonObject.PolicyID);
        $('#action').val('edit');
        $('#policyCompanyName').val(jsonObject.Company);
        $('#policyCompanyURL').val(jsonObject.URL);
        $('#policyNumber').val(jsonObject.PolicyNo);
        $('#policyDate').val(jsonObject.DatePolicy);
        $('#policyAgenctNameFirst').val(jsonObject.AgentNameFirst);
        $('#policyAgenctNameLast').val(jsonObject.AgentNameLast);
        $('#policyAgentEmail').val(jsonObject.AgentEmail);
        $('#policyAgentPhone').val(jsonObject.AgentPhone);
        $('#policyAgentFax').val(jsonObject.AgentFax);

        $('#containerPolicyInformation').hide();
        $('#containerFormPolicy').slideDown('fast');

        $('#policyDate').datepicker({
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
        });
    },
    getPolicyForEdit : function(policyID) {
        $.ajax({
		    type:  "POST",
		    url:   siteURL+'insurance/getPolicy/'+policyID,
			dataType: 'json',
		    success: function(payload){
                settingsSupportObj.populateInsuranceFormForEdit(payload);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    }
}

var dataImportObj = {
    dataImportVersion1 : function() {
        if ($('#version10UserName').val() == '' || $('#version10Password').val() == '') {
            $('#messageDataImport').systemMessage({
                status:  'error',
                size:    'Big',
                message: commonTerms['error_login_empty_field']
            });
        } else {
            $('#messageDataImport').addClass('activityMessageBig').html(commonTerms['loadingData']);

            var formHash = {
                userName : $('#version10UserName').val(),
                password : $('#version10Password').val()
            }

            $.ajax({
                type:  "POST",
                data:  formHash,
                dataType: 'json',
                url:   siteURL+'settings/dataImportVersion10',
                success: function(payload) {
                    var status = payload.Status;
                    var message = payload.Message;
                    if (payload.Summary) {
                        $('#importSummary').html(payload.Summary);
                    }
                    $('#messageDataImport').removeClass('activityMessageBig');
                    $('#messageDataImport').systemMessage({
                        status:  status,
                        size:    'Big',
                        message: message
                    });
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
    },
    dataImportVersion1SelfHosted : function() {
        if ($('#version10DbaseHost').val() == '' || $('#version10DbaseName').val() == '' || $('#version10DbaseUsername').val() == '' || $('#version10DbasePassword').val() == '') {
            $('#messageDataImportSelfHosted').systemMessage({
                status:  'error',
                size:    'Big',
                message: commonTerms['error_login_empty_field']
            });
        } else {
            $('#messageDataImportSelfHosted').addClass('activityMessageBig').html(commonTerms['loadingData']);

            var formHash = {
                dbaseHostname : $('#version10DbaseHost').val(),
                dbaseDatabase : $('#version10DbaseName').val(),
                dbaseUserName : $('#version10DbaseUsername').val(),
                dbasePassword : $('#version10DbasePassword').val(),
                filePath      : $('#filePath').val()
            }

            $.ajax({
                type:  "POST",
                data:  formHash,
                dataType: 'json',
                url:   siteURL+'settings/dataImportVersion10SelfHosted',
                success: function(payload) {
                    var status = payload.Status;
                    var message = payload.Message;
                    if (payload.Summary) {
                        $('#importSummarySelfHosted').html(payload.Summary);
                    }
                    $('#messageDataImportSelfHosted').removeClass('activityMessageBig');
                    $('#messageDataImportSelfHosted').systemMessage({
                        status:  status,
                        size:    'Big',
                        message: message
                    });
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
    },
    dataExport : function() {
        exportObj.exportAccountData();
    }
}

$(document).ready(function() {
	if ($('#pageLeftColumn').attr('pageID') == 'settings') {
		initSettingsObj.initSettings();
		helperContactsObj.initContactsForm('T');
	}
});