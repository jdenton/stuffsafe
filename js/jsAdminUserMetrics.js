var createChartObj = {
	createLoginChart : function() {
		$.ajax({
		    type: "POST",
		    url:  siteURL+'adminusermetrics/createLoginChart',
		    data: 'userID=',
		    success: function(msg){
		    	var chartFile = msg;
		    	$('#chartContainerLogins').html('<img src="'+chartFile+'" />');
		    },
		    failure: function() {
		    	
		    }
	    });
	},
	createSignupChart : function() {
		$.ajax({
		    type: "POST",
		    url:  siteURL+'adminusermetrics/createSignupChart',
		    data: 'userID=',
		    success: function(msg){
		    	var chartFile = msg;
		    	alert('<img src="'+chartFile+'" />');
		    	$('#chartContainerSignup').html('<img src="'+chartFile+'" />');
		    },
		    failure: function() {
		    	
		    }
	    });
	},
    initOpenFlashCharts : function(year) {
        /*
         * Initialize Open Flash Chart
         */
        chartInitObj.initOpenFlashChart(717,450,siteURL+'adminusermetrics/reportNewAccountsByDate/'+year+'/json/0','chartContainerSignup');
        chartInitObj.initOpenFlashChart(357,300,siteURL+'adminusermetrics/reportAccountCountPieByDate/'+year+'/json/0','chartContainerPieAccounts');
        chartInitObj.initOpenFlashChart(355,300,siteURL+'adminusermetrics/reportAccountIncomePieByDate/'+year+'/json/0','chartContainerPiePrice');
    }
}

$(document).ready(function() {
	/*
	 * Get default chart
	 */
	//createChartObj.createLoginChart();
	//createChartObj.createSignupChart();

    $('#selectYear').change(function() {
        createChartObj.initOpenFlashCharts($(this).val());
    });

    var year = $('#selectYear').val();
    createChartObj.initOpenFlashCharts(year);
});