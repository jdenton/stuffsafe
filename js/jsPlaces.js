var initPlacesObj = {
    initPlacesView : function() {
        $('#tabsPlaces').tabs({
            fx: { opacity: 'toggle' },
            show: function(event,ui) {
				var panel = ui.panel.id;
                if (panel == 'windowViewPlaces') {
                    getPlaceDataObj.getPlaceList();
                } else if (panel == 'windowInventoryReports') {
                    /*
                     * Get initial report data
                     */
                    getReportDataObj.getReportValueByProperty();
                }
            }
        });

        var totalPlaces = parseInt(numbers[4]);
        $('#buttonnewProperty').click(function() {
            if ($('.placeRow').length >= totalPlaces) {
                $('#modalAccountLimit').dialog('open');
                $('#maxPlacesContainer').show();
                $('#maxTeamMembersContainer').hide();
                $('#maxFilesContainer').hide();
            } else {
                window.location = siteURL+'placeupdate';
            }
        });

        /*
         * Initialize Open Flash Chart
         */
        chartInitObj.initOpenFlashChart(700,450,siteURL+'reports/getReportValueByProperty/json/0','chartContainer');

        reportSupportObj.attachReportChangeHandlers();
    },
    initPlaceView : function() {
        $('#tabsPlace').tabs({
            fx: { opacity: 'toggle' },
            show: function(event,ui) {
				var panel = ui.panel.id;
                var placeID = $('#placeID').val();
                if (panel == 'windowViewPlace') {
                

                } else if (panel == 'windowInventory') {
                    getInventoryDataObj.getInventory(placeID);
                }
            }
        });
        renderPlaceDataObj.renderPlaceDetails();
        renderInventoryDataObj.attachInventoryContainerEventHandlers();
        inventorySupportObj.updateGlobalInventoryValue();
        this.initPlaceMaps();
    },
    initPlaceForm : function() {
        $('#buttonCancel').click(function() {
            if ($('#placeID').val()>0) {
                var placeID = $('#placeID').val();
                window.location = siteURL+'places/index/'+placeID;
            } else {
                window.location = siteURL+'places';
            }
            return false;
         });

        $('#buttonTagEdit').makeCategoryEditBox({
			listJSON: tagJSON,
			listType: 'tag',
			instanceName: 'tags',
			siteArea: 'place'
		});

		$('textarea.expanding').elastic();
    },
    initPlaceMaps : function() {
        $('#modalMap').dialog({
        	autoOpen:      false,
        	closeOnEscape: true,
        	width:         '600px',
            height:        '300',
        	position:      'center',
			resizable:     false,
			zIndex:        1000,
            modal:         false,
			open:          function(event,ui) {

			},
            close:         function(event,ui) {

            }
        });

        $('.lnkMap').click(function() {
            var mapAddress = $(this).data('address');
            $('#modalMap').dialog('open');
            $('#modalMap').gMap({
                markers: [{ address: mapAddress, html: '_address' }],
                controls: ['GSmallMapControl'],
                address: mapAddress,
                zoom: 15
            });
            
            return false;
        });
    }
}

var getPlaceDataObj = {
    getPlaceList : function(tag) {
        if (tag == '' || typeof tag == 'undefined') {
			tag = '0';
		}
        $.ajax({
		    type:     "POST",
		    url:      siteURL+'places/getPlaces/'+tag+'/json/0',
			dataType: 'json',
		    success: function(payload){
                var jsonObject = payload;
		    	placeArray = jsonObject.Places;
                renderPlaceDataObj.renderPlaceGrid(jsonObject);

                /*
                 * Are we maxed out on places?
                 */
                if ($('#maxPlaces').val() == 1) {
                    supportPlaceObj.showMaxPlacesModal();
                }
                initPlacesObj.initPlaceMaps();
                inventorySupportObj.updateGlobalInventoryValue();
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getPlaceDetails : function(placeID) {
        $.ajax({
		    type:     "POST",
		    url:      siteURL+'place/getPlace/'+placeID,
			dataType: 'json',
		    success: function(payload){
                var jsonObject = payload;
		    	placeArray = jsonObject.Places;
                renderPlaceDataObj.renderPlaceGrid(jsonObject);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    }
}

var renderPlaceDataObj = {
    renderPlaceGrid : function(jsonObj) {
        var places = jsonObj.Places;
        if (places.length<1) {
            $('#placeListContainer')
            .removeClass('activityMessageBig')
            .empty()
            .systemMessage({
                status: 'information',
                noFade:  1,
                size:    'Big',
                message: commonTerms['noPlaces']
            });
        } else {
            var stringOut = '<input type="hidden" id="acdc" value="asc" />';
            stringOut    += '<input type="hidden" id="origSort" value="title" />';
            for(a=0;a<=places.length-1;a++) {
                stringOut += renderPlaceDataObj.getPlaceRenderString(places[a],'list');
            }
            $('#placeListContainer')
                .removeClass('activityMessageBig')
                .html(stringOut)
                .alternateContainerRowColors();

            /*
             * Make little edit/delete buttons (jquery.commonAppMethods.js)
             */
            $('#placeListContainer').hoverButtons({
                inventoryOption: true,
                elementClass:    'placeRow',
                editClass:       'placeUpdate',
                deleteClass:     'placeDelete',
                width:           '70px'
            });

            /*
             * Attach event handlers
             */
            $('.buttonEdit').click(function() {
                var placeID = $(this).attr('itemID');
                window.location = siteURL+'placeupdate/index/'+placeID;
                return false;
            });

            $('.buttonDelete').click(function() {
                var placeID = $(this).attr('itemID');
                $('#placeRow'+placeID).fadeOut('slow');
                updateTrashObj.sendToTrash('property',placeID);
                return false;
            });

            $('.buttonArchive').click(function() {
                var projectID = $(this).attr('itemID');
                $('#projRow'+projectID).fadeOut('slow');
                supportProjectObj.updateArchive(projectID);
                return false;
            });

            $('.buttonOpen').click(function() {
                var projectID = $(this).attr('itemID');
                supportProjectObj.updateArchive(projectID);
                return false;
            });

            $('.buttonInventory').click(function() {
                var placeID = $(this).attr('itemID');
                window.location = siteURL+'place/getPlace/'+placeID+'#windowInventory';
                return false;
            });

            $('.linkFiles').click(function(){
                /*
                 * Click over to files tab for this project
                 */
                var projectID = $(this).attr('projID');
                var linkURL = siteURL+'projects/ProjectDetail/index/'+projectID+'#windowFiles'
                window.location = linkURL;
                $('#tabsProjectDashboard > ul').tabs('select', 1);
                return false;
            });
            permissionsObj.renderControlsBasedOnPermissions();
        }
    },
    getPlaceRenderString : function(placeArray,type) {
        var stringOut = [];
        var placeID          = placeArray.PlaceID;
        var placeName        = placeArray.PlaceName;
        var placeAddress     = placeArray.Address;
        var placeCity        = placeArray.City;
        var placeState       = placeArray.State;
        var placeZip         = placeArray.Zip;
        var placeCountry     = placeArray.Country;
        var placeDesc        = placeArray.Description;
        var placeDateEntered = placeArray.DateEntered;
        var placeTags        = placeArray.Tags;
        var placeCategory    = placeArray.Category
        var placeArea        = placeArray.Area;
        var placeAreaUnit    = placeArray.AreaUnit;
        var enteredBy        = placeArray.NameFirst+' '+placeArray['NameLast'];
        var dateEntered      = placeArray.DateEnteredHuman;
        var totalPricePurchase = placeArray.TotalPricePurchase;
        var totalPriceReplace  = placeArray.TotalPriceReplace;
        var placeAreaString = '', placeCategoryRender = '', placeTagsRender = '', addressRender = '';

        var totalPricePurchaseRender = accounting.formatMoney(totalPricePurchase, commonTerms['currencySymbol'], 2, ",", ".");
        var totalPriceReplaceRender  = accounting.formatMoney(totalPriceReplace, commonTerms['currencySymbol'], 2, ",", "."); // €4.999,99

        if (totalPricePurchaseRender == '' || totalPricePurchaseRender == null) {
            totalPricePurchaseRender = '';
        }
        if (totalPriceReplaceRender == '' || totalPriceReplaceRender == null) {
            totalPriceReplaceRender = '';
        }

        if (placeCategory != '' && placeCategory != null && placeCategory != 'null') {
            placeCategoryRender = '<p class="icon_category_small">'+placeCategory+'</p>';
        }
        if (placeTags != '' && placeTags != null && placeCategory != 'null') {
            placeTagsRender = '<p class="icon_tag">'+placeTags+'</p>';
        }

        /*
         * Create address
         */
        var address = '', description = '';
        if (placeAddress != '' && placeAddress != 'null') {
            address += '<span class="street-address">'+placeAddress+'</span>';
        }
        if (placeCity != '' && placeCity != 'null') {
            address += '<br /><span class="locality">'+placeCity+'</span> ';
        }
        if (placeState != '' && placeState != 'null') {
            address += '<span class="region">'+placeState+'</span> ';
        }
        if (placeZip != '' && placeZip != 'null') {
            address += '<span class="postal-code">'+placeZip+'</span> ';
        }
        if (placeCountry != '' && placeCountry != 'null') {
            address += placeCountry;
        }
        if (address != '') {
            var addressData = placeAddress+' '+placeCity+' '+placeState+' '+placeZip+' '+placeCountry;
            addressRender = address+'<a href="#" class="icon_map_marker lnkMap" data-address="'+addressData+'">'+commonTerms['viewMap']+'</a>'
        }
        if (placeDesc != '' && placeDesc != null) {
            description += '<p>'+placeDesc+'</p>';
        }
        if (placeArea != '' && placeArea != null) {
            placeAreaString = '<strong>'+commonTerms['areaOfProperty']+'</strong> '+placeArea+' square '+placeAreaUnit;
        }

        stringOut.push('<div class="row placeRow" style="padding: 6px; width: 700px;" id="placeRow'+placeID+'" itemID="'+placeID+'">');
            stringOut.push('<h3 style="float: left; width: 550px;"><a href="'+siteURL+'place/getPlace/'+placeID+'">'+placeName+'</a></h3>');
            stringOut.push('<div style="float: left;" class="totalPriceReplace bigText" data-totalInventoryAmount="'+totalPriceReplace+'">'+totalPriceReplaceRender+'</div>');
            stringOut.push('<div style="clear: left;"></div>');
            stringOut.push('<p class="subText mBottom12">'+commonTerms['created_by']+' '+enteredBy+' on '+dateEntered+'</p>');
            stringOut.push('<div style="float: left; width: 40%;">'+addressRender+'</div>');
            stringOut.push('<div style="float: left; width: 60%;"><p>'+description+'</p><p style="margin-top: 12px;">'+placeAreaString+'</p>'+placeCategoryRender+placeTagsRender+'</div>');
            stringOut.push('<div style="clear: left;"></div>');
        stringOut.push('</div>');

        var newOutString = stringOut.join('');
        return newOutString;
    },
    renderPlaceDetails : function() {
        var stringOut = renderPlaceDataObj.getPlaceRenderString(placeData,'single');
        $('#placeInformationContainer').html(stringOut).removeClass('activityMessageBig');
    }
}

var supportPlacesObj = {
    showMaxPlacesModal : function() {
        $('#modalAccountLimit').dialog('open');
        $('#maxPlacesContainer').show();
        $('#maxTeamMembersContainer').hide();
        $('#maxFilesContainer').hide();
    }
}

$(document).ready(function() {
	if ($('#pageLeftColumn').attr('pageID') == 'updatePlace') {
		initPlacesObj.initPlaceForm();
	} else if ($('#pageLeftColumn').attr('pageID') == 'viewAllPlaces') {
		initPlacesObj.initPlacesView();
	} else if ($('#pageLeftColumn').attr('pageID') == 'viewPlace') {
		initPlacesObj.initPlaceView();
	}
});