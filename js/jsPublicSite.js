var initSiteObj = {
    initSignup : function() {
        /*
         * Event handlers for signup form
         */
        $('.plan').click(function() {
            $('#boxPlan').removeClass('errorPanel').addClass('boxYellow');

            $('.plan').removeClass('iconCheckOrange').addClass('iconCheckGray');
            $('.plan').removeClass('selected');

            var planName;
            if ($(this).hasClass('free')) {
                planName = 'planfree';
                $('#billingTextFree').show();
                $('#billingTextPaid').hide();
                $('#selectedPlan').val('free_0');
            } else if ($(this).hasClass('basic')) {
                planName = 'planbasic';
                $('#billingTextFree').hide();
                $('#billingTextPaid').show();
                $('#selectedPlan').val('basic_1');
            } else if ($(this).hasClass('premium')) {
                planName = 'planpremium';
                $('#billingTextFree').hide();
                $('#billingTextPaid').show();
                $('#selectedPlan').val('premium_2');
            } else if ($(this).hasClass('max')) {
                planName = 'planmax';
                $('#billingTextFree').hide();
                $('#billingTextPaid').show();
                $('#selectedPlan').val('max_3');
            }

            $(this).removeClass('iconCheckGray').addClass('iconCheckOrange selected');

            var planDescription = $('.'+planName).html();


            $('#planDescription').html(planDescription);
            return false;
        });

        $('#firstName').blur(function() {
            signupFormObj.checkFirstName($(this).val());
            return false;
        });

        $('#email').blur(function() {
            signupFormObj.checkEmailAddress($(this).val());
            return false;
        });

        $('#passwordConfirm').blur(function() {
            signupFormObj.checkEmailPassword($('#email').val(),$('#password').val(),$(this).val());
            return false;
        });

        $('#acceptTOS').click(function() {
           if ($(this).attr('checked') == true) {
               $('#boxSubmit').removeClass('errorPanel').addClass('boxBlue');
           }
        });

        $('#formSignup').submit(function() {
            /*
             * Run validations
             */
            var checkEmailAddress  = signupFormObj.checkEmailAddress($('#email').val());
            var checkFirstName     = signupFormObj.checkFirstName($('#firstName').val());
            var checkEmailPassword = signupFormObj.checkEmailPassword($('#email').val(),$('#password').val(),$('#passwordConfirm').val());
            var checkPlanSelection = signupFormObj.checkPlanSelection();
            var checkTOS           = signupFormObj.checkTOS();

            if (
                checkEmailAddress  == false ||
                checkFirstName     == false ||
                checkEmailPassword == false ||
                checkPlanSelection == false ||
                checkTOS           == false
                ) {
                    return false;
                } else {
                    return true;
                }
        });
    },
    initPayment : function() {
        /*
         * If this is a postback...
         */
        if ($('#countryHolder').val() != '') {
            $('#country').val($('#countryHolder').val());
            signupFormObj.checkCountryState();
        }

        if ($('#stateHolder').val() != '') {
            if ($('#countryHolder').val() != 'US') {
                $('#stateRegion').val($('#stateHolder').val());
            } else {
                $('#stateUS').val($('#stateHolder').val());
            }
        }

        if ($('#currencyHolder').val() != '') {
            $('#currency').val($('#currencyHolder').val());
        }

        /*
         * Event handlers for payment form
         */
        $('#firstName').blur(function() {
            signupFormObj.checkFirstName($(this).val());
            return false;
        });

        $('#lastName').blur(function() {
            signupFormObj.checkLastName($(this).val());
            return false;
        });

        $('#address').blur(function() {
            signupFormObj.checkAddress($(this).val());
            return false;
        });

        $('#city').blur(function() {
            signupFormObj.checkAddress($(this).val());
            return false;
        });

        $('#state').blur(function() {
            signupFormObj.checkAddress($(this).val());
            return false;
        });

        $('#zip').blur(function() {
            signupFormObj.checkAddress($(this).val());
            return false;
        });
        
        $('#cardtype').blur(function() {
            signupFormObj.checkCardType($(this).val());
            return false;
        });

        $('#cardnumber').blur(function() {
            signupFormObj.checkCardNumber($(this).val());
            return false;
        });

        $('#cvv2number').blur(function() {
            signupFormObj.checkCardCVV2($(this).val());
            return false;
        });

        $('#expyear').blur(function() {
            signupFormObj.checkExpYear($(this).val(),$('#currentYear').val());
            return false;
        });
        
        $('#formPayment').submit(function() {
            var checkFirstName   = signupFormObj.checkFirstName($('#firstName').val());
            var checkLastName    = signupFormObj.checkLastName($('#lastName').val());
            var checkAddress     = signupFormObj.checkAddress($('#address').val());
            var checkCity        = signupFormObj.checkAddress($('#city').val());
            var checkState       = signupFormObj.checkAddress($('#state').val());
            var checkZip         = signupFormObj.checkAddress($('#zip').val());
            var checkCardType    = signupFormObj.checkCardType($('#cardtype').val());
            var checkCardNumber  = signupFormObj.checkCardNumber($('#cardnumber').val());
            var checkCardCVV2    = signupFormObj.checkCardCVV2($('#cvv2number').val());
            var expDate          = signupFormObj.checkExpYear($('#expyear').val(),$('#currentYear').val());

            if (
                checkFirstName  == false ||
                checkLastName   == false ||
                checkAddress    == false ||
                checkCity       == false ||
                checkState      == false ||
                checkZip        == false ||
                checkCardType   == false ||
                checkCardNumber == false ||
                checkCardCVV2   == false ||
                expDate         == false
                ) {
                    return false;
                } else {
                    $('#processMessageContainer').show();
                    $('#confirmPayment').hide();
                    return true;
                }
        });
    }
}

var tourObj = {
    initTourThumbs : function() {
        $(".thumbs").colorbox({
            rel: 'thumbs',
            transition: 'fade',
            current: ''
        });

        $(".video").colorbox({iframe:true, innerWidth: 800, innerHeight: 500});
        
    }
}

var signupFormObj = {
    checkWebAddress : function(webAddress) {
        var formHash = {webAddress : webAddress}
        $.ajax({
		    type:     'POST',
		    url:      siteURL+'signup/checkWebAddress',
			dataType: 'json',
            data:     formHash,
		    success: function(payload){
                if (payload.Status == 'false') {
                    $('#webAddress').parent().addClass('errorPanel');
                    $('#errorWebAddress').html(payload.Message);
                } else {
                    $('#webAddress').parent().removeClass('errorPanel');
                    $('#errorWebAddress').html('');
                }
		    },
		    error: function (xhr, desc, exceptionobj) {
				//errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    checkCompanyName : function(companyName) {
        $('#companyName').parent().removeClass('errorPanel');
        $('#errorCompanyName').html('');
        if (companyName == '') {
            $('#companyName').parent().addClass('errorPanel');
            $('#errorCompanyName').html('Please enter your company name.');
            return false;
        }
    },
    checkEmailAddress : function(email) {
        $('#email').parent().removeClass('errorPanel');
        $('#errorEmail').html('');
        if (email == '') {
            $('#email').parent().addClass('errorPanel');
            $('#errorEmail').html('Please enter an email address.');
            return false;
        }
    },
    checkFirstName : function(firstName) {
        $('#firstName').parents('div.messageBlock').removeClass('errorPanel');
        $('#errorName').html('');
        if (firstName == '') {
            $('#firstName').parents('div.messageBlock').addClass('errorPanel');
            $('#errorName').html('Please enter your first name.');
            return false;
        }
    },
    checkLastName : function(lastName) {
        $('#firstName').parents('div.messageBlock').removeClass('errorPanel');
        $('#errorName').html('');
        if (lastName == '') {
            $('#lastName').parents('div.messageBlock').addClass('errorPanel');
            $('#errorName').html('Please enter your last name.');
            return false;
        }
    },
    checkAddress : function(addressItem) {
        $('#address').parents('div.messageBlock').removeClass('errorPanel');
        $('#errorAddress').html('');
        if (addressItem == '') {
            $('#address').parents('div.messageBlock').addClass('errorPanel');
            $('#errorAddress').html('Please enter your complete billing address.');
            return false;
        }
    },
    checkCardType : function(cardType) {
        $('#cardtype').parents('div.messageBlock').removeClass('errorPanel');
        $('#errorCardType').html('');
        if (cardType == '' || cardType == '0') {
            $('#cardtype').parents('div.messageBlock').addClass('errorPanel');
            $('#errorCardType').html('Select credit card type.');
            return false;
        }
    },
    checkCardNumber : function(cardNumber) {
        $('#cardnumber').parents('div.messageBlock').removeClass('errorPanel');
        $('#errorCreditCard').html('');
        if (cardNumber == '' || ($('#cardtype').val() == 'Amex' && cardNumber.length < 15) || ($('#cardtype').val() != 'Amex' && cardNumber.length < 16)) {
            $('#cardnumber').parents('div.messageBlock').addClass('errorPanel');
            $('#errorCreditCard').html('Please enter a valid credit card number.');
            return false;
        }
    },
    checkCardCVV2 : function(cvv2Number) {
        $('#cvv2number').parents('div.messageBlock').removeClass('errorPanel');
        $('#errorCreditCard').html('');
        if (cvv2Number == '' || cvv2Number.length < 3) {
            $('#cvv2number').parents('div.messageBlock').addClass('errorPanel');
            $('#errorCreditCard').html('Please enter a valid security code.');
            return false;
        }
    },
    checkExpYear : function(expYear,currentYear) {
        $('#expyear').parents('div.messageBlock').removeClass('errorPanel');
        $('#errorExpDate').html('');
        if (expYear < currentYear) {
            $('#expyear').parents('div.messageBlock').addClass('errorPanel');
            $('#errorExpDate').html('Please enter a valid expiration date.');
            return false;
        }
    },
    checkEmailPassword : function(email,password,passwordConfirm) {
        $('#password').parents('div.messageBlock').removeClass('errorPanel');
        $('#errorPassword').html('');

        /*
         * First let's check our password confirm
         */
        if (password != passwordConfirm) {
            $('#password').parents('div.messageBlock').addClass('errorPanel');
            $('#errorPassword').html('Your password was not confirmed.');
            return false;
        } else if (password == '' || passwordConfirm == '') {
            $('#password').parents('div.messageBlock').addClass('errorPanel');
            $('#errorPassword').html('Enter a password and confirm it.');
            return false;
        } else {            
            var formHash = {
                                email    : email,
                                password : password
                            }
            $.ajax({
                type:     'POST',
                url:      siteURL+'signup/checkEmailPassword',
                dataType: 'json',
                data:     formHash,
                success: function(payload){
                    if (payload.Status == 'false') {
                        $('#password').parents('div.messageBlock').addClass('errorPanel');
                        $('#errorPassword').html(payload.Message);
                    } else {
                        $('#password').parent().removeClass('errorPanel');
                        $('#errorPassword').html('');
                    }
                },
                error: function (xhr, desc, exceptionobj) {
                    //errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
    },
    checkPlanSelection : function() {
        if ($('#selectedPlan').val() == '') {
            $('#boxPlan').removeClass('boxYellow').addClass('errorPanel');

            return false;
        }
    },
    checkTOS : function() {
        if ($('#acceptTOS').attr('checked') == false) {
            $('#boxSubmit').removeClass('boxBlue').addClass('errorPanel');

            return false;
        }
    },
    changeAccountConfirm : function(plan) {
        var accountUserid = $('#accountUserid').val();
        $.ajax({
		    type:     'POST',
		    url:      siteURL+'signup/changeAccountConfirm/'+accountUserid+'/'+plan,
		    success: function(payload){
                $('#pageTitle').html('Your plan has been changed.');
                $('#introTextConfirm').hide();
                $('#introTextConfirmed').show();
                $('#buttonCancel').hide();
                $('#buttonChangeAccountConfirm').hide();
                $('#buttonGoToAccount').show();
		    },
		    error: function (xhr, desc, exceptionobj) {
				//errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    checkCountryState : function() {
        if ($('#country').val() != 'US') {
            $('#stateUS').val('').hide();
            $('#stateRegion').show();
        } else {
            $('#stateRegion').val('').hide();
            $('#stateUS').show();
        }
    }
}

$(document).ready(function() {
    /*
     * Init popup
     */
    $('.popup').click(function() {
       var url = $(this).attr('href');
       window.open(url,'popupWindow',"menubar=no,width=600,height=500,toolbar=no,scrollbars=yes,status=no");

       return false;
    });

    $('#buttonChangeAccountConfirm').click(function() {
        var plan = $(this).attr('plan');
        signupFormObj.changeAccountConfirm(plan);
        return false;
    });

    $('#helpCCCode').click(function() {
       $('#cc_verificationLocation').show();
       $(this).hide();
    });    

    signupFormObj.checkCountryState();

    $('#country').change(function() {
        signupFormObj.checkCountryState();
    });

    $('#whyCreditCardNumber').click(function() {
       if ($('#whyCreditCardNumberPanel').is(':visible')) {
           $('#whyCreditCardNumberPanel').slideUp('fast');
       } else {
           $('#whyCreditCardNumberPanel').slideDown('fast');
       }
       return false;
    });
    
	initSiteObj.initSignup();
    initSiteObj.initPayment();
    tourObj.initTourThumbs();

    $('#homeMoreLink').click( function () {
        $('#moreFeaturesContainer').slideDown('fast');
        return false;
    });

    $('#selfHostedBadge').click(function() {
        window.location = siteURL+'selfhosted';
    });

    $('.flexslider').flexslider({
        animation: 'slide',
        controlNav: false,
        slideshowSpeed: 7000,
        animationDuration: 300
    });

    /*
    flowplayer("player", "http://dev.stuffsafe.com/flash/screencasts/flowplayer-3.2.7.swf", {
        plugins: {
            controls: {
                url: 'http://dev.stuffsafe.com/flash/screencasts/flowplayer.controls-3.2.5.swf'
            }
        },
        clip: {
            url: "http://dev.stuffsafe.com/flash/screencasts/tour.flv",
            autoPlay: false
        }
    });
    */
});

var contactFieldObj = {
    attachEventHandlers : function() {
    },
    checkValueOnSubmit : function() {

    }


}