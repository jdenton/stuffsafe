var roomArray = [];
var inventoryIDEdit = 0;

var getInventoryDataObj = {
	getInventory : function(placeID) {
		$.ajax({
		    type:  "POST",
		    url:   siteURL+'inventory/getInventoryForPlace/'+placeID+'/json',
            dataType: 'json',
		    success: function(payload){
				if (payload != 0) {
                    inventoryArray = payload;
                    renderInventoryDataObj.renderInventoryGrid(inventoryArray);
				} else {
                    $('#inventoryInformationContainer').empty().systemMessage({
                        status: 'information',
                        noFade:  1,
                        size:    'Big',
                        message: commonTerms['noInventory']
                    });
                }
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
    getInventoryFromSearch : function(searchTerm) {
        var hash = {q : searchTerm}
        var projectID = $('#projectID').val();

        $.ajax({
		    type:     "POST",
		    url:      siteURL+'inventory/searchInventory/0/'+placeID,
            data:     hash,
            dataType: 'json',
		    success: function(payload){
                /*
				if (payload != 0) {
					var jsonObject = payload;
					taskArray = jsonObject.MilestonesTasks;
                    milestoneArray = jsonObject;
					renderTaskDataObj.renderTaskGrid(milestoneArray);
				}
                */
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getInventoryStats : function() {

    }
}

var renderInventoryDataObj = {
    renderInventoryGrid : function(jsonObject) {
        /*
         * Remove any existing elements and event handlers
         */
        $('#inventoryInformationContainer').empty();
        roomArray = jsonObject;
		var totalRooms = roomArray.length;
		var stringOut  = [];

		for(var a=0;a<=(totalRooms)-1;a++) {
            roomArray[a].Index = a;
            var roomID = roomArray[a].RoomID;
            var roomString = renderInventoryDataObj.renderRoomRow(roomArray[a],true);
			$('#inventoryInformationContainer').append(roomString);
            $('#roomRow'+roomID).data('itemIndex',a);
            $('#roomRow'+roomID).data('roomID',roomID);

			/*
			 * Now print inventory items
			 */
			var inventoryStringOut = [];
			inventoryStringOut.length = 0;
			var inventory   = roomArray[a].Inventory;
			var totalInventory  = inventory.length;

			if (totalInventory>0) {
				inventoryStringOut.push('<div id="InventoryGrid'+roomID+'" data-roomID="'+roomID+'">');
				for(var b=0;b<=(totalInventory)-1;b++) {
                    inventory[b].ItemIndex = b;
                    inventory[b].RoomID    = roomID;
                    inventoryStringOut.push(renderInventoryDataObj.renderInventoryRow(inventory[b],true));
				}
				inventoryStringOut.push('</div>');

				var inventoryString = inventoryStringOut.join('');
				$('#inventoryListContainer'+roomID).html(inventoryString);
			}
		}
        $('.itemCost').formatCurrency({symbol: commonTerms['currencySymbol'],useHtml: true});

		renderInventoryDataObj.attachInventoryPlugins();
        permissionsObj.renderControlsBasedOnPermissions();

        attachedFilesView = true;
        fileManagerObj.attachFilePlugins();
        inventorySupportObj.updateInventoryValueTotals();
    },
    renderInventoryRow : function(inventoryObject,wrapper) {
          if (typeof(inventoryObject) != 'object') {
               var inventoryObject = eval('('+ inventoryObject + ')');
		  }

          var inventoryStringOut = [];
          var inventoryID       = inventoryObject.InventoryID;
          var roomID            = inventoryObject.RoomID;
          var itemCategory      = inventoryObject.MainCat;
          var itemName          = inventoryObject.ItemName;
          var itemDescription   = inventoryObject.ItemDesc;
          var itemMake          = inventoryObject.ItemMake;
          var itemModel         = inventoryObject.ItemModel;
          var itemSN            = inventoryObject.ItemSerial;
          var itemPricePurchase = parseFloat(inventoryObject.PricePurchase);
          var itemPriceReplace  = parseFloat(inventoryObject.PriceReplace);
          var itemCondition     = inventoryObject.ItemCondition;
          var itemDatePurchase  = inventoryObject.DatePurchase;
          var itemDatePurchaseHuman  = inventoryObject.DatePurchaseHuman;
          var itemTags          = inventoryObject.Tags;
          var itemDateEntered   = inventoryObject.DateEntered;
          var itemFiles         = inventoryObject.Files;
          var itemIndex         = inventoryObject.ItemIndex;

          var dateEntered       = inventoryObject.DateEnteredHuman;
          var creatorPID        = inventoryObject.PID;
          var creatorName       = inventoryObject.EnteredBy;

          var itemPricePurchaseRender = accounting.formatMoney(itemPricePurchase,commonTerms['currencySymbol']);
          var itemPriceReplaceRender  = accounting.formatMoney(itemPriceReplace,commonTerms['currencySymbol']);

          if (itemCondition == null || itemCondition == 'null') {
              itemCondition = '';
          }
          if (itemCategory == null) {
              itemCategory = '';
          }
		  
          if (wrapper == true) {
            inventoryStringOut.push('<div class="row itemRow inventoryRow inventoryContainer" id="inventoryRow'+inventoryID+'" itemIndex="'+itemIndex+'" itemID="inventory_'+inventoryID+'">');
            inventoryStringOut.push('<div class="controls noPrint" id="controls'+inventoryID+'" itemID="'+inventoryID+'" style="width: 50px; float: left;">&nbsp;</div>');
            inventoryStringOut.push('<div id="inventoryContainer'+inventoryID+'">');
          }
                inventoryStringOut.push('<input type="hidden" id="inventoryRoomID'+inventoryID+'" value="'+roomID+'" />');
                inventoryStringOut.push('<div style="padding-left: 6px; width: 45%; float: left;">');
                    inventoryStringOut.push('<strong id="inventoryName'+inventoryID+'"><a href="#" class="inventoryTitleLink" itemID="'+inventoryID+'">'+itemName+'</a></strong><br />');
                    inventoryStringOut.push('<span class="subText">'+commonTerms['created_by']+' <a href="#" class="personLink" personID="'+creatorPID+'" id="'+creatorPID+'">'+creatorName+'</a> on '+dateEntered+'</span>');
                inventoryStringOut.push('</div>');
                inventoryStringOut.push('<div style="float: right;"><div class="dragHandle" title="Drag me to reorder"></div></div>');
                inventoryStringOut.push('<div style="float: right; margin-right: 12px; font-weight: bold;"><span class="itemCost">'+itemPriceReplace+'</span></div>');
                inventoryStringOut.push('<div style="clear: both;"></div>');
                inventoryStringOut.push('<div style="margin: 6px 0 0 56px; display: none;" class="inventoryDetailsContainer doPrint" id="inventoryDetailsContainer'+inventoryID+'">');
                    inventoryStringOut.push('<div style="float: left; width: 40%;"><strong>'+commonTerms['description']+'</strong><p>'+itemDescription+'</p></div>');
                    inventoryStringOut.push('<div style="float: left;">');
                        inventoryStringOut.push('<table class="layout"><tr><td><strong>'+commonTerms['make']+':</strong> '+itemMake+'</td><td><strong>'+commonTerms['model']+':</strong> '+itemModel+'</td></tr>');
                        inventoryStringOut.push('<tr><td><strong>'+commonTerms['pricePurchase']+':</strong> '+itemPricePurchaseRender+'</td><td><strong>'+commonTerms['priceReplacement']+':</strong> '+itemPriceReplaceRender+'</td></tr>');
                        inventoryStringOut.push('<tr><td><strong>'+commonTerms['purchaseDate']+':</strong> '+itemDatePurchaseHuman+'</td><td><strong>'+commonTerms['condition']+':</strong> '+itemCondition+'</td></tr></table>');
                        inventoryStringOut.push('<p style="margin-left: 4px;"><strong>'+commonTerms['serialNumber']+':</strong> '+itemSN+'</p>');
                    inventoryStringOut.push('</div>');
                    inventoryStringOut.push('<div style="clear: both;"></div>');

                    if (itemCategory != '') {
                        inventoryStringOut.push('<p class="icon_category_small" style="margin-top: 6px;">'+itemCategory+'</p>');
                    }
                    if (itemTags != '' && itemTags != null) {
                        var itemTagLinks = '';
                        var tagArray = itemTags.split(' ');
                        $.each(tagArray, function( intIndex, objValue ) {
                            itemTagLinks += '<a href="'+siteURL+'sitesearch/SearchTags/'+objValue+'">'+objValue+'</a>&nbsp;';
                        });
                        inventoryStringOut.push('<p class="icon_tag" style="margin-top: 6px;">'+itemTagLinks+'</p>');
                    }

                    inventoryStringOut.push('<div style="margin-bottom: 12px;" class="fileListContainer" id="fileListContainer_'+inventoryID+'">');
                    if (itemFiles.length>0) {
                        inventoryStringOut.push(renderFileObj.renderFileWindow(inventoryObject,$('#fileListContainer_'+inventoryID),true,true));
                    }
                    inventoryStringOut.push('</div><div style="clear: both;"></div>');
				inventoryStringOut.push('</div><div style="clear: both;"></div>');

          if (wrapper == true) {
            inventoryStringOut.push('</div>');
            inventoryStringOut.push('</div>');
          }

          return inventoryStringOut.join('');
     },
     renderRoomRow : function(roomObject,wrapper) {
		var roomIndex   = roomObject.Index;
		var roomID      = roomObject.RoomID;
        var roomName    = roomObject.RoomName;
		var tags        = roomObject.Tags;
        var creatorPID  = roomObject.PID;
        var creatorName = roomObject.EnteredBy;

		var totalInventory   = roomObject.Inventory.length;
		var stringOut    = [];
		stringOut.length = 0;

        if (totalInventory<1) {
			var inventoryCount = '0';
		} else {
			inventoryCount = totalInventory;
        }

        var itemText = commonTerms['items'];
        if (inventoryCount == 1) {
            itemText = commonTerms['item'];
        }

        var itemRowClass = '';

        if (wrapper == true) {
            stringOut.push('<div class="barBlue bottom '+itemRowClass+' itemRow roomRow clickable roomContainer" id="roomRow'+roomID+'" itemID="room_'+roomID+'">');
            stringOut.push('<div class="controls noPrint" id="roomControls'+roomID+'" itemID="'+roomID+'" style="width: 50px; float: left;">&nbsp;</div>');
            stringOut.push('<div id="roomContainer'+roomID+'">');
        }

            stringOut.push('<div style="width: 520px; float: left;">');
                stringOut.push('<h2><span id="roomTitle_'+roomID+'">'+roomName+'</span> <span class="lightBlue">( <span class="lightBlue" id="inventoryCount'+roomID+'">'+inventoryCount+' '+itemText+'</span> )</span></h2>');
                stringOut.push('<span class="subText">'+commonTerms['created_by']+' <a href="#" class="personLink" personID="'+creatorPID+'" id="'+creatorPID+'">'+creatorName+'</a></span>');
            stringOut.push('</div>');
            stringOut.push('<div style="float: right; padding-right: 28px;"><h2><span class="bigText lightBlue" id="roomTotal_'+roomID+'"></span></h2></div>');
            stringOut.push('<div style="clear: both;"></div>');

        if (wrapper == true) {
            stringOut.push('</div>');
            stringOut.push('</div>');
            stringOut.push('<div class="inventoryListContainer" id="inventoryListContainer'+roomID+'"></div>');
        }
		return stringOut.join('');
	 },
     attachInventoryPlugins : function() {
         $('#inventoryInformationContainer')
            .hiliteContainerRow();

        $('#inventoryInformationContainer').hoverButtons({
            elementClass: 'itemRow',
            insertionElement: 'controls',
            editClass: 'inventoryUpdate',
            deleteClass: 'inventoryDelete',
            position: ''
        });

        $('.roomContainer').droppable({
            hoverClass: 'active',
            drop: function(event,ui) {
                var item = ui.draggable;
                var itemType   = item.attr('itemtype');
                var dragItemID = item.attr('itemid');
                var dropItemID = $(this).attr('itemid');
                var dragItemIDArray = dragItemID.split('_');
                var dropItemIDArray = dropItemID.split('_');

                dragItemID = dragItemIDArray[1];
                dropItemID = dropItemIDArray[1];
                $.get(siteURL+'inventory/swapInventoryItemRooms/'+dragItemID+'/'+dropItemID, function() {
                    item.fadeOut('fast');
                    var inventoryItemArray;
                    var inventoryID       = dragItemID;
                    var originalRoomID    = $('#inventoryRoomID'+inventoryID).val();
                    var originalRoomIndex = $('#roomRow'+originalRoomID).data('itemIndex');
                    var newRoomIndex      = $('#roomRow'+dropItemID).data('itemIndex');
                    var inventoryItemIndex = 0;
                    $.each(inventoryArray[originalRoomIndex].Inventory, function(key,value) {
                        if (this.InventoryID == inventoryID) {
                            inventoryItemArray = inventoryArray[originalRoomIndex].Inventory[inventoryItemIndex];
                            inventoryArray[originalRoomIndex].Inventory.splice(inventoryItemIndex,1);
                        }
                        inventoryItemIndex++;
                    });
                    inventoryArray[newRoomIndex].Inventory.push(inventoryItemArray);
                    renderInventoryDataObj.renderInventoryGrid(inventoryArray);
                });
            }
        });
     },
     attachInventoryContainerEventHandlers : function() {
        $('#inventoryInformationContainer').delegate('.roomRow', 'click', function() {
            var roomID = $(this).data('roomID');
            if ($('#inventoryListContainer'+roomID).is(':visible')) {
                $('#inventoryListContainer'+roomID).slideUp('fast');
            } else {
                $('#inventoryListContainer'+roomID).slideDown('fast');
            }

            return false;
        });

        $('#inventoryInformationContainer').delegate('.inventoryTitleLink', 'click', function() {
            var inventoryID = $(this).attr('itemID');
            if ($('#inventoryDetailsContainer'+inventoryID).is(':visible')) {
                $('#inventoryDetailsContainer'+inventoryID).slideUp('fast');
            } else {
                $('#inventoryDetailsContainer'+inventoryID).slideDown('fast');
            }

            return false;
        });

        $('#inventoryInformationContainer').delegate('.buttonEdit', 'click', function(e) {
            e.stopPropagation();
            var itemID = $(this).attr('itemID');
            var itemArray   = itemID.split('_');
            var itemType    = itemArray[0];
            var inventoryID = itemArray[1];
            if (itemType == 'inventory') {
                inventoryInitObj.populateInventoryFormForEdit(inventoryID);
            } else {
                inventoryInitObj.roomNameInlineEdit(inventoryID);
            }
            return false;
        });

        $('#inventoryInformationContainer').delegate('.buttonDelete', 'click', function(e) {
            var itemID = $(this).attr('itemID');
            var itemArray = itemID.split('_');
            var itemType = itemArray[0];
            itemID   = itemArray[1];
            var roomIDString, roomID;
            if (itemType == 'inventory') {
                roomID = $('#inventoryRoomID'+itemID).val();
                $('#inventoryRow'+itemID).fadeOut('slow', function() {
                    $('#inventoryRow'+itemID).remove();
                });
                updateTrashObj.sendToTrash(itemType,itemID);

                /*
                 * Remove item from inventoryArray
                 */
                var inventoryItemIndex = 0;
                var originalRoomIndex = $('#roomRow'+roomID).data('itemIndex');
                $.each(inventoryArray[originalRoomIndex].Inventory, function(key,value) {
                    if (this.InventoryID == itemID) {
                        inventoryArray[originalRoomIndex].Inventory.splice(inventoryItemIndex,1);
                    }
                    inventoryItemIndex++;
                });
            } else {
                /*
                * If Room delete...
                *
                * 1) Check if room contains any inventory items
                * 2) If no items, just delete room
                * 3) If items > 0, show the popup dialog
                * 4) Ask user if they want to move inventory items
                *    to another room (show list of rooms for property)
                */
                roomIDString = $(this).attr('itemid');
                var roomIDArray  = roomIDString.split('_');
                roomID       = roomIDArray[1];
                var roomName = $('#roomTitle_'+roomID).html();
                $.get(siteURL+'inventory/getNumberOfItemsForRoom/'+roomID, function(payload) {
                    if (payload>0) {
                        inventoryUpdateObj.deleteRoomModal(roomID, roomName, payload);
                    } else {
                        inventoryUpdateObj.deleteRoom(roomID);
                    }
                });
            }
            inventorySupportObj.updateInventoryValueTotals();
            return false;
        });

        $('.inventoryContainer').liveDraggable({
            revert: 'invalid',
            axis:   'y',
            handle: '.dragHandle'
        });
     }
}

var inventoryInitObj = {
    inventoryInit : function() {
        inventoryInitObj.inventoryModalInit();
        $('#buttonAddInventory').click(function() {
            $('#formInventoryContainer').dialog('open');
            return false;
        });

        $( "#invRoom" ).autocomplete({
			source: rooms,
            select: function(event, ui) {
                $('#invRoom').val(ui.item.label);
                $('#invRoomID').val(ui.item.value);
                return false;
            },
            focus: function(event, ui) {
                $('#invRoom').val(ui.item.label);
                $('#invRoomID').val(ui.item.value);
                return false;
            }
		});

        $('#invName').focus(function() {
            if ($(this).val() == 'Item name') {
                $(this).val('');
            }
        });

        $('#buttonSave').click(function() {
            inventoryUpdateObj.saveInventoryItem();
            return false;
        });

        $('#buttonInventoryFormCancel').click(function() {
            $('#formInventoryContainer').dialog('close');
            //inventorySupportObj.clearInventoryForm();
            return false;
        });

        /*
		 * Category and Tag edit boxes
		 */
		$('#buttonCategoryEdit').makeCategoryEditBox({
			listJSON:     categoryJSON,
			listType:     'category',
			instanceName: 'categories',
			siteArea:     'inventory',
			selectID:     'invCategory'
		});

		$('#buttonTagEdit').makeCategoryEditBox({
			listJSON: tagJSON,
			listType: 'tag',
			instanceName: 'tags',
			siteArea: 'inventory'
		});

        $('#invPricePurchase').focus(function() {
           if ($(this).val() == '0.00') {
               $(this).val('');
           }
        });

        $('#invPriceReplace').focus(function() {
           if ($(this).val() == '0.00') {
               $(this).val('');
           }
        });
    },
    inventoryModalInit : function() {
        $('#formInventoryContainer').dialog({
        	autoOpen:      false,
        	closeOnEscape: true,
        	width:         '930px',
        	position:      'center',
			resizable:     false,
			zIndex:        1000,
            modal:         false,
			open:          function(event,ui) {
                if (uploaderInit == false && !$.browser.msie) {
                    filesObj.initPluploadDragNDrop(inventoryIDEdit,'inventory',$('#invFile_fileContainer'));
                }
                inventoryInitObj.initFormElements();
                inventoryModalOpen = true;
			},
            close:         function(event,ui) {
                inventoryModalOpen = false;
                inventorySupportObj.clearInventoryForm();
            }
        });
    },
    initFormElements : function() {
        $('#invDescription').wysiwyg({
            controls: {
                h2                   : {visible: false},
                h3                   : {visible: false},
                insertImage          : {visible: false},
                insertTable          : {visible: false},
                justifyFull          : {visible: false},
                insertHorizontalRule : {visible: false},
                strikeThrough        : {visible: false},
                subscript            : {visible : false},
                superscript          : {visible : false},
                undo                 : {visible : false},
                redo                 : {visible : false}
            },
            css: siteURL+'css/editorCSS.css'
        });
        $('#invDescription').wysiwyg('clear');

        $('#invDate').datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});
    },
    populateInventoryFormForEdit : function(inventoryID) {
        inventoryIDEdit = inventoryID;
        $('#formInventoryContainer').dialog('open');

        $.ajax({
		    type:  "POST",
		    url:   siteURL+'inventory/getInventoryItem/'+inventoryID+'/json',
            dataType: 'json',
		    success: function(payload){
                $('#invAction').val('edit');
                $('#originalRoomID').val(payload.RoomID);
                $('#invRoom').val(payload.RoomName);
                $('#invID').val(payload.InventoryID);
                $('#invName').val(genericFunctions.txtToField(payload.ItemName));
                $('#invCategory').val(payload.CatID);
                $('#invDescription').wysiwyg('setContent',payload.ItemDesc);
                $('#invMake').val(payload.ItemMake);
                $('#invModel').val(payload.ItemModel);
                $('#invSN').val(payload.ItemSN);
                $('#invDate').val(payload.DatePurchase);
                $('#invCondition').val(payload.ItemCondition);
                $('#invPricePurchase').val(payload.PricePurchase);
                $('#invPriceReplace').val(payload.PriceReplace);
                $('#buttonTagEdit').val(payload.Tags);
                $('#invFile').attr('itemID',payload.InventoryID);
                renderFileObj.renderFileWindow(payload,$('#invFile_fileContainer'),true);
                
                attachedFilesView = true;
                fileManagerObj.attachFilePlugins();
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
        });
    },
    roomNameInlineEdit : function(roomID) {
        var originalRoomName = $('#roomTitle_'+roomID).html();
        var replacedNodes = $('#roomContainer'+roomID);
        $('#roomContainer'+roomID+' div').hide();
        $('#roomContainer'+roomID).append('<input type="text" id="roomEditBox_'+roomID+'" class="bigText" style="width: 400px;" value="'+originalRoomName+'" />');
        
        /*
         * Event handlers
         */
        $('#roomEditBox_'+roomID).blur(function() {
            var newRoomName = $('#roomEditBox_'+roomID).val();
            inventoryUpdateObj.updateRoomName(roomID,newRoomName);
        });
        
        $('#roomEditBox_'+roomID).bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
                var newRoomName = $('#roomEditBox_'+roomID).val();
                inventoryUpdateObj.updateRoomName(roomID,newRoomName);
                return false;
            }
        });
    }
}

var inventoryUpdateObj = {
    saveInventoryItem : function() {
        var invDescription = $('#invDescription').wysiwyg('getContent');
        if (invDescription == '<html/>' || invDescription == '<br/>') {
            invDescription = '';
        }
        var action = $('#invAction').val();
        var inventoryID = $('#invID').val();
        var formHash = {
            save           : 1,
            invID          : inventoryID,
            placeID        : $('#placeID').val(),
            action         : action,
            invName        : $('#invName').val(),
            invRoomNew     : $('#invRoom').val(),
            invRoomID      : $('#invRoomID').val(),
            invCategory    : $('#invCategory').val(),
            invDescription : invDescription,
            invMake        : $('#invMake').val(),
            invModel       : $('#invModel').val(),
            invSN          : $('#invSN').val(),
            invDate        : $('#invDate').val(),
            invCondition   : $('#invCondition').val(),
            invTags        : $('#invTags').val(),
            invPricePurchase : $('#invPricePurchase').val(),
            invPriceReplace  : $('#invPriceReplace').val()
        }

        $.ajax({
		    type:     'POST',
		    url:      siteURL+'inventory/saveInventoryItem',
		    data:     formHash,
            dataType: 'json',
		    success: function(payload) {
                $('#invMessage').systemMessage({
                   message: 'Your inventory item has been saved.',
                   status: 'success'
                });
                var roomID = payload.RoomID;
                var inventory = payload;
                /*
                 * Have we added a new room?
                 */
                if (payload.Inventory) {
                    inventoryArray.push(inventory);
                    renderInventoryDataObj.renderInventoryGrid(inventoryArray);
                } else {
                    var originalRoomID = $('#originalRoomID').val();
                    var originalRoomIndex = $('#roomRow'+originalRoomID).data('itemIndex');
                    var roomIndex  = $('#roomRow'+roomID).data('itemIndex');
                    var inventoryString;
                    if (action == 'edit') {
                        /*
                         * Remove edited item if moved to a different room.
                         * Refresh the inventory list.
                         */
                        if (originalRoomID != $('#invRoomID').val()) {
                            var inventoryItemIndex = 0;
                            $.each(inventoryArray[originalRoomIndex].Inventory, function(key,value) {
                                if (this.InventoryID == inventoryID) {
                                    inventoryArray[originalRoomIndex].Inventory.splice(inventoryItemIndex,1);
                                }
                                inventoryItemIndex++;
                            });
                        }
                        inventoryArray[roomIndex].Inventory.push(inventory);
                        renderInventoryDataObj.renderInventoryGrid(inventoryArray);
                        $('#formInventoryContainer').dialog('close');
                    } else {
                        inventoryArray[roomIndex].Inventory.push(inventory);
                        inventory.ItemIndex = inventoryArray[roomIndex].Inventory.length-1;
                        inventoryString = renderInventoryDataObj.renderInventoryRow(inventory,true);
                        $('#inventoryListContainer'+roomID).append(inventoryString);
                        permissionsObj.renderControlsBasedOnPermissions();
                        renderInventoryDataObj.attachInventoryPlugins();
                    }
                }
                inventorySupportObj.clearInventoryForm();
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    updateRoomName : function(roomID,roomName) {
        var formHash = {
            roomID   : roomID,
            roomName : roomName
        }

        $.ajax({
		    type:     'POST',
		    url:      siteURL+'inventory/updateRoomName',
		    data:     formHash,
            dataType: 'json',
		    success: function(payload) {
                $('#roomEditBox_'+roomID).remove();
                $('#roomContainer'+roomID+' div').show();
                $('#roomTitle_'+roomID).html(roomName);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    deleteRoomModal : function(roomID,roomName,itemCount) {
        var modalContents = [];
        modalContents.push('<div class="errorMessageBig">'+commonTerms['deleteRoom']+': '+roomName+'</div>');
        modalContents.push('<p>'+commonTerms['moveInventoryNewRoom']+'</p>');
        modalContents.push('<p><select id="selectMoveToRoom" style="margin: 3px 0 3px 0; width: 300px;"></select></p>');
        var outString = modalContents.join('');
        $('#deleteContentsContainer').html(outString);
        inventorySupportObj.populateRoomSelectFromJSON(roomArray,$('#selectMoveToRoom'));

        $('#selectMoveToRoom option').each(function() {
            if ($(this).val() == roomID) {
                $(this).remove();
            }
        });

        $('#buttonDeleteModalDelete').click(function(){
            var moveToRoomID = $('#selectMoveToRoom').val();
            var deleteInventory = 0;
            if (moveToRoomID == 'deleteInventoryToo') {
                deleteInventory = true;
            }
            inventoryUpdateObj.deleteRoom(roomID,moveToRoomID,deleteInventory);
        });
        $('#modalDelete').dialog('open');
    },
    deleteRoom : function(deleteRoomID,moveToRoomID,deleteInventory) {
        if (deleteInventory == true) {
            updateTrashObj.sendToTrash('roomWithInventory',deleteRoomID);
        } else {
            $.ajax({
				type: "POST",
				url:  siteURL+'inventory/moveInventoryToRoom/'+deleteRoomID+'/'+moveToRoomID,
				success: function(payload) {
                    var placeID = $('#placeID').val();
                    getInventoryDataObj.getInventory(placeID);
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
            updateTrashObj.sendToTrash('room',deleteRoomID);
        }
        $('#roomRow'+deleteRoomID).fadeOut('slow');
		$('#inventoryListContainer'+deleteRoomID).fadeOut('slow');
        $('#modalDelete').dialog('close');

        /*
         * Remove room from inventoryArray
         */
        var roomIndex = $('#roomRow'+deleteRoomID).data('itemIndex');
        inventoryArray.splice(roomIndex,1);

        /*
         * Add new indexes to rooms
         */
        $('.roomRow').each(function(i) {
           $(this).data('itemIndex',i)
        });
    }
}

var inventorySupportObj = {
    clearInventoryForm : function() {
        $('#originalRoomID').val('');
        $('#invAction').val('add');
        $('#invID').val('');
        $('#invName').val('');
        $('#invRoom').val('');
        $('#invRoomID').val('');
        $('#invCategory').val('');
        $('#invDescription').wysiwyg('clear');
        $('#invMake').val('');
        $('#invModel').val('');
        $('#invSN').val('');
        $('#invDate').val('');
        $('#invCondition').val('');
        $('#invPricePurchase').val('');
        $('#invPriceReplace').val('');
        $('#invTags').val('');
        $('#invFile_fileContainer').html('');

        /*
         * Delete any extraneous attached files.
         */
        $.get(siteURL+'filemanager/removeStrayFileLinks', function(payload) {});
        inventoryIDEdit = 0;
    },
    populateRoomSelectFromJSON : function(roomArray,selectObj) {
        selectObj.html('<option value="deleteInventoryToo">'+commonTerms['deleteInventoryToo']+'</option><option value="0"></option>');
		for(var a=0;a<=(roomArray.length-1);a++) {
			var roomID   = roomArray[a].RoomID;
			var roomName = roomArray[a].RoomName;

			var option = '<option value="'+roomID+'">'+roomName+'</option>';
			selectObj.append(option);
		}
	},
    updateInventoryValueTotals : function() {
        var totalInventoryAmount = 0;
        $.each(inventoryArray,function(index,value) {
            var itemArray = this.Inventory;
            var roomID = this.RoomID;
            var roomTotal = 0;

            $.each(itemArray,function() {
               var itemPrice = parseFloat(this.PriceReplace);
               roomTotal = parseFloat(roomTotal+itemPrice);
               totalInventoryAmount = parseFloat(totalInventoryAmount+itemPrice);
            });
            var roomTotalRender = accounting.formatMoney(roomTotal, commonTerms['currencySymbol'], 2, ",", ".")
            var inventoryTotalRender = accounting.formatMoney(totalInventoryAmount, commonTerms['currencySymbol'], 2, ",", ".")
            $('#roomTotal_'+roomID).html(roomTotalRender);
            $('#inventoryTotalAmount').html(inventoryTotalRender);
            $('.totalPriceReplace').html(inventoryTotalRender);
            $('.totalPriceReplace').attr('data-totalInventoryAmount',totalInventoryAmount);
        });
    },
    updateGlobalInventoryValue : function() {
        /*
         * Get inventory total
         */
        var inventoryTotal = 0;
        $('.totalPriceReplace').each(function() {
            inventoryTotal = parseFloat(inventoryTotal+$(this).data('totalinventoryamount'));
        });
        $('#inventoryTotalAmount').html(accounting.formatMoney(inventoryTotal, commonTerms['currencySymbol'], 2, ",", "."));
    }
}

$(document).ready(function() {
    if ($('#pageLeftColumn').attr('pageID') == 'viewPlace') {
        inventoryInitObj.inventoryInit();
    }
});