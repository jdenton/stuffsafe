var messageData;
var jsonFileData;
var fileManagerTriggerID;
var swfUploader;
var fileManagerNodeID = 0;
var inventoryModalOpen = false;
var uploaderInit = false;

var inventoryArray   = [];
var roomArray        = [];
var teamArray        = [];
var fileArray        = [];
var messageArray     = [];
var itemsInTrash     = [];

var currentMonthName;
var currentMonthNum;
var currentYear;

var timersRunning = 0;
var widgetLoaded_timer    = false;
var widgetLoaded_projects = false;
var widgetLoaded_calendar = false;
var widgetLoaded_notes    = false;
var widgetLoaded_tasks    = false;
var widgetLoaded_invoices = false;
var widgetProjectsObj, widgetInvoicesObj, widgetTasksObj;

var chartColors = [ '#0662B0',
				    '#E48701',
				    '#A5bC4E',
					'#1B95D9',
					'#CACA9E',
					'#6693B0',
					'#F05E27',
					'#86D1E4',
					'#E4F9A0',
					'#FFD512',
					'#75B000',
					'#F984A1',
					'#FF66CC',
					'#FF0000',
					'#F6BD0F',
					'#FF9933',
					'#e0ad4d',
					'#BF9A55',
					'#ad7f2a',
					'#CCCC00',
					'#DEDDDD',
					'#999999',
					'#0099CC',
					'#99FFCC'];

/*
 * Some constants
 */

var USER_TYPE_OWNER  = 1;
var USER_TYPE_TEAM   = 2;
var USER_TYPE_CLIENT = 3;
var USER_TYPE_GOD    = 100;