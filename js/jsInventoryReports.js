var getReportDataObj = {
    getReportPurchaseReplace : function(type) {
        $.ajax({
		    type:  "POST",
		    url:   siteURL+'reports/getReportPurchaseReplace/'+type+'/json/0',
			dataType: 'json',
		    success: function(payload){
                renderReportObj.renderReportPurchaseReplace(payload);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getReportValueByProperty : function() {
        $.ajax({
		    type:  "POST",
		    url:   siteURL+'reports/getReportValueByProperty/json/0',
			dataType: 'json',
		    success: function(payload){
                renderReportObj.renderReportValueByProperty(payload);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getReportValueByCategory : function() {
        $.ajax({
		    type:  "POST",
		    url:   siteURL+'reports/getReportValueByCategory/json/0',
			dataType: 'json',
		    success: function(payload){
                renderReportObj.renderReportValueByCategory(payload);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    }
}

var renderReportObj = {
    renderReportPurchaseReplace : function(jsonObject) {
        var chartData = jsonObject.ChartData;
        var gridData = jsonObject.GridData;
        var tmpChart = findSWF("chartContainer");
        var x = tmpChart.load( JSON.stringify(chartData) );
        var stringOut = [];

    },
    renderReportValueByCategory : function(jsonObject) {
        var chartData = jsonObject.ChartData;
        var gridData = jsonObject.GridData;
        var tmpChart = findSWF("chartContainer");
        var x = tmpChart.load( JSON.stringify(chartData) );

        var stringOut = [];
        stringOut.push('<table class="dataTable" id="reportGrid">');
        stringOut.push('<tbody>');

        for(a=0;a<=((gridData.length)-1);a++) {
            if (gridData[a].Label == 'null' || gridData[a].Label == null) {
                gridData[a].Label = '';
            }

            stringOut.push('<tr>');
            stringOut.push('<td>'+gridData[a].Label+'</td>');
            stringOut.push('<td style="text-align: right;">'+gridData[a].Percentage+'</td>');
            stringOut.push('<td style="text-align: right;">'+gridData[a].Total+'</td>');
            stringOut.push('</tr>');
        }

        stringOut.push('</tbody>');
        stringOut.push('<tfoot><tr><td>'+commonTerms['total']+'</td><td></td>');
        stringOut.push('<td style="text-align: right;">'+jsonObject['GrandTotal']+'</td></tr></tfoot>');
        stringOut.push('</table>');
        var stringOutFinal = stringOut.join('');
        $('#gridContainer').html(stringOutFinal);
        $('#reportGrid')
			.alternateTableRowColors()
			.hiliteTableRow();
    },
    renderReportValueByProperty : function(jsonObject) {
        var chartData = jsonObject.ChartData;
        var gridData = jsonObject.GridData;
        var tmpChart = findSWF("chartContainer");
        var x = tmpChart.load( JSON.stringify(chartData) );

        var stringOut = [];        
        stringOut.push('<table class="dataTable" id="reportGrid">');
        stringOut.push('<tbody>');

        for(a=0;a<=((gridData.length)-1);a++) {
            if (gridData[a].Label == 'null' || gridData[a].Label == null) {
                gridData[a].Label = '';
            }

            stringOut.push('<tr>');
            stringOut.push('<td>'+gridData[a].Label+'</td>');
            stringOut.push('<td style="text-align: right;">'+gridData[a].Percentage+'</td>');
            stringOut.push('<td style="text-align: right;">'+gridData[a].Total+'</td>');
            stringOut.push('</tr>');
        }

        stringOut.push('</tbody>');
        stringOut.push('<tfoot><tr><td>'+commonTerms['total']+'</td><td></td>');
        stringOut.push('<td style="text-align: right;">'+jsonObject['GrandTotal']+'</td></tr></tfoot>');
        stringOut.push('</table>');
        var stringOutFinal = stringOut.join('');
        $('#gridContainer').html(stringOutFinal);
        $('#reportGrid')
			.alternateTableRowColors()
			.hiliteTableRow();
    }
}

var reportSupportObj = {
    attachReportChangeHandlers : function() {
        $('#selectReportType').change(function() {
            reportSupportObj.getReportDataFromReportType($(this).val());
        });
    },
    prepInputData : function() {
        var reportType = $('#selectReportType').val();

        reportSupportObj.getReportDataFromReportType(reportType);
        var itemIDString = reportType;
    },
    getReportDataFromReportType : function(reportType) {
        switch (reportType) {
            case 'repInventoryProperty':
                getReportDataObj.getReportValueByProperty();
                break;
            case 'repInventoryCategory':
                getReportDataObj.getReportValueByCategory();
                break;
            case 'repInventoryPurchaseReplacementCategory':
                getReportDataObj.getReportPurchaseReplace('category');
                break;
            case 'repInventoryPurchaseReplacementProperty':
                getReportDataObj.getReportPurchaseReplace('property');
                break;
            default:
              getReportDataObj.getReportValueByProperty();
        }
    }
}